-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2016 at 11:10 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wikiloka`
--

-- --------------------------------------------------------

--
-- Table structure for table `poin`
--

CREATE TABLE IF NOT EXISTS `poin` (
`id_poin` int(11) NOT NULL,
  `nama_poin` varchar(20) NOT NULL,
  `jumlah_poin` int(11) NOT NULL DEFAULT '0',
  `harga_poin` int(11) NOT NULL DEFAULT '0',
  `gambar_poin` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `poin`
--

INSERT INTO `poin` (`id_poin`, `nama_poin`, `jumlah_poin`, `harga_poin`, `gambar_poin`) VALUES
(1, 'Poin Paket 1', 10, 10000, '15.png'),
(2, 'Poin Paket 2', 30, 25000, '50.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `poin`
--
ALTER TABLE `poin`
 ADD PRIMARY KEY (`id_poin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `poin`
--
ALTER TABLE `poin`
MODIFY `id_poin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
