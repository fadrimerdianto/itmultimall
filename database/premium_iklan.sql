-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2016 at 11:11 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wikiloka`
--

-- --------------------------------------------------------

--
-- Table structure for table `premium_iklan`
--

CREATE TABLE IF NOT EXISTS `premium_iklan` (
`id_premium` int(11) NOT NULL,
  `nama_premium` varchar(20) NOT NULL,
  `harga_premium` int(11) NOT NULL DEFAULT '0',
  `gambar_premium` varchar(50) NOT NULL,
  `masa_aktif` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `premium_iklan`
--

INSERT INTO `premium_iklan` (`id_premium`, `nama_premium`, `harga_premium`, `gambar_premium`, `masa_aktif`) VALUES
(1, 'Top 25', 3, '15.png', 2),
(2, 'Top Shop', 5, '50.png', 1),
(3, 'Recommended', 7, '100.png', 5),
(4, 'Terlaris', 10, '300.png', 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `premium_iklan`
--
ALTER TABLE `premium_iklan`
 ADD PRIMARY KEY (`id_premium`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `premium_iklan`
--
ALTER TABLE `premium_iklan`
MODIFY `id_premium` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
