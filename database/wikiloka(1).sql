-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2016 at 11:19 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wikiloka`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
`id_berita` int(11) NOT NULL,
  `judul_berita` varchar(200) NOT NULL,
  `isi_berita` text NOT NULL,
  `seo_berita` varchar(200) NOT NULL,
  `sumber_berita` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `judul_berita`, `isi_berita`, `seo_berita`, `sumber_berita`) VALUES
(1, 'Sepekan Dirilis, Angry Birds Raup US$ 43 Juta', 'Liputan6.com, Jakarta - Film Angry Birds baru saja dirilis serentak di beberapa negara. Menurut laporan, film besutan Rovio Entertainment menuai respon yang cukup positif.\r\n\r\nSeperti diberitakan Venture Beats, Senin (16/5/2016), film yang diangkat dari gim populer Angry Birds ini meraup US$ 43 juta pada pekan pertama perilisannya.\r\n\r\nRovio Entertainment melalui anak usahanya The Angry Birds Movie mengungkap bahwa film ini merajai box office di 37 negara, termasuk Amerika Serikat (AS).\r\n\r\nSebetulnya, ini merupakan strategi Rovio untuk meraup untung untuk bangkit dari keterpurukan.', 'sepekan-dirilis-angry-birds-raup-us-43-juta', 'http://tekno.liputan6.com/read/2507790/sepekan-dirilis-angry-birds-raup-us-43-juta');

-- --------------------------------------------------------

--
-- Table structure for table `detail_premium`
--

CREATE TABLE IF NOT EXISTS `detail_premium` (
`id_detail_premium` int(11) NOT NULL,
  `id_iklan` int(11) NOT NULL,
  `top25` smallint(6) NOT NULL,
  `tanggal_aktif_top25` date NOT NULL,
  `topwebsite` smallint(6) NOT NULL,
  `tanggal_aktif_topwebsite` date NOT NULL,
  `terlaris` smallint(6) NOT NULL,
  `tanggal_aktif_terlaris` date NOT NULL,
  `recommended` smallint(6) NOT NULL,
  `tanggal_aktif_recommended` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `detail_premium`
--

INSERT INTO `detail_premium` (`id_detail_premium`, `id_iklan`, `top25`, `tanggal_aktif_top25`, `topwebsite`, `tanggal_aktif_topwebsite`, `terlaris`, `tanggal_aktif_terlaris`, `recommended`, `tanggal_aktif_recommended`) VALUES
(1, 25, 0, '2016-06-01', 0, '0000-00-00', 0, '2016-04-03', 1, '2016-03-21'),
(22, 26, 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00'),
(23, 27, 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00'),
(24, 28, 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `favorit`
--

CREATE TABLE IF NOT EXISTS `favorit` (
`id_favorit` int(11) NOT NULL,
  `id_iklan` int(11) NOT NULL,
  `username` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `favorit`
--

INSERT INTO `favorit` (`id_favorit`, `id_iklan`, `username`) VALUES
(1, 19, 'wiratmoko11');

-- --------------------------------------------------------

--
-- Table structure for table `iklan`
--

CREATE TABLE IF NOT EXISTS `iklan` (
`id_iklan` int(11) NOT NULL,
  `judul_iklan` varchar(100) NOT NULL,
  `seo_iklan` varchar(120) NOT NULL,
  `deskripsi_iklan` text NOT NULL,
  `harga_iklan` bigint(20) NOT NULL,
  `web_iklan` varchar(100) NOT NULL,
  `owner` varchar(40) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `kategori` int(11) DEFAULT NULL,
  `sub1_kategori` int(11) DEFAULT NULL,
  `sub2_kategori` int(11) DEFAULT NULL,
  `kota` int(11) DEFAULT NULL,
  `tanggal_post` datetime DEFAULT NULL,
  `id_premium` int(11) DEFAULT '0',
  `tanggal_aktif_premium` date DEFAULT NULL,
  `jenis_iklan` int(11) DEFAULT '0',
  `hide_email` int(11) DEFAULT '0',
  `hide_bbm` int(11) DEFAULT '0',
  `wa_available` int(11) DEFAULT '0',
  `dilihat` int(11) DEFAULT '0',
  `like` int(11) DEFAULT '0',
  `unlike` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `iklan`
--

INSERT INTO `iklan` (`id_iklan`, `judul_iklan`, `seo_iklan`, `deskripsi_iklan`, `harga_iklan`, `web_iklan`, `owner`, `status`, `kategori`, `sub1_kategori`, `sub2_kategori`, `kota`, `tanggal_post`, `id_premium`, `tanggal_aktif_premium`, `jenis_iklan`, `hide_email`, `hide_bbm`, `wa_available`, `dilihat`, `like`, `unlike`) VALUES
(25, 'APARTEMEN CHADSTONE CIKARANG LIFE STYLE MALL ', 'apartemen-chadstone-cikarang-life-style-mall--5738d1fea7e1c', '<p>APARTEMEN CHADSTONE CIKARANG LIFE STYLE MALL<br />\r\nMAU Beli PROPERTY tanpa DP?? Atau<br />\r\nCicilan cuma Rp.4,5 juta/bulan??<br />\r\nCuma Apt CHADSTONE jawaban nya!!<br />\r\nKali ini untuk setiap pembelian 1 unit Chadstone type apa saja..cara bayar apa saja..mendapatkan hadiah langsung 1 Unit TV LED 50 inch lho..(khusus pembelian dalam investor gathering)<br />\r\nWowww..gak kebayang cuman beli apartment 400jutaan dpt hadiah sedahsyat itu?!!!<br />\r\nsatu2nya Apartment diatas MALL di Cikarang Barat yg berdekatan dgn gerbang tol.<br />\r\nCara byr tanpa DP bisa SAMPAI DGN 60x .<br />\r\nCIKARANG merupakan pangsa 99% ex.patriat (tenaga kerja asing) dgn NILAI SEWA sampai dgn 25%/tahun dari nilai unit yg akan dibeli saat ini.<br />\r\nKlik Panduan INFO SEWA apartment di cikarang saat ini yaitu link:<br />\r\nhttp://www.sancrest-apartment.com/en/room/<br />\r\nJangan sampai terlewatkan krn promo terbatas<br />\r\nhttps://youtu.be/h_79fe7E7zU<br />\r\nChadstone Apartement, Luxury Life &amp; High return investment 25% p.a<br />\r\nBooking Fee Rp.15juta sdh langsung punya apartment.<br />\r\nSEGERA NUP utk mendapatkan unit yg terbaik dan harga yg lebih menarik.</p>\r\n', 850000000, '', 'wiratmoko11', 1, 6, 18, 0, 215, '2016-05-16 02:46:06', 1, '2016-05-20', 1, 0, 0, 1, 109, 43, 37),
(26, 'velg hartge 17x8/9 pcd 8x100/114,3', 'velg-hartge-17x89-pcd-8x1001143-574e9ebc86c9d', '<p>permisi semuanya<br />\r\nmau cari velg di sini tempatnya<br />\r\ntersedia berbagai model velg di toko kami<br />\r\ndari ring 15 hingga 22<br />\r\ntinggal pilih sesuai selera anda&#39;&#39;&#39;&#39;&#39;&#39;<br />\r\nfree n2 balancing setiap pemasangan<br />\r\nkami juga berikan membert card yg berfungsi:<br />\r\nN2 dan tambal tubles di seluruh cabang kami<br />\r\nkami berikan free selama pemakaian velg dari kami,<br />\r\ntersedia juga ban import dan local berbagai ukuran,<br />\r\nuntuk import ada toyo ( japan ) landsail ( china )<br />\r\ndan localnya accelera ( indo )<br />\r\npembayaran bisa dengan debit BCA dan MANDIRI<br />\r\nuntuk lebih jelasnya datang aja ke toko kami di :<br />\r\njl raya serpong KM8 NO45 sebrang hotel soll marina<br />\r\nsebelah plaza serpong lama RS SPEED TOYO<br />\r\ndi tunggu terimakasih</p>\r\n', 5100000, '', 'wirat_m', 1, 8, 12, 0, 209, '2016-06-01 15:37:16', 0, NULL, 1, 0, 0, 1, 2, 0, 0),
(27, 'Jazz 2012 ex wanita White Pearl km 51 rb plat Bogor dg Jok Kulit asli', 'jazz-2012-ex-wanita-white-pearl-km-51-rb-plat-bogor-dg-jok-kulit-asli-574e9fa5c3390', '<p>Jazz 2012 ex wanita White Pearl km 51 rb plat Bogor dg Jok Kulit asli, Asuransi valid s/d 2017. Pemakai istri CEO,dijamin mobil terawat baik, dijamin tidak kecewa. Harga Jok Kulit asli saja sdh 6 jt. Janjian lihat mobil bs di Mampang Jkt ato Pajajaran Bogor.<br />\r\nHP / WA nol pan tu pan nol wa tu pat tu tu tu nam</p>\r\n', 175000000, '', 'wirat_m', 1, 8, 8, 30, 227, '2016-06-01 15:41:09', 0, NULL, 2, 0, 0, 0, 1, 3, 2),
(28, 'Dijual motor honda supra x 2004 hitam ', 'dijual-motor-honda-supra-x-2004-hitam--574ea4f1db02e', '<p>dijual motor honda supra x 2004 hitam,harga pas 10 juta. Stnk mati,motor disimpan tidak kehujanan dan tidak kepanasan dan tidak pernah dihidupkan. Km asli belum 13000.</p>\r\n', 10000000, '', 'wirat_m', 1, 7, 13, 73, 136, '2016-06-01 16:03:45', 0, NULL, 2, 0, 0, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `iklan_photo`
--

CREATE TABLE IF NOT EXISTS `iklan_photo` (
`id_photo` int(11) NOT NULL,
  `id_iklan` int(11) NOT NULL,
  `photo` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `iklan_photo`
--

INSERT INTO `iklan_photo` (`id_photo`, `id_iklan`, `photo`) VALUES
(1, 21, 'wiratmoko11-Iklan-52e5da125b9958dec3e8e9'),
(2, 1, 'wiratmoko11-Iklan-1.jpg'),
(3, 2, 'wiratmoko11-Iklan-2.jpg'),
(4, 3, 'wiratmoko11-Iklan-3.jpg'),
(5, 4, 'wiratmoko11-Iklan-4.jpg'),
(6, 5, 'wiratmoko11-Iklan-5.jpg'),
(7, 6, 'wiratmoko11-Iklan-6.jpg'),
(8, 7, 'wiratmoko11-Iklan-7.jpg'),
(9, 8, 'wiratmoko11-Iklan-8.jpg'),
(10, 9, 'wiratmoko11-Iklan-9.jpg'),
(11, 10, 'wiratmoko11-Iklan-10.jpg'),
(12, 11, 'wiratmoko11-Iklan-11.jpg'),
(13, 12, 'wiratmoko11-Iklan-12.jpg'),
(14, 13, 'wiratmoko11-Iklan-13.jpg'),
(15, 13, 'wiratmoko11-Iklan-13.jpg'),
(16, 13, 'wiratmoko11-Iklan-13.jpg'),
(17, 13, 'wiratmoko11-Iklan-13.jpg'),
(18, 14, 'wiratmoko11-Iklan-14.jpg'),
(19, 14, 'wiratmoko11-Iklan-14.jpeg'),
(20, 14, 'wiratmoko11-Iklan-14.jpeg'),
(21, 14, 'wiratmoko11-Iklan-14.jpeg'),
(22, 14, 'wiratmoko11-Iklan-14.jpg'),
(23, 15, 'wiratmoko11-iklan-15-0.jpg'),
(24, 15, 'wiratmoko11-iklan-15-1.jpg'),
(25, 15, 'wiratmoko11-iklan-15-2.jpg'),
(26, 15, 'wiratmoko11-iklan-15-3.jpg'),
(27, 15, 'wiratmoko11-iklan-15-4.jpg'),
(28, 16, 'wiratmoko11-iklan-16-.jpg'),
(29, 16, 'wiratmoko11-iklan-16-.jpg'),
(30, 16, 'wiratmoko11-iklan-16-.jpg'),
(31, 16, 'wiratmoko11-iklan-16-.jpg'),
(32, 16, 'wiratmoko11-iklan-16-.jpg'),
(33, 17, 'wiratmoko11-iklan-17-90246.jpg'),
(34, 17, 'wiratmoko11-iklan-17-18301.jpg'),
(35, 17, 'wiratmoko11-iklan-17-74713.jpg'),
(36, 17, 'wiratmoko11-iklan-17-53280.jpg'),
(37, 17, 'wiratmoko11-iklan-17-53076.jpg'),
(38, 18, 'wiratmoko11-iklan-18-13119.jpg'),
(39, 18, 'wiratmoko11-iklan-18-38012.jpg'),
(40, 18, 'wiratmoko11-iklan-18-18557.jpg'),
(41, 19, 'wiratmoko11-iklan-19-7327.jpg'),
(42, 19, 'wiratmoko11-iklan-19-47283.jpg'),
(43, 20, 'wiratmoko11-iklan-20-98287.jpg'),
(44, 21, 'wiratmoko11-iklan-21-57250.jpg'),
(45, 22, 'wiratmoko11-iklan-22-60244.jpg'),
(46, 23, 'wiratmoko11-iklan-23-26074.jpg'),
(47, 23, 'wiratmoko11-iklan-23-29763.jpg'),
(48, 23, 'wiratmoko11-iklan-23-5364.jpg'),
(49, 23, 'wiratmoko11-iklan-23-50582.jpg'),
(50, 23, 'wiratmoko11-iklan-23-90270.jpg'),
(51, 24, 'wiratmoko11-iklan-24-90914.jpg'),
(52, 25, 'wiratmoko11-iklan-25-61590.jpg'),
(53, 26, 'wirat_m-iklan-26-14904.jpg'),
(54, 26, 'wirat_m-iklan-26-81045.jpg'),
(55, 27, 'wirat_m-iklan-27-14639.jpg'),
(56, 27, 'wirat_m-iklan-27-50543.jpg'),
(57, 27, 'wirat_m-iklan-27-93759.jpg'),
(58, 27, 'wirat_m-iklan-27-40234.jpg'),
(59, 27, 'wirat_m-iklan-27-23220.jpg'),
(60, 28, 'wirat_m-iklan-28-52127.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`id_kategori` int(11) NOT NULL,
  `kategori` varchar(60) NOT NULL,
  `icon` varchar(40) DEFAULT NULL,
  `seo_kategori` varchar(255) DEFAULT ''
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`, `icon`, `seo_kategori`) VALUES
(1, 'Kuliner', 'kategori-kuliner-30.png', 'kuliner'),
(2, 'Otomotif', 'kategori-otomotif-5.png', 'otomotif'),
(3, 'Travel', 'kategori-travel-16.png', 'travel'),
(4, 'Elektronik', 'kategori-elektronik-7.png', 'elektronik'),
(5, 'Fashion', 'kategori-fashion-51.png', 'fashion'),
(6, 'Properti', 'kategori-properti-46.png', 'properti'),
(7, 'Motor', 'kategori-olahraga-22.png', 'motor'),
(8, 'Mobil', 'kategori-rumah-tangga-18.png', 'mobil'),
(9, 'Perlengkapan Anak', 'kategori-perlengkapan-anak-95.png', 'perlengkapan-anak');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
`id_komentar` int(11) NOT NULL,
  `id_iklan` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `komentar` text NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_iklan`, `username`, `komentar`, `waktu`) VALUES
(1, 25, 'wiratmoko11', 'ini dimana gan posisi barang nya ?', '2016-05-20 19:59:36');

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE IF NOT EXISTS `kota` (
`id_kota` int(11) NOT NULL,
  `nama_area` varchar(40) NOT NULL,
  `id_provinsi` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=468 ;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id_kota`, `nama_area`, `id_provinsi`) VALUES
(1, 'Aceh Selatan ', 1),
(2, 'Aceh Tenggara ', 1),
(3, 'Aceh Timur', 1),
(4, 'Aceh Tengah ', 1),
(5, 'Aceh Barat', 1),
(6, 'Aceh Besar ', 1),
(7, 'Pidie ', 1),
(8, 'Aceh Utara', 1),
(9, 'Simeulue', 1),
(10, 'Aceh Singkil ', 1),
(11, 'Bireuen ', 1),
(12, 'Aceh Barat Daya ', 1),
(13, 'Gayo Lues ', 1),
(14, 'Aceh Jaya ', 1),
(15, 'Nagan Raya ', 1),
(16, 'Aceh Tamiang ', 1),
(17, 'Bener Meriah ', 1),
(18, 'Pidie Jaya', 1),
(19, 'Kota Banda Aceh ', 1),
(20, 'Kota Sabang ', 1),
(21, 'Kota Lhokseumawe ', 1),
(22, 'Kota Langsa', 1),
(23, 'Kota Subulussalam', 1),
(24, 'Tapanuli Tengah ', 2),
(25, 'Tapanuli Utara ', 2),
(26, 'Tapanuli Selatan', 2),
(27, 'Nias ', 2),
(28, 'Langkat ', 2),
(29, 'Karo ', 2),
(30, 'Deli Serdang ', 2),
(31, 'Simalungun ', 2),
(32, 'Asahan ', 2),
(33, 'Labuhan Batu ', 2),
(34, 'Dairi ', 2),
(35, 'Toba Samosir ', 2),
(36, 'Mandailing Natal ', 2),
(37, 'Nias Selatan ', 2),
(38, 'Pakpak Bharat ', 2),
(39, 'Kab Humbang Hasundutan ', 2),
(40, 'Samosir ', 2),
(41, 'Serdang Bedagai ', 2),
(42, 'Batu Bara', 2),
(43, 'Padang Lawas Utara', 2),
(44, 'Padang Lawas', 2),
(45, 'Kota Medan', 2),
(46, 'Kota Pematang Siantar ', 2),
(47, 'Kota Sibolga ', 2),
(48, 'Kota Tanjung Balai ', 2),
(49, 'Kota Binjai ', 2),
(50, 'Kota Tebing Tinggi ', 2),
(51, 'Kota Padang Sidempuan ', 2),
(52, 'Pesisir Selatan ', 3),
(53, 'Solok ', 3),
(54, 'Sw.Lunto/Sijunjung ', 3),
(55, 'Tanah Datar ', 3),
(56, 'Padang Pariaman ', 3),
(57, 'Agam ', 3),
(58, 'Lima Puluh Kota ', 3),
(59, 'Pasaman ', 3),
(60, 'Kepulauan Mentawai ', 3),
(61, 'Dharmasraya ', 3),
(62, 'Solok Selatan ', 3),
(63, 'Pasaman Barat ', 3),
(64, 'Kota Padang ', 3),
(65, 'Kota Solok ', 3),
(66, 'Kota Sawahlunto ', 3),
(67, 'Kota Padang Panjang ', 3),
(68, 'Kota Bukittinggi ', 3),
(69, 'Kota Payakumbuh ', 3),
(70, 'Kota Pariaman ', 3),
(71, 'Kampar ', 4),
(72, 'Indragiri Hulu ', 4),
(73, 'Bengkalis ', 4),
(74, 'Indragiri Hilir ', 4),
(75, 'Pelalawan ', 4),
(76, 'Rokan Hulu ', 4),
(77, 'Rokan Hilir ', 4),
(78, 'Siak ', 4),
(79, 'Kuantan Singingi ', 4),
(80, 'Kota Pekanbaru ', 4),
(81, 'Kota Dumai ', 4),
(82, 'Kerinci', 5),
(83, 'Merangin ', 5),
(84, 'Sarolangun ', 5),
(85, 'Batanghari ', 5),
(86, 'Muaro Jambi ', 5),
(87, 'Kab Tanjung Jabung Barat ', 5),
(88, 'Kab Tanjung Jabung Timur ', 5),
(89, 'Bungo ', 5),
(90, 'Tebo ', 5),
(91, 'Kota Jambi ', 5),
(92, 'Ogan Komering Ulu ', 6),
(93, 'Ogan Komering Ilir ', 6),
(94, 'Muara Enim ', 6),
(95, 'Lahat ', 6),
(96, 'Musi Rawas ', 6),
(97, 'Musi Banyuasin ', 6),
(98, 'Banyuasin ', 6),
(99, 'Oku Timur ', 6),
(100, 'Oku Selatan ', 6),
(101, 'Ogan Ilir ', 6),
(102, 'Empat Lawang', 6),
(103, 'Kota Palembang ', 6),
(104, 'Kota Pagar Alam ', 6),
(105, 'Kota Lubuk Linggau ', 6),
(106, 'Kota Prabumulih ', 6),
(107, 'Bengkulu Selatan ', 7),
(108, 'Rejang Lebong ', 7),
(109, 'Bengkulu Utara ', 7),
(110, 'Kaur ', 7),
(111, 'Seluma ', 7),
(112, 'Muko Muko ', 7),
(113, 'Lebong ', 7),
(114, 'Kepahiang ', 7),
(115, 'Kota Bengkulu ', 7),
(116, 'Lampung Selatan ', 8),
(117, 'Lampung Tengah ', 8),
(118, 'Lampung Utara ', 8),
(119, 'Lampung Barat ', 8),
(120, 'Tulang Bawang ', 8),
(121, 'Tanggamus ', 8),
(122, 'Lampung Timur ', 8),
(123, 'Way Kanan ', 8),
(124, 'Pesawaran', 8),
(125, 'Kota Bandar Lampung ', 8),
(126, 'Kota Metro ', 8),
(127, 'Bangka ', 9),
(128, 'Belitung ', 9),
(129, 'Bangka Selatan ', 9),
(130, 'Bangka Tengah ', 9),
(131, 'Bangka Barat ', 9),
(132, 'Belitung Timur ', 9),
(133, 'Kota Pangkal Pinang ', 9),
(134, 'Bintan ', 10),
(135, 'Karimun ', 10),
(136, 'Natuna ', 10),
(137, 'Lingga ', 10),
(138, 'Kota Batam ', 10),
(139, 'Kota Tanjung Pinang ', 10),
(140, 'Adm. Kep. Seribu ', 11),
(141, 'Kodya Jakarta Pusat ', 11),
(142, 'Kodya Jakarta Utara ', 11),
(143, 'Kodya Jakarta Barat ', 11),
(144, 'Kodya Jakarta Selatan ', 11),
(145, 'Kodya Jakarta Timur ', 11),
(146, 'Bogor ', 12),
(147, 'Sukabumi ', 12),
(148, 'Cianjur ', 12),
(149, 'Bandung ', 12),
(150, 'Garut ', 12),
(151, 'Tasikmalaya ', 12),
(152, 'Ciamis ', 12),
(153, 'Kuningan ', 12),
(154, 'Cirebon ', 12),
(155, 'Majalengka ', 12),
(156, 'Sumedang ', 12),
(157, 'Indramayu ', 12),
(158, 'Subang ', 12),
(159, 'Purwakarta ', 12),
(160, 'Karawang ', 12),
(161, 'Bekasi ', 12),
(162, 'Bandung Barat ', 12),
(163, 'Kota Bogor ', 12),
(164, 'Kota Sukabumi ', 12),
(165, 'Kota Bandung ', 12),
(166, 'Kota Cirebon ', 12),
(167, 'Kota Bekasi ', 12),
(168, 'Kota Depok ', 12),
(169, 'Kota Cimahi ', 12),
(170, 'Kota Tasikmalaya ', 12),
(171, 'Kota Banjar ', 12),
(172, 'Cilacap ', 13),
(173, 'Banyumas ', 13),
(174, 'Purbalingga ', 13),
(175, 'Banjarnegara ', 13),
(176, 'Kebumen ', 13),
(177, 'Purworejo ', 13),
(178, 'Wonosobo ', 13),
(179, 'Magelang ', 13),
(180, 'Boyolali ', 13),
(181, 'Klaten ', 13),
(182, 'Sukoharjo ', 13),
(183, 'Wonogiri ', 13),
(184, 'Karanganyar ', 13),
(185, 'Sragen ', 13),
(186, 'Grobogan ', 13),
(187, 'Blora ', 13),
(188, 'Rembang ', 13),
(189, 'Pati ', 13),
(190, 'Kudus ', 13),
(191, 'Jepara ', 13),
(192, 'Demak ', 13),
(193, 'Semarang ', 13),
(194, 'Temanggung ', 13),
(195, 'Kendal ', 13),
(196, 'Batang ', 13),
(197, 'Pekalongan ', 13),
(198, 'Pemalang ', 13),
(199, 'Tegal ', 13),
(200, 'Brebes ', 13),
(201, 'Kota Magelang ', 13),
(202, 'Kota Surakarta ', 13),
(203, 'Kota Salatiga ', 13),
(204, 'Kota Semarang ', 13),
(205, 'Kota Pekalongan ', 13),
(206, 'Kota Tegal ', 13),
(207, 'Kulon Progo ', 14),
(208, 'Bantul ', 14),
(209, 'Gunung Kidul ', 14),
(210, 'Sleman ', 14),
(211, 'Kota Yogyakarta ', 14),
(212, 'Pacitan', 15),
(213, 'Ponorogo ', 15),
(214, 'Trenggalek ', 15),
(215, 'Tulungagung ', 15),
(216, 'Blitar ', 15),
(217, 'Kediri ', 15),
(218, 'Malang ', 15),
(219, 'Lumajang', 15),
(220, 'Jember ', 15),
(221, 'Banyuwangi ', 15),
(222, 'Bondowoso ', 15),
(223, 'Situbondo ', 15),
(224, 'Probolinggo ', 15),
(225, 'Pasuruan ', 15),
(226, 'Sidoarjo ', 15),
(227, 'Mojokerto ', 15),
(228, 'Jombang ', 15),
(229, 'Nganjuk ', 15),
(230, 'Madiun ', 15),
(231, 'Magetan ', 15),
(232, 'Ngawi ', 15),
(233, 'Bojonegoro ', 15),
(234, 'Tuban ', 15),
(235, 'Lamongan ', 15),
(236, 'Gresik ', 15),
(237, 'Bangkalan ', 15),
(238, 'Sampang ', 15),
(239, 'Pamekasan ', 15),
(240, 'Sumenep ', 15),
(241, 'Kota Kediri ', 15),
(242, 'Kota Blitar ', 15),
(243, 'Kota Malang ', 15),
(244, 'Kota Probolinggo ', 15),
(245, 'Kota Pasuruan ', 15),
(246, 'Kota Mojokerto ', 15),
(247, 'Kota Madiun ', 15),
(248, 'Kota Surabaya ', 15),
(249, 'Kota Batu ', 15),
(250, 'Pandeglang ', 16),
(251, 'Lebak ', 16),
(252, 'Tangerang ', 16),
(253, 'Serang ', 16),
(254, 'Kota Tangerang ', 16),
(255, 'Kota Cilegon ', 16),
(256, 'Kota Serang', 16),
(257, 'Jembrana', 17),
(258, 'Tabanan ', 17),
(259, 'Badung ', 17),
(260, 'Gianyar ', 17),
(261, 'Klungkung ', 17),
(262, 'Bangli ', 17),
(263, 'Karangasem ', 17),
(264, 'Buleleng ', 17),
(265, 'Kota Denpasar ', 17),
(266, 'Lombok Barat ', 18),
(267, 'Lombok Tengah ', 18),
(268, 'Lombok Timur ', 18),
(269, 'Sumbawa ', 18),
(270, 'Dompu ', 18),
(271, 'Bima ', 18),
(272, 'Sumbawa Barat ', 18),
(273, 'Kota Mataram ', 18),
(274, 'Kota Bima ', 18),
(275, 'Kupang ', 19),
(276, 'Kab Timor Tengah Selatan ', 19),
(277, 'Timor Tengah Utara ', 19),
(278, 'Belu ', 19),
(279, 'Alor ', 19),
(280, 'Flores Timur ', 19),
(281, 'Sikka ', 19),
(282, 'Ende ', 19),
(283, 'Ngada', 19),
(284, 'Manggarai ', 19),
(285, 'Sumba Timur ', 19),
(286, 'Sumba Barat', 19),
(287, 'Lembata', 19),
(288, 'Rote Ndao ', 19),
(289, 'Manggarai Barat ', 19),
(290, 'Nagekeo', 19),
(291, 'Sumba Tengah', 19),
(292, 'Sumba Barat Daya', 19),
(293, 'Manggarai Timur', 19),
(294, 'Kota Kupang ', 19),
(295, 'Sambas ', 20),
(296, 'Pontianak ', 20),
(297, 'Sanggau ', 20),
(298, 'Ketapang ', 20),
(299, 'Sintang ', 20),
(300, 'Kapuas Hulu ', 20),
(301, 'Bengkayang ', 20),
(302, 'Landak ', 20),
(303, 'Sekadau ', 20),
(304, 'Melawi ', 20),
(305, 'Kayong Utara', 20),
(306, 'Kubu Raya', 20),
(307, 'Kota Pontianak ', 20),
(308, 'Kota Singkawang ', 20),
(309, 'Kotawaringin Barat ', 21),
(310, 'Kotawaringin Timur ', 21),
(311, 'Kapuas ', 21),
(312, 'Barito Selatan ', 21),
(313, 'Barito Utara ', 21),
(314, 'Katingan ', 21),
(315, 'Seruyan ', 21),
(316, 'Sukamara ', 21),
(317, 'Lamandau ', 21),
(318, 'Gunung Mas ', 21),
(319, 'Pulang Pisau ', 21),
(320, 'Murung Raya ', 21),
(321, 'Barito Timur ', 21),
(322, 'Kota Palangkaraya ', 21),
(323, 'Tanah Laut ', 22),
(324, 'Kotabaru ', 22),
(325, 'Banjar ', 22),
(326, 'Barito Kuala ', 22),
(327, 'Tapin ', 22),
(328, 'Hulu Sungai Selatan ', 22),
(329, 'Hulu Sungai Tengah ', 22),
(330, 'Hulu Sungai Utara ', 22),
(331, 'Tabalong ', 22),
(332, 'Tanah Bumbu ', 22),
(333, 'Balangan ', 22),
(334, 'Kota Banjarmasin ', 22),
(335, 'Kota Banjarbaru ', 22),
(336, 'Berau ', 23),
(337, 'Kutai Kertanegara ', 23),
(338, 'Kutai Barat ', 23),
(339, 'Kutai Timur ', 23),
(340, 'Paser ', 23),
(341, 'Penajam Paser Utara ', 23),
(342, 'Mahakam Ulu', 23),
(343, 'Kota Balikpapan ', 23),
(344, 'Kota Bontang ', 23),
(345, 'Kota Samarinda ', 23),
(346, 'Bulungan ', 24),
(347, 'Nunukan ', 24),
(348, 'Malinau ', 24),
(349, 'Tana Tidung', 24),
(350, 'Kota Tarakan', 24),
(351, 'Bolaang Mongondow', 25),
(352, 'Minahasa ', 25),
(353, 'Kepulauan Sangihe ', 25),
(354, 'Kepulauan Talaud ', 25),
(355, 'Minahasa Selatan ', 25),
(356, 'Minahasa Utara ', 25),
(357, 'Minahasa Tenggara', 25),
(358, 'Bolmong Utara', 25),
(359, 'Kep. Sitaro', 25),
(360, 'Kota Manado ', 25),
(361, 'Kota Bitung ', 25),
(362, 'Kota Tomohon ', 25),
(363, 'Kota Kotamobagu', 25),
(364, 'Banggai ', 26),
(365, 'Poso ', 26),
(366, 'Donggala ', 26),
(367, 'Toli Toli ', 26),
(368, 'Buol ', 26),
(369, 'Morowali ', 26),
(370, 'Kab Banggai Kepulauan ', 26),
(371, 'Parigi Moutong ', 26),
(372, 'Tojo Una Una ', 26),
(373, 'Kota Palu ', 26),
(374, 'Selayar ', 27),
(375, 'Bulukumba ', 27),
(376, 'Bantaeng ', 27),
(377, 'Jeneponto ', 27),
(378, 'Takalar ', 27),
(379, 'Gowa ', 27),
(380, 'Sinjai ', 27),
(381, 'Bone ', 27),
(382, 'Maros ', 27),
(383, 'Pangkajene Kep. ', 27),
(384, 'Barru ', 27),
(385, 'Soppeng ', 27),
(386, 'Wajo ', 27),
(387, 'Sidenreng Rapang ', 27),
(388, 'Pinrang ', 27),
(389, 'Enrekang  ', 27),
(390, 'Luwu ', 27),
(391, 'Tana Toraja ', 27),
(392, 'Luwu Utara ', 27),
(393, 'Luwu Timur ', 27),
(394, 'Kota Makasar ', 27),
(395, 'Kota Pare Pare ', 27),
(396, 'Kota Palopo ', 27),
(397, 'Kolaka ', 28),
(398, 'Konawe ', 28),
(399, 'Muna ', 28),
(400, 'Buton ', 28),
(401, 'Konawe Selatan ', 28),
(402, 'Bombana ', 28),
(403, 'Wakatobi ', 28),
(404, 'Kolaka Utara ', 28),
(405, 'Konawe Utara', 28),
(406, 'Buton Utara', 28),
(407, 'Kota Kendari ', 28),
(408, 'Kota Bau Bau ', 28),
(409, 'Gorontalo ', 29),
(410, 'Boalemo ', 29),
(411, 'Bone Bolango ', 29),
(412, 'Pahuwato ', 29),
(413, 'Gorontalo Utara', 29),
(414, 'Kota Gorontalo ', 29),
(415, 'Mamuju Utara ', 30),
(416, 'Mamuju ', 30),
(417, 'Mamasa ', 30),
(418, 'Polewali Mamasa ', 30),
(419, 'Majene ', 30),
(420, ' Maluku Tengah ', 31),
(421, 'Maluku Tenggara', 31),
(422, 'Kab Maluku Tenggara Brt ', 31),
(423, 'Buru ', 31),
(424, 'Seram Bagian Timur ', 31),
(425, 'Seram Bagian Barat ', 31),
(426, 'Kepulauan Aru ', 31),
(427, 'Kota Ambon ', 31),
(428, 'Kota Tual', 31),
(429, 'Halmahera Barat ', 32),
(430, 'Halmahera Tengah ', 32),
(431, 'Halmahera Utara ', 32),
(432, 'Halmahera Selatan ', 32),
(433, 'Kepulauan Sula ', 32),
(434, 'Halmahera Timur ', 32),
(435, 'Kota Ternate ', 32),
(436, 'Kota Tidore Kepulauan ', 32),
(437, 'Merauke ', 33),
(438, 'Jayawijaya ', 33),
(439, 'Jayapura ', 33),
(440, 'Nabire ', 33),
(441, 'Yapen Waropen ', 33),
(442, 'Biak Numfor ', 33),
(443, 'Puncak Jaya ', 33),
(444, 'Paniai ', 33),
(445, 'Mimika ', 33),
(446, 'Sarmi', 33),
(447, 'Keerom ', 33),
(448, 'Kab Pegunungan Bintang ', 33),
(449, 'Yahukimo ', 33),
(450, 'Tolikara ', 33),
(451, 'Waropen', 33),
(452, 'Boven Digoel ', 33),
(453, 'Mappi ', 33),
(454, 'Asmat ', 33),
(455, 'Supiori ', 33),
(456, 'Mamberamo Raya', 33),
(457, 'Kota Jayapura ', 33),
(458, 'Sorong ', 34),
(459, 'Manokwari ', 34),
(460, 'Fak Fak ', 34),
(461, 'Sorong Selatan ', 34),
(462, 'Raja Ampat ', 34),
(463, 'Teluk Bentuni ', 34),
(464, 'Teluk Wondama ', 34),
(465, 'Kaimana', 34),
(466, 'Kota Sorong ', 34),
(467, 'Bangkalan', 15);

-- --------------------------------------------------------

--
-- Table structure for table `laporan_penjual`
--

CREATE TABLE IF NOT EXISTS `laporan_penjual` (
`id_laporan` int(11) NOT NULL,
  `nama_pelapor` varchar(40) NOT NULL,
  `email_pelapor` varchar(40) NOT NULL,
  `pesan` text NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `laporan_penjual`
--

INSERT INTO `laporan_penjual` (`id_laporan`, `nama_pelapor`, `email_pelapor`, `pesan`, `waktu`) VALUES
(1, 'Wirat Moko Hadi S', 'wiratmoko11@gmail.com', 'Palus', '2016-05-24 17:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `member_akun`
--

CREATE TABLE IF NOT EXISTS `member_akun` (
  `username` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(1) DEFAULT '0',
  `token` varchar(100) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_akun`
--

INSERT INTO `member_akun` (`username`, `email`, `password`, `status`, `token`) VALUES
('140411100037', '140411100037@student.trunojoyo.ac.id', 'e807f1fcf82d132f9bb018ca6738a19f', 0, '0b5ea166285ff1bf94f6b7e33878025f573be90d8b058'),
('chandra21', 'chandra_febryan21@yahoo.com', 'e807f1fcf82d132f9bb018ca6738a19f', 1, ''),
('umamanon', 'umam.505@gmail.com', 'e807f1fcf82d132f9bb018ca6738a19f', 1, ''),
('wiratmoko11', 'wiratmoko11@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 1, ''),
('wiratmoko12', 'wiratmoko12@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 0, '76ace0b67f844c108f91fea50569300957331c878dfbc'),
('wirat_m', 'wirat_m@yahoo.com', '827ccb0eea8a706c4c34a16891f84e7b', 0, 'edb5494f935c29f120a28f60f916a957574e9e2d90ca7');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(40);

-- --------------------------------------------------------

--
-- Table structure for table `poin`
--

CREATE TABLE IF NOT EXISTS `poin` (
`id_poin` int(11) NOT NULL,
  `nama_poin` varchar(20) NOT NULL,
  `jumlah_poin` int(11) NOT NULL DEFAULT '0',
  `harga_poin` int(11) NOT NULL DEFAULT '0',
  `gambar_poin` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `poin`
--

INSERT INTO `poin` (`id_poin`, `nama_poin`, `jumlah_poin`, `harga_poin`, `gambar_poin`) VALUES
(1, '500 Poin', 500, 400000, '500.png'),
(2, '250 Poin', 250, 215000, '250.png'),
(3, '100 Poin', 100, 90000, '100(1).png'),
(4, '50 Poin', 50, 50000, '50(1).png'),
(5, '1000 Poin', 1000, 750000, '1000.png');

-- --------------------------------------------------------

--
-- Table structure for table `premium_iklan`
--

CREATE TABLE IF NOT EXISTS `premium_iklan` (
`id_premium` int(11) NOT NULL,
  `nama_premium` varchar(20) NOT NULL,
  `seo_premium` varchar(20) NOT NULL,
  `harga_premium` int(11) NOT NULL DEFAULT '0',
  `gambar_premium` varchar(50) NOT NULL,
  `masa_aktif` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `premium_iklan`
--

INSERT INTO `premium_iklan` (`id_premium`, `nama_premium`, `seo_premium`, `harga_premium`, `gambar_premium`, `masa_aktif`) VALUES
(1, 'Top 25', 'top-25', 50, '15.png', 7),
(2, 'Recommended', 'recommended', 10, '100.png', 30),
(3, 'Terlaris', 'terlaris', 10, '300.png', 30);

-- --------------------------------------------------------

--
-- Table structure for table `profil_akun`
--

CREATE TABLE IF NOT EXISTS `profil_akun` (
`id_profil` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `provinsi_member` int(11) NOT NULL,
  `kota_member` int(11) NOT NULL,
  `nama_member` varchar(80) NOT NULL DEFAULT '',
  `telepon_member` varchar(20) NOT NULL DEFAULT '',
  `bbm_member` varchar(10) NOT NULL DEFAULT '',
  `wa_member` varchar(20) NOT NULL DEFAULT '',
  `poin_member` int(11) DEFAULT '0',
  `telepon_member2` varchar(15) DEFAULT NULL,
  `id_line` varchar(25) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `profil_akun`
--

INSERT INTO `profil_akun` (`id_profil`, `username`, `provinsi_member`, `kota_member`, `nama_member`, `telepon_member`, `bbm_member`, `wa_member`, `poin_member`, `telepon_member2`, `id_line`, `alamat`) VALUES
(2, 'wiratmoko11', 15, 227, 'Wirat Moko Hadi S', '0898989898', 'B8GaA', '', 4, '', 'wiratmoko111', 'Dsn Unggahan RT 03 RW 07 Ds. Banjaragung Kec. Puri Kab Mojokerto'),
(3, 'umamanon', 15, 226, 'Umam Fajri', '', '', '', 0, NULL, NULL, NULL),
(4, 'wiratmoko12', 0, 0, '', '', '', '', 0, NULL, NULL, NULL),
(5, '140411100037', 0, 0, '', '', '', '', 0, NULL, NULL, NULL),
(6, 'Admin', 0, 0, 'Administrator', '', '', '', 0, NULL, NULL, NULL),
(7, 'chandra21', 0, 0, '', '', '', '', 0, NULL, NULL, NULL),
(8, 'wirat_m', 5, 86, 'Wirat Moko Hadi S', '08989878', 'IH97GJ', '', 0, '09897878', 'wiratmoko111', 'Jalan Gunung Raya Nomer 4');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE IF NOT EXISTS `provinsi` (
`id_provinsi` int(11) NOT NULL,
  `nama_provinsi` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `nama_provinsi`) VALUES
(1, 'Nanggroe Aceh Darussalam'),
(2, 'Sumatera Utara'),
(3, 'Sumatera Barat'),
(4, 'Riau'),
(5, 'Jambi'),
(6, 'Sumatera Selatan '),
(7, 'Bengkulu'),
(8, 'Lampung'),
(9, 'Kep. Bangka Belitung'),
(10, 'Kepulauan Riau'),
(11, 'Dki Jakarta'),
(12, 'Jawa Barat'),
(13, 'Jawa Tengah'),
(14, 'Yogyakarta '),
(15, 'Jawa Timur'),
(16, 'Banten'),
(17, 'Bali'),
(18, 'Nusa Tenggara Barat'),
(19, 'Nusa Tenggara Timur'),
(20, 'Kalimantan Barat'),
(21, 'Kalimantan Tengah'),
(22, 'Kalimantan Selatan'),
(23, 'Kalimantan Timur'),
(24, 'Kalimantan Utara'),
(25, 'Sulawesi Utara'),
(26, 'Sulawesi Tengah'),
(27, 'Sulawesi Selatan'),
(28, 'Sulawesi Tenggara'),
(29, 'Gorontalo'),
(30, 'Sulawesi Barat'),
(31, 'Maluku'),
(32, 'Maluku Utara'),
(33, 'Papua'),
(34, 'Papua Barat');

-- --------------------------------------------------------

--
-- Table structure for table `reply_komentar`
--

CREATE TABLE IF NOT EXISTS `reply_komentar` (
`id_reply` int(11) NOT NULL,
  `id_komentar` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `reply` text NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `reply_komentar`
--

INSERT INTO `reply_komentar` (`id_reply`, `id_komentar`, `username`, `reply`, `waktu`) VALUES
(1, 1, 'wiratmoko11', 'Di Banjarmasin gan, otw mojokerto', '2016-05-20 21:49:11'),
(2, 1, 'admin', '', '2016-05-21 10:04:14'),
(3, 1, 'admin', 'okee gan', '2016-05-21 10:04:41'),
(4, 1, 'admin', 'siaap', '2016-05-21 10:07:14'),
(5, 1, 'admin', 'siaap', '2016-05-21 10:07:29'),
(6, 1, 'admin', 'siaap', '2016-05-21 10:09:30'),
(7, 1, 'admin', 'siaap', '2016-05-21 10:09:34'),
(8, 1, 'admin', 'siaap', '2016-05-21 10:10:22'),
(9, 1, 'admin', 'siaap', '2016-05-21 10:11:37'),
(10, 1, 'admin', 'test', '2016-05-21 10:20:04'),
(11, 1, 'admin', 'holaa', '2016-05-21 10:26:09'),
(12, 1, 'admin', 'asdasdasd', '2016-05-21 10:28:37'),
(13, 1, 'admin', 'testsdasdas', '2016-05-21 10:31:35'),
(14, 1, 'admin', 'testsdasdas', '2016-05-21 10:31:42'),
(15, 1, 'admin', 'moko', '2016-05-21 10:32:01'),
(16, 1, 'admin', 'noo', '2016-05-21 10:34:52'),
(17, 1, 'admin', 'aadaaaaa', '2016-05-21 10:36:25'),
(18, 1, 'admin', 'aadaaaaa', '2016-05-21 10:36:34'),
(19, 1, 'admin', 'aadaaaaa', '2016-05-21 10:36:46'),
(20, 1, 'admin', 'f', '2016-05-21 10:37:52'),
(21, 1, 'admin', 'fa', '2016-05-21 10:42:34'),
(22, 1, 'admin', 'fa', '2016-05-21 10:44:52'),
(23, 1, 'admin', 'Oke Gan Makasih', '2016-05-21 10:46:20'),
(24, 1, 'admin', 'Siap', '2016-05-21 10:48:53'),
(25, 1, 'admin', 'Sipp', '2016-05-21 10:50:35'),
(26, 1, 'admin', 'wokeeh', '2016-05-21 10:52:48'),
(27, 1, 'admin', 'yee', '2016-05-21 10:52:54'),
(28, 1, 'admin', 'but way?', '2016-05-21 10:52:57'),
(29, 1, 'admin', 'hbsdbdhsds dsuc dscgds gcydsgcyudgsy cgdsy cg uydsg cy udsgcyugdsycg dsygc yudgsycg dsygcdsy gcudgscuydg scyugdscy ugdsy cgyusd gcy gsc sydgcudsgca ashdiagsd asd as dasdasd asd asdasdasd fgdfgdfg dfgdfgdfgsdf dsfdsfasfa fasfqwfewfwefew sdfdsfdsfdsf dsfasfasdasd ', '2016-05-21 10:53:37'),
(30, 1, 'admin', 'lo', '2016-05-21 12:43:49'),
(31, 1, 'admin', 'guwee', '2016-05-21 12:43:54'),
(32, 1, 'admin', 'yes', '2016-05-21 13:31:31'),
(33, 1, 'wiratmoko11', 'okee', '2016-05-23 20:58:00'),
(34, 1, 'wiratmoko11', 'ok', '2016-05-28 11:18:22');

-- --------------------------------------------------------

--
-- Table structure for table `slider_replika`
--

CREATE TABLE IF NOT EXISTS `slider_replika` (
`id_slider` int(11) NOT NULL,
  `id_website` int(11) NOT NULL,
  `slider1` varchar(40) NOT NULL,
  `slider2` varchar(40) NOT NULL,
  `slider3` varchar(40) NOT NULL,
  `slider4` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `slider_replika`
--

INSERT INTO `slider_replika` (`id_slider`, `id_website`, `slider1`, `slider2`, `slider3`, `slider4`) VALUES
(2, 3, 'krasivyi_korabl_-1024x768.jpg', 'image-slider-4.jpg', 'dnivo-slider-7.jpg', 'sweden.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sub1_kategori`
--

CREATE TABLE IF NOT EXISTS `sub1_kategori` (
`id_sub1_kategori` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `sub1_kategori` varchar(60) NOT NULL,
  `seo_sub1_kategori` varchar(255) DEFAULT ''
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `sub1_kategori`
--

INSERT INTO `sub1_kategori` (`id_sub1_kategori`, `id_kategori`, `sub1_kategori`, `seo_sub1_kategori`) VALUES
(1, 5, 'Baju Dewasa', 'baju-dewasa'),
(2, 5, 'Make Up & Parfum', 'make-up--parfum'),
(3, 5, 'Fashion Pria', 'fashion-pria'),
(4, 5, 'Fashion Wanita', 'fashion-wanita'),
(5, 5, 'Jam Tangan', 'jam-tangan'),
(6, 5, 'Perhiasan', 'perhiasan'),
(8, 8, 'Merk', 'merk'),
(9, 8, 'Aksesoris', 'aksesoris'),
(10, 8, 'Audio Mobil', 'audio-mobil'),
(11, 8, 'Sparepart', 'sparepart'),
(12, 8, 'Velg dan Ban', 'velg-dan-ban'),
(13, 7, 'Merk', 'merk'),
(14, 7, 'Aksesoris', 'aksesoris'),
(15, 7, 'Helm', 'helm'),
(16, 7, 'Sparepart', 'sparepart'),
(17, 6, 'Rumah', 'rumah'),
(18, 6, 'Apartmen', 'apartmen'),
(19, 6, 'Indekos', 'indekos'),
(20, 6, 'Bangunan Komersil', 'bangunan-komersil'),
(21, 6, 'Tanah', 'tanah'),
(22, 6, 'Properti Lainnya', 'properti-lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `sub2_kategori`
--

CREATE TABLE IF NOT EXISTS `sub2_kategori` (
`id_sub2_kategori` int(11) NOT NULL,
  `id_sub1_kategori` int(11) NOT NULL,
  `sub2_kategori` varchar(60) NOT NULL,
  `seo_sub2_kategori` varchar(255) DEFAULT ''
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `sub2_kategori`
--

INSERT INTO `sub2_kategori` (`id_sub2_kategori`, `id_sub1_kategori`, `sub2_kategori`, `seo_sub2_kategori`) VALUES
(1, 1, 'Kemeja', 'kemeja'),
(2, 1, 'Celana', 'celana'),
(3, 4, 'Atasan', 'atasan'),
(4, 4, 'Celana', 'celana'),
(5, 4, 'Rok', 'rok'),
(6, 4, 'Busana Muslim', 'busana-muslim'),
(7, 4, 'Dress', 'dress'),
(8, 4, 'Kebaya', 'kebaya'),
(9, 4, 'Sepatu & Sandal', 'sepatusandal'),
(10, 4, 'Aksesoris', 'aksesoris'),
(11, 4, 'Tas & Dompet', 'tasdompet'),
(12, 8, 'Audi', 'audi'),
(13, 8, 'Bentley', 'bentley'),
(14, 8, 'BMW', 'bmw'),
(15, 8, 'Bus', 'bus'),
(16, 8, 'Chery', 'chery'),
(17, 8, 'Chevrolet', 'chevrolet'),
(18, 8, 'Chrysler', 'chrysler'),
(19, 8, 'Citroen', 'citroen'),
(20, 8, 'Daewoo', 'daewoo'),
(21, 8, 'Daihatsu', 'daihatsu'),
(22, 8, 'Datsun', 'datsun'),
(23, 8, 'Dodge', 'dodge'),
(24, 8, 'Ferrari', 'ferrari'),
(25, 8, 'Fiat', 'fiat'),
(26, 8, 'Ford', 'ford'),
(27, 8, 'Foton', 'foton'),
(28, 8, 'Geely', 'geely'),
(29, 8, 'Holden', 'holden'),
(30, 8, 'Honda', 'honda'),
(31, 8, 'Hummer', 'hummer'),
(32, 8, 'Hyundai', 'hyundai'),
(33, 8, 'Infiniti', 'infiniti'),
(34, 8, 'Isuzu', 'isuzu'),
(35, 8, 'Jaguar', 'jaguar'),
(36, 8, 'Jeep', 'jeep'),
(37, 8, 'KIA', 'kia'),
(38, 8, 'Klasik dan Antik', 'klasik-dan-antik'),
(39, 8, 'Lamborghini', 'lamborghini'),
(40, 8, 'Land Rover', 'land-rover'),
(41, 8, 'Lexus', 'lexus'),
(42, 8, 'Maserati', 'maserati'),
(43, 8, 'Mazda', 'mazda'),
(44, 8, 'Mercedes-Benz', 'mercedesbenz'),
(45, 8, 'Mini Cooper', 'mini-cooper'),
(46, 8, 'Mitsubishi', 'mitsubishi'),
(47, 8, 'Mobil CBU', 'mobil-cbu'),
(48, 8, 'Nissan', 'nissan'),
(49, 8, 'Opel', 'opel'),
(50, 8, 'Peugeot', 'peugeot'),
(51, 8, 'Porsche', 'porsche'),
(52, 8, 'Proton', 'proton'),
(53, 8, 'Renault', 'renault'),
(54, 8, 'Roll-Royce', 'rollroyce'),
(55, 8, 'Smart', 'smart'),
(56, 8, 'Ssang Yong', 'ssang-yong'),
(57, 8, 'Subaru', 'subaru'),
(58, 8, 'Suzuki', 'suzuki'),
(59, 8, 'Tata', 'tata'),
(60, 8, 'Timor', 'timor'),
(61, 8, 'Toyota', 'toyota'),
(62, 8, 'Truk', 'truk'),
(63, 8, 'Volkswagen', 'volkswagen'),
(64, 8, 'Volvo', 'volvo'),
(65, 8, 'Lain-lain', 'lainlain'),
(66, 13, 'Aprilia', 'aprilia'),
(67, 13, 'Bajaj', 'bajaj'),
(68, 13, 'Benelli', 'benelli'),
(69, 13, 'BMW', 'bmw'),
(70, 13, 'Ducati', 'ducati'),
(71, 13, 'Gilera', 'gilera'),
(72, 13, 'Harley Davidson', 'harley-davidson'),
(73, 13, 'Honda', 'honda'),
(74, 13, 'Husqvarna', 'husqvarna'),
(75, 13, 'Hyosung', 'hyosung'),
(76, 13, 'Jialing', 'jialing'),
(77, 13, 'Kanzen', 'kanzen'),
(78, 13, 'Kawasaki', 'kawasaki'),
(79, 13, 'Kreidler', 'kreidler'),
(80, 13, 'KTM', 'ktm'),
(81, 13, 'Kymco', 'kymco'),
(82, 13, 'Minerva', 'minerva'),
(83, 13, 'Off-Road dan Auto-Cross', 'offroad-dan-autocross'),
(84, 13, 'Piaggio', 'piaggio'),
(85, 13, 'Quads dan Trikes', 'quads-dan-trikes'),
(86, 13, 'Sanex', 'sanex'),
(87, 13, 'Suzuki', 'suzuki'),
(88, 13, 'Tossa', 'tossa'),
(89, 13, 'Triumph', 'triumph'),
(90, 13, 'TVS', 'tvs'),
(91, 13, 'Viar', 'viar'),
(92, 13, 'Victory', 'victory'),
(93, 13, 'Yamaha', 'yamaha'),
(94, 13, 'Zundapp', 'zundapp'),
(95, 13, 'Lain-lain', 'lainlain'),
(96, 17, 'Dijual', 'dijual'),
(97, 17, 'Disewakan', 'disewakan'),
(98, 18, 'Dijual', 'dijual'),
(99, 18, 'Disewakan', 'disewakan'),
(100, 20, 'Dijual', 'dijual'),
(101, 20, 'Disewakan', 'disewakan'),
(102, 21, 'Dijual', 'dijual'),
(103, 21, 'Disewakan', 'disewakan');

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE IF NOT EXISTS `template` (
`id_template` int(11) NOT NULL,
  `nama_template` varchar(30) NOT NULL,
  `harga_template` int(11) NOT NULL,
  `thumbnail_template` varchar(80) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id_template`, `nama_template`, `harga_template`, `thumbnail_template`) VALUES
(1, 'Aditiion', 0, ''),
(2, 'Bloyyn', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_iklan_premium`
--

CREATE TABLE IF NOT EXISTS `transaksi_iklan_premium` (
`id_transaksi` int(11) NOT NULL,
  `id_premium_iklan` int(11) NOT NULL,
  `id_iklan` int(11) NOT NULL,
  `tanggal_transaksi` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `transaksi_iklan_premium`
--

INSERT INTO `transaksi_iklan_premium` (`id_transaksi`, `id_premium_iklan`, `id_iklan`, `tanggal_transaksi`) VALUES
(1, 1, 1, '2016-04-26 16:14:20'),
(2, 4, 12, '2016-04-26 16:14:44'),
(3, 1, 2, '2016-04-26 16:17:49'),
(4, 1, 1, '2016-04-26 17:49:33'),
(5, 1, 1, '2016-04-26 17:50:07'),
(6, 1, 1, '2016-04-26 17:53:27'),
(7, 1, 1, '2016-04-26 17:53:45'),
(8, 1, 1, '2016-04-26 18:38:03'),
(9, 1, 1, '2016-04-26 18:38:31'),
(10, 1, 1, '2016-04-26 18:39:03'),
(11, 3, 12, '2016-04-28 18:31:11'),
(12, 1, 7, '2016-04-29 03:17:11'),
(13, 1, 7, '2016-04-29 03:28:08'),
(14, 1, 25, '2016-05-17 18:14:37');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_poin`
--

CREATE TABLE IF NOT EXISTS `transaksi_poin` (
`id_transaksi_poin` int(11) NOT NULL,
  `id_poin` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `tanggal_transaksi` datetime NOT NULL,
  `isKonfirmasi` int(11) NOT NULL DEFAULT '0',
  `biaya_transaksi` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `transaksi_poin`
--

INSERT INTO `transaksi_poin` (`id_transaksi_poin`, `id_poin`, `username`, `tanggal_transaksi`, `isKonfirmasi`, `biaya_transaksi`) VALUES
(1, 1, 'wiratmoko11', '2016-04-27 14:02:06', 0, 20131),
(2, 1, 'wiratmoko11', '2016-04-27 17:26:31', 1, 40123),
(3, 1, 'wiratmoko11', '2016-04-27 17:29:33', 1, 41232),
(4, 1, 'wiratmoko11', '2016-04-28 19:06:43', 0, 0),
(5, 1, 'wiratmoko11', '2016-04-28 19:17:03', 0, 0),
(6, 1, 'wiratmoko11', '2016-04-28 19:17:10', 0, 0),
(7, 1, 'wiratmoko11', '2016-04-28 19:17:13', 0, 0),
(8, 1, 'wiratmoko11', '2016-04-28 19:17:20', 0, 0),
(9, 2639, 'wiratmoko11', '2016-04-28 19:18:14', 0, 0),
(10, 2, 'wiratmoko11', '2016-04-28 19:20:58', 0, 25319),
(11, 1, 'wiratmoko11', '2016-04-28 19:22:51', 0, 11462),
(12, 1, 'wiratmoko11', '2016-04-28 19:23:43', 0, 11257),
(13, 1, 'wiratmoko11', '2016-04-28 19:25:26', 0, 12355),
(14, 2, 'wiratmoko11', '2016-04-29 03:05:22', 0, 25646),
(15, 4, 'wiratmoko11', '2016-05-28 15:44:16', 0, 52194);

-- --------------------------------------------------------

--
-- Table structure for table `website_replika`
--

CREATE TABLE IF NOT EXISTS `website_replika` (
`id_website` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `nama_website` varchar(40) NOT NULL,
  `alamat_website` varchar(40) NOT NULL,
  `template` int(11) NOT NULL,
  `langkah_pembuatan` int(11) NOT NULL,
  `about` text,
  `about_foto` varchar(100) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `website_replika`
--

INSERT INTO `website_replika` (`id_website`, `username`, `nama_website`, `alamat_website`, `template`, `langkah_pembuatan`, `about`, `about_foto`, `logo`) VALUES
(3, 'wiratmoko11', 'Lanaya Online Catering', 'lanaya', 1, 4, '<p>Catering Online</p>\r\n', '1000px-The_GIMP_icon_-_gnome_svg1.png', 'Logo_Tagline_Putih.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`username`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
 ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `detail_premium`
--
ALTER TABLE `detail_premium`
 ADD PRIMARY KEY (`id_detail_premium`);

--
-- Indexes for table `favorit`
--
ALTER TABLE `favorit`
 ADD PRIMARY KEY (`id_favorit`);

--
-- Indexes for table `iklan`
--
ALTER TABLE `iklan`
 ADD PRIMARY KEY (`id_iklan`);

--
-- Indexes for table `iklan_photo`
--
ALTER TABLE `iklan_photo`
 ADD PRIMARY KEY (`id_photo`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
 ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
 ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `laporan_penjual`
--
ALTER TABLE `laporan_penjual`
 ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `member_akun`
--
ALTER TABLE `member_akun`
 ADD PRIMARY KEY (`username`);

--
-- Indexes for table `poin`
--
ALTER TABLE `poin`
 ADD PRIMARY KEY (`id_poin`);

--
-- Indexes for table `premium_iklan`
--
ALTER TABLE `premium_iklan`
 ADD PRIMARY KEY (`id_premium`);

--
-- Indexes for table `profil_akun`
--
ALTER TABLE `profil_akun`
 ADD PRIMARY KEY (`id_profil`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
 ADD PRIMARY KEY (`id_provinsi`), ADD UNIQUE KEY `id_provinsi_2` (`id_provinsi`), ADD KEY `id_provinsi` (`id_provinsi`);

--
-- Indexes for table `reply_komentar`
--
ALTER TABLE `reply_komentar`
 ADD PRIMARY KEY (`id_reply`);

--
-- Indexes for table `slider_replika`
--
ALTER TABLE `slider_replika`
 ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `sub1_kategori`
--
ALTER TABLE `sub1_kategori`
 ADD PRIMARY KEY (`id_sub1_kategori`);

--
-- Indexes for table `sub2_kategori`
--
ALTER TABLE `sub2_kategori`
 ADD PRIMARY KEY (`id_sub2_kategori`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
 ADD PRIMARY KEY (`id_template`);

--
-- Indexes for table `transaksi_iklan_premium`
--
ALTER TABLE `transaksi_iklan_premium`
 ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `transaksi_poin`
--
ALTER TABLE `transaksi_poin`
 ADD PRIMARY KEY (`id_transaksi_poin`);

--
-- Indexes for table `website_replika`
--
ALTER TABLE `website_replika`
 ADD PRIMARY KEY (`id_website`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `detail_premium`
--
ALTER TABLE `detail_premium`
MODIFY `id_detail_premium` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `favorit`
--
ALTER TABLE `favorit`
MODIFY `id_favorit` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `iklan`
--
ALTER TABLE `iklan`
MODIFY `id_iklan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `iklan_photo`
--
ALTER TABLE `iklan_photo`
MODIFY `id_photo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
MODIFY `id_kota` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=468;
--
-- AUTO_INCREMENT for table `laporan_penjual`
--
ALTER TABLE `laporan_penjual`
MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `poin`
--
ALTER TABLE `poin`
MODIFY `id_poin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `premium_iklan`
--
ALTER TABLE `premium_iklan`
MODIFY `id_premium` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `profil_akun`
--
ALTER TABLE `profil_akun`
MODIFY `id_profil` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
MODIFY `id_provinsi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `reply_komentar`
--
ALTER TABLE `reply_komentar`
MODIFY `id_reply` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `slider_replika`
--
ALTER TABLE `slider_replika`
MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sub1_kategori`
--
ALTER TABLE `sub1_kategori`
MODIFY `id_sub1_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `sub2_kategori`
--
ALTER TABLE `sub2_kategori`
MODIFY `id_sub2_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `transaksi_iklan_premium`
--
ALTER TABLE `transaksi_iklan_premium`
MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `transaksi_poin`
--
ALTER TABLE `transaksi_poin`
MODIFY `id_transaksi_poin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `website_replika`
--
ALTER TABLE `website_replika`
MODIFY `id_website` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
