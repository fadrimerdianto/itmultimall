var myUrl = window.location.href;

getSpesialFilter($('#filter-sub select').val(),$('#filter-sub2 select').val());

$('#filterlist-subkategori').parent().hide();

$('#filterlist-subkategori2').parent().hide();

$('#filterlist-kategori li').on('click', function(){
	var kategori = $(this).attr('data-id');
	var seo = $(this).attr('data-seo');
	myUrl = base_url+'mobile/iklan/kategori/'+seo
	$.ajax({
		method:'get',
		url:base_url+'mobile/ajax/filter_subkategori',
		data:{kategori:kategori},
		success:function(result){
			$('#filterlist-subkategori li').remove();
			var data = JSON.parse(result);
			for (var i = 0; i < data.length; i++) {
				$('#filterlist-subkategori').append('<li data-seo="'+data[i].seo_sub1_kategori+'" data-id="'+data[i].id_sub1_kategori+'"><a href="#" data-ajax="false" data-icon="false">'+data[i].sub1_kategori+'</a></li>');
			}
			$('#filterlist-subkategori').listview('refresh');
			$('#filterlist-kategori').parent().parent().collapsible('option', 'collapsed', 'true');
			$('#filterlist-subkategori').parent().parent().collapsible('option', 'collapsed', 'false');
			$('#filterlist-subkategori').parent().parent().show();
			aksi_sub1();
		}
	});
})

function aksi_sub1(){
	$('#filterlist-subkategori li').on('click', function(){
		var subkategori = $(this).attr('data-id');
		var seo = $(this).attr('data-seo');
		myUrl = myUrl+'/'+seo;
		$.ajax({
			method:'get',
			url:base_url+'mobile/ajax/filter_subkategori2',
			data:{subkategori:subkategori},
			success:function(result){
				var data = JSON.parse(result);
				if(data.length > 0){
					for (var i = 0; i < data.length; i++) {
						$('#filterlist-subkategori2').append('<li data-seo="'+data[i].seo_sub2_kategori+'" data-id="'+data[i].id_sub2_kategori+'"><a href="#" data-ajax="false" data-icon="false">'+data[i].sub2_kategori+'</a></li>');
					}
					$('#filterlist-subkategori2').listview('refresh');
					$('#filterlist-subkategori').parent().parent().collapsible('option', 'collapsed', 'true');
					$('#filterlist-subkategori2').parent().parent().collapsible('option', 'collapsed', 'false');
					$('#filterlist-subkategori2').parent().parent().show();
					aksi_sub2();
				}else{
					window.location.href= myUrl;
				}
			}
		})
	})
}

function aksi_sub2(){
	$('#filterlist-subkategori2 li').on('click', function(){
		var seo = $(this).attr('data-seo');
		addQSParm("filter", seo);
		window.location.href= myUrl;

	});
}

function addQSParm(name, value) {
	var re = new RegExp("([?&]" + name + "=)[^&]+", "");

	function add(sep) {
		myUrl += sep + name + "=" + encodeURIComponent(value);
	}

	function change() {
		myUrl = myUrl.replace(re, "$1" + encodeURIComponent(value));
	}
	if (myUrl.indexOf("?") === -1) {
		add("?");
	} else {
		if (re.test(myUrl)) {
			change();
		} else {
			add("&");
		}
	}
}

$('input[type=radio][name=sorting]').click(function(){
	if ($(this).is(':checked'))
	{
		addQSParm("sort", $(this).val());
		window.location.href = myUrl;
	}
	
})

$("#form-filter").submit(function(e){
	e.preventDefault();

	var data = $(this).serializeArray();
	for (var i = 0; i < data.length; i++) {
		if(data[i].name == 'keyword' && data[i].value == ''){
			//delete parameter
		}else{
			if(data[i].name == 'kategori'){
				myUrl = base_url+'mobile/iklan/kategori/'+data[i].value;
			}else if(data[i].name == 'subkategori'){
				myUrl = myUrl+'/'+data[i].value;
			}else{
				if((data[i].name == 'keyword' && data[i].value != '') || data[i].name != 'keyword'){
					if(data[i].name == 'sub2kategori'){
						addQSParm('filter', data[i].value);
					}else{
						addQSParm(data[i].name, data[i].value);
					}
				}
			}
		}
	};
	window.location.href = myUrl;
})

$('select[name=kategori]').on('change', function(){
	getSubKategori($(this).val());
});

function getSubKategori(kategori){
	$.ajax({
		method:'get',
		url:base_url+'mobile/ajax/filter_subkategori',
		data:{kategori:kategori},
		success:function(result){
			$('#filter-sub select option').remove();
			var data = JSON.parse(result);
			$('#filter-sub select').append('<option selected disabled>Pilih Sub Kategori</option>');
			for (var i = 0; i < data.length; i++) {
				$('#filter-sub select').append('<option value="'+data[i].seo_sub1_kategori+'">'+data[i].sub1_kategori+'</option>');
			};
			$('#filter-sub select').selectmenu('refresh');
			$('#filter-sub').show();
			$('#filter-sub2').hide();
		}
	})
}


$('select[name=subkategori]').on('change', function(){
	getSub2Kategori($(this).val());
})

function getSub2Kategori(subkategori){
	$.ajax({
		method:'get',
		url:base_url+'mobile/ajax/filter_subkategori2',
		data:{subkategori:subkategori},
		success:function(result){
			$('#filter-sub2 select option').remove();
			var data = JSON.parse(result);
			if(data.length > 0){
				$('#filter-sub2 select').append('<option selected disabled>Pilih Sub 2 Kategori</option>');
				for (var i = 0; i < data.length; i++) {
					$('#filter-sub2 select').append('<option value="'+data[i].seo_sub2_kategori+'">'+data[i].sub2_kategori+'</option>');
				};
				$('#filter-sub2 select').selectmenu('refresh');
				$('#filter-sub2').show();
			}
		}
	})
}

$('select[name=sub2kategori]').on('change', function(){
	getSpesialFilter($('#filter-sub select').val(),$(this).val());
})

function getSpesialFilter(subkategori, sub2kategori){
	if(subkategori != null && sub2kategori != null){
		$.ajax({
			method:'get',
			url:base_url+'mobile/ajax/filter_spesial',
			data:{subkategori:subkategori, sub2kategori:sub2kategori},
			success:function(result){
				$('#filter-khusus').html(result);
				$('#filter-khusus').trigger('create');
			}
		})
	}
	
}