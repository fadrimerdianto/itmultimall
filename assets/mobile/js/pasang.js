$('#pilih-kategori').change(function(){
	$.ajax({
		method:'get',
		url: base_url+'mobile/ajax/subkategori',
		data:{kategori:$(this).val()},
		success:function(result){
			$('#pilih-subkategori').html(result).selectmenu('refresh');
			$('#wrap-pilih-subkategori').show();
			$('#wrap-pilih-sub2kategori').hide();
			$('#form-filter').html('');

		}
	})
})

$('#pilih-subkategori').change(function(){
	$.ajax({
		method:'get',
		url:base_url+'mobile/ajax/sub2kategori',
		data:{subkategori:$(this).val()},
		success:function(result){
			if(result !== '		'){
				$('#pilih-sub2kategori').html(result).selectmenu('refresh');
				$('#wrap-pilih-sub2kategori').show();
			}else{
				$('#wrap-pilih-sub2kategori').hide();
			}
			$('#form-filter').html('');
		}
	})
})

$('#pilih-sub2kategori').change(function(){
	$.ajax({
		method:'get',
		url:base_url+'mobile/ajax/formfilter',
		data:{sub2kategori:$(this).val(), subkategori:$('#pilih-subkategori').val()},
		success:function(result){
			$('#form-filter').html(result).trigger('create');
			if($('#pilih-subkategori').val() == 8){
				$('#pilih-transmisi').selectmenu();
				$('#pilih-tahun').selectmenu();
				$('#pilih-tipe').selectmenu();
			}else if(null){

			}else{
				
			}
		}
	})
})

$('#pilih-provinsi').change(function(){
	$.ajax({
		method:'get',
		url:base_url+'mobile/ajax/kota',
		data:{provinsi:$(this).val()},
		success:function(result){
			$('#pilih-kota').html(result);
			$('#pilih-kota').selectmenu('refresh');			
			$('#wrap-pilih-kota').show();
		}
	})
})