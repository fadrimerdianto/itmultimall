<?php 
	class KategoriModel extends CI_Model{
		var $foto_kategori_path;	
		function __construct(){
			parent::__construct();
			$this->foto_kategori_path = realpath(APPPATH .'../images/kategori/');
			$this->load->model('IklanModel');
		}

		function getKategori(){
			$kategori = $this->db->order_by('id_kategori', 'ASC')->get('kategori');
			$kategori = $kategori->result();
			foreach ($kategori as $q_kategori) {
				$subkategori = $this->db->get_where('sub1_kategori', ['id_kategori'=>$q_kategori->id_kategori]);
				$q_kategori->sub1_kategori = $subkategori->result();
				foreach ($q_kategori->sub1_kategori as $q_sub1) {
					# code...
					$sub2kategori = $this->db->get_where('sub2_kategori', ['id_sub1_kategori'=>$q_sub1->id_sub1_kategori]);
					$q_sub1->sub2_kategori = $sub2kategori->result();
				}
				$jumlahiklan = $this->IklanModel->getJumlahIklanByKategori($q_kategori->id_kategori);
				$q_kategori->jumlahkategoriiklan = $jumlahiklan;
			}
			// foreach ($kategori->sub1_kategori as $q_sub1) {
			// 	$sub2kategori = 
			// 	$q_sub1->sub2_kategori = $sub2kategori->result();
			// }
			return $kategori;
		}
		function KategoriById($id){
			$kategori = $this->db->get_where('kategori',['id_kategori' => $id]);
			return $kategori;
		}
		function Sub1KategoriById($id){
	        $this->db->from('sub1_kategori');
	        $this->db->join('kategori', 'kategori.id_kategori = sub1_kategori.id_kategori');
	        $this->db->where('sub1_kategori.id_sub1_kategori', $id);
	        $this->db->order_by('id_sub1_kategori', 'ASC');
			$kategori = $this->db->get();
			return $kategori;
		}
		function Sub2KategoriById($id){
			$kategori = $this->db->order_by('id_sub2_kategori', 'ASC')->get_where('sub2_kategori',['id_sub2_kategori' => $id]);
			return $kategori;
		}
		function getSub1Kategori(){
	        $this->db->from('sub1_kategori');
	        $this->db->join('kategori', 'kategori.id_kategori = sub1_kategori.id_kategori');
	        $this->db->order_by('id_sub1_kategori', 'ASC');

	        $sub1_kategori = $this->db->get();
			$sub1_kategori = $sub1_kategori->result();
			foreach ($sub1_kategori as $q_sub1) {
				$sub2kategori = $this->db->get_where('sub2_kategori', ['id_sub1_kategori'=>$q_sub1->id_sub1_kategori]);
				$q_sub1->sub2_kategori = $sub2kategori->result();
			}
			return $sub1_kategori;
		}
		function getSub2Kategori(){
			$sub1_kategori = $this->db->order_by('id_sub2_kategori', 'ASC')->get('sub2_kategori')->result();
			return $sub1_kategori;
		}
		function insert(){
			$kategori = $this->input->post('kategori');
			$seo = seo($kategori);

			$r = rand(1,99);
            $extension = pathinfo($_FILES['foto_kategori']['name'],PATHINFO_EXTENSION);
			$new_file_name = 'kategori-'.$seo.'-'.$r.".".$extension;
			$config = array(
				'allowed_types' => 'jpg|jpeg|png',
				'upload_path' => $this->foto_kategori_path,
				'file_name' => $new_file_name
				);
			$this->load->library('upload', $config);
			$uploading = $this->upload->do_upload('foto_kategori');

			if (!$uploading) {
				$data = ['kategori' => $kategori,
            			'seo_kategori'=>$seo
            			];
 				$this->db->insert('kategori', $data);
			} else {
				$data = ['kategori' => $kategori,
					 	'icon' => $new_file_name,
            			'seo_kategori'=>$seo
 						];
 				$this->db->insert('kategori', $data);
			}
		}

		function insert_sub1(){
			$kategori = $this->input->post();
			$this->db->insert('sub1_kategori', 
					['id_kategori'=>$kategori['id_kategori'],
					 'sub1_kategori'=>$kategori['sub1_kategori'],
            		 'seo_sub1_kategori'=>seo($kategori['sub1_kategori'])
					]);
		}
		function insert_sub2(){
			$kategori = $this->input->post();
			$this->db->insert('sub2_kategori', 
					['id_sub1_kategori'=>$kategori['id_sub1_kategori'],
					 'sub2_kategori'=>$kategori['sub2_kategori'],
            		 'seo_sub2_kategori'=>seo($kategori['sub2_kategori'])
					]);
		}
		function update($id){
			//ambil data kategori
			$where = ['id_kategori' => $id];
			$ka = $this->db->get_where('kategori',$where);
			$kategori = $ka->row();
			//ambil data dari post
			$nama_kategori = $this->input->post('kategori');
			$seo = seo($nama_kategori);
			$r = rand(1,99);
            $extension = pathinfo($_FILES['foto_kategori']['name'],PATHINFO_EXTENSION);
			$new_file_name = 'kategori-'.$seo.'-'.$r.".".$extension;

			$config = array(
					'allowed_types' => 'jpg|jpeg|png',
					'file_name' =>$new_file_name,
					'upload_path' => $this->foto_kategori_path
			);
			$this->load->library('upload', $config);
			$uploading = $this->upload->do_upload('foto_kategori');

			if(!$uploading){
				$data = ['kategori' => $nama_kategori,
            			 'seo_kategori'=>$seo];
			}else{
				$data_kategori  = $this->upload->data();
				$gambar_kategori = $data_kategori['file_name'];
				//hapus gambar lama
				unlink('images/kategori/'.$kategori->icon);
				$data = ['kategori' => $nama_kategori,
						 'icon' => $new_file_name,
            			 'seo_kategori'=>$seo
						];
			}
			$this->db->update('kategori', $data, $where);
		}

		function updatesub1($id){
			$where = ['id_sub1_kategori' => $id];
			$id_kategori = $this->input->post('id_kategori');
			$nama_sub = $this->input->post('sub1_kategori');
			$data = ['id_kategori' => $id_kategori,
					 'sub1_kategori' => $nama_sub,
            		 'seo_sub1_kategori'=>seo($nama_sub)
					];
			$this->db->update('sub1_kategori', $data, $where);
		}
		
		function updatesub2($id){
			$where = ['id_sub2_kategori' => $id];
			$id_kategori = $this->input->post('id_sub1_kategori');
			$nama_sub = $this->input->post('sub2_kategori');
			$data = ['id_sub1_kategori' => $id_kategori,
					 'sub2_kategori' => $nama_sub,
            		 'seo_sub2_kategori'=>seo($nama_sub)
					];
			$this->db->update('sub2_kategori', $data, $where);
		}
		function delete($id){
			$where = ['id_kategori' => $id];
			$kategori = $this->db->get_where('kategori',$where);
			$kategori = $kategori->row();
			if($kategori->icon != ''){
				unlink('images/kategori/'.$kategori->icon);
			}
			
			$this->db->delete('kategori', $where);			
		}
		function deleteSub1($id){
			$where = ['id_sub1_kategori' => $id];
			$this->db->delete('sub1_kategori', $where);			
		}
		function deleteSub2($id){
			$where = ['id_sub2_kategori' => $id];
			$this->db->delete('sub2_kategori', $where);			
		}
		function CekSub1($id){
			$where = ['id_kategori' => $id];
			$kategori = $this->db->get_where('sub1_kategori',$where);
			$ka = $kategori->num_rows();
			return $ka;		
		}
		function CekSub2($id){
			$where = ['id_sub1_kategori' => $id];
			$kategori = $this->db->get_where('sub2_kategori',$where);
			$ka = $kategori->num_rows();
			return $ka;				
		}
		function getKategoriBySeo($seo){
			return $this->db->get_where('kategori',['seo_kategori'=>$seo])->row();
		}
	}