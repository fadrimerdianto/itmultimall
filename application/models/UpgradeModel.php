<?php
	class UpgradeModel extends CI_Model{
		function getUpgradeByUsername($username){
			return $this->db->get_where('upgrade', ['username'=>$username])->row();
		}
		function upgradeUpload10($username){
			$harga = 50;
			$member = $this->db->get_where('profil_akun', ['username'=>$username])->row();
			$poin = $member->poin_member;
			if($poin >= $harga){
				$new_poin = $poin - $harga;
				$this->db->update('profil_akun', ['poin_member'=>$new_poin], ['username'=>$username]);
				$this->db->update('upgrade', ['upload10'=>1, 'expired_upload10'=>date('Y-m-d', strtotime("+365 days"))], ['username'=>$username]);
				$this->session->set_flashdata('upgrade_sukses', ' ');
			}else{
				$this->session->set_flashdata('poin_kurang', ' ');
			}
			redirect('iklanku/upgrade');
		}
		function upgradeUpload25($username){
			$harga = 75;
			$member = $this->db->get_where('profil_akun', ['username'=>$username])->row();
			$poin = $member->poin_member;
			if($poin >= $harga){
				$new_poin = $poin - $harga;
				$this->db->update('profil_akun', ['poin_member'=>$new_poin], ['username'=>$username]);
				$this->db->update('upgrade', ['upload25'=>1, 'expired_upload25'=>date('Y-m-d', strtotime("+365 days"))], ['username'=>$username]);
				$this->session->set_flashdata('upgrade_sukses', ' ');
			}else{
				$this->session->set_flashdata('poin_kurang', ' ');
			}
			redirect('iklanku/upgrade');

		}
		function upgradeUpload50($username){
			$harga = 100;
			$member = $this->db->get_where('profil_akun', ['username'=>$username])->row();
			$poin = $member->poin_member;
			if($poin >= $harga ){
				$new_poin = $poin - $harga;
				$this->db->update('profil_akun', ['poin_member'=>$new_poin], ['username'=>$username]);
				$this->db->update('upgrade', ['upload50'=>1, 'expired_upload50'=>date('Y-m-d', strtotime("+365 days"))], ['username'=>$username]);
				$this->session->set_flashdata('upgrade_sukses', ' ');
			}else{
				$this->session->set_flashdata('poin_kurang', ' ');
			}
			redirect('iklanku/upgrade');
		}
		function upgradeUploadUnl($username){
			$harga = 150;
			$member = $this->db->get_where('profil_akun', ['username'=>$username])->row();
			$poin = $member->poin_member;
			if($poin >= $harga){
				$new_poin = $poin - $harga;
				$this->db->update('profil_akun', ['poin_member'=>$new_poin], ['username'=>$username]);
				$this->db->update('upgrade', ['uploadunl'=>1, 'expired_uploadunl'=>date('Y-m-d', strtotime("+365 days"))], ['username'=>$username]);
				$this->session->set_flashdata('upgrade_sukses', ' ');
			}else{
				$this->session->set_flashdata('poin_kurang', ' ');
			}
			redirect('iklanku/upgrade');
		}
		function upgradePembaruan($username){
			$harga = 25;
			$member = $this->db->get_where('profil_akun', ['username'=>$username])->row();
			$poin = $member->poin_member;
			if($poin >= $harga){
				$new_poin = $poin - $harga;
				$this->db->update('profil_akun', ['poin_member'=>$new_poin], ['username'=>$username]);
				$this->db->update('upgrade', ['pembaruan'=>1, 'expired_pembaruan'=>date('Y-m-d', strtotime("+365 days"))], ['username'=>$username]);
				$this->session->set_flashdata('upgrade_sukses', ' ');
			}else{
				$this->session->set_flashdata('poin_kurang', ' ');
			}
			redirect('iklanku/upgrade');

		}
		function upgradeDomainSendiri($username){
			$harga = 600;
			$member = $this->db->get_where('profil_akun', ['username'=>$username])->row();
			$poin = $member->poin_member;
			if($poin >= $harga){
				$new_poin = $poin - $harga;
				$this->db->update('profil_akun', ['poin_member'=>$new_poin], ['username'=>$username]);
				$this->db->update('upgrade', ['domainsendiri'=>1, 'expired_domainsendiri'=>date('Y-m-d', strtotime("+365 days"))], ['username'=>$username]);
				$this->session->set_flashdata('upgrade_sukses', ' ');
			}else{
				$this->session->set_flashdata('poin_kurang', ' ');
			}
			redirect('iklanku/upgrade');
		}
		function upgradeMultiKategori($username){
			$harga = 400;
			$member = $this->db->get_where('profil_akun', ['username'=>$username])->row();
			$poin = $member->poin_member;
			if($poin >= $harga){
				$new_poin = $poin - $harga;
				$this->db->update('profil_akun', ['poin_member'=>$new_poin], ['username'=>$username]);
				$this->db->update('upgrade', ['multikategori'=>1, 'expired_multikategori'=>date('Y-m-d', strtotime("+365 days"))], ['username'=>$username]);
				$this->session->set_flashdata('upgrade_sukses', ' ');
			}else{
				$this->session->set_flashdata('poin_kurang', ' ');
			}
			redirect('iklanku/upgrade');
		}
		function getMaksimalIklan($username){
			$count = 25;
			$upgrade = $this->db->get_where('upgrade', ['username'=>$username])->row();
			if($upgrade->upload10 == 1){
				$count += 10;
			}
			if($upgrade->upload25 == 1){
				$count += 25;
			}
			if($upgrade->upload50 == 1){
				$count += 50;
			}
			if($upgrade->uploadunl == 1){
				$count += 999999;
			}
			return $count;
		}
	}