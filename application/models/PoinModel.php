<?php
class PoinModel extends CI_Model{
	var $foto_poin_path;
	function __construct()
	{
		parent::__construct();
		$this->foto_poin_path = realpath(APPPATH .'../images/poin/');
	}
	function getAll(){
		return $this->db->query('SELECT * FROM poin ORDER BY harga_poin ASC')->result();
	}
	function getPoinById($id){
		return $this->db->get_where('poin', ['id_poin'=>$id])->row();
	}
	function insert(){
		$namaPoin = $this->input->post('poin');
		$jumlahPoin = $this->input->post('jumlah_poin');
		$hargaPoin = $this->input->post('harga');

		$config = array(
			'allowed_types' => 'jpg|jpeg|png',
			'upload_path' => $this->foto_poin_path
			);
		$this->load->library('upload', $config);
		$uploading = $this->upload->do_upload('gambar_poin');

		$data_poin  = $this->upload->data();
		$gambar_poin = $data_poin['file_name'];

		if(!$uploading){
			$this->session->set_flashdata('message_error_upload', $this->upload->display_errors());
			redirect('aksa_admin/poin/tambah');
			var_dump($this->upload->display_errors());
		}else{
			$data = ['nama_poin' => $namaPoin,
			'jumlah_poin' => $jumlahPoin,
			'harga_poin' => $hargaPoin,
			'gambar_poin' => $gambar_poin
			];
			$this->db->insert('poin', $data);
		}
	}
	function update($id){
		//ambil data poin
		$poin = $this->db->query("SELECT gambar_poin FROM poin WHERE id_poin = '$id'");
		$poin = $poin->row();

		$namaPoin = $this->input->post('poin');
		$jumlahPoin = $this->input->post('jumlah_poin');
		$hargaPoin = $this->input->post('harga');

		$config = array(
			'allowed_types' => 'jpg|jpeg|png',
			'upload_path' => $this->foto_poin_path
			);
		$this->load->library('upload', $config);
		$uploading = $this->upload->do_upload('gambar_poin');

		if(!$uploading){
			$data = ['nama_poin' => $namaPoin,
			'jumlah_poin' =>$jumlahPoin,
			'harga_poin' => $hargaPoin
			];
		}else{
			$data_poin  = $this->upload->data();
			$gambar_poin = $data_poin['file_name'];
				//hapus gambar lama
			unlink('images/poin/'.$poin->gambar_poin);
			$data = ['nama_poin' => $namaPoin,
			'jumlah_poin' =>$jumlahPoin,
			'harga_poin' => $hargaPoin,
			'gambar_poin' => $gambar_poin
			];
		}
		$this->db->update('poin', $data, ['id_poin' => $id]);	
	}
	function delete($id){
		$poin = $this->db->query("SELECT gambar_poin FROM poin WHERE id_poin = '$id'");
		$poin = $poin->row();
		$where = ['id_poin' => $id];
		if($poin->gambar_poin != ''){
			unlink('images/poin/'.$poin->gambar_poin);
		}
		$this->db->delete('poin', $where);
	}

}