<?php
	class ApiModel extends CI_Model{
		
		function get_merk($post){
			$data_merk=$this->db->get_where('tipe_mobil',['id_merk'=>$post['id_merk']])->result();
			$rows=array();
			foreach ($data_merk as $data) 
			{
				$rows[]=$data;
			}
			
			return json_encode($rows);
		}

		function register($register){
			$cek_data_username = $this->db->get_where('member_akun', ['username'=>$register['username']]);
			$cek_data_email = $this->db->get_where('member_akun', ['email'=>$register['email']]);

			if($cek_data_email->num_rows() > 0){
				return "Email Sudah Terdaftar";
			}elseif ($cek_data_username->num_rows() > 0) {
				return "Username Sudah Terdaftar";
			}else{

				$data_register = [
					'username' => $register['username'],
					'email' => $register['email'],
					'password' => md5($register['password']),
					'status'=>0,
					'token'=>md5($register['email']).uniqid()
				];
				$this->db->insert('member_akun', $data_register);
				$data_profil = [
					'username'=>$register['username'],
					'provinsi_member'=>0,
					'kota_member'=>0,
					'nama_member'=>'',
					'telepon_member'=>'',
					'bbm_member'=>'',
					'wa_member'=>''
				];

				$this->db->insert('profil_akun', $data_profil);
				$data_upgrade = [
					'username'=>$register['username'],
					'upload10'=>0,
					'upload25'=>0,
					'upload50'=>0,
					'uploadunl'=>0,
					'pembaruan'=>0,
					'domainsendiri'=>0,
					'multikategori'=>0
				];
				$this->db->insert('upgrade', $data_upgrade);
			}

			$member = $this->getProfil($register['username']);
			if($member){
				$this->load->library('My_PHPMailer');
				$datamember = $member;
				$mail = new My_PHPMailer();
			    $toEmail = $member->email;
			    $toName = $member->username;
			    $subject = 'Konfirmasi Pendaftaran';
			    $link_konfirmasi = base_url().'main/konfirmasi/'.$member->token;
			    //$mail->email_view();
			    $kirim = $mail->send_email($toEmail, $toName, $subject, $mail->template_konfirmasi('wikiloka.com', $toName, base_url(), $link_konfirmasi));
				return "Success";
			}	
		}
		function like($form){
			$iklan = $this->db->get_where('iklan', ['id_iklan'=>$form['id_iklan']])->row();
	        $this->db->update('iklan', ['like'=>$iklan->like+1], ['id_iklan'=>$form['id_iklan']]);
	        return $iklan->like+1;
		}
		function unlike($form){
			$iklan = $this->db->get_where('iklan', ['id_iklan'=>$form['id_iklan']])->row();
	        $this->db->update('iklan', ['unlike'=>$iklan->unlike+1], ['id_iklan'=>$form['id_iklan']]);
	        return $iklan->unlike+1;
		}

		
		function laporkan($form){
			$data = [
				'nama_pelapor'=>$form['nama'],
				'email_pelapor'=>$form['email'],
				'pesan'=>$form['pesan']
			];

			$this->db->insert('laporan_penjual', $data);
			return "Success";
		}
		function detail_iklan($seo) {
			$b=$seo['seo_iklan'];
			$dilihat = $this->db->query("SELECT dilihat FROM iklan WHERE seo_iklan = '$b'")->row();
	        $dilihat = $dilihat->dilihat;
	        $this->db->update('iklan', ['dilihat'=>$dilihat+1], ['seo_iklan'=>$b]);

			$this->db->select('*');
            $this->db->from('iklan'); 
            $this->db->join('profil_akun', 'iklan.owner=profil_akun.username', 'left');
            $get=$this->db->where('iklan.seo_iklan',$seo['seo_iklan'])->get();


        
     	return json_encode(array($get->row()));
    	}

    	function detail_foto_iklan($seo) {
			$this->db->select('*');
            $this->db->from('iklan');
            $get=$this->db->where('iklan.seo_iklan',$seo['seo_iklan'])->get();
			
			$cek_baris  = $this->db->get_where('iklan_photo',['id_iklan'=>$get->row()->id_iklan]);
			if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$data->photo=base_url()."images/iklan/".$data->photo;
					$rows[]=$data;
				}
				
				return json_encode($rows);
			}
    	}
		function getProfil($username){
			$profil = $this->db->query("SELECT * FROM member_akun JOIN profil_akun ON member_akun.username = profil_akun.username order by profil_akun.id_profil desc");
			return $profil->first_row();
		}

		function txt_prov($form){
			$data = $this->db->get_where('provinsi', ['id_provinsi'=>$form['id_prov']]);
			$hasil = $data->row();
			return $hasil->nama_provinsi;
		}

		function txt_kota($form){
			$data = $this->db->get_where('kota', ['id_kota'=>$form['id_kota']]);
			$hasil = $data->row();
			return $hasil->nama_area;
		}

		function daftarberbayar($transaksi){
			//var_dump($transaksi);
			$iklan = $this->db->get_where('iklan', ['seo_iklan'=>$transaksi['seo_iklan']]);
			$iklan = $iklan->row();

			$premium = $this->db->get_where('premium_iklan', ['id_premium'=>$transaksi['pilihpremium']]);
			$premium = $premium->row();

			$member = $this->db->get('profil_akun', ['username'=>$transaksi['username']]);
			$member = $member->row();


			$data = [
				'id_premium_iklan' => $transaksi['pilihpremium'],
				'id_iklan'		=> $iklan->id_iklan,
				'tanggal_transaksi' => date('Y-m-d H:i:s')
			];

			$this->db->insert('transaksi_iklan_premium', $data);
			$this->db->update('iklan', ['id_premium'=>$transaksi['pilihpremium'], 'tanggal_aktif_premium'=>date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'))], ['seo_iklan'=>$transaksi['seo_iklan']]);
			$this->db->update('profil_akun', ['poin_member'=>($member->poin_member - $premium->harga_premium)], ['username'=>$transaksi['username']]);
			return "success";
		}

		function beli_poin($form){
			$this->load->library('My_PHPMailer');
		
			$kode = rand(0, 4000);

			$poin = $this->db->get_where('poin', ['id_poin'=>$form['id_poin']]);
			$poin = $poin->row();

			$data = [
				'id_poin' => $form['id_poin'],
				'username'=> $form['username'],
				'tanggal_transaksi'=>date('Y-m-d H:i:s'),
				'isKonfirmasi'=> 0,
				'biaya_transaksi'=>$poin->harga_poin+$kode
			];

			//var_dump(uniqid());
			$this->db->insert('transaksi_poin', $data);

			$transaksipoin = $this->db->query("SELECT * FROM transaksi_poin JOIN poin ON transaksi_poin.id_poin = poin.id_poin ORDER BY id_transaksi_poin DESC");
			

			$member = $this->db->get_where('member_akun', ['username'=>$form['username']]);
			$member = $member->row();

			$mail = new My_PHPMailer();
			$toEmail = $member->email;
			$datatransaksi['biaya_transaksi'] = $poin->harga_poin+$kode;
			$datatransaksi['paket_poin'] =$poin->nama_poin;
		    $toName = $form['username'];
		    $subject = 'Invoice Pembelian Poin';
		    
		    $kirim = $mail->send_email($toEmail, $toName, $subject, $mail->template_invoice('wikiloka.com', $toName, base_url(), $datatransaksi));
		    return "Success";

		}
		function get_poin(){
			$cek_baris= $this->db->query('SELECT * FROM poin ORDER BY harga_poin ASC');
			$rows=array();
			foreach ($cek_baris->result() as $data) 
			{
				$data->gambar_poin=base_url()."images/poin/".$data->gambar_poin;
				$rows[]=$data;
			}
			return json_encode($rows);
		}
		function getWebsiteByUsername($username){
			$this->db->select('*');
			$this->db->from('website_replika');
			$this->db->join('slider_replika', 'website_replika.id_website = slider_replika.id_website');
			$this->db->where(['username'=>$username]);
			$query = $this->db->get();
			return $query->row();
			//return $this->db->get_where('website_replika', ['username'=>$username])->row();
		}
		function get_premium($form){
			$cek_baris=$this->db->order_by('id_premium', 'DESC')->get('premium_iklan');
			$web_replika=$this->getWebsiteByUsername($form['username']);
			$rows=array();
			foreach ($cek_baris->result() as $data) 
			{
				if($data->id_premium == 4){
					if($web_replika == null){
						$data->status_web="tidakada";
					}else{
						$data->status_web="ada";		
					}
					
				}else{
					$data->status_web="ada";
				}
				$data->gambar_premium=base_url()."images/premium/".$data->gambar_premium;
				$rows[]=$data;
			}
			return json_encode($rows);
		}
		function beli_premium($form){
			$iklan = $this->db->get_where('iklan', ['seo_iklan'=>$form['seo_iklan']]);
			$iklan = $iklan->row();

			$premium = $this->db->get_where('premium_iklan', ['id_premium'=>$form['id_premium']]);
			$premium = $premium->row();

			$detail_premium = $this->db->get_where('detail_premium', ['id_iklan'=>$iklan->id_iklan])->row();

			$member = $this->db->get('profil_akun', ['username'=>$form['username']]);
			$member = $member->row();

			$top25 = 0;
			$recommended = 0;
			$terlaris = 0;
			$topwebsite = 0;

			$aktif_top25 = $detail_premium->tanggal_aktif_top25;
			$aktif_recommended = $detail_premium->tanggal_aktif_recommended;
			$aktif_terlaris = $detail_premium->tanggal_aktif_terlaris;
			$aktif_topwebsite = $detail_premium->tanggal_aktif_topwebsite;

			if($form['id_premium'] == 1){
				$top25 = 1;
				$aktif_top25 = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
			}
			if($form['id_premium'] == 2){
				$recommended = 1;
				$aktif_recommended = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
			}
			if($form['id_premium'] == 3){
				$terlaris = 1;
				$aktif_terlaris = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
			}
			if($form['id_premium'] == 4){
				$topwebsite = 1;
				$aktif_topwebsite = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
			}

			$data = [
				'id_premium_iklan' =>$form['id_premium'],
				'id_iklan'		=> $iklan->id_iklan,
				'tanggal_transaksi' => date('Y-m-d H:i:s')
			];

			$this->db->insert('transaksi_iklan_premium', $data);


			$data_premium = [
				'top25'=>$top25,
				'tanggal_aktif_top25'=>$aktif_top25,
				'recommended'=>$recommended,
				'tanggal_aktif_recommended'=>$aktif_recommended,
				'terlaris'=>$terlaris,
				'tanggal_aktif_terlaris'=>$aktif_terlaris,
				'topwebsite'=>$topwebsite,
				'tanggal_aktif_topwebsite'=>$aktif_topwebsite
			];

			$this->db->update('detail_premium', $data_premium, ['id_iklan'=>$iklan->id_iklan]);
			$this->db->update('profil_akun', ['poin_member'=>($member->poin_member - $premium->harga_premium)], ['username'=>$form['username']]);
			return "success";
		}

		function daftar_premium($form)
		{
			$transaksi = $form;
			//var_dump($transaksi);
			$premium = $this->db->get_where('premium_iklan', ['id_premium'=>$transaksi['id_premium']]);
			$premium = $premium->row();
			$member = $this->db->get_where('profil_akun', ['username'=>$transaksi['username']]);
			$member = $member->row();
			$totalbiaya = $premium->harga_premium*count($transaksi['seo_iklan']);
			
			//var_dump($member->poin_member - ($totalbiaya) < 0);
			if($member->poin_member - ($totalbiaya) < 0){
				return 'poin_kurang';
				//echo $premium->harga_premium*sizeof($transaksi['iklan']);
			}else{
				foreach ($transaksi['seo_iklan'] as $q_iklan) {
					$iklan = $this->db->get_where('iklan', ['seo_iklan'=>$q_iklan])->row();
					$detail_premium = $this->db->get_where('detail_premium', ['id_iklan'=>$iklan->id_iklan])->row();

					// $member = $this->db->get('profil_akun', ['username'=>$iklan->owner]);
					// $member = $member->row();

					$top25 = 0;
					$recommended = 0;
					$terlaris = 0;
					$topwebsite = 0;

					$aktif_top25 = $detail_premium->tanggal_aktif_top25;
					$aktif_recommended = $detail_premium->tanggal_aktif_recommended;
					$aktif_terlaris = $detail_premium->tanggal_aktif_terlaris;
					$aktif_topwebsite = $detail_premium->tanggal_aktif_topwebsite;

					if($transaksi['id_premium'] == 1){
						$top25 = 1;
						$aktif_top25 = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
					}
					if($transaksi['id_premium'] == 2){
						$recommended = 1;
						$aktif_recommended = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
					}
					if($transaksi['id_premium'] == 3){
						$terlaris = 1;
						$aktif_terlaris = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
					}
					if($transaksi['id_premium'] == 4){
						$topwebsite = 1;
						$aktif_topwebsite = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
					}

					$data = [
					'id_premium_iklan' => $transaksi['id_premium'],
					'id_iklan'		=> $iklan->id_iklan,
					'tanggal_transaksi' => date('Y-m-d H:i:s')
					];

					$this->db->insert('transaksi_iklan_premium', $data);

					$data_premium = [
					'id_iklan'=>$iklan->id_iklan,
					'top25'=>$top25,
					'tanggal_aktif_top25'=>$aktif_top25,
					'recommended'=>$recommended,
					'tanggal_aktif_recommended'=>$aktif_recommended,
					'terlaris'=>$terlaris,
					'tanggal_aktif_terlaris'=>$aktif_terlaris,
					'topwebsite'=>$topwebsite,
					'tanggal_aktif_topwebsite'=>$aktif_topwebsite
					];

					$this->db->update('detail_premium', $data_premium, ['id_iklan'=>$iklan->id_iklan]);
				}
				
				$sisa = $member->poin_member-$totalbiaya;
				$this->db->update('profil_akun', ['poin_member'=>$sisa], ['username'=>$transaksi['username']]);
				return 'success';
			}

			
		}
		function login($login){
			$cek_login  = $this->db->get_where('member_akun', ['username'=>$login['username'], 'password'=>md5($login['password'])]);
			if($cek_login->num_rows() == 1){
				$data[0]=$cek_login->row();
				$data[1]=$this->db->get_where('profil_akun', ['username'=>$login['username']])->row();
				if($cek_login->row()->status=="0"){
					return "Akun ini belum di konfirmasi";
				}
				else
				{
					return json_encode($data);
				}
				
			}else{
				return "Username atau Password tidak terdaftar";
			}
		}
		function ubahaktifkelaku($form){
        	$this->db->update('iklan', ['status'=>3], ['id_iklan'=>$form['id']]);
        	return "Success";
    	}
		function pasangiklan($iklan){

			$iklan = $this->input->post();

	        if(isset($iklan['kategori'])){
	            $kategori = $iklan['kategori'];
	        }else{
	            $kategori = '0';
	        }

	        if(isset($iklan['sub1_kategori'])){
	            $sub1_kategori = $iklan['sub1_kategori'];
	        }else{
	            $sub1_kategori = '0';
	        }

	        if(isset($iklan['sub2_kategori'])){
	            $sub2_kategori = $iklan['sub2_kategori'];
	        }else{
	            $sub2_kategori = '0';
	        }


	        if(isset($iklan['hide_email'])){
	            $hide_email = 1;
	        }else{
	            $hide_email = 0;
	        }
	        if(isset($iklan['hide_bbm'])){
	            $hide_bbm = 1;
	        }else{
	            $hide_bbm = 0;
	        }

	        if(isset($iklan['wa_available'])){
	            $wa_available = 1;
	        }else{
	            $wa_available = 0;
	        }

			$dataIklan = [
				'judul_iklan'=>$iklan['judul_iklan'],
				'deskripsi_iklan'=>$iklan['deskripsi_iklan'],
				'harga_iklan'=>$iklan['harga_iklan'],
				'web_iklan'=>$iklan['web'],
				'owner'=>$iklan['username'],
				'status'=>0,
	            'seo_iklan'=>seo($iklan['judul_iklan']).'-'.uniqid(),
	            'kategori'=>$kategori,
	            'sub1_kategori'=>$sub1_kategori,
	            'sub2_kategori'=>$sub2_kategori,
	            'kota'=>$iklan['kota'],
            	'tanggal_post'=>date('Y-m-d H:i:s'),
		        'hide_email'=>$hide_email,
		        'hide_bbm'=>$hide_bbm,
		        'wa_available'=>$wa_available,
            	//1 baru,2 bekas
            	'jenis_iklan'=>$iklan['jenis_iklan']
			];

			$this->db->insert('iklan', $dataIklan);
			$id_iklan = $this->db->insert_id();
			$this->db->insert('detail_premium', ['id_iklan'=>$id_iklan, 'top25'=>0, 'tanggal_aktif_top25'=>'', 'topwebsite'=>0, 'tanggal_aktif_topwebsite'=>'','terlaris'=>0,'tanggal_aktif_terlaris'=>'', 'recommended'=>0,'tanggal_aktif_recommended'=>'']);

			if($iklan['sub1_kategori'] == 8){
	            $dataFilter = [
	                'id_iklan'=>$id_iklan,
	                'id_sub2_kategori'=>$sub2_kategori,
	                'transmisi'=> $iklan['transmisi'],
	                'tipe'=>$iklan['tipe'],
	                'tahun'=>$iklan['tahun']
	            ];

	            $this->db->insert('iklan_merk_mobil', $dataFilter);
	        }
	        elseif ($iklan['id_sub1_kategori'] == 17) {
	             $dataFilter = [
	                'id_iklan'=>$id_iklan,
	                'luas_tanah'=> $iklan['luas_tanah'],
	                'luas_bangunan'=> $iklan['luas_bangunan'],
	                'lantai'=> $iklan['lantai'],
	                'kamar_tidur'=> $iklan['kamar_tidur'],
	                'kamar_mandi'=> $iklan['kamar_mandi'],
	                'sertifikasi'=> $iklan['sertifikasi'],
	                'alamat_lokasi'=> $iklan['alamat_lokasi']
	            ];
	            $this->db->insert('iklan_rumah_properti', $dataFilter);
	            $id_iklan_rumah_properti = $this->db->insert_id();

	            if(isset($iklan['ac'])){
	                $ac = 1;
	            }else{
	                $ac = 0;
	            }

	            if(isset($iklan['swimming_pool'])){
	                $swimming_pool = 1;
	            }else{
	                $swimming_pool = 0;
	            }

	            if(isset($iklan['carport'])){
	                $carport = 1;
	            }else{
	                $carport = 0;
	            }
	            if(isset($iklan['garden'])){
	                $garden = 1;
	            }else{
	                $garden = 0;
	            }
	            if(isset($iklan['garasi'])){
	                $garasi = 1;
	            }else{
	                $garasi = 0;
	            }
	            if(isset($iklan['telephone'])){
	                $telephone = 1;
	            }else{
	                $telephone = 0;
	            }
	            if(isset($iklan['pam'])){
	                $pam = 1;
	            }else{
	                $pam = 0;
	            }
	            if(isset($iklan['water_heater'])){
	                $water_heater = 1;
	            }else{
	                $water_heater = 0;
	            }
	            if(isset($iklan['refrigerator'])){
	                $refrigerator = 1;
	            }else{
	                $refrigerator = 0;
	            }
	            if(isset($iklan['stove'])){
	                $stove = 1;
	            }else{
	                $stove = 0;
	            }
	            if(isset($iklan['microwave'])){
	                $microwave = 1;
	            }else{
	                $microwave = 0;
	            }
	            if(isset($iklan['oven'])){
	                $oven = 1;
	            }else{
	                $oven = 0;
	            }
	            if(isset($iklan['fire_extenguisher'])){
	                $fire_extenguisher = 1;
	            }else{
	                $fire_extenguisher = 0;
	            }
	            if(isset($iklan['gordyn'])){
	                $gordyn = 1;
	            }else{
	                $gordyn = 0;
	            }
	            $dataFasilitas = [
	                'id_iklan_rumah'=>$id_iklan_rumah_properti,
	                'ac'=>$ac,
	                'swimming_pool'=>$swimming_pool,
	                'carport'=>$carport,
	                'garden'=>$garden,
	                'garasi'=>$garasi,
	                'telephone'=>$telephone,
	                'pam'=>$pam,
	                'water_heater'=>$water_heater,
	                'refrigerator'=>$refrigerator,
	                'stove'=>$stove,
	                'microwave'=>$microwave,
	                'oven'=>$oven,
	                'fire_extenguisher'=>$fire_extenguisher,
	                'gordyn'=>$gordyn
	            ];
	            $this->db->insert('fasilitas_rumah', $dataFasilitas);
	        }

			$iklan1 = $this->db->query("SELECT * FROM iklan ORDER BY id_iklan DESC");
			$iklan2 = $iklan1->row();
			
			$i=0;
			$pesan_err="";
			foreach ($_FILES['userfile']['name'] as $key => $name) {
			 	$extension = pathinfo($_FILES['userfile']['name'][$i],PATHINFO_EXTENSION);
                //random token
                $token = rand(0, 99999);
               
            	$new_file_name = $iklan['username'].'-iklan-'.$iklan2->id_iklan.'-'.$token.'.'.$extension;
            	$data_photo_iklan = [
                	'id_iklan'=>$iklan2->id_iklan,
                	'photo'=> $new_file_name
                ];
				if(move_uploaded_file($_FILES['userfile']['tmp_name'][$i], "images/iklan/".$new_file_name)){
					$this->db->insert('iklan_photo', $data_photo_iklan);
				}else{
					return "gagal upload";
				}
	            $i++;
			}
			return $pesan_err;
		}


		// function getIklanByFilter($kategori, $jenis_iklan, $subkategori, $kota, $filter){
		function iklan_by_filter($get){
			if($get['jenis']=='semua'){
				$where = ['status'=>1];
			}else if($get['jenis']=='baru'){
				$where = ['status'=>1];
				$where = array_merge($where, ['jenis_iklan'=>1]);
			}else if($get['jenis']=='bekas'){
				$where = ['status'=>1];
				$where = array_merge($where, ['jenis_iklan'=>2]);
			}
			$this->load->model('KategoriModel');
			$this->load->model('SubKategoriModel');
	        if($get['kategori']!= 'semua'){
	            $dataKategori = $this->KategoriModel->getKategoriBySeo($get['kategori']);
	            $where = array_merge($where, ['kategori'=>$dataKategori->id_kategori]);
	        }
	        if($get['sub1_kategori'] != 'semua'){
	            $dataSubKategori = $this->SubKategoriModel->getSubKategoriBySeo($get['sub1_kategori']);
	            $where = array_merge($where, ['sub1_kategori'=>$dataSubKategori->id_sub1_kategori]);
	        }
	        if($get['sub2_kategori'] != 'semua'){
	            $dataSubKategori = $this->SubKategoriModel->getSubKategoriBySeo2($get['sub2_kategori']);
	            $where = array_merge($where, ['sub2_kategori'=>$dataSubKategori->id_sub2_kategori]);
	        }
	        if($get['kota']  != 'semua'){
	            $dataKota = $this->db->get_where('kota', ['nama_area'=>$get['kota']])->row();
	            $where = array_merge($where, ['kota'=>$dataKota->id_kota]);
	        }

	        //variabel filter
	        if(isset($get['filter'])){
	            $dataSub2Kategori = $this->db->get_where('sub2_kategori', ['seo_sub2_kategori'=>$get['filter']])->row();
	            $where = array_merge($where, ['sub2_kategori'=>$dataSub2Kategori->id_sub2_kategori]);
	        }
	        if(isset($get['keyword'])){
	            $where = array_merge($where, ['judul_iklan'=>$get['keyword']]);
	        }
	        if(isset($get['sort'])){
	            switch ($get['sort']) {
	            	case 'asc_juduliklan':
	            		$this->db->order_by('judul_iklan', 'asc');
	            		break;
	            	case 'desc_juduliklan':
	            		$this->db->order_by('judul_iklan', 'desc');
	            		break;
	                case 'termurah':
	                    $this->db->order_by('harga_iklan', 'asc');
	                    break;
	                case 'terbaru':
	                    $this->db->order_by('tanggal_post', 'desc');
	                    break;
	                default:
	                    break;
	            }
	        } else{
	            $this->db->order_by('tanggal_post', 'desc');
	        }  

	        if(isset($get['premium'])){
	            if($get['premium'] == 'top-25'){
	                //$this->db->where(['id_premium'=>1]);
	                $this->db->where('tanggal_aktif_top25 >', date('Y-m-d'));
	            }elseif($get['premium'] == 'top-shop'){
	                //$this->db->where(['id_premium'=>2]);
	                $this->db->where('tanggal_aktif_premium >', date('Y-m-d'));
	            }elseif($get['premium'] == 'recommended'){
	                //$this->db->where(['id_premium'=>3]);
	                $this->db->where('tanggal_aktif_recommended >', date('Y-m-d'));
	            }elseif ($get['premium'] == 'terlaris') {
	                //$this->db->where(['id_premium'=>4]);
	                $this->db->where('tanggal_aktif_terlaris >', date('Y-m-d'));

	            }
	        }
	         $now = date('Y-m-d');
	       	$this->db->where("DATEDIFF('$now', tanggal_post) <", 60);
			$this->db->like($where);
        	$this->db->join('detail_premium', 'detail_premium.id_iklan = iklan.id_iklan');
	        $iklan = $this->db->get('iklan',$get['limit1'],$get['limit2'])->result();
	        $rows=array();
	        foreach ($iklan as $q_iklan) {
	        	if(isset($get['username'])){
	        		$username=$get['username'];
	        	}
	        	else{
	        		$username="";
	        	}
	            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$username]);
	            $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
	            $q_iklan->kota = $kota->row();
	            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
	            $q_iklan->provinsi = $provinsi->row();
	            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
	            $q_iklan->kategori = $kategori->row();
	            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
	            $q_iklan->sub1 = $sub1->row();
	           // $favorit = $favorit->row();
	            if($favorit->num_rows() > 0 ){
	                $q_iklan->status = 1;
	            }else{
	                $q_iklan->status = 0;
	            }
	           	$photo_iklan= $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$q_iklan->id_iklan'")->first_row();
	           	$q_iklan->photo=base_url().'images/iklan/'.$photo_iklan->photo;
	            $rows[]=$q_iklan;

	        }
	        return json_encode($rows);
		}
		function search($get){
			if($get['jenis']=='semua'){
				$where = ['status'=>1];
			}else if($get['jenis']=='baru'){
				$where = ['status'=>1];
				$where = array_merge($where, ['jenis_iklan'=>1]);
			}else if($get['jenis']=='bekas'){
				$where = ['status'=>1];
				$where = array_merge($where, ['jenis_iklan'=>2]);
			}

	        if($get['word'] != ''){
	            $where = array_merge($where, ['judul_iklan'=>$get['word']]);
	        }
	        if($get['kota'] != ''){
	            $where = array_merge($where, ['kota'=>$get['kota']]);
	        }
	        $kat = $this->db->get_where('kategori', ['seo_kategori'=>$get['seo_kategori']]);
	        if($kat->num_rows() > 0){
	            $where = array_merge($where, ['kategori'=>$kat->row()->id_kategori]);
	        }
	        $this->db->like($where);
	        $iklan = $this->db->get('iklan')->result();
	        $rows=array();
	        foreach ($iklan as $q_iklan) {
	        	if(isset($get['username'])){
	        		$username=$get['username'];
	        	}
	        	else{
	        		$username="";
	        	}
	            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$username]);
	            $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
	            $q_iklan->kota = $kota->row();
	            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
	            $q_iklan->provinsi = $provinsi->row();
	            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
	            $q_iklan->kategori = $kategori->row();
	            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
	            $q_iklan->sub1 = $sub1->row();
	           // $favorit = $favorit->row();
	            if($favorit->num_rows() > 0 ){
	                $q_iklan->status = 1;
	            }else{
	                $q_iklan->status = 0;
	            }
	           	$photo_iklan= $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$q_iklan->id_iklan'")->first_row();
	           	$q_iklan->photo=base_url().'images/iklan/'.$photo_iklan->photo;
	            $rows[]=$q_iklan;

	        }
	        return json_encode($rows);
	    }
	    function all_iklan ($jenis=null) {
	    	if($jenis==null){
	    		$cek_baris = $this->db->order_by('id_iklan', 'desc')->get_where('iklan', ['status'=>'1']);
				if($cek_baris->num_rows() >= 1){
					$rows=array();
					foreach ($cek_baris->result() as $data) 
					{
						
						$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();
						//$rows['photo']=base_url().'images/iklan/'.$get->photo;
						$data->photo=base_url().'images/iklan/'.$get->photo;
						$rows[]=$data;
					}
					return json_encode($rows);
				}
	    	}elseif ($jenis=="baru") {
	    		$cek_baris = $this->db->order_by('id_iklan', 'desc')->get_where('iklan', ['status'=>'1','jenis_iklan'=>'1']);
				if($cek_baris->num_rows() >= 1){
					$rows=array();
					foreach ($cek_baris->result() as $data) 
					{
						
						$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();
						//$rows['photo']=base_url().'images/iklan/'.$get->photo;
						$data->photo=base_url().'images/iklan/'.$get->photo;
						$rows[]=$data;
					}
					return json_encode($rows);
				}
	    	}elseif ($jenis=="bekas") {
	    		$cek_baris = $this->db->order_by('id_iklan', 'desc')->get_where('iklan', ['status'=>'1','jenis_iklan'=>'2']);
				if($cek_baris->num_rows() >= 1){
					$rows=array();
					foreach ($cek_baris->result() as $data) 
					{
						
						$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();
						//$rows['photo']=base_url().'images/iklan/'.$get->photo;
						$data->photo=base_url().'images/iklan/'.$get->photo;
						$rows[]=$data;
					}
					return json_encode($rows);
				}
	    	}
	    }


		function all_iklan_nopremium($form){
	        $now = date('Y-m-d');
	        $this->db->where("DATEDIFF('$now', tanggal_post) <", 60);
	        if($form['no_premium'] == 1){
	            $cek_baris = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_top25 < '$now' AND status='1' AND owner='".$form['username']."'");
	        }elseif ($form['no_premium'] == 2) {
	            $cek_baris = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_top25 < '$now' AND status='1' AND owner='".$form['username']."'");
	        }elseif ($form['no_premium'] == 3) {
	            $cek_baris = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_terlaris < '$now' AND status='1' AND owner='".$form['username']."'");
	        }elseif ($form['no_premium'] == 4) {
	            $cek_baris = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_topwebsite < '$now' AND status='1' AND owner='".$form['username']."'");
	        }
	        //$iklan = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE (tanggal_aktif_top25 > '$now' OR tanggal_aktif_recommended > '$now' OR tanggal_aktif_terlaris > '$now') AND iklan.owner = '$username'");
	        /*$iklan = $iklan->result();
	        foreach ($iklan as $q_iklan) {
	            $photo = $this->db->get_where('iklan_photo', ['id_iklan'=>$q_iklan->id_iklan]);
	            $q_iklan->gambar = $photo->result();
	        }
	        return $iklan;*/
	        if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();

					$data->photo=base_url().'images/iklan/'.$get->photo;
					$data->tanggal_post=date("d-m-Y h:i", strtotime(str_replace('-','/',$data->tanggal_post)));
					$rows[]=$data;
				}
				return json_encode($rows);
			}
    	}

	    function all_iklanaktif_by_username($post){
	    	$now = date('Y-m-d');
	    	$this->db->where("DATEDIFF('$now', tanggal_post) <", 60);
	    	$cek_baris = $this->db->get_where('iklan', ['status'=>1,'owner'=>$post['username']]);
			if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();

					$data->photo=base_url().'images/iklan/'.$get->photo;
					$data->tanggal_post=date("d-m-Y h:i", strtotime(str_replace('-','/',$data->tanggal_post)));
					$ambil_kota=$this->db->query("SELECT * FROM kota WHERE id_kota=".$data->kota)->first_row();
					$data->kota=$ambil_kota->nama_area;
					$rows[]=$data;
				}
				return json_encode($rows);
			}
	    }

	    function all_iklanditolak_by_username($post){
	    	$now = date('Y-m-d');
	    	$cek_baris = $this->db->order_by('id_iklan', 'desc')->get_where('iklan', ['status'=>'2','owner'=>$post['username']]);
			if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();

					$data->photo=base_url().'images/iklan/'.$get->photo;
					$data->tanggal_post=date("d-m-Y h:i", strtotime(str_replace('-','/',$data->tanggal_post)));
					$rows[]=$data;
				}
				return json_encode($rows);
			}
	    }

	    function all_iklanpremium_by_username($post){
	    	$now = date('Y-m-d');
	    	$username=$post['username'];
	    	$cek_baris = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE (tanggal_aktif_top25 > '$now' OR tanggal_aktif_recommended > '$now' OR tanggal_aktif_terlaris > '$now') AND iklan.owner = '$username'");
			if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();

					$data->photo=base_url().'images/iklan/'.$get->photo;
					$data->tanggal_post=date("d-m-Y h:i", strtotime(str_replace('-','/',$data->tanggal_post)));
					$rows[]=$data;
				}
				return json_encode($rows);
			}
	    }

	    function all_iklanlaku_by_username($post){
	    	$cek_baris = $this->db->query("SELECT * FROM iklan WHERE status = 3 AND owner = '".$post['username']."'");
    		if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();
					//$rows['photo']=base_url().'images/iklan/'.$get->photo;
					$data->photo=base_url().'images/iklan/'.$get->photo;
					$rows[]=$data;
				}
				return json_encode($rows);
			}
    	}

	    function all_iklantidakaktif_by_username($post){
	    	 $now = date('Y-m-d');
	    	$cek_baris = $this->db->query("SELECT * FROM iklan WHERE  owner = '".$post['username']."' AND (DATEDIFF('$now', tanggal_post) > 60 AND status = 1) OR (status = 0 OR status = 2)");

	    	if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$dif_time = strtotime(date('Y-m-d H:i:s')) - strtotime($data->tanggal_post);
					$days = (floor($dif_time / (60*60*24)));
					if($data->status == 0){
						$data->status_label="Validasi";
					}elseif ($data->status == 2) {
						$data->status_label="Ditolak";
					}elseif($days > 60){
						$data->status_label="Expired";
					}
					$cek_upgrade=$this->db->query("SELECT * FROM upgrade WHERE username='".$post['username']."'")->first_row();

					if($days > 60){
						if($cek_upgrade->pembaruan == 1){
							$data->status_renew='renew';
						}else{
							$data->status_renew='upgrade';

						}
					}
					else{
						$data->status_renew='validasi';
					}

					$get = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$data->id_iklan'")->first_row();
					//$rows['photo']=base_url().'images/iklan/'.$get->photo;
					$data->photo=base_url().'images/iklan/'.$get->photo;
					$ambil_kota=$this->db->query("SELECT * FROM kota WHERE id_kota=".$data->kota)->first_row();
					$data->kota=$ambil_kota->nama_area;
					$rows[]=$data;
				}
				return json_encode($rows);
			}
	    }

		private function set_upload_options($new_name)
	    {
	        //upload an image options
	        $config = array();
	        $config['file_name'] = $new_name;
	        $config['upload_path'] = 'images/iklan/';
	        $config['allowed_types'] = 'gif|jpg|png|jpeg';
	        $config['overwrite']     = FALSE;
	        $config['max_size']    = '5000';
	        return $config;
	    }
	    public function get_kategori_utama($post){
	    	$cek_baris  = $this->db->get('kategori');
			if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$data->icon=base_url()."images/kategori/".$data->icon;
					$rows[]=$data;
				}
				
				return json_encode($rows);
			}
	    }
	    public function sub_kategori_satu($post){
	    	$cek_baris  = $this->db->get_where('sub1_kategori', ['id_kategori'=>$post['id_kategori']]);
			if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$cek_baris1  = $this->db->get_where('sub2_kategori', ['id_sub1_kategori'=>$data->id_sub1_kategori]);
					if($cek_baris1->num_rows() >= 1){
						$data->ada="ada";
					}else{
						$data->ada="kosong";
					}
					
					$rows[]=$data;
				}
				return json_encode($rows);
			}
	    }
	    public function sub_kategori_dua($post){
	    	$cek_baris  = $this->db->get_where('sub2_kategori', ['id_sub1_kategori'=>$post['id_kategori']]);;
			if($cek_baris->num_rows() >= 1){
				$rows=array();
				foreach ($cek_baris->result() as $data) 
				{
					$data->ada="kosong";
					$rows[]=$data;
				}
				return json_encode($rows);
			}
		}
	    function getSubKategori(){
	    	$data = $this->input->post();
	    	$sub = $this->db->get_where('sub1_kategori', ['id_kategori'=>$data['id_kategori']]);
	    	return $sub->result_array();
	    }
	    function getSub2Kategori(){
	    	$data = $this->input->post();
	    	$sub = $this->db->get_where('sub2_kategori', ['id_sub1_kategori'=>$data['id_sub1_kategori']]);
	    	return $sub->result_array();
	    }
	    public function get_profil($post){
	    	$cek_baris  = $this->db->get_where('profil_akun', ['username'=>$post['username']]);
	    	$email=$this->db->get_where('member_akun', ['username'=>$post['username']])->first_row()->email;
			if($cek_baris->num_rows() >= 1){
				$data=$cek_baris->result();
				$data[0]->email=$email;
				//$data->email=$email;
				return json_encode($data);
			}
	    }
	    function updateprofil($post){
			$data = [
				'provinsi_member'=>$post['provinsi_member'],
				'kota_member'=>$post['kota_member'],
				'nama_member'=>$post['nama'],
				'telepon_member'=>$post['telepon'],
				'bbm_member'=>$post['pinbb'],
				'telepon_member2'=>$post['telepon_member2'],
				'id_line'=>$post['id_line'],
				'alamat'=>$post['alamat']
			];
			$this->db->update('profil_akun', $data, ['username'=>$post['username']]);
			return "Success";
		}
		function tambahfavorit($post){
			$cek_baris  = $this->db->get_where('favorit', ['id_iklan'=>$post['id_iklan'],'username'=>$post['username']]);
			if($cek_baris->num_rows() ==0){
		    	$data = [
		    		'id_iklan'=>$post['id_iklan'],
		    		'username'=>$post['username']
		    	];
				$this->db->insert('favorit', $data);
		    	return "Success";
	    	}
	    	return "Success";
	    }
	    function hapusfavorit($post){
	    	$data = [
	    		'id_iklan'=>$post['id_iklan'],
	    		'username'=>$post['username']
	    	];
			$this->db->delete('favorit', $data);
	    	return "Success";
	    }

	    function addFavorit(){
	    	$favorit = $this->input->post();

	    	$data = [
	    		'id_iklan'=>$favorit['id_iklan'],
	    		'username'=>$this->session->userdata('username')
	    	];

	    	if($this->db->get_where('favorit', $data)->num_rows() == 0){
	    		$this->db->insert('favorit', $data);
	    		return ['status'=>'success'];
	    	}else{
	    		return ['status'=>'gagal'];
	    	}

	    	
	    }

	    function removeFavorit(){
	    	$favorit = $this->input->post();

	    	$data = [
	    		'id_iklan'=>$favorit['id_iklan'],
	    		'username'=>$this->session->userdata('username')
	    	];

	    	$this->db->delete('favorit', $data);

	    	return ['status'=>'success'];
	    }
	    function getKota(){
	    	$provinsi = $this->input->post();

	    	return $this->db->get_where('kota', ['id_provinsi'=>$provinsi['id_provinsi']])->result();
	    }
	   function get_provinsi(){
	   		$cek_baris  = $this->db->get('provinsi');
			if($cek_baris->num_rows() >= 1){
				$data=$cek_baris->result();
				return json_encode($data);
			}
	   }
	   function get_provinsi_id($id){
	   		$cek_baris  = $this->db->get_where('provinsi',['id_provinsi'=>$id['id_provinsi']]);
			if($cek_baris->num_rows() >= 1){
				$data=$cek_baris->result();
				return json_encode($data);
			}
	   }
	   function get_kota($post){
	    	$cek_baris  = $this->db->get_where('kota', ['id_provinsi'=>$post['id_prov']]);;
			if($cek_baris->num_rows() >= 1){
				$data=$cek_baris->result();
				return json_encode($data);
			}
	    }
	    function get_kota_id($id){
	   		$cek_baris  = $this->db->get_where('kota',['id_kota'=>$id['id_kota']]);
			if($cek_baris->num_rows() >= 1){
				$data=$cek_baris->result();
				return json_encode($data);
			}
	   }
	   	function get_favorit_iklan($post){
	   		$this->db->select('*');
            $this->db->from('favorit a'); 
            $this->db->join('iklan b', 'a.id_iklan=b.id_iklan', 'left');

            $this->db->where('a.username',$post['username']);    
            $query = $this->db->get(); 
            if($query->num_rows() >=1)
            {
                $favorit=$query->result();
                foreach ($favorit as $q_favorit) {
					$photoiklan = $this->db->get_where('iklan_photo', ['id_iklan'=>$q_favorit->id_iklan]);
					$q_favorit->gambar = base_url()."images/iklan/".$photoiklan->first_row()->photo;
				}
				return json_encode($favorit);
            }
			/*$favorit = $this->db->query("SELECT * FROM favorit JOIN iklan ON favorit.id_iklan = iklan.id_iklan WHERE username = '$username'")->result();
			foreach ($favorit as $q_favorit) {
				$photoiklan = $this->db->get_where('iklan_photo', ['id_iklan'=>$q_favorit->id_iklan]);
				$q_favorit->gambar = $photoiklan->result();
			}
			return $favorit;*/
		}
		function getUpgradeByUsername($post){
			return json_encode($this->db->get_where('upgrade', ['username'=>$post['username']])->row());
		}
		function upgradeUpload10($post){
			$this->db->update('upgrade', ['upload10'=>'1'], ['username'=>$post['username']]);
		}
		function upgradeUpload25($post){
			$this->db->update('upgrade', ['upload25'=>'1'], ['username'=>$post['username']]);
		}
		function upgradeUpload50($post){
			$this->db->update('upgrade', ['upload50'=>'1'], ['username'=>$post['username']]);
		}
		function upgradeUploadUnl($post){
			$this->db->update('upgrade', ['uploadunl'=>'1'], ['username'=>$post['username']]);
		}
		function upgradePembaruan($post){
			$this->db->update('upgrade', ['pembaruan'=>'1'], ['username'=>$post['username']]);
		}
		function upgradeDomainSendiri($post){
			$this->db->update('upgrade', ['domainsendiri'=>'1'], ['username'=>$post['username']]);
		}
		function upgradeMultiKategori($post){
			$this->db->update('upgrade', ['multikategori'=>'1'], ['username'=>$post['username']]);
		}
		function getMaksimalIklan($post){
			$count = 25;
			$upgrade = $this->db->get_where('upgrade', ['username'=>$post['username']])->row();
			if($upgrade->upload10 == 1){
				$count += 10;
			}
			if($upgrade->upload25 == 1){
				$count += 25;
			}
			if($upgrade->upload50 == 1){
				$count += 50;
			}
			if($upgrade->uploadunl == 1){
				$count += 999999;
			}
			return $count;
		}
		 function renew($post){
	        $this->db->update('iklan', ['tanggal_post'=>date('Y-m-d')], ['id_iklan'=>$post['id_iklan']]);
	    }
	    function hapusiklan($post){
	        $this->db->delete('iklan', ['id_iklan'=>$post['id_iklan']]);
	        $this->db->delete('detail_premium', ['id_iklan'=>$post['id_iklan']]);
	    }
}