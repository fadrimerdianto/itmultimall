<?php
	class LaporkanModel extends CI_Model{
		function insert($seo){
			$url = 'https://www.google.com/recaptcha/api/siteverify';
			$secret = '6LcGpSATAAAAAIGq7PMQ1-oosEq-bPAuOuGR-Q2O';
			$laporan = $this->input->post();
			$response = file_get_contents($url.'?secret='.$secret.'&response='.$laporan['g-recaptcha-response']);
			$res = json_decode($response);
			if($res->success){
				$data = [
					'nama_pelapor'=>$laporan['nama'],
					'email_pelapor'=>$laporan['email'],
					'pesan'=>$laporan['pesan']
				];

				$this->db->insert('laporan_penjual', $data);
				$this->session->set_flashdata('laporan', 'Terima Kasih telah melaporkan, selanjutnya akan kami selidiki terlebih dahulu');
			}else{
				$this->session->set_flashdata('laporan', 'Maaf Laporan anda tidak masuk');
			}
			redirect(base_url().'iklan/detail/'.$seo);
		}
	}