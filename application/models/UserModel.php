<?php
	class UserModel extends CI_Model{
		function register(){
			$this->load->library('My_PHPMailer');
			$register = $this->input->post();

			$cek_data_username = $this->db->get_where('member_akun', ['username'=>$register['username']]);
			$cek_data_email = $this->db->get_where('member_akun', ['email'=>$register['email']]);

			if($cek_data_email->num_rows() > 0){
				$this->session->set_flashdata('email_available', "Email Sudah Terdaftar");
				//redirect(base_url().'main/register');
				return false;
			}elseif ($cek_data_username->num_rows() > 0) {
				$this->session->set_flashdata('username_available', "Username Sudah Terdaftar");
				//redirect(base_url().'main/register');
				return false;
			}else{
				$data_register = [
					'username' => $register['username'],
					'email' => $register['email'],
					'password' => md5($register['password']),
					'status'=>0,
					'token'=>md5($register['email']).uniqid()
				];
				$this->db->insert('member_akun', $data_register);

				$data_profil = [
					'username'=>$register['username'],
					'provinsi_member'=>0,
					'kota_member'=>0,
					'nama_member'=>'',
					'telepon_member'=>'',
					'bbm_member'=>'',
					'wa_member'=>''
				];

				$this->db->insert('profil_akun', $data_profil);
				$data_upgrade = [
					'username'=>$register['username'],
					'upload10'=>0,
					'upload25'=>0,
					'upload50'=>0,
					'uploadunl'=>0,
					'pembaruan'=>0,
					'domainsendiri'=>0,
					'multikategori'=>0
				];
				$this->db->insert('upgrade', $data_upgrade);

				$member = $this->UserModel->getProfil($register['username']);
				if($member){
					$datamember = $member;
					$mail = new My_PHPMailer();
				    $toEmail = $member->email;
				    $toName = $member->username;
				    $subject = 'Konfirmasi Pendaftaran';
				    $link_konfirmasi = base_url().'main/konfirmasi/'.$member->token;
				    //$mail->email_view();
				    $kirim = $mail->send_email($toEmail, $toName, $subject, $mail->template_konfirmasi('wikiloka.com', $toName, base_url(), $link_konfirmasi));
				}
				return true;
			}
			
			//redirect(base_url().'main/registered');
		}

		function getProfil($username){
			$profil = $this->db->query("SELECT * FROM member_akun JOIN profil_akun ON member_akun.username = profil_akun.username WHERE member_akun.username = '$username'");
			return $profil->row();
		}

		function updateProfil($username){
			$profil = $this->input->post();
			$data = [
				'nama_member'=>$profil['nama'],
				'telepon_member'=>$profil['telepon'],
				'telepon_member2'=>$profil['telepon2'],
				'id_line'=>$profil['id_line'],
				'bbm_member'=>$profil['pinbb'],
				'provinsi_member'=>$profil['provinsi'],
				'kota_member'=>$profil['kota'],
				'alamat'=>$profil['alamat']
			];

			$this->db->update('profil_akun', $data, ['username'=>$username]);
			$this->db->update('member_akun', ['status'=>1], ['username'=>$username]);
		}

		function update_profil(){
			$profil = $this->input->post();

		}
		function getMember(){
			$member = $this->db->get('member_akun');
			return $member->result();
		}
		function getNewMember(){
			$member = $this->db->get_where('member_akun', ['status'=>0]);
			return $member->result();
		}
		function getUserByToken($token){
			if($token == ''){
				redirect(base_url());
			}
			$member = $this->db->get_where('member_akun', ['token'=>$token]);

			if($member->num_rows() == 1){
				$this->db->update('member_akun', ['status'=>1, 'token'=>''], ['token'=>$token]);
				$data_user = $member->row();
				$newsession = [
					'username' => $data_user->username,
					'tipe_user' => 'member'
				];
				$this->session->set_userdata($newsession);
				redirect(base_url().'iklanku/pengaturan/');
			}else{
				redirect(base_url().'main/expired/'.$token);
			}
		}
		function approveUser($username){
			$user = $this->db->get_where('member_akun', ['username'=>$username]);
			$user = $user->row();

			$token = md5($user->email);

			$this->db->update('member_akun', ['token'=>$token], ['username'=>$username]);

			//send Email
		}
		function loginadmin(){
			$dataLog = $this->input->post();
			$cekLogin = $this->db->get_where('admin', ['username'=>$dataLog['username'], 'password'=>md5($dataLog['password'])]);
			if($cekLogin->num_rows() == 1){
				$newsession = [
					'username'=>$dataLog['username'],
					'tipe_user'=>'admin'
				];
				$this->session->set_userdata($newsession);
				redirect(base_url().'aksa_admin');
			}else{
				$this->session->set_flashdata('wrong_pass', "Username atau Password anda salah");
				redirect(base_url().'main/aksa_login');
			}
		}
	}

