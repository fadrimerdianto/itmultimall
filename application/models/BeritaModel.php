<?php 
	class BeritaModel extends CI_Model{
		function getAll(){
			return $this->db->order_by('id_berita', 'DESC')->get('berita')->result();
		}
		function getById($seo){
			return $this->db->get_where('berita', ['seo_berita'=>$seo])->row();
		}
		function insert(){
			$berita = $this->input->post();
			$data = [
				'judul_berita'=>$berita['judul'],
				'seo_berita'=>seo($berita['judul']),
				'isi_berita'=>$berita['isi'],
				'sumber_berita'=>$berita['sumber_berita']
			];
			$this->db->insert('berita', $data);
		}
		function update($seo){
			$berita = $this->input->post();
			$data = [
				'judul_berita'=>$berita['judul'],
				'seo_berita'=>seo($berita['judul']),
				'isi_berita'=>$berita['isi'],
				'sumber_berita'=>$berita['sumber_berita']
			];
			$where = [
				'seo_berita'=>$seo
			];
			$this->db->update('berita', $data, $where);
		}
		function delete($id){
			$this->db->delete('berita', ['id_berita'=>$id]);
		}


	    function getAll_berita($limit, $start){
	        $berita =  $this->db->order_by('id_berita', 'desc')
	                        ->get_where('berita', $limit, $start);
	        $berita = $berita->result();
	        return $berita;
	    }
	    
	}