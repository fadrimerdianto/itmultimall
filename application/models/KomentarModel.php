<?php
	class KomentarModel extends CI_Model{
		function getKomentarByIklan($id_iklan){
			$komentar =  $this->db->get_where('komentar', ['id_iklan'=>$id_iklan])->result();

			foreach ($komentar as $q_komentar) {
				$member = $this->db->get_where('profil_akun', ['username'=>$q_komentar->username])->row();
				$q_komentar->member = $member;
				$reply = $this->db->get_where('reply_komentar', ['id_komentar'=>$q_komentar->id_komentar])->result();
				$q_komentar->reply = $reply;
				foreach ($q_komentar->reply as $q_reply) {
					$member_reply = $this->db->get_where('profil_akun', ['username'=>$q_reply->username])->row();
					$q_reply->member = $member_reply;
				}
			}
			return $komentar;
		}
		function postReply(){
			$reply = $this->input->post();

			$data = [
				'username'=>$this->session->userdata['username'],
				'id_komentar'=>$reply['id_komentar'],
				'reply'=>$reply['reply_komentar'],
				'waktu'=>date('Y-m-d H:i:s')
			];

			$this->db->insert('reply_komentar', $data);
			//echo $this->db->insert_id();
			return $this->db->get_where('reply_komentar', ['id_reply'=>$this->db->insert_id()])->row();
		}
		function getReply($id_komentar){
			$this->db->join('profil_akun', 'profil_akun.username = reply_komentar.username');
			return $this->db->get_where('reply_komentar', ['id_komentar'=>$id_komentar])->result();
		}
		function postKomentar(){
			$komentar = $this->input->post();
			var_dump($komentar);
			$data = [
				'username'=>$this->session->userdata('username'),
				'komentar'=>$komentar['komentar'],
				'waktu'=>date('Y-m-d H:i:s'),
				'id_iklan'=>$komentar['id_iklan']
			];

			$this->db->insert('komentar', $data);

			return $this->db->get_where('komentar', ['id_komentar'=>$id_komentar])->row();


		}
		function getKomentar(){
			$komentar = $this->input->post();

			$data = [
				'username'=>$this->session->username['username'],
				'id_iklan'=>$komentar['id_iklan'],
				'komentar'=>$komentar['komentar'],
				'waktu'=>date('Y-m-d H:i:s')
			];

			$this->db->insert('komentar', $data);
			return $this->db->get_where('komentar', ['id_komentar'=>$this->db->insert_id()])->row();
		}
	}