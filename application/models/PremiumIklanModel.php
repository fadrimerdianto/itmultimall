<?php
	class PremiumIklanModel extends CI_Model{
		var $path_iklan;	
		function __construct(){
			parent::__construct();
			$this->path_iklan = realpath(APPPATH .'../images/premium/');
		}
		function getAll(){
			return $this->db->order_by('id_premium', 'DESC')->get('premium_iklan')->result();
		}
		function getPremiumById($id){
			return $this->db->get_where('premium_iklan', ['id_premium'=>$id]);
		}
		function insert(){
			$premium = $this->input->post();
			$config = array(
				'allowed_types' => 'jpg|jpeg|png',
				'upload_path' => $this->path_iklan
				);
			$this->load->library('upload', $config);
			$uploading = $this->upload->do_upload('gambar');

			$data_premium  = $this->upload->data();
			$gambar_premium = $data_premium['file_name'];
			if (!$uploading) {
				$data = ['nama_premium' => $premium['nama'],
					'harga_premium' => $premium['harga'],
					'masa_aktif' => $premium['masa']
					];
			} else {
				$data = ['nama_premium' => $premium['nama'],
					'harga_premium' => $premium['harga'],
					'masa_aktif' => $premium['masa'],
				 	'gambar_premium' => $gambar_premium
					];
			}
			$this->db->insert('premium_iklan', $data);
		}

		function update($id){
			$where = ['id_premium' => $id];
			$gbr = $this->db->query("SELECT gambar_premium FROM premium_iklan WHERE id_premium = '$id'")->row();
			$premium = $this->input->post();
			$config = array(
				'allowed_types' => 'jpg|jpeg|png',
				'upload_path' => $this->path_iklan
				);
			$this->load->library('upload', $config);
			$uploading = $this->upload->do_upload('gambar');

			$data_premium  = $this->upload->data();
			$gambar_premium = $data_premium['file_name'];
			if (!$uploading) {
				$data = ['nama_premium' => $premium['nama'],
					'harga_premium' => $premium['harga'],
					'masa_aktif' => $premium['masa']
					];
			} else {
				unlink('images/premium/'.$gbr->gambar_premium);
				$data = ['nama_premium' => $premium['nama'],
					'harga_premium' => $premium['harga'],
					'masa_aktif' => $premium['masa'],
				 	'gambar_premium' => $gambar_premium
					];
			}
			$this->db->update('premium_iklan', $data, $where);
		}

		function delete($id){
			$where = ['id_premium' => $id];			
			$premium = $this->db->query("SELECT gambar_premium FROM premium_iklan WHERE id_premium = '$id'")->row();
			if($premium->gambar_premium != ''){
				unlink('images/premium/'.$premium->gambar_premium);
			}			
			$this->db->delete('premium_iklan', $where);			
		}
	}