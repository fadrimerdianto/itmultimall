<?php
	class KotaModel extends CI_Model{
		function getKotaFromId($id){
			$kota = $this->db->get_where('kota', ['id_kota'=>$id])->row();

			$provinsi = $this->db->get_where('provinsi', ['id_provinsi'=>$kota->id_provinsi])->row();

			$kota->provinsi = $provinsi;

			return $kota;
		}
		function getKota(){
			$kota = $this->db->get('kota')->result();

			return $kota;
		}
	}