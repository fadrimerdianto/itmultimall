<?php
	class TransaksiBeliPoinModel extends CI_Model{
		function insert($username){
			$this->load->library('My_PHPMailer');
			$transaksi = $this->input->post();

			$kode = rand(0, 4000);

			$poin = $this->db->get_where('poin', ['id_poin'=>$transaksi['poin']]);
			$poin = $poin->row();

			$data = [
				'id_poin' => $transaksi['poin'],
				'username'=> $username,
				'tanggal_transaksi'=>date('Y-m-d H:i:s'),
				'isKonfirmasi'=> 0,
				'biaya_transaksi'=>$poin->harga_poin+$kode
			];

			//var_dump(uniqid());

			$this->db->insert('transaksi_poin', $data);

			$transaksipoin = $this->db->query("SELECT * FROM transaksi_poin JOIN poin ON transaksi_poin.id_poin = poin.id_poin ORDER BY id_transaksi_poin DESC");
			

			$member = $this->db->get_where('member_akun', ['username'=>$username]);
			$member = $member->row();

			$mail = new My_PHPMailer();
			$toEmail = $member->email;
			$datatransaksi['biaya_transaksi'] = $poin->harga_poin+$kode;
			$datatransaksi['paket_poin'] =$poin->nama_poin;
		    $toName = $username;
		    $subject = 'Invoice Pembelian Poin';
		    //$mail->email_view();
		    $kirim = $mail->send_email($toEmail, $toName, $subject, $mail->template_invoice('wikiloka.com', $toName, base_url(), $datatransaksi));

		}

		function getUnconfirmedTransaksi(){
				return $this->db->query("SELECT * FROM transaksi_poin JOIN profil_akun ON transaksi_poin.username = profil_akun.username WHERE isKonfirmasi = 0")->result();
		}
		function getTransaksiById($id){
			return $this->db->query("SELECT * FROM transaksi_poin JOIN profil_akun ON transaksi_poin.username = profil_akun.username JOIN poin ON transaksi_poin.id_poin = poin.id_poin WHERE id_transaksi_poin = '$id'")->row();
		}

		function konfirmasi(){
			$id_transaksi = $this->input->post('id_transaksi');
			$transaksi = $this->db->query("SELECT * FROM transaksi_poin JOIN profil_akun ON transaksi_poin.username = profil_akun.username JOIN poin ON transaksi_poin.id_poin = poin.id_poin WHERE id_transaksi_poin = '$id_transaksi'");
			if($transaksi->num_rows() > 0 AND $transaksi->row()->isKonfirmasi == 0){
				$transaksi = $transaksi->row();
				$newPoin = $transaksi->poin_member + $transaksi->jumlah_poin;
				$this->db->update('transaksi_poin', ['isKonfirmasi'=>1], ['id_transaksi_poin'=>$id_transaksi]);
				$this->db->update('profil_akun', ['poin_member'=>$newPoin], ['username'=>$transaksi->username]);
				return 1;
			}else{
				return 0;
			}
			
		}

		function getPoin() {
			return $this->db->order_by('id_poin','DESC')->get('poin');
		}

		function getPoinById($id){
			$query = $this->db->get_where('poin', ['id_poin',$id]);
			return $query;
		}

		function tambah(){
			$poin = $this->input->post();
			$data = ['nama_poin' => $poin['poin'],
					'jumlah_poin' => $poin['jumlah'],
					'harga_poin' => $poin['harga']
					];
			$this->db->insert('poin', $data);
		}

		function update($id){
			$poin = $this->input->post();
			$data = ['nama_poin' => $poin['poin'],
					'jumlah_poin' => $poin['jumlah'],
					'harga_poin' => $poin['harga']
					];
			$where = ['id_poin' => $id];
			$this->db->update('poin', $data, $where);
		}

		function hapus($id){
			$del = $this->db->delete('poin',['id_poin',$id]);
			return $del;
		}
	}