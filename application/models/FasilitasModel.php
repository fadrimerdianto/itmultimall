<?php
class FasilitasModel extends CI_Model{
	function insert_fasilitas($iklan, $id_iklan){
		if(isset($iklan['ac'])){
			$ac = 1;
		}else{
			$ac = 0;
		}

		if(isset($iklan['swimming_pool'])){
			$swimming_pool = 1;
		}else{
			$swimming_pool = 0;
		}

		if(isset($iklan['carport'])){
			$carport = 1;
		}else{
			$carport = 0;
		}
		if(isset($iklan['garden'])){
			$garden = 1;
		}else{
			$garden = 0;
		}
		if(isset($iklan['garasi'])){
			$garasi = 1;
		}else{
			$garasi = 0;
		}
		if(isset($iklan['telephone'])){
			$telephone = 1;
		}else{
			$telephone = 0;
		}
		if(isset($iklan['pam'])){
			$pam = 1;
		}else{
			$pam = 0;
		}
		if(isset($iklan['water_heater'])){
			$water_heater = 1;
		}else{
			$water_heater = 0;
		}
		if(isset($iklan['refrigerator'])){
			$refrigerator = 1;
		}else{
			$refrigerator = 0;
		}
		if(isset($iklan['stove'])){
			$stove = 1;
		}else{
			$stove = 0;
		}
		if(isset($iklan['microwave'])){
			$microwave = 1;
		}else{
			$microwave = 0;
		}
		if(isset($iklan['oven'])){
			$oven = 1;
		}else{
			$oven = 0;
		}
		if(isset($iklan['fire_extenguisher'])){
			$fire_extenguisher = 1;
		}else{
			$fire_extenguisher = 0;
		}
		if(isset($iklan['gordyn'])){
			$gordyn = 1;
		}else{
			$gordyn = 0;
		}

		$dataFasilitas = [
		'id_iklan_detail'=>$id_iklan,
		'ac'=>$ac,
		'swimming_pool'=>$swimming_pool,
		'carport'=>$carport,
		'garden'=>$garden,
		'garasi'=>$garasi,
		'telephone'=>$telephone,
		'pam'=>$pam,
		'water_heater'=>$water_heater,
		'refrigerator'=>$refrigerator,
		'stove'=>$stove,
		'microwave'=>$microwave,
		'oven'=>$oven,
		'fire_extenguisher'=>$fire_extenguisher,
		'gordyn'=>$gordyn
		];
		$this->db->insert('fasilitas', $dataFasilitas);
	}
}