<?php
class WebsiteReplikaModel extends CI_Model{
	var $foto_about_path;
	var $foto_logo_path;
	var $foto_slider_path;
	function __construct(){
		parent::__construct();
		$this->foto_about_path = realpath(APPPATH .'../images/about_replika/');
		$this->foto_logo_path = realpath(APPPATH .'../images/logo_replika/');
		$this->foto_slider_path = realpath(APPPATH .'../images/slider_replika/');
	}
	function do_step1(){
		$website = $this->input->post();

		$config = array(
			'allowed_types' => 'jpg|jpeg|png',
			'upload_path' => $this->foto_about_path
			);
		$this->load->library('upload', $config);
		$uploading = $this->upload->do_upload('about_foto');

		$data_about  = $this->upload->data();
		$gambar_about = $data_about['file_name'];

		if($uploading){
			$data = [
			'username'=>$this->session->userdata('username'),
			'alamat_website'=>$website['alamat_website'],
			'about'=>$website['about'],
			'about_foto'=>$gambar_about,
			'template'=>0,
			'langkah_pembuatan'=>1
			];

		}else{
			$data = [
			'username'=>$this->session->userdata('username'),
			'alamat_website'=>$website['alamat_website'],
			'about'=>$website['about'],
			'about_foto'=>null,
			'template'=>0,
			'langkah_pembuatan'=>1
			];
		}

		$this->db->insert('website_replika', $data);
		// $id_website = $this->db->insert_id();
		// $this->db->insert('slider_replika', ['id_website'=>$id_website]);
	}

	function do_step2(){
		$template = $this->input->post();

		$data = [
		'template'=>$template['template'],
		'langkah_pembuatan'=>2
		];
		$where = [
		'username'=>$this->session->userdata('username')
		];
		$this->db->update('website_replika', $data, $where);
	}
	function do_step3(){
		$modifikasi = $this->input->post();

		$slider = $this->upload_slider();

		$logo = $this->upload_logo();

		$data = [
		'langkah_pembuatan'=>3,
		'logo'=>$logo
		];
		$where = [
		'username'=>$this->session->userdata('username')
		];

		$this->db->update('website_replika', $data, $where);
	}
	private function set_upload_options_slider($new_name)
	{
        //upload an image options
		$config = array();
		$config['file_name'] = $new_name;
		$config['upload_path'] = 'images/slider';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['overwrite']     = FALSE;
		$config['max_size']    = '5000';
		return $config;
	}
	function upload_slider(){

		$website = $this->db->get_where('website_replika', ['username'=>$this->session->userdata('username')])->row();


		$config = array(
			'allowed_types' => 'jpg|jpeg|png',
			'upload_path' => $this->foto_slider_path
			);

		$this->load->library('upload', $config);
		$uploading_slider1 = $this->upload->do_upload('slider1');

		$data_slider1  = $this->upload->data();
		$gambar_slider1 = $data_slider1['file_name'];

		$uploading_slider2 = $this->upload->do_upload('slider2');
		if($uploading_slider2){
			$data_slider2  = $this->upload->data();
			$gambar_slider2 = $data_slider2['file_name'];
		}else{
			$gambar_slider2 = '';
		}

		$uploading_slider3 = $this->upload->do_upload('slider3');
		if($uploading_slider3){
			$data_slider3  = $this->upload->data();
			$gambar_slider3 = $data_slider3['file_name'];
		}else{
			$gambar_slider3 = '';
		}
		
		$uploading_slider4 = $this->upload->do_upload('slider4');
		if($uploading_slider4){
			$data_slider4  = $this->upload->data();
			$gambar_slider4 = $data_slider4['file_name'];
		}else{
			$gambar_slider4 = '';
		}

		$data = [
			'id_website'=>$website->id_website,
			'slider1'=>$gambar_slider1,
			'slider2'=>$gambar_slider2,
			'slider3'=>$gambar_slider3,
			'slider4'=>$gambar_slider4,
		];

		$this->db->insert('slider_replika', $data);
	}
	function upload_logo(){
		$config = array(
			'allowed_types' => 'jpg|jpeg|png',
			'upload_path' => $this->foto_logo_path
			);
		//$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$uploading = $this->upload->do_upload('logo');

		$data_logo  = $this->upload->data();
		$gambar_logo = $data_logo['file_name'];
		return $gambar_logo;
	}
	function getWebsiteByUsername($username){
		$this->db->select('*');
		$this->db->from('website_replika');
		$this->db->join('slider_replika', 'website_replika.id_website = slider_replika.id_website');
		$this->db->where(['username'=>$username]);
		$query = $this->db->get();
		return $query->row();
		//return $this->db->get_where('website_replika', ['username'=>$username])->row();
	}
	function getWebsiteByAlamat($alamat){
		$this->db->select('*');
		$this->db->from('website_replika');
		$this->db->join('slider_replika', 'website_replika.id_website = slider_replika.id_website');
		$this->db->join('profil_akun', 'website_replika.username = profil_akun.username');
		$this->db->join('member_akun', 'website_replika.username = member_akun.username');
		$this->db->where(['alamat_website'=>$alamat]);
		$query = $this->db->get();
		return $query->row();	}
	function updateAbout(){
		$website = $this->getWebsiteByUsername($this->session->userdata('username'));
		$this->load->library('upload');
		$config = array(
			'allowed_types' => 'jpg|jpeg|png',
			'upload_path' => $this->foto_about_path
		);
		$this->upload->initialize($config);
		$uploading_about = $this->upload->do_upload('aboutfoto');
		var_dump($uploading_about);
		$data_about = $this->upload->data();
		$gambar_about = $data_about['file_name'];
		if($uploading_about){
			unlink('images/about_replika/'.$website->about_foto);
			$data = [
				'about'=>$this->input->post('aboutdesc'),
				'about_foto'=>$gambar_about
			];
		}else{
			$data = [
				'about'=>$this->input->post('aboutdesc')
			];
		}
		$this->db->update('website_replika', $data, ['id_website'=>$website->id_website]);
	}
	function updateTampilan(){
		$website = $this->getWebsiteByUsername($this->session->userdata('username'));
		$this->load->library('upload');
		$config = array(
			'allowed_types' => 'jpg|jpeg|png',
			'upload_path' => $this->foto_logo_path
		);
		$this->upload->initialize($config);
		$this->load->library('upload', $config);

		$uploading_logo = $this->upload->do_upload('logo');
		$data_logo = $this->upload->data();
		$gambar_logo = $data_logo['file_name'];

		if($uploading_logo){
			unlink('images/logo_replika/'.$website->logo);
			$this->db->update('website_replika', ['logo'=>$gambar_logo], ['username'=>$this->session->userdata('username')]);
		}

		$config_slider = array(
			'allowed_types'=>'jpg|jpeg|png',
			'upload_path'=>$this->foto_slider_path
		);
		$this->upload->initialize($config_slider);
		//$this->load->library('upload', $config_slider);

		$uploading_slider1 = $this->upload->do_upload('slider1');

		$data_slider1  = $this->upload->data();
		$gambar_slider1 = $data_slider1['file_name'];
		if($uploading_slider1){
			unlink('images/slider_replika/'.$website->slider1);
			$this->db->update('slider_replika', ['slider1'=>$gambar_slider1], ['id_website'=>$website->id_website]);
		}
		
		$uploading_slider2 = $this->upload->do_upload('slider2');

		$data_slider2  = $this->upload->data();
		$gambar_slider2 = $data_slider2['file_name'];
		if($uploading_slider2){
			if($website->slider2 != ''){
					unlink('images/slider_replika/'.$website->slider2);
			}
			$this->db->update('slider_replika', ['slider2'=>$gambar_slider2], ['id_website'=>$website->id_website]);
		}

		$uploading_slider3 = $this->upload->do_upload('slider3');

		$data_slider3  = $this->upload->data();
		$gambar_slider3 = $data_slider3['file_name'];
		if($uploading_slider3){
			if($website->slider3 != ''){
				unlink('images/slider_replika/'.$website->slider3);
			}
			$this->db->update('slider_replika', ['slider3'=>$gambar_slider3], ['id_website'=>$website->id_website]);
		}

		$uploading_slider4 = $this->upload->do_upload('slider4');

		$data_slider4  = $this->upload->data();
		$gambar_slider4 = $data_slider4['file_name'];
		if($uploading_slider4){
			if($website->slider4 != ''){
				unlink('images/slider_replika/'.$website->slider4);
			}
			$this->db->update('slider_replika', ['slider4'=>$gambar_slider4], ['id_website'=>$website->id_website]);
		}

		//ganti template
		$this->db->update('website_replika', ['template'=>$this->input->post('template')], ['id_website'=>$website->id_website]);


		// $data_slider2  = $this->upload->data();
		// $gambar_slider2 = $data_slider2['file_name'];

		// $uploading_slider3 = $this->upload->do_upload('slider3');

		// $data_slider3  = $this->upload->data();
		// $gambar_slider3 = $data_slider3['file_name'];

		// $uploading_slider4 = $this->upload->do_upload('slider4');

		// $data_slider4  = $this->upload->data();
		// $gambar_slider4 = $data_slider4['file_name'];

	}



}