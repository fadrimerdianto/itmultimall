<?php
	class MallModel extends CI_Model{
		private $table = 'mall';
		private $toko = 'toko';
		function getMall(){
			$mall = $this->db->get($this->table)->result();
			return $mall;
		}

        function getMallFromKota($id){
            $kota = $this->db->get_where($this->table, ['id_kota'=>$id])->result();

            return $kota;
        }
        function getMallFromID($id){
            return $this->db->get_where($this->table, ['id_mall'=>$id])->row();
        }
        
        function getTokoFromMall($id){
            return $this->db->get_where($this->toko, ['id_mall'=>$id])->result();
        }

        
        function getTokoFromID($id){
            return $this->db->get_where($this->toko, ['id_toko'=>$id])->row();
        }

		function insertToko(){

            $toko = $this->input->post();
            if(isset($toko['nama'])){
                $nama = $toko['nama'];
            }else{
                $nama = null;
            }

            if(isset($toko['mall'])){
                $mall = $toko['mall'];
            }else{
                $mall = null;
            }

            if(isset($toko['alamat_toko'])){
                $alamat_toko = $toko['alamat_toko'];
            }else{
                $alamat_toko = null;
            }
            if(isset($toko['kontak_toko'])){
                $kontak_toko = $toko['kontak'];
            }else{
                $kontak_toko = null;
            }

            $data = array(
            	'nama_toko' => $nama,
                'alamat_toko' => $alamat_toko,
                'kontak_toko' => $kontak_toko,
            	'id_mall' => $mall,
            	'id_user' => $this->session->userdata('username'),
                'status' => 0
            	);

            $this->db->insert($this->toko, $data);

		}

		function getToko($id){
			$toko = $this->db->get_where($this->toko, ['id_user'=>$id])->result();

			return $toko;

		}

        function insert(){

            $mall = $this->input->post();
            if(isset($mall['nama'])){
                $nama = $mall['nama'];
                $seo_mall = seo($mall['nama']);
            }else{
                $nama = null;
                $seo_mall = rand(9092);
            }

            $kota = '248';

            if(isset($mall['alamat'])){
                $alamat_mall = $mall['alamat'];
            }else{
                $alamat_mall = null;
            }

            $data = array(
                'nama_mall' => $nama,
                'alamat_mall' => $alamat_mall,
                'id_kota' => $kota,
                'seo_mall' => $seo_mall
                );

            $this->db->insert($this->table, $data);
        }

        function delete($id_toko){
            $this->db->delete('iklan', ['toko'=>$id_toko]);
            $this->db->delete('toko', ['id_toko'=>$id_toko]);
        }
	}