<?php
Class TransaksiPremiumModel extends CI_Model{

	function daftar(){

		$transaksi = $this->input->post();
		//var_dump($transaksi);
		$premium = $this->db->get_where('premium_iklan', ['id_premium'=>$transaksi['pilihpremium']]);
		$premium = $premium->row();
		$member = $this->db->get_where('profil_akun', ['username'=>$this->session->userdata('username')]);
		$member = $member->row();
		$totalbiaya = $premium->harga_premium*sizeof($transaksi['iklan']);
		echo $member->poin_member;
		echo $totalbiaya;
		//var_dump($member->poin_member - ($totalbiaya) < 0);
		if($member->poin_member - ($totalbiaya) < 0){
			$this->session->set_flashdata('poin_kurang', ' ');
			redirect('iklanku/berbayar');
			//echo $premium->harga_premium*sizeof($transaksi['iklan']);
		}else{
			foreach ($transaksi['iklan'] as $q_iklan) {
				$iklan = $this->db->get_where('iklan', ['seo_iklan'=>$q_iklan])->row();
				$detail_premium = $this->db->get_where('detail_premium', ['id_iklan'=>$iklan->id_iklan])->row();

				// $member = $this->db->get('profil_akun', ['username'=>$iklan->owner]);
				// $member = $member->row();

				$top25 = 0;
				$recommended = 0;
				$terlaris = 0;
				$topwebsite = 0;

				$aktif_top25 = $detail_premium->tanggal_aktif_top25;
				$aktif_recommended = $detail_premium->tanggal_aktif_recommended;
				$aktif_terlaris = $detail_premium->tanggal_aktif_terlaris;
				$aktif_topwebsite = $detail_premium->tanggal_aktif_topwebsite;

				if($transaksi['pilihpremium'] == 1){
					$top25 = 1;
					$aktif_top25 = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
				}
				if($transaksi['pilihpremium'] == 2){
					$recommended = 1;
					$aktif_recommended = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
				}
				if($transaksi['pilihpremium'] == 3){
					$terlaris = 1;
					$aktif_terlaris = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
				}
				if($transaksi['pilihpremium'] == 4){
					$topwebsite = 1;
					$aktif_topwebsite = date('Y-m-d', strtotime('+ '.$premium->masa_aktif.' days'));
				}

				$data = [
				'id_premium_iklan' => $transaksi['pilihpremium'],
				'id_iklan'		=> $iklan->id_iklan,
				'tanggal_transaksi' => date('Y-m-d H:i:s')
				];

				$this->db->insert('transaksi_iklan_premium', $data);

				$data_premium = [
				'id_iklan'=>$iklan->id_iklan,
				'top25'=>$top25,
				'tanggal_aktif_top25'=>$aktif_top25,
				'recommended'=>$recommended,
				'tanggal_aktif_recommended'=>$aktif_recommended,
				'terlaris'=>$terlaris,
				'tanggal_aktif_terlaris'=>$aktif_terlaris,
				'topwebsite'=>$topwebsite,
				'tanggal_aktif_topwebsite'=>$aktif_topwebsite
				];

				$this->db->update('detail_premium', $data_premium, ['id_iklan'=>$iklan->id_iklan]);
			}
		}

		$sisa = $member->poin_member-$totalbiaya;
		echo $sisa;
		$this->db->update('profil_akun', ['poin_member'=>$sisa], ['username'=>$this->session->userdata('username')]);
	}
}