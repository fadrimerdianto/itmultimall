<?php

class IklanModel extends CI_Model{

    function watermark($images, $overlay, $percent_image){
        $this->load->library('image_lib');
        $config['image_library']    = 'gd2';
        $config['source_image']     = $images;
        $config['width']            = 250 * $percent_image;
        $config['wm_type']          = 'overlay';
            $config['wm_overlay_path']  = $overlay; //the overlay image
            $config['wm_opacity']       = 60;
            $config['wm_vrt_alignment'] = 'bottom';
            $config['wm_hor_alignment'] = 'right';
            
            $this->image_lib->initialize($config);
            
            if (!$this->image_lib->watermark()) {
                echo $this->image_lib->display_errors();
            }
            
        }

        function insert_iklan(){
            $this->load->model('FasilitasModel');
            $iklan = $this->input->post();

            var_dump($iklan);

            if(isset($iklan['kategori'])){
                $kategori = $iklan['kategori'];
            }else{
                $kategori = null;
            }

            if(isset($iklan['sub1_kategori'])){
                $sub1_kategori = $iklan['sub1_kategori'];
            }else{
                $sub1_kategori = null;
            }

            if(isset($iklan['sub2_kategori'])){
                $sub2_kategori = $iklan['sub2_kategori'];
            }else{
                $sub2_kategori = null;
            }

            //sub 2 kategori
            if($iklan['id_sub2_kategori'] == ''){
                $sub2 = 0;
            }else{
                $sub2 = $iklan['id_sub2_kategori'];
            }

            if(isset($iklan['hide_email'])){
                $hide_email = 1;
            }else{
                $hide_email = 0;
            }
            if(isset($iklan['hide_bbm'])){
                $hide_bbm = 1;
            }else{
                $hide_bbm = 0;
            }

            if(isset($iklan['wa_available'])){
                $wa_available = 1;
            }else{
                $wa_available = 0;
            }

            if(isset($iklan['mall'])){
                $mall = $iklan['mall'];
            }else{
                $mall = null;
            }
            $dataIklan = [
            'judul_iklan'=>$iklan['judul_iklan'],
            'deskripsi_iklan'=>$iklan['deskripsi_iklan'],
            'harga_iklan'=>$iklan['harga_iklan'],
            'web_iklan'=>$iklan['web'],
            'owner'=>$this->session->userdata('username'),
            'status'=>0,
            'seo_iklan'=>seo($iklan['judul_iklan']).'-'.uniqid(),
            'kategori'=>$iklan['id_kategori'],
            'sub1_kategori'=>$iklan['id_sub1_kategori'],
            'sub2_kategori'=>$sub2,
            'kota'=>$iklan['kota'],
            'tanggal_post'=>date('Y-m-d H:i:s'),
            'hide_email'=>$hide_email,
            'hide_bbm'=>$hide_bbm,
            'toko' => $mall,
            'wa_available'=>$wa_available,
            'jenis_iklan'=>$iklan['jenis_iklan']
            ];

            $this->db->insert('iklan', $dataIklan);

            $id_iklan = $this->db->insert_id();

            $this->db->insert('detail_premium', ['id_iklan'=>$id_iklan, 'top25'=>0, 'topwebsite'=>0, 'terlaris'=>0, 'recommended'=>0]);
            //merk mobil
            if($iklan['id_sub1_kategori'] == 8){
                $dataFilter = [
                'id_iklan'=>$id_iklan,
                'id_sub2_kategori'=>$sub2,
                'transmisi'=> $iklan['transmisi'],
                'tipe'=>$iklan['tipe'],
                'tahun'=>$iklan['tahun']
                ];

                $this->db->insert('iklan_merk_mobil', $dataFilter);
            //merk motor
            }elseif ($iklan['id_sub1_kategori'] == 13) {
                $dataFilter = [
                'id_iklan'=>$id_iklan,
                'id_sub2_kategori'=>$sub2,
                'tipe'=>$iklan['tipe'],
                'tahun'=>$iklan['tahun']
                ];

                $this->db->insert('iklan_merk_motor', $dataFilter);
            }
            //rumah
            elseif ($iklan['id_sub1_kategori'] == 17) {
               $dataFilter = [
               'id_iklan'=>$id_iklan,
               'luas_tanah'=> $iklan['luas_tanah'],
               'luas_bangunan'=> $iklan['luas_bangunan'],
               'lantai'=> $iklan['lantai'],
               'kamar_tidur'=> $iklan['kamar_tidur'],
               'kamar_mandi'=> $iklan['kamar_mandi'],
               'sertifikasi'=> $iklan['sertifikasi'],
               'alamat_lokasi'=> $iklan['alamat_lokasi']
               ];
               $this->db->insert('iklan_rumah_properti', $dataFilter);
               $id_iklan_rumah = $this->db->insert_id();
            // insert fasilitas
               $this->FasilitasModel->insert_fasilitas($iklan, $id_iklan_rumah);
           }

        //apartement
           elseif ($iklan['id_sub1_kategori'] == 18) {
               $dataFilter = [
               'id_iklan'=>$id_iklan,
               'luas_bangunan'=> $iklan['luas_bangunan'],
               'lantai'=> $iklan['lantai'],
               'kamar_tidur'=> $iklan['kamar_tidur'],
               'sertifikasi'=> $iklan['sertifikasi'],
               'alamat_lokasi'=> $iklan['alamat_lokasi']
               ];
               $this->db->insert('iklan_apartement_properti', $dataFilter);
               $id_iklan_apartement_properti = $this->db->insert_id();

               $this->FasilitasModel->insert_fasilitas($iklan, $id_iklan_apartement_properti);
           }
        //indekos
           elseif ($iklan['id_sub1_kategori'] == 19) {
               $dataFilter = [
               'id_iklan'=>$id_iklan,
               'luas_bangunan'=> $iklan['luas_bangunan'],
               'alamat_lokasi'=> $iklan['alamat_lokasi'],
               'kamar_mandi'=> $iklan['kamar_mandi']
               ];
               $this->db->insert('iklan_apartement_properti', $dataFilter);
               $id_iklan_indekos_properti = $this->db->insert_id();

               $this->FasilitasModel->insert_fasilitas($iklan, $id_iklan_indekos_properti);

           }
        //bangunan komersil
           elseif ($iklan['id_sub1_kategori'] == 20) {
               $dataFilter = [
               'id_iklan'=>$id_iklan,
               'luas_bangunan'=> $iklan['luas_bangunan'],
               'alamat_lokasi'=> $iklan['alamat_lokasi'],
               'kamar_mandi'=> $iklan['kamar_mandi']
               ];
               $this->db->insert('iklan_bangunan_komersil_properti', $dataFilter);
               $id_iklan_komersil_properti = $this->db->insert_id();

               $this->FasilitasModel->insert_fasilitas($iklan, $id_iklan_komersil_properti);

           }
            //tanah
           elseif ($iklan['id_sub1_kategori'] == 21) {
               $dataFilter = [
               'id_iklan'=>$id_iklan,
               'luas_bangunan'=> $iklan['luas_bangunan'],
               'alamat_lokasi'=> $iklan['alamat_lokasi'],
               'kamar_mandi'=> $iklan['kamar_mandi']
               ];
               $this->db->insert('iklan_tanah_properti', $dataFilter);
               $id_iklan_tanah_properti = $this->db->insert_id();

               $this->FasilitasModel->insert_fasilitas($iklan, $id_iklan_tanah_properti);
           }
           $iklan = $this->db->query("SELECT * FROM iklan ORDER BY id_iklan DESC");
           $iklan = $iklan->row();
           $this->load->library('upload');
		//$files = $_FILES;
           $jumlah_image = count($_FILES['userfile']['size']);

           foreach ($_FILES as $files) {
            //var_dump($files);
            for ($i=0; $i < $jumlah_image; $i++) { 
                $_FILES['userfile']['name'] = $files['name'][$i];
                $_FILES['userfile']['type'] = $files['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['tmp_name'][$i];
                $_FILES['userfile']['error'] = $files['error'][$i];
                $_FILES['userfile']['size'] = $files['size'][$i];

                //file extension
                $extension = pathinfo($_FILES['userfile']['name'],PATHINFO_EXTENSION);
                //random token
                $token = rand(0, 99999);

                $new_file_name = $this->session->userdata('username').'-iklan-'.$iklan->id_iklan.'-'.$token.'.'.$extension;

                $this->upload->initialize($this->set_upload_options($new_file_name));
                //$this->load->library('upload', );
                $data_photo_iklan = [
                'id_iklan'=>$iklan->id_iklan,
                'photo'=> $new_file_name
                ];

                $uploading = $this->upload->do_upload('userfile');

                $data_gambar = $this->upload->data();
                $image_width = $data_gambar['image_width'];
                $percent_image = $image_width / 780;
                if($uploading){
                    //$this->watermark('images/iklan/'.$new_file_name, 'images/watermark.png', $percent_image);
                    $this->db->insert('iklan_photo', $data_photo_iklan);
                }else{

                }
            }
        }

    }

    private function set_upload_options($new_name)
    {
        //upload an image options
        $config = array();
        $config['file_name'] = $new_name;
        $config['upload_path'] = 'images/iklan';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['overwrite']     = FALSE;
        $config['max_size']    = '5000';
        return $config;
    }

    function getFeatured(){
        $this->db->limit(12);
        $this->db->from('iklan');
        $this->db->where('iklan.status', 1);
        $this->db->join('toko', 'toko.id_toko = iklan.toko');
        $this->db->join('mall', 'toko.id_mall = mall.id_mall');
        $this->db->order_by('id_iklan', 'DESC');

        $iklan = $this->db->get();
        return $iklan->result();
    }

    function getNewest(){
        $this->db->limit(5);
        $this->db->order_by('tanggal_post', 'desc');
        $iklan = $this->db->get_where('iklan', ['status'=>1]);
        return $iklan->result();
    }

    function getNewIklan(){
        $iklan = $this->db->get_where('iklan', ['status'=>0]);
        return $iklan->result();
    }
    function getIklanByMall($seo){ 
        $this->db->from('iklan');
        $this->db->join('mall','iklan.mall = mall.id_mall');
        $this->db->join('kategori','iklan.kategori = kategori.id_kategori');
        $this->db->join('kota','iklan.kota = kota.id_kota');
        $this->db->where('mall.seo_mall', $seo);
        $iklan = $this->db->get();
        return $iklan;  
    }
    function getIklanBySeo($seo){
        $iklan = $this->db->get_where('iklan', ['seo_iklan'=>$seo]);
        $iklan =  $iklan->row();

        $gambar_iklan = $this->db->get_where('iklan_photo', ['id_iklan'=>$iklan->id_iklan]);
        $iklan->gambar = $gambar_iklan->result();
        return $iklan;
    }
    function approveIklan($id){
        $this->db->update('iklan', ['status'=>1], ['id_iklan'=>$id]);
    }
    function getIklanByUser($username){
        $now = date('Y-m-d');
        $this->db->where("DATEDIFF('$now', tanggal_post) <", 60);
        $iklan = $this->db->get_where('iklan', ['owner'=>$username, 'status'=>1]);
        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $detail_premium = $this->db->get_where('detail_premium', ['id_iklan'=>$q_iklan->id_iklan])->row();
            $q_iklan->detail_premium = $detail_premium;
            $photo = $this->db->get_where('iklan_photo', ['id_iklan'=>$q_iklan->id_iklan]);
            $q_iklan->gambar = $photo->result();
            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
            $kota = $this->db->get_where('kota', ['id_kota'=>'248']);
            $q_iklan->kota = $kota->row();
            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
            $q_iklan->provinsi = $provinsi->row();
            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
            $q_iklan->kategori = $kategori->row();
            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
            $q_iklan->sub1 = $sub1->row();

            if($favorit->num_rows() > 0 ){
                $q_iklan->status = 1;
            }else{
                $q_iklan->status = 0;
            }
        }
        return $iklan;
    }

    function getIklanByUserOff($username){
        $now = date('Y-m-d');
        //$iklan = $this->db->get_where('iklan', ['owner'=>$username, 'status'=>1]);
        // $this->db->where(['status'=>0]);
        // $this->db->or_where(['status'=>2]);
        // $this->db->or_where("DATEDIFF('$now', tanggal_post) >", 60);
        // $this->db->where(['status'=>1]);
        // $this->db->where(['owner'=>$username]);

        // $iklan = $this->db->get('iklan');
        $iklan = $this->db->query("SELECT * FROM iklan WHERE ((status = 0) OR (DATEDIFF('$now', tanggal_post) > 60 AND status = 1)) AND owner = '$username'");
// =======
//         $iklan = $this->db->query("SELECT * FROM iklan WHERE  owner = '$username' AND (DATEDIFF('$now', tanggal_post) > 60 AND status = 1) OR (status = 0 OR status = 2)");
// >>>>>>> 2e01c0776f994fb77bd63c7fa0e0193bfc0d42dc
        //$iklan = $this->db->query("SELECT * FROM iklan WHERE status = 0 OR status = 2 AND owner = '$username'");
        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $photo = $this->db->get_where('iklan_photo', ['id_iklan'=>$q_iklan->id_iklan]);
            $q_iklan->gambar = $photo->result();
        }
        return $iklan;
    }

    function getIklanByUserDitolak($username){
        $iklan = $this->db->query("SELECT * FROM iklan WHERE status = 2 AND owner = '$username'");
        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $photo = $this->db->get_where('iklan_photo', ['id_iklan'=>$q_iklan->id_iklan]);
            $q_iklan->gambar = $photo->result();
        }
        return $iklan;
    }

    function getIklanByUserLaku($username){
        //$iklan = $this->db->get_where('iklan', ['owner'=>$username, 'status'=>1]);
        $iklan = $this->db->query("SELECT * FROM iklan WHERE status = 3 AND owner = '$username'");
        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $photo = $this->db->get_where('iklan_photo', ['id_iklan'=>$q_iklan->id_iklan]);
            $q_iklan->gambar = $photo->result();
        }
        return $iklan;
    }

    function getIklanByUserPremium($username){
        $now = date('Y-m-d');
        $iklan = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE (tanggal_aktif_top25 > '$now' OR tanggal_aktif_recommended > '$now' OR tanggal_aktif_terlaris > '$now') AND iklan.owner = '$username'");
        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $photo = $this->db->get_where('iklan_photo', ['id_iklan'=>$q_iklan->id_iklan]);
            $q_iklan->gambar = $photo->result();
        }
        return $iklan;
    }
    function getIklanByUserNoPremium($username, $kodepremium){
        $now = date('Y-m-d');
        $this->db->where("DATEDIFF('$now', tanggal_post) <", 60);
        if($kodepremium == 1){
            $iklan = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_top25 < '$now' AND status='1' AND iklan.owner = '$username'");
        }elseif ($kodepremium == 2) {
            $iklan = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_recommended < '$now' AND status='1' AND iklan.owner = '$username'");
        }elseif ($kodepremium == 3) {
            $iklan = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_terlaris < '$now' AND status='1' AND iklan.owner = '$username'");
        }elseif ($kodepremium == 4) {
            $iklan = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_topwebsite < '$now' AND status='1' AND iklan.owner = '$username'");
        }
        //$iklan = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE (tanggal_aktif_top25 > '$now' OR tanggal_aktif_recommended > '$now' OR tanggal_aktif_terlaris > '$now') AND iklan.owner = '$username'");
        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $photo = $this->db->query("SELECT photo FROM iklan_photo WHERE id_iklan = '$q_iklan->id_iklan'");
            $q_iklan->gambar = $photo->result();
        }
        return $iklan;
    }
    function delete($id_iklan){
        $this->db->delete('iklan', ['id_iklan'=>$id_iklan]);
        $this->db->delete('detail_premium', ['id_iklan'=>$id_iklan]);
    }
    function all_iklan () {

        $get = $this->db->query("SELECT * FROM iklan WHERE status='1' ORDER BY id_iklan DESC");
        return $get;
    }
    function getAll_iklan($limit, $start){
        $iklan =  $this->db->order_by('tanggal_post', 'desc')
        ->get_where('iklan', ['status' => '1'], $limit, $start);

        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
            $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
            $q_iklan->kota = $kota->row();
            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
            $q_iklan->provinsi = $provinsi->row();
            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
            $q_iklan->kategori = $kategori->row();
            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
            $q_iklan->sub1 = $sub1->row();
            //$favorit = $favorit->row();
            if($favorit->num_rows() > 0 ){
                $q_iklan->status = 1;
            }else{
                $q_iklan->status = 0;
            }
        }
        return $iklan;
    }
    function getAll_iklanBy($id,$limit, $start, $jenis_iklan){
        $iklan =  $this->db->order_by('tanggal_post', 'desc')
        ->get_where('iklan', ['status' => '1','kategori' => $id, 'jenis_iklan'=>$jenis_iklan], $limit, $start);

        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
            $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
            $q_iklan->kota = $kota->row();
            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
            $q_iklan->provinsi = $provinsi->row();
            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
            $q_iklan->kategori = $kategori->row();
            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
            $q_iklan->sub1 = $sub1->row();
            //$favorit = $favorit->row();
            if($favorit->num_rows() > 0 ){
                $q_iklan->status = 1;
            }else{
                $q_iklan->status = 0;
            }
        }
        return $iklan;
    }

    function getIklanByKategoriAndJenis($id,$limit, $start, $jenis_iklan){
        if($jenis_iklan == null){
            $where = ['status' => '1','kategori' => $id];
        }else{
            $where = ['status' => '1','kategori' => $id, 'jenis_iklan'=>$jenis_iklan];
        }
        $iklan =  $this->db->order_by('tanggal_post', 'desc')
        ->get_where('iklan', $where, $limit, $start);

        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
            $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
            $q_iklan->kota = $kota->row();
            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
            $q_iklan->provinsi = $provinsi->row();
            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
            $q_iklan->kategori = $kategori->row();
            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
            $q_iklan->sub1 = $sub1->row();
            if($favorit->num_rows() > 0 ){
                $q_iklan->status = 1;
            }else{
                $q_iklan->status = 0;
            }
        }
        return $iklan;
    }

    function getIklanBySubKategoriAndJenis($id,$limit, $start, $jenis_iklan){
        if($jenis_iklan == null){
            $where = ['status' => '1','sub1_kategori' => $id];
        }else{
            $where = ['status' => '1','sub1_kategori' => $id, 'jenis_iklan'=>$jenis_iklan];
        }
        $iklan =  $this->db->order_by('tanggal_post', 'desc')
        ->get_where('iklan', $where, $limit, $start);

        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
            $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
            $q_iklan->kota = $kota->row();
            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
            $q_iklan->provinsi = $provinsi->row();
            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
            $q_iklan->kategori = $kategori->row();
            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
            $q_iklan->sub1 = $sub1->row();
            if($favorit->num_rows() > 0 ){
                $q_iklan->status = 1;
            }else{
                $q_iklan->status = 0;
            }
        }
        return $iklan;
    }

    function gbr_iklan ($id) {
        $get = $this->db->query("SELECT * FROM iklan_photo WHERE id_iklan = '$id'");
        return $get;
    }

    function detail_iklan ($seo) {
        $iklan = $this->db->query("SELECT * FROM iklan WHERE seo_iklan = '$seo'")->row();
        $this->db->select('*');
        $this->db->from('iklan');
        $this->db->join('profil_akun','iklan.owner = profil_akun.username');
        if($iklan->sub1_kategori == 17){
            $this->db->join('iklan_rumah_properti','iklan.id_iklan = iklan_rumah_properti.id_iklan');
            $this->db->join('fasilitas','iklan_rumah_properti.id_iklan_rumah_properti = fasilitas.id_iklan_detail');
        }elseif ($iklan->sub1_kategori == 18) {
             $this->db->join('iklan_apartement_properti','iklan.id_iklan = iklan_apartement_properti.id_iklan');
            $this->db->join('fasilitas','iklan_apartement_properti.id_iklan_apartement_properti = fasilitas.id_iklan_detail');
        }elseif ($iklan->sub1_kategori == 19) {
             $this->db->join('iklan_indekos_properti','iklan.id_iklan = iklan_indekos_properti.id_iklan');
            $this->db->join('fasilitas','iklan_indekos_properti.id_iklan_indekos_properti = fasilitas.id_iklan_detail');
        }elseif ($iklan->sub1_kategori == 20) {
             $this->db->join('iklan_bangunan-komersil_properti','iklan.id_iklan = iklan_apartement_properti.id_iklan');
            $this->db->join('fasilitas','iklan_apartement_properti.id_iklan_apartement_properti = fasilitas.id_iklan_detail');
        }elseif ($iklan->sub1_kategori == 21) {
             $this->db->join('iklan_tanah_properti','iklan.id_iklan = iklan_tanah_properti.id_iklan');
        }elseif ($iklan->sub1_kategori == 8) {
            $this->db->join('iklan_merk_mobil','iklan.id_iklan = iklan_merk_mobil.id_iklan');
            $this->db->join('tipe_mobil','iklan_merk_mobil.tipe = tipe_mobil.id_tipe_mobil');
        }elseif ($iklan->sub1_kategori == 13) {
            $this->db->join('iklan_merk_motor','iklan.id_iklan = iklan_merk_motor.id_iklan');
            $this->db->join('tipe_motor','iklan_merk_motor.tipe = tipe_motor.id_tipe_motor');
        }
        $this->db->where('seo_iklan', $seo);
        $get = $this->db->get();
        return $get->row();
    }
    function getIklanByKota($kota){
        $iklan =  $this->db->get_where('iklan', ['kota'=>$kota, 'status'=>1]);
        $iklan = $iklan->result();
        foreach ($iklan as $q_iklan) {
            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
            $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
            $q_iklan->kota = $kota->row();
            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
            $q_iklan->provinsi = $provinsi->row();
            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
            $q_iklan->kategori = $kategori->row();
            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
            $q_iklan->sub1 = $sub1->row();
            //$favorit = $favorit->row();
            if($favorit->num_rows() > 0 ){
                $q_iklan->status = 1;
            }else{
                $q_iklan->status = 0;
            }
        }
        return $iklan;
    }

    function search($get){
        $where = ['status'=>1];
        if($get['keyword'] != ''){
            $where = array_merge($where, ['judul_iklan'=>$get['keyword']]);

        }
        if($get['kota'] != ''){
            $where = array_merge($where, ['kota'=>$get['kota']]);
        }
        $kat = $this->db->get_where('kategori', ['seo_kategori'=>$get['kategori']]);
        if($kat->num_rows() > 0){
            $where = array_merge($where, ['kategori'=>$kat->row()->id_kategori]);
        }
        $this->db->like($where);
        $iklan = $this->db->get('iklan')->result();
        foreach ($iklan as $q_iklan) {
            $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
            $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
            $q_iklan->kota = $kota->row();
            $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
            $q_iklan->provinsi = $provinsi->row();
            $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
            $q_iklan->kategori = $kategori->row();
            $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
            $q_iklan->sub1 = $sub1->row();
            //$favorit = $favorit->row();
            if($favorit->num_rows() > 0 ){
                $q_iklan->status = 1;
            }else{
                $q_iklan->status = 0;
            }
        }
        return $iklan;
    }

    function getJumlahIklanByKategori($kategori){
        $now = date('Y-m-d');

        $this->db->where("DATEDIFF('$now', tanggal_post) <", 60);
        return $this->db->get_where('iklan', ['kategori'=>$kategori, 'status'=>1])->num_rows();
    }

    //gwe sing iki
    function getIklanByFilter($kategori, $jenis_iklan, $subkategori, $kota, $filter){
        $where = [];
        if($kategori != 'semua'){
            $dataKategori = $this->KategoriModel->getKategoriBySeo($kategori);
            //$where = array_merge($where, ['kategori'=>$dataKategori->id_kategori]);
            $this->db->where(['kategori'=>$dataKategori->id_kategori]);
        }

        if($subkategori != 'semua'){
            //$dataSubKategori = $this->SubKategoriModel->getSubKategoriBySeo($subkategori);
            $dataSubKategori = $this->db->query("SELECT * FROM sub1_kategori WHERE seo_sub1_kategori = '$subkategori' AND id_kategori = '$dataKategori->id_kategori'")->row();
            //var_dump($dataSubKategori);
            //$where = array_merge($where, ['sub1_kategori'=>$dataSubKategori->id_sub1_kategori]);
            $this->db->where(['sub1_kategori'=>$dataSubKategori->id_sub1_kategori]);
        }

        if($kota != 0){
            //$dataKota = $this->db->get_where('kota', ['id_kota'=>$kota])->row();
            $dataKota = $this->db->query("SELECT * FROM kota WHERE id_kota = '$kota'");
            if(sizeof($dataKota) > 0){
                $where = array_merge($where, ['kota'=>$kota]);
            }
        }

        if(isset($filter['filter'])){
            $dataSub2 = $filter['filter'];
            // var_dump($dataSub2);
            // var_dump($dataSubKategori);
            //$dataSub2Kategori = $this->db->get_where('sub2_kategori', ['seo_sub2_kategori'=>$filter['filter']])->row();
            $dataSub2Kategori = $this->db->query("SELECT * FROM sub2_kategori WHERE seo_sub2_kategori = '$dataSub2' AND id_sub1_kategori ='$dataSubKategori->id_sub1_kategori'")->row();
            //var_dump($dataSub2Kategori);
            //$where = array_merge($where, ['sub2_kategori'=>$dataSub2Kategori->id_sub2_kategori]);
            $this->db->where(['sub2_kategori'=>$dataSub2Kategori->id_sub2_kategori]);
        }

        //filter harga
        if(isset($filter['start']) AND isset($filter['end'])){
            $this->db->where('harga_iklan BETWEEN '.$filter['start'].' AND '. $filter['end'].'');
        }elseif(isset($filter['start'])){
            $this->db->where('harga_iklan > ', $filter['start']);
        }elseif (isset($filter['end'])) {
            $this->db->where('harga_iklan < ', $filter['end']);
        }

        //filter berdasarkan kategori dan sub mobil
        if($kategori == 'mobil' AND $subkategori == 'merk'){
            $this->db->join('iklan_merk_mobil','iklan_merk_mobil.id_iklan = iklan.id_iklan');
            //filter tipe
            if(isset($filter['tipe'])){
                $this->db->join('tipe_mobil','iklan_merk_mobil.tipe = tipe_mobil.id_tipe_mobil');
                //$dataSub2Kategori = $this->db->query("SELECT * FROM sub2_kategori WHERE seo_sub2_kategori = '$dataSub2'")->row();
                //$this->db->where(['sub2_kategori'=>$filter['tipe']]);
                $this->db->where(['id_tipe_mobil'=>$filter['tipe']]);
            }
            //filter tranmsisi
            if(isset($filter['transmisi'])){
                $this->db->where(['transmisi'=>$filter['transmisi']]);
            }
           
            //filter tahun
            if(isset($filter['tahun'])){
                $this->db->where(['tahun'=>$filter['tahun']]);
            }
            //filter motor merk
        }elseif ($kategori == 'motor' AND $subkategori == 'merk') {
            $this->db->join('iklan_merk_motor','iklan_merk_motor.id_iklan = iklan.id_iklan');
            //filter tipe
            if(isset($filter['tipe'])){
                $this->db->join('tipe_motor','iklan_merk_motor.tipe = tipe_motor.id_tipe_motor');
                //$dataSub2Kategori = $this->db->query("SELECT * FROM sub2_kategori WHERE seo_sub2_kategori = '$dataSub2'")->row();
                //$this->db->where(['sub2_kategori'=>$filter['tipe']]);
                $this->db->where(['id_tipe_motor'=>$filter['tipe']]);
            }

            //filter tahun
            if(isset($filter['tahun'])){
                $this->db->where(['tahun'=>$filter['tahun']]);
            }
            //filter properti rumah
        }elseif ($kategori == 'properti' AND $subkategori == 'rumah') {
            $this->db->join('iklan_rumah_properti','iklan_rumah_properti.id_iklan = iklan.id_iklan');
            if(isset($filter['kamar_tidur'])){
                if($filter['kamar_tidur'] <= 10){
                    $this->db->where(['kamar_tidur'=>$filter['kamar_tidur']]);
                }else{
                 $this->db->where('kamar_tidur >', 10);
             }
         }
        //kamar_mandi
         if(isset($filter['kamar_mandi'])){
            if($filter['kamar_mandi'] <= 10){
                $this->db->where(['kamar_mandi'=>$filter['kamar_mandi']]);
            }else{
                $this->db->where('kamar_mandi >', 10);
            }
        }
        //luas bangunan
        if(isset($filter['luas_bangunan_awal']) AND isset($filter['luas_bangunan_akhir'])){
            $this->db->where('luas_bangunan BETWEEN '.$filter['luas_bangunan_awal'].' AND '. $filter['luas_bangunan_akhir'].'');
        }elseif (isset($filter['luas_bangunan_awal'])) {
            $this->db->where('luas_bangunan > ', $filter['luas_bangunan_awal']);
        }elseif (isset($filter['luas_bangunan_akhir'])) {
            $this->db->where('luas_bangunan < ', $filter['luas_bangunan_akhir']);
        }
         //luas tanah
        if(isset($filter['luas_tanah_awal']) AND isset($filter['luas_tanah_akhir'])){
            $this->db->where('luas_tanah BETWEEN '.$filter['luas_tanah_awal'].' AND '. $filter['luas_tanah_akhir'].'');
        }elseif (isset($filter['luas_tanah_awal'])) {
            $this->db->where('luas_tanah > ', $filter['luas_tanah_awal']);
        }elseif (isset($filter['luas_tanah_akhir'])) {
            $this->db->where('luas_tanah < ', $filter['luas_tanah_akhir']);
        }
        //sertifikasi
        if(isset($filter['sertifikasi'])){
            $this->db->where(['sertifikasi'=>$filter['sertifikasi']]);
        }
        //lantai
        if(isset($filter['lantai'])){
            $this->db->where(['lantai'=>$filter['lantai']]);
        }


    //apartement
    }elseif ($kategori == 'properti' AND $subkategori == 'apartemen') {
        $this->db->join('iklan_apartement_properti','iklan_apartement_properti.id_iklan = iklan.id_iklan');
        //luas bangunan
        if(isset($filter['luas_bangunan_awal']) AND isset($filter['luas_bangunan_akhir'])){
            $this->db->where('luas_bangunan BETWEEN '.$filter['luas_bangunan_awal'].' AND '. $filter['luas_bangunan_akhir'].'');
        }elseif (isset($filter['luas_bangunan_awal'])) {
            $this->db->where('luas_bangunan > ', $filter['luas_bangunan_awal']);
        }elseif (isset($filter['luas_bangunan_akhir'])) {
            $this->db->where('luas_bangunan < ', $filter['luas_bangunan_akhir']);
        }
        //luas tanah
        if(isset($filter['luas_tanah_awal']) AND isset($filter['luas_tanah_akhir'])){
            $this->db->where('luas_tanah BETWEEN '.$filter['luas_tanah_awal'].' AND '. $filter['luas_tanah_akhir'].'');
        }elseif (isset($filter['luas_tanah_awal'])) {
            $this->db->where('luas_tanah > ', $filter['luas_tanah_awal']);
        }elseif (isset($filter['luas_tanah_akhir'])) {
            $this->db->where('luas_tanah < ', $filter['luas_tanah_akhir']);
        }
        if(isset($filter['sertifikasi'])){
            $this->db->where('iklan_apartement_properti', ['sertifikasi'=>$filter['sertifikasi']]);
        }
        //lantai
        if(isset($filter['lantai'])){
            $this->db->where(['lantai'=>$filter['lantai']]);
        }
      
        //indekos
    }elseif ($kategori == 'properti' AND $subkategori == 'indekos') {
        $this->db->join('iklan_indekos_properti','iklan_indekos_properti.id_iklan = iklan.id_iklan');
        //luas bangunan
        if(isset($filter['luas_bangunan_awal']) AND isset($filter['luas_bangunan_akhir'])){
            $this->db->where('luas_bangunan BETWEEN '.$filter['luas_bangunan_awal'].' AND '. $filter['luas_bangunan_akhir'].'');
        }elseif (isset($filter['luas_bangunan_awal'])) {
            $this->db->where('luas_bangunan > ', $filter['luas_bangunan_awal']);
        }elseif (isset($filter['luas_bangunan_akhir'])) {
            $this->db->where('luas_bangunan < ', $filter['luas_bangunan_akhir']);
        }
       
         //kamar_mandi
        if(isset($filter['kamar_mandi'])){
            if($filter['kamar_mandi'] <= 10){
                $this->db->where(['kamar_mandi'=>$filter['kamar_mandi']]);
            }else{
                $this->db->where('kamar_mandi >', 10);
            }
        }
    }elseif ($kategori == 'properti' AND $subkategori == 'bangunan-komersil') {
        
        //luas bangunan
        if(isset($filter['luas_bangunan_awal']) AND isset($filter['luas_bangunan_akhir'])){
            $this->db->where('luas_bangunan BETWEEN '.$filter['luas_bangunan_awal'].' AND '. $filter['luas_bangunan_akhir'].'');
        }elseif (isset($filter['luas_bangunan_awal'])) {
            $this->db->where('luas_bangunan > ', $filter['luas_bangunan_awal']);
        }elseif (isset($filter['luas_bangunan_akhir'])) {
            $this->db->where('luas_bangunan < ', $filter['luas_bangunan_akhir']);
        }
         //sertifikasi
        if(isset($filter['sertifikasi'])){
            $this->db->where(['sertifikasi'=>$filter['sertifikasi']]);
        }
        //luas tanah
        if(isset($filter['luas_tanah_awal']) AND isset($filter['luas_tanah_akhir'])){
            $this->db->where('luas_tanah BETWEEN '.$filter['luas_tanah_awal'].' AND '. $filter['luas_tanah_akhir'].'');
        }elseif (isset($filter['luas_tanah_awal'])) {
            $this->db->where('luas_tanah > ', $filter['luas_tanah_awal']);
        }elseif (isset($filter['luas_tanah_akhir'])) {
            $this->db->where('luas_tanah < ', $filter['luas_tanah_akhir']);
        }

    }elseif ($kategori == 'properti' AND $subkategori == 'tanah') {
        //sertifikasi
        if(isset($filter['sertifikasi'])){
            $this->db->where(['sertifikasi'=>$filter['sertifikasi']]);
        }
        //luas tanah
        if(isset($filter['luas_tanah_awal']) AND isset($filter['luas_tanah_akhir'])){
            $this->db->where('luas_tanah BETWEEN '.$filter['luas_tanah_awal'].' AND '. $filter['luas_tanah_akhir'].'');
        }elseif (isset($filter['luas_tanah_awal'])) {
            $this->db->where('luas_tanah > ', $filter['luas_tanah_awal']);
        }elseif (isset($filter['luas_tanah_akhir'])) {
            $this->db->where('luas_tanah < ', $filter['luas_tanah_akhir']);
        }
    }

    if(isset($filter['keyword'])){
        $this->db->like('judul_iklan', $filter['keyword']);
            //$where = array_merge($where, ['judul_iklan'=>$filter['keyword']]);
    }
    if(isset($filter['sort'])){
        switch ($filter['sort']) {
            case 'termurah':
            $this->db->order_by('harga_iklan', 'asc');
            break;
            case 'terbaru':
            $this->db->order_by('tanggal_post', 'desc');
            break;
            default:
                    # code...
            break;
        }
    }else{
        $this->db->order_by('tanggal_post', 'desc');
    }

    if(isset($filter['premium'])){
        if($filter['premium'] == 'top-25'){
                //$this->db->where(['id_premium'=>1]);
            $this->db->where('tanggal_aktif_top25 >', date('Y-m-d'));
        }elseif($filter['premium'] == 'top-shop'){
                //$this->db->where(['id_premium'=>2]);
            $this->db->where('tanggal_aktif_premium >', date('Y-m-d'));
        }elseif($filter['premium'] == 'recommended'){
                //$this->db->where(['id_premium'=>3]);
            $this->db->where('tanggal_aktif_recommended >', date('Y-m-d'));
        }elseif ($filter['premium'] == 'terlaris') {
                //$this->db->where(['id_premium'=>4]);
            $this->db->where('tanggal_aktif_terlaris >', date('Y-m-d'));

        }
    }
    if($jenis_iklan != 0){
        $this->db->where(['jenis_iklan'=>$jenis_iklan]);
    }
    $now = date('Y-m-d');
        //var_dump( $this->db->where("DATEDIFF($now, tanggal_post) <=", 60));
    $this->db->where(['status'=>1]);
    $this->db->where("DATEDIFF('$now', tanggal_post) <", 60);

    $this->db->like($where);
    $this->db->join('detail_premium', 'detail_premium.id_iklan = iklan.id_iklan');
        //$this->db->join('detail_premium', 'detail_premium.id_iklan = iklan.id_iklan');
    $iklan = $this->db->get('iklan')->result();

    foreach ($iklan as $q_iklan) {
        $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
        $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
        $q_iklan->kota = $kota->row();
        // $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
        $provinsi = $this->db->get_where('provinsi',['id_provinsi' => '15']);
        $q_iklan->provinsi = $provinsi->row();
        $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
        $q_iklan->kategori = $kategori->row();
        $toko = $this->db->get_where('toko', ['id_toko'=>$q_iklan->toko]);
        $q_iklan->toko = $toko->row();
        $mall = $this->db->get_where('mall', ['id_mall'=>$q_iklan->toko->id_mall]);
        $q_iklan->mall = $mall->row();
        $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
        $q_iklan->sub1 = $sub1->row();
        $sub2 = $this->db->get_where('sub2_kategori', ['id_sub2_kategori'=>$q_iklan->sub2_kategori]);
        $q_iklan->sub2 = $sub2->row();

        if($favorit->num_rows() > 0 ){
            $q_iklan->status = 1;
        }else{
            $q_iklan->status = 0;
        }
    }
    return $iklan;

}
function ubahAktifKeLaku($id){
    $this->db->update('iklan', ['status'=>3], ['id_iklan'=>$id]);
}

function getIklanPremium($seo){
    $premium_iklan = $this->db->get_where('premium_iklan', ['seo_premium'=>$seo])->row();
    $iklan = $this->db->get_where('iklan', ['id_premium'=>$premium_iklan->id_premium])->result();

    foreach ($iklan as $q_iklan) {
        $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
        $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
        $q_iklan->kota = $kota->row();
        $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
        $q_iklan->provinsi = $provinsi->row();
        $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
        $q_iklan->kategori = $kategori->row();
        $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
        $q_iklan->sub1 = $sub1->row();

        if($favorit->num_rows() > 0 ){
            $q_iklan->status = 1;
        }else{
            $q_iklan->status = 0;
        }
    }
    return $iklan;
}

function dilihat($seo){
    $dilihat = $this->db->query("SELECT dilihat FROM iklan WHERE seo_iklan = '$seo'")->row();
    $dilihat = $dilihat->dilihat;

    $this->db->update('iklan', ['dilihat'=>$dilihat+1], ['seo_iklan'=>$seo]);
}

function like($id_iklan){
    $iklan = $this->db->get_where('iklan', ['id_iklan'=>$id_iklan])->row();
    $this->db->update('iklan', ['like'=>$iklan->like+1], ['id_iklan'=>$id_iklan]);
    return ['total_like'=>$iklan->like+1];
}
function unlike($id_iklan){
    $iklan = $this->db->get_where('iklan', ['id_iklan'=>$id_iklan])->row();
    $this->db->update('iklan', ['unlike'=>$iklan->unlike+1], ['id_iklan'=>$id_iklan]);
    return ['total_like'=>$iklan->unlike+1];
}
function renew($id_iklan){
    $this->db->update('iklan', ['tanggal_post'=>date('Y-m-d')], ['id_iklan'=>$id_iklan]);
}

function getJumlahIklan($username){
    return $this->db->get_where('iklan', ['owner'=>$username])->num_rows();
}
function getIklanTopWebsite(){
    $now = date('Y-m-d');
    $iklan = $this->db->query("SELECT * FROM iklan JOIN detail_premium ON iklan.id_iklan = detail_premium.id_iklan WHERE tanggal_aktif_topwebsite > '$now' AND DATEDIFF('$now', tanggal_post) < 60")->result();

    foreach ($iklan as $q_iklan) {
        $favorit = $this->db->get_where('favorit', ['id_iklan'=>$q_iklan->id_iklan, 'username'=>$this->session->userdata('username')]);
        $kota = $this->db->get_where('kota', ['id_kota'=>$q_iklan->kota]);
        $q_iklan->kota = $kota->row();
        $provinsi = $this->db->get_where('provinsi',['id_provinsi' => $q_iklan->kota->id_provinsi]);
        $q_iklan->provinsi = $provinsi->row();
        $kategori = $this->db->get_where('kategori', ['id_kategori'=>$q_iklan->kategori]);
        $q_iklan->kategori = $kategori->row();
        $sub1 = $this->db->get_where('sub1_kategori', ['id_sub1_kategori'=>$q_iklan->sub1_kategori]);
        $q_iklan->sub1 = $sub1->row();
        $sub2 = $this->db->get_where('sub2_kategori', ['id_sub2_kategori'=>$q_iklan->sub2_kategori]);
        $q_iklan->sub2 = $sub2->row();

        if($favorit->num_rows() > 0 ){
            $q_iklan->status = 1;
        }else{
            $q_iklan->status = 0;
        }
    }
    return $iklan;
}
}