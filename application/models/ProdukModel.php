<?php
class ProdukModel extends CI_Model{
	private $kategori = 'kategori';
	private $sub1_kategori = 'sub1_kategori';

	function getKategori()
	{
		$kategori = $this->db->get($this->kategori)->result();
		return $kategori;
	}

	function getProdukByKategori($id)
	{
		$sub1_kategori = $this->db->get_where($this->sub1_kategori,['id_kategori'=>$id])->result();
		return $sub1_kategori;
	}

}