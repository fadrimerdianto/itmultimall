<?php
	class ProvinsiModel extends CI_Model{
		function getProvinsi(){
			return $this->db->get('provinsi')->result();
		}
		function getProvinsiWithkota(){
			$provinsi = $this->db->get('provinsi')->result();
			foreach ($provinsi as $q_provinsi) {
				$q_provinsi->kota = $this->db->get_where('kota', ['id_provinsi'=>$q_provinsi->id_provinsi])->result();
			}
			return $provinsi;
		}
		function getKotaFromProvinsi($provinsi){
			return $this->db->get_where('kota', ['id_provinsi'=>$provinsi])->result();
		}
		function getProvinsiFromId($id){
			return $this->db->get_where('provinsi', ['id_provinsi'=>$id])->row();
		}
	}