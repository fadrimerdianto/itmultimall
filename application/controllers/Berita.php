<?php
	class Berita extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('template_web');
			$this->load->model('BeritaModel');
			$this->load->model('favoritmodel');
			$this->load->model('kategorimodel');
			$this->load->model('UserModel');
			$this->data['favorit'] = $this->favoritmodel->getFavoritByUsername($this->session->userdata('username'));
			$this->data['kategori'] = $this->kategorimodel->getKategori();
			$this->data['profil'] = $this->UserModel->getProfil($this->session->userdata('username'));

		}

		function index(){
			$data = $this->data;			
			$total = $this->db->get('berita')->num_rows();

			$config['base_url'] = base_url().'berita/index';
			$config['total_rows'] = $total;
			$config['per_page'] = $per_page = 5;
			$config['uri_segment'] = 3;
			$config['prev_tag_open'] ='<li>';
			$config['prev_tag_close'] = '</li><li class="divider"><span>|</span></li>';
			$config['num_tag_open'] ='<li>';
			$config['num_tag_close'] = '</li><li class="divider"><span>|</span></li>';
			$config['full_tag_open'] ='<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<li><a href="#">';
			$config['cur_tag_close'] = '</a></li><li class="divider"><span>|</span></li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Prev';

			$this->pagination->initialize($config);
			$data['paging'] =$this->pagination->create_links();
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : '';
			$data['berita'] = $this->BeritaModel->getAll();
			//$data['berita'] = $this->db->get('berita')->result();
			$this->template_web->display('web/content/berita',$data);
		}

		function detail($seo){
			
		}
		
		
	}