<?php
	class Aksa_admin extends CI_Controller{
		var $data;
		function __construct(){
			parent::__construct();
			if($this->session->userdata('tipe_user')!='admin'){
				redirect(base_url().'main/aksa_login');
			}
			$this->load->library('template_admin');
			$this->load->model('UserModel');
			$this->load->model('IklanModel');
			$this->load->model('KategoriModel');
			$this->load->model('BeritaModel');
			$this->load->model('PoinModel');
			$this->load->model('PremiumIklanModel');
			$this->load->model('TransaksiBeliPoinModel');			
		}	

		function index(){
			$data = $this->data;
			$this->template_admin->display('admin/content/home', $data);
		}
		function member($aksi='index', $id=null){
			switch ($aksi) {
				case 'index':
					$data = $this->data;
					$data['member'] = $this->UserModel->getMember();
					$this->template_admin->display('admin/content/member/all', $data);
					break;
				case 'baru':
					$data = $this->data;
					$data['newmember'] = $this->UserModel->getNewMember();
					$this->template_admin->display('admin/content/member/pendaftaran', $data);
					break;
				case 'approve':
					$this->session->set_flashdata('approve_member', 'Member telah diterima');
					$this->UserModel->approveUser($id);
					$this->sendemail('member_approve', $id);
					//redirect(base_url().'aksa_admin/member/baru');
					break;
				case 'detail':
					$data = $this->data;
					$user = $this->uri->segment();
					echo $user;
					break;
				default:
					# code...
					break;
			}
		}
		function kategori($aksi='index', $id=null){
			switch ($aksi) {
				case 'index':
					$data['kategori'] = $this->KategoriModel->getKategori();
					$this->template_admin->display('admin/content/kategori/index_kategori', $data);
					break;
				case 'sub1':
					$data['subkategori'] = $this->KategoriModel->getSub1Kategori();
					$this->template_admin->display('admin/content/kategori/index_sub1kategori', $data);
					break;
				case 'sub2':
					$data['sub2kategori'] = $this->KategoriModel->getSub2Kategori();
					$this->template_admin->display('admin/content/kategori/index_sub2kategori', $data);
					break;
				case 'tambah':
					$this->template_admin->display('admin/content/kategori/tambah_kategori');
					break;
				case 'tambahsub1':
					$data['kategori'] = $this->KategoriModel->getKategori();
					$this->template_admin->display('admin/content/kategori/tambah_sub1kategori', $data);
					break;
				case 'tambahsub2':
					$data['sub1kategori'] = $this->KategoriModel->getSub1Kategori();
					$this->template_admin->display('admin/content/kategori/tambah_sub2kategori', $data);
					break;
				case 'edit':
					$data['kategori'] = $this->KategoriModel->KategoriById($id);
					$this->template_admin->display('admin/content/kategori/edit_kategori', $data);
					break;
				case 'editsub1':
					$data['kategori'] = $this->KategoriModel->getKategori();
					$data['sub1kategori'] = $this->KategoriModel->Sub1KategoriById($id);
					$this->template_admin->display('admin/content/kategori/edit_sub1kategori', $data);
					break;
				case 'editsub2':
					$data['sub1kategori'] = $this->KategoriModel->getSub1Kategori();
					$data['sub2kategori'] = $this->KategoriModel->Sub2KategoriById($id);
					$this->template_admin->display('admin/content/kategori/edit_sub2kategori', $data);
					break;
				case 'insert_kategori':
					$this->session->set_flashdata('tambah_kategori', 'Kategori berhasil ditambahkan');
					$this->KategoriModel->insert();
					redirect(base_url().'aksa_admin/kategori');
					break;
				case 'insert_sub1':
					$this->session->set_flashdata('tambah_kategori', 'Data berhasil ditambahkan');
					$this->KategoriModel->insert_sub1();
					redirect(base_url().'aksa_admin/kategori/sub1');
					break;
				case 'insert_sub2':
					$this->session->set_flashdata('tambah_kategori', 'Data berhasil ditambahkan');
					$this->KategoriModel->insert_sub2();
					redirect(base_url().'aksa_admin/kategori/sub2');
					break;
				case 'update':
					$this->session->set_flashdata('message_update', "Data berhasil di ubah");
					$this->KategoriModel->update($id);
					redirect(base_url().'aksa_admin/kategori');
					break;
				case 'updatesub1':
					$this->session->set_flashdata('message_update', "Data berhasil di ubah");
					$this->KategoriModel->updatesub1($id);
					redirect(base_url().'aksa_admin/kategori/sub1');
					break;
				case 'updatesub2':
					$this->session->set_flashdata('message_update', "Data berhasil di ubah");
					$this->KategoriModel->updatesub2($id);
					redirect(base_url().'aksa_admin/kategori/sub2');
					break;
				case 'delete':					
					$this->session->set_flashdata('tambah_kategori', "Data berhasil dihapus");
					// cek apakah ada sub1
					if ($this->KategoriModel->CekSub1($id) == 0){
						$this->KategoriModel->delete($id);
						redirect('aksa_admin/kategori');
					} else {
						 echo "<script>window.alert('Kategori Ini Masih mempunyai Sub');window.location=('aksa_admin/kategori')</script>";
					}
					break;
				case 'deletesub1':					
					$this->session->set_flashdata('tambah_kategori', "Data berhasil dihapus");
					// cek apakah ada sub1
					if ($this->KategoriModel->CekSub2($id) == 0){
						$this->KategoriModel->deleteSub1($id);
						redirect(base_url().'aksa_admin/kategori/sub1');
					} else {
						 echo "<script>window.alert('Kategori Ini Masih mempunyai Sub');window.location=('aksa_admin/kategori/sub1')</script>";
					}
					break;
				case 'deletesub2':					
					$this->session->set_flashdata('tambah_kategori', "Data berhasil dihapus");
						$this->KategoriModel->deleteSub2($id);
						redirect(base_url().'aksa_admin/kategori/sub2');
					break;
				default:
					# code...
					break;
			}
		}
		function iklan($aksi='index', $id=null){
			switch ($aksi) {
				case 'all':
					$data = $this->data;
					$data['iklan'] = $this->db->get('iklan');
					$this->template_admin->display('admin/content/iklan/all', $data);
					break;
				case 'baru':
					$data = $this->data;
					$data['newiklan'] = $this->IklanModel->getNewIklan();
					$this->template_admin->display('admin/content/iklan/baru', $data);
					break;
				case 'detail':
					$data = $this->data;
					$data['iklan'] = $this->IklanModel->getIklanBySeo($id);
					$this->template_admin->display('admin/content/iklan/detail', $data);
					break;
				case 'approve':
					$this->session->set_flashdata('approve_iklan', 'iklan telah di terima');
					$this->IklanModel->approveIklan($id);
					redirect(base_url().'aksa_admin/iklan/baru');
					break;
				case 'decline':
					# code...
					break;
				case 'index':
					# code...
					break;
				default:
					# code...
					break;
			}
		}
		
		function premiumiklan($aksi='index', $id='null'){
			$data = $this->data;
			switch ($aksi) {
				case 'index':
					$data['premium'] = $this->PremiumIklanModel->getAll();
					$this->template_admin->display('admin/content/premiumiklan/index', $data);
					break;
				case 'tambah':
					$this->template_admin->display('admin/content/premiumiklan/tambah');
					break;
				case 'insert':
					$this->session->set_flashdata('tambah_iklan', "Data berhasil ditambahkan");
					$this->PremiumIklanModel->insert();
					redirect('aksa_admin/premiumiklan');
					break;
				case 'edit':
					$data['premium'] = $this->PremiumIklanModel->getPremiumById($id)->row();
					$this->template_admin->display('admin/content/premiumiklan/edit', $data);
					break;
				case 'update':
					$this->session->set_flashdata('tambah_iklan', "Data berhasil di ubah");
					$this->PremiumIklanModel->update($id);
					redirect(base_url().'aksa_admin/premiumiklan');
					break;
				case 'delete':
					$this->session->set_flashdata('tambah_iklan', "Data berhasil dihapus");
					$this->PremiumIklanModel->delete($id);
					redirect('aksa_admin/premiumiklan');
					break;
				default:
					# code...
					break;
			}
		}

		function poin($aksi='index', $id=null){
			$data = $this->data;
			switch ($aksi) {
				case 'index':
					$data['poin'] = $this->TransaksiBeliPoinModel->getPoin();
					$this->template_admin->display('admin/content/transaksipoin/poin', $data);					
					break;
				case 'tambah':
					$this->template_admin->display('admin/content/poin/tambah', $data);
					break;
				case 'insertpoin':
					$this->session->set_flashdata('tambah_poin', "Poin berhasil ditambahkan");
					$this->PoinModel->insert();
					redirect('aksa_admin/poin');
					break;
				case 'konfirmasi':
					$data['transaksi'] = $this->TransaksiBeliPoinModel->getUnconfirmedTransaksi();
					$this->template_admin->display('admin/content/transaksipoin/index', $data);
					break;
				case 'detailtransaksi':
					$data['transaksi'] = $this->TransaksiBeliPoinModel->getTransaksiById($id);
					$this->template_admin->display('admin/content/transaksipoin/detailtransaksi', $data);
					break;
				case 'approve':
					$this->session->set_flashdata('approve_transaksi', "Transaksi telah di terima");
					$status = $this->TransaksiBeliPoinModel->konfirmasi($id);
					if($status == 1){
						redirect(base_url().'aksa_admin/poin/konfirmasi');
					}
					break;
				case 'insert':
					$this->TransaksiBeliPoinModel->insert();
					redirect(base_url().'aksa_admin/poin');
					break;
				case 'edit':
					//$data['poin'] = $this->TransaksiBeliPoinModel->getPoinById($id)->row();
					$data['poin'] = $this->PoinModel->getPoinById($id);
					$this->template_admin->display('admin/content/poin/edit', $data);
					break;				
				case 'update':
					$this->session->set_flashdata('message_update', "Data berhasil di ubah");
					$this->PoinModel->update($id);
					redirect(base_url().'aksa_admin/poin');
					break;
				case 'delete':					
					$this->session->set_flashdata('approve_poin', "Data berhasil dihapus");
					$this->PoinModel->delete($id);
					redirect('aksa_admin/poin');					
					break;
				default:
					# code...
					break;
			}
		}
		function berita($aksi = 'index', $id = null){
			$data = $this->data;
			switch ($aksi) {
				case 'index':
					$data['berita'] = $this->BeritaModel->getall();
					$this->template_admin->display('admin/content/berita/index', $data);
					break;
				case 'tambah':
					$this->template_admin->display('admin/content/berita/tambah', $data);
					break;
				case 'edit':
					$seo = $this->uri->segment(4);
					$data['berita'] = $this->BeritaModel->getById($seo);
					$this->template_admin->display('admin/content/berita/edit', $data);
					break;
				case 'insert':
					$this->BeritaModel->insert();
					redirect(base_url().'aksa_admin/berita');
					break;
				case 'update':
					$seo = $this->uri->segment(4);
					$this->BeritaModel->update($seo);
					// echo $seo;
					redirect(base_url().'aksa_admin/berita');
					break;
				case 'delete':
					$this->BeritaModel->delete($id);
					redirect(base_url().'aksa_admin/berita');
					break;
				
				default:
					# code...
					break;
			}
		}
		function mall($aksi = 'index', $id = null){
			$data = $this->data;
			$this->load->model('MallModel');
			switch ($aksi) {
				case 'index':
					$data['mall'] = $this->MallModel->getMall();
					$this->template_admin->display('admin/content/mall/index', $data);
					break;
				case 'toko':
					$data['toko'] = $this->MallModel->getTokoFromMall($id);
					$this->template_admin->display('admin/content/mall/toko/index', $data);
					break;
				case 'tambah':
					$this->template_admin->display('admin/content/mall/tambah', $data);
					break;
				case 'edit':
					$seo = $this->uri->segment(4);
					$data['berita'] = $this->BeritaModel->getById($seo);
					$this->template_admin->display('admin/content/berita/edit', $data);
					break;
				case 'insert':
					$this->MallModel->insert();
					redirect(base_url().'aksa_admin/mall');
					break;
				case 'update':
					$seo = $this->uri->segment(4);
					$this->BeritaModel->update($seo);
					// echo $seo;
					redirect(base_url().'aksa_admin/berita');
					break;
				case 'delete':
					$this->BeritaModel->delete($id);
					redirect(base_url().'aksa_admin/berita');
					break;
				
				default:
					# code...
					break;
			}
		}
		function logout(){
			$this->session->sess_destroy();
			redirect(base_url().'main/aksa_login');
		}																
	}