<?php
class Iklanku extends CI_Controller{
	var $data;
	function __construct(){
		parent::__construct();
		$this->load->model('UserModel');
		if($this->session->userdata('tipe_user') != 'member'){
			$this->session->set_flashdata('login_first', 'Harap login terlebih dahulu');
			redirect(base_url().'main/login');
		}
		$this->profil = $this->UserModel->getProfil($this->session->userdata('username'));
		//$profil = $profil->row();

		$this->load->library('template_web');
		$this->load->library('sidebar');
		$this->load->model('IklanModel');
		$this->load->model('KategoriModel');
		$this->load->model('ApiModel');
		$this->load->model('ProvinsiModel');
		$this->load->model('FavoritModel');
		$this->load->model('TransaksiPremiumModel');
		$this->load->model('TransaksiBeliPoinModel');
		$this->load->model('PremiumIklanModel');
		$this->load->model('PoinModel');
		$this->load->model('TemplateModel');
		$this->load->model('WebsiteReplikaModel');
		$this->load->model('UpgradeModel');
		$this->load->model('MallModel');

		$this->data['mall'] = $this->MallModel->getMall();
		$this->data['favorit'] = $this->FavoritModel->getFavoritByUsername($this->session->userdata('username'));
		$this->data['kategori'] = $this->KategoriModel->getKategori();
		$this->data['provinsi'] = $this->ProvinsiModel->getProvinsi();
		$this->data['sidebar'] = $this->sidebar->sidebar();
		$this->data['profil'] = $this->UserModel->getProfil($this->session->userdata('username'));
		$this->data['upgrade'] = $this->UpgradeModel->getUpgradeByUsername($this->session->userdata('username'));
		$this->data['jumlah_iklan'] = $this->IklanModel->getJumlahIklan($this->session->userdata('username'));
		$this->data['maks_iklan'] = $this->UpgradeModel->getMaksimalIklan($this->session->userdata('username'));
	}

	function data_iklan(){
		
		return $data;
	}

	function index(){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;

		$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
		$data['iklan_off'] = $this->IklanModel->getIklanByUserOff($this->session->userdata('username'));
		$data['iklan_laku'] = $this->IklanModel->getIklanByUserLaku($this->session->userdata('username'));
		$data['iklan_ditolak'] = $this->IklanModel->getIklanByUserDitolak($this->session->userdata('username'));
		$data['iklan_premium'] = $this->IklanModel->getIklanByUserPremium($this->session->userdata('username'));

		$this->template_web->display('web/content/member/home', $data);
	}
	function tidakaktif($aksi='index', $id=null){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
			$data['iklan_off'] = $this->IklanModel->getIklanByUserOff($this->session->userdata('username'));
			$data['iklan_laku'] = $this->IklanModel->getIklanByUserLaku($this->session->userdata('username'));
			$data['iklan_ditolak'] = $this->IklanModel->getIklanByUserDitolak($this->session->userdata('username'));
			$data['iklan_premium'] = $this->IklanModel->getIklanByUserPremium($this->session->userdata('username'));	

			$this->template_web->display('web/content/member/iklan_off', $data);
			break;
			case 'renew':
			$this->IklanModel->renew($id);
			break;
			default:
				# code...
			break;
		}
	}

	function laku($aksi='index', $id=null){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		switch ($aksi) {
			case 'index':
			$data = $this->data;

			$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
			$data['iklan_off'] = $this->IklanModel->getIklanByUserOff($this->session->userdata('username'));
			$data['iklan_laku'] = $this->IklanModel->getIklanByUserLaku($this->session->userdata('username'));
			$data['iklan_ditolak'] = $this->IklanModel->getIklanByUserDitolak($this->session->userdata('username'));
			$data['iklan_premium'] = $this->IklanModel->getIklanByUserPremium($this->session->userdata('username'));
			
			$this->template_web->display('web/content/member/iklan_laku', $data);
			break;
			case 'do':
			$this->IklanModel->ubahAktifKeLaku($id);
			redirect('iklanku/laku');
			break;
			default:
					# code...
			break;
		}
	}
	function premium($aksi='index', $id=null){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}		
		switch ($aksi) {
			case 'index':
			$data = $this->data;

			$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
			$data['iklan_off'] = $this->IklanModel->getIklanByUserOff($this->session->userdata('username'));
			$data['iklan_laku'] = $this->IklanModel->getIklanByUserLaku($this->session->userdata('username'));
			$data['iklan_ditolak'] = $this->IklanModel->getIklanByUserDitolak($this->session->userdata('username'));
			$data['iklan_premium'] = $this->IklanModel->getIklanByUserPremium($this->session->userdata('username'));
			
			$this->template_web->display('web/content/member/iklan_premium', $data);
			break;
			
			default:
			if($this->profil->status == 0){
				redirect(base_url().'iklanku/pengaturan');
			}	
			break;
		}
		
	}
	function ditolak($aksi='index', $id=null){
		switch ($aksi) {
			case 'index':
			$data = $this->data;

			$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
			$data['iklan_off'] = $this->IklanModel->getIklanByUserOff($this->session->userdata('username'));
			$data['iklan_laku'] = $this->IklanModel->getIklanByUserLaku($this->session->userdata('username'));
			$data['iklan_ditolak'] = $this->IklanModel->getIklanByUserDitolak($this->session->userdata('username'));
			$data['iklan_premium'] = $this->IklanModel->getIklanByUserPremium($this->session->userdata('username'));
			
			$this->template_web->display('web/content/member/iklan_ditolak', $data);
			break;
			
			default:
			if($this->profil->status == 0){
				redirect(base_url().'iklanku/pengaturan');
			}	
			break;
		}
	}

	function hapus($id_iklan){
		$this->IklanModel->delete($id_iklan);
		$this->session->set_flashdata('hapus_iklan', 'sukses');
		redirect(base_url().'iklanku');
	}

	function hapus_toko($id_toko){
		$this->load->model('MallModel');
		$this->MallModel->delete($id_toko);
		$this->session->set_flashdata('hapus_toko', 'sukses');
		redirect(base_url().'iklanku/toko');
	}

	function konfirmasi($token, $aksi='index'){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['user'] = $this->UserModel->getkonfirmasi($token);
			$this->template_web->display('web/content/member/konfirmasi', $data);
			break;
			case 'confirm':

			break;
			default:
					# code...
			break;
		}

	}
	function pasang($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$this->load->model('MallModel');
			$data['kategori'] = $this->KategoriModel->getKategori();
			$data['sub1kategori'] = $this->KategoriModel->getSub1Kategori();
			$data['member'] = $this->UserModel->getProfil($this->session->userdata('username'));
			$data['provinsi'] = $this->ProvinsiModel->getProvinsi();
			$data['provinsi'] = $this->ProvinsiModel->getProvinsi();
			$data['mall'] = $this->MallModel->getMallFromKota('248');
			$this->template_web->display('web/content/member/pasang', $data);
			break;
			case 'do':
			$this->IklanModel->insert_iklan();
			redirect(base_url().'iklanku');
			break;
			default:
					# code...
			break;
		}
	}
	function filter_form($sub2){
		if($this->input->is_ajax_request()){
			$sub2_kategori = $this->KategoriModel->Sub2KategoriById($sub2);
			if($sub2_kategori != NULL){
				$data_filter['sub'] = $sub2;
				if($data_filter['sub'] == 8){
					$this->load->model('TipeMobilModel');
					$data_filter['tipe_mobil'] = $this->TipeMobilModel->getTipeByMerk($sub2_kategori->id_sub2_kategori);
				}elseif($data_filter['sub'] == 6){

				}elseif ($data_filter['sub'] == 13) {
					$this->load->model('TipeMotorModel');
					$data_filter['tipe_motor'] = $this->TipeMotorModel->getTipeByMerk($sub2_kategori->id_sub2_kategori);
				}
				$this->load->view('utility/form_filter', $data_filter);
			}
		}else{
			echo "Hayo, kate lapoo ?";
		}
	}

	function favorit($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['iklan'] = $this->FavoritModel->getFavoritIklan($this->session->userdata('username'));
			$this->template_web->display('web/content/member/favorit', $data);
			break;
			case 'add':
			$status = $this->ApiModel->addFavorit();
			echo json_encode($status);
			break;
			case 'remove':
			$status = $this->ApiModel->removeFavorit();
			echo json_encode($status);
			break;	
			default:
					# code...
			break;
		}
	}

	function pengaturan($aksi='index'){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$username = $_SESSION['username'];
			$profil = $this->UserModel->getProfil($username);
			$data['profil'] = $profil;
			$data['kota'] = $this->ProvinsiModel->getKotaFromProvinsi($profil->provinsi_member);
			$this->template_web->display('web/content/member/pengaturan', $data);
			break;
			case 'update':
			$this->session->set_flashdata('update_profil', 'Profil berhasil di ubah');
			$this->UserModel->updateProfil($_SESSION['username']);
			redirect(base_url().'iklanku');
			break;
			default:
					# code...
			break;
		}
	}
	function berbayar($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
			$data['premium'] = $this->PremiumIklanModel->getAll();
			$data['web_replika'] = $this->WebsiteReplikaModel->getWebsiteByUsername($this->session->userdata('username'));
			$this->template_web->display('web/content/member/berbayar', $data);
			break;
			case 'daftar':
			$this->session->set_flashdata('berbayar', 'Iklan anda telah terdaftar sebagai iklan pemium');
			$this->TransaksiPremiumModel->daftar();
			redirect(base_url().'iklanku');
			break;
			default:
					# code...
			break;
		}
	}
	function belipoin($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['poin'] = $this->PoinModel->getAll();
			$this->template_web->display('web/content/member/belipoin', $data);
			break;
			case 'beli':
			$this->session->set_flashdata('belipoin', 'Terima Kasih telah membeli poin. Segera Cek Email Anda untuk melihat invoice pembayaran');
			$this->TransaksiBeliPoinModel->insert($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			default:
					# code...
			break;
		}
	}
	function toko($aksi='index'){
		$this->load->model('MallModel');
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['mall'] = $this->MallModel->getMallFromKota('248');
			$data['toko'] = $this->MallModel->getToko($this->session->userdata('username'));
			$this->template_web->display('web/content/member/toko', $data);
			break;
			case 'new':
			$this->MallModel->insertToko();
			redirect(base_url().'iklanku');
			break;
			default:
					# code...
			break;
		}
	}
	function kelola($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		$website = $this->WebsiteReplikaModel->getWebsiteByUsername($this->session->userdata('username'));
		$data['website'] = $website;
		switch ($aksi) {
			case 'index':
			$this->template_web->display('web/content/member/kelolahome', $data);
			break;
			case 'about-us':
			$this->template_web->display('web/content/member/kelolaabout', $data);
			break;
			case 'kontak-kami':
				# code...
			break;
			case 'tampilan':
			$data['template'] = $this->TemplateModel->getAll();
			$this->template_web->display('web/content/member/kelolatampilan', $data);
			break;
			case 'upgrade':
			$this->template_web->display('web/content/member/kelolaupgrade', $data);
			break;
			case 'update-about-us':
			$this->WebsiteReplikaModel->updateAbout();
			$this->session->set_flashdata('update-about', 'asdasd');
			redirect('iklanku/kelola/about-us');
			break;
			case 'update-tampilan':
			$this->WebsiteReplikaModel->updateTampilan();
			$this->session->set_flashdata('update-tampilan', 'asdasd');
			redirect('iklanku/kelola/tampilan');
			break;
			default:
				# code...
			break;
		}
	}
	function buatwebsite($aksi='step1'){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		$website = $this->WebsiteReplikaModel->getWebsiteByUsername($this->session->userdata('username'));
		if ($website != null) {
			if($website->langkah_pembuatan == 4){
				redirect('iklanku/kelola');
			}
		}
		switch ($aksi) {
			case 'step1':
			$nilai = 1;
			if($website != null){
				if($nilai == $website->langkah_pembuatan){
					redirect(base_url().'iklanku/buatwebsite/step'.($website->langkah_pembuatan+1));
				}
			}
			$data['website'] = $this->WebsiteReplikaModel->getWebsiteByUsername($this->session->userdata('username'));
			$this->template_web->display('web/content/member/buatwebsite', $data);
			break;
			case 'register':
			$this->WebsiteReplikaModel->do_step1();
			redirect(base_url().'iklanku/buatwebsite/step2', $data);
			break;
			case 'step2':
			$nilai = 2;
			if($website != null){
				if($nilai != $website->langkah_pembuatan+1){
					redirect(base_url().'iklanku/buatwebsite/step'.($website->langkah_pembuatan+1));
				}
			}
			$data['template'] = $this->TemplateModel->getAll();
			$this->template_web->display('web/content/member/pilihtemplate', $data);
			break;
			case 'step3':
			$nilai = 3;
			if($website != null){
				if($nilai != $website->langkah_pembuatan+1){
					redirect(base_url().'iklanku/buatwebsite/step'.($website->langkah_pembuatan+1));
				}
			}
			$this->template_web->display('web/content/member/tinjau', $data);
			break;
			case 'step4':
			$nilai = 4;
			if($website != null){
				//if($nilai != $website->langkah_pembuatan){
				//	redirect(base_url().'iklanku/buatwebsite/step'.($website->langkah_pembuatan+1));
				//}
			}
			$data['website'] = $website;
			$data['profil'] = $this->UserModel->getProfil($this->session->userdata('username'));
			$this->template_web->display('web/content/member/finishbuatwebsite', $data);
			break;
			case 'finish':
				# code...
			break;
			case 'pilihtemplate':
			$this->WebsiteReplikaModel->do_step2();
			redirect(base_url().'iklanku/buatwebsite/step3');
			break;
			case 'do_step3':
			$this->WebsiteReplikaModel->do_step3();
			redirect(base_url().'iklanku/buatwebsite/step4');
			break;
			default:
				# code...
			break;
		}
		
	}


	function upgrade($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$this->template_web->display('web/content/member/upgrade', $data);
			break;
			case 'upgrade-upload10':
			$this->UpgradeModel->upgradeUpload10($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-upload25':
			$this->UpgradeModel->upgradeUpload25($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-upload50':
			$this->UpgradeModel->upgradeUpload50($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-uploadunl':
			$this->UpgradeModel->upgradeUploadUnl($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-pembaruan':
			$this->UpgradeModel->upgradePembaruan($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-domainsendiri':
			$this->UpgradeModel->upgradeDomainSendiri($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-multikategori':
			$this->UpgradeModel->upgradeMultiKategori($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			default:
				# code...
			break;
		}
	}

	function contoh($opo = null){
		if($opo==1){
			$data['data'] = array(1,2,3,4);
		}
		else {
			$data['data'] = array(4,3,2,1);
		}
		$this->load->view('test',$data);
	}

	function iklannopremium($kodepremium){
		$now = date('Y-m-d');
		//$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
		
		$data['iklan'] = $this->IklanModel->getIklanByUserNoPremium($this->session->userdata('username'), $kodepremium);
		
		//$data['iklan'] = $this->db->query("SELECT * FROM i")
		//var_dump($data['iklan']->result());
		$this->load->view('utility/iklanpremium', $data);
	}


	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}