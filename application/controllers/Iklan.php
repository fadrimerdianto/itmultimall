<?php
class Iklan extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->library('template_web');
		$this->load->library('Aksa_pagination');
		$this->load->model('IklanModel');

		$this->load->model('FavoritModel');
		$this->load->model('SubKategoriModel');
		$this->load->model('ProvinsiModel');
		$this->load->model('KotaModel');

		$this->load->model('MallModel');

		$this->data['mall'] = $this->MallModel->getMall();
		$this->load->model('KomentarModel');
		$this->load->model('LaporkanModel');
		$this->data['favorit'] = $this->FavoritModel->getFavoritByUsername($this->session->userdata('username'));
		$this->load->model('KategoriModel');
		$this->data['kategori'] = $this->KategoriModel->getKategori();
		$this->data['profil'] = $this->UserModel->getProfil($this->session->userdata('username'));

	}

	function index(){
		$data = $this->data;			
		$iklan = $this->IklanModel->all_iklan();
		$total = $iklan->num_rows();

		$config['base_url'] = base_url().'iklan/index';
		$config['total_rows'] = $total;
		$config['per_page'] = $per_page = 15;
		$config['uri_segment'] = 3;
		$config['prev_tag_open'] ='<li>';
		$config['prev_tag_close'] = '</li><li class="divider"><span>|</span></li>';
		$config['num_tag_open'] ='<li>';
		$config['num_tag_close'] = '</li><li class="divider"><span>|</span></li>';
		$config['full_tag_open'] ='<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] ='<li><a href="#">';
		$config['cur_tag_close'] = '</a></li><li class="divider"><span>|</span></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';

		$this->pagination->initialize($config);
		$data['paging'] =$this->pagination->create_links();
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : '';
		$data['iklan'] = $this->IklanModel->getAll_iklan($per_page, $page);
		$this->template_web->display('web/content/list',$data);
	}

	function iklanmall($seo){
		$data = $this->data;			
		$iklan = $this->IklanModel->getIklanByMall($seo);
		$total = $iklan->num_rows();

		$config['base_url'] = base_url().'iklan/index';
		$config['total_rows'] = $total;
		$config['per_page'] = $per_page = 15;
		$config['uri_segment'] = 3;
		$config['prev_tag_open'] ='<li>';
		$config['prev_tag_close'] = '</li><li class="divider"><span>|</span></li>';
		$config['num_tag_open'] ='<li>';
		$config['num_tag_close'] = '</li><li class="divider"><span>|</span></li>';
		$config['full_tag_open'] ='<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] ='<li><a href="#">';
		$config['cur_tag_close'] = '</a></li><li class="divider"><span>|</span></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';

		$this->pagination->initialize($config);
		$data['paging'] =$this->pagination->create_links();
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : '';
		$data['iklan'] = $iklan->result();
		// var_dump($data['iklan']);
		$this->template_web->display('web/content/listbymall',$data);

	}

	function detail($seo){
		$this->load->model('MallModel');
		$data = $this->data;
		$this->IklanModel->dilihat($seo);
			//$seo = $this->uri->segment(3);			
		$data['detail'] = $this->IklanModel->detail_iklan($seo);
			//echo json_encode($data['detail']->result());
		$data['kota'] = $this->KotaModel->getKotaFromId($data['detail']->kota);
		$data['kota_member'] = $this->KotaModel->getKotaFromId($data['detail']->kota_member);
		$data['related'] = $this->IklanModel->getIklanByKota($data['detail']->kota);
		$data['komentar'] = $this->KomentarModel->getKomentarByIklan($data['detail']->id_iklan);
		$data['toko'] = $this->MallModel->getTokoFromID($data['detail']->toko);
		$data['mall_id'] = $this->MallModel->getMallFromID($data['toko']->id_mall);
		$this->template_web->display('web/content/detailiklan2',$data);
	}

	function category($kategori, $jenis = 'baru'){
		$data = $this->data;

		$jenis_iklan = null;
		if($jenis == 'bekas'){
			$jenis_iklan = 2;
		}elseif ($jenis == 'baru') {
			$jenis_iklan = 1;
		}


		$seo = $kategori;

		$kat = $this->db->get_where('kategori', ['seo_kategori' =>$seo]);
		$kat = $kat->row();
		$data['kategori'] = $kat->seo_kategori;
		$total = $this->db->get_where('iklan', ['status' => 1, 'kategori' => $kat->id_kategori])->num_rows();

		$config['base_url'] = base_url().'iklan/category/'.$seo;
		$config['total_rows'] = $total;
		$config['per_page'] = $per_page = 15;
		$config['uri_segment'] = 4;
		$config['prev_tag_open'] ='<li>';
		$config['prev_tag_close'] = '</li><li class="divider"><span>|</span></li>';
		$config['num_tag_open'] ='<li>';
		$config['num_tag_close'] = '</li><li class="divider"><span>|</span></li>';
		$config['full_tag_open'] ='<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] ='<li><a href="#">';
		$config['cur_tag_close'] = '</a></li><li class="divider"><span>|</span></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';

		$this->pagination->initialize($config);
		$data['paging'] =$this->pagination->create_links();
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : '';

		$data['barang_baru'] = sizeof($this->IklanModel->getIklanByKategoriAndJenis($kat->id_kategori,999999, 0, 1));
		$data['barang_bekas'] = sizeof($this->IklanModel->getIklanByKategoriAndJenis($kat->id_kategori,999999, 0, 2));
		$data['barang_semua'] = sizeof($this->IklanModel->getIklanByKategoriAndJenis($kat->id_kategori,999999, 0, null));


		$data['iklan'] = $this->IklanModel->getIklanByKategoriAndJenis($kat->id_kategori,$per_page, $page, $jenis_iklan);
			// $data['iklan'] = $this->db->get_where('iklan', ['status' => 1, 'kategori' => $kat->id_kategori],$per_page, $page);
		$this->template_web->display('web/content/kategoriiklan',$data);
	}

	// function kategori($kategori, $subkategori=null, $kota='semua'){
	// 	var_dump($this->input->get());
	// 	$data = $this->data;
	// 	$kat = explode('-', $kategori);
	// 	$jenis_iklan = null;
	// 	if(isset($kat[1])){
	// 		if($kat[1] == 'bekas'){
	// 			$jenis_iklan = 2;
	// 		}elseif ($kat[1] == 'baru') {
	// 			$jenis_iklan = 1;
	// 		}
	// 	}		
	// 	if($subkategori == 'pilih'){
	// 		$this->template_web->display('web/content/subkategori', $data);
	// 	}elseif($subkategori != null){
	// 		$subkat = $this->db->get_where('sub1_kategori', ['seo_sub1_kategori' =>$subkategori]);
	// 		$subkat = $subkat->row();
	// 		$data['iklan'] = $this->IklanModel->getIklanBySubKategoriAndJenis($subkat->id_sub1_kategori, 999999, 0, $jenis_iklan);
	// 		$data['barang_baru'] = sizeof($this->IklanModel->getIklanBySubKategoriAndJenis($subkat->id_sub1_kategori,999999, 0, 1));
	// 		$data['barang_bekas'] = sizeof($this->IklanModel->getIklanBySubKategoriAndJenis($subkat->id_sub1_kategori,999999, 0, 2));
	// 		$data['barang_semua'] = sizeof($this->IklanModel->getIklanBySubKategoriAndJenis($subkat->id_sub1_kategori,999999, 0, null));
	// 		$data['paging'] = null;
	// 		$data['kategori'] = $kat[0];
	// 		$data['subkategori'] = $subkategori;
	// 		$this->template_web->display('web/content/subkategoriiklan', $data);
	// 	}elseif(!isset($kat[1])){
	// 		$jeniskat = 'baru';
	// 		$this->category($kat[0], $jeniskat);
	// 	}elseif(isset($kat[1])){
	// 		$this->category($kat[0], $kat[1]);
	// 	}

	// }

	function premium($seo_premium){
		$data = $this->data;

		//$data['paging'] = $this->Aksa_pagination->paginate();
		// $data['uri1'] = $this->uri->segment(0);
		// $data['uri2'] = $this->uri->segment(1);

		// $data['uri3'] = $this->uri->segment(2);
		// $data['uri4'] = $this->uri->segment(3);
		// $data['uri5'] = $this->uri->segment(4);

		$data['iklan'] = $this->IklanModel->getIklanPremium($seo_premium);
		$data['paging'] = null;

		$this->template_web->display('web/content/premium', $data);
	}

	function kategori($kategori, $subkategori='semua', $sub2kategori='semua'){
		$data = $this->data;

		$data['provinsi'] = $this->ProvinsiModel->getProvinsi();

		$data['paging'] = null;
		$kat = explode('-', $kategori);
		$jenis_iklan = 0;
		$seo = '';
		
		if(sizeof($kat) == 1){
			$seo = $kat[0];
		}elseif (sizeof($kat) == 2) {
			if($kat[sizeof($kat)-1] == 'bekas'){
				$jenis_iklan = 2;
				$seo = $kat[0];
			}elseif ($kat[sizeof($kat)-1] == 'baru') {
				$jenis_iklan = 1;
				$seo = $kat[0];
			}else{
				$seo = $kat[0].'-'.$kat[1];
			}
		}elseif (sizeof($kat) == 3) {
			if($kat[sizeof($kat)-1] == 'bekas'){
				$jenis_iklan = 2;
				$seo = $kat[0].'-'.$kat[1];
			}elseif ($kat[sizeof($kat)-1] == 'baru') {
				$jenis_iklan = 1;
				$seo = $kat[0].'-'.$kat[1];
			}else{
				$seo = $kat[0].'-'.$kat[1];
			}
		}
		$data['jenisiklan'] = $jenis_iklan;
		$filter = $this->input->get();

		if($subkategori == 'pilih'){
			$this->template_web->display('web/content/subkategori', $data);
		}else{
			//$data['paging'] = $this->Aksa_pagination->paginate();
			$data['uri1'] = $this->uri->segment(0);
			$data['uri2'] = $this->uri->segment(1);

			$data['uri3'] = $this->uri->segment(2);

			$data['uri4'] = $this->uri->segment(3);
			//subkategori
			$data['uri5'] = $this->uri->segment(4);
			//kota
			if(isset($filter['kota'])){
				$data['kota'] = $this->db->get_where('kota',['id_kota'=>$filter['kota']])->row();
				$kota = $filter['kota'];
			}else{
				$kota = 0;
			} 
			$data['uri6'] = $this->uri->segment(5);
			$data['uri7'] = $this->uri->segment(6);
			$data['uri8'] = $this->uri->segment(7);

			//ambil data per kategori
			$data['datakategori'] = $this->KategoriModel->getKategoriBySeo($seo);
			$id_kategori = $data['datakategori']->id_kategori;
			//ambil data per sub1 kategori
			//$data['sub1_kategori'] = $this->SubKategoriModel->getSubKategoriBySeo($subkategori);
			$data['sub1_kategori'] = $this->db->query("SELECT * FROM sub1_kategori WHERE seo_sub1_kategori = '$subkategori' AND id_kategori = '$id_kategori'")->row();
			$data['subkategori'] = $this->SubKategoriModel->getSub1Kategori($data['datakategori']->id_kategori);
			@$data['sub2kategori'] = $this->SubKategoriModel->getSub2Kategori($data['sub1_kategori']->id_sub1_kategori);
			if(isset($filter['filter'])){
				$this->load->model('TipeMobilModel');
				$this->load->model('TipeMotorModel');
				//ambil per sub2 kategori
				$sub2_kategori = $this->SubKategoriModel->getSubKategoriBySeo2($filter['filter']);
				$data['tipe'] = $this->TipeMobilModel->getTipeByMerk($sub2_kategori->id_sub2_kategori);
				$data['tipe_motor'] = $this->TipeMotorModel->getTipeByMerk($sub2_kategori->id_sub2_kategori);
			}
			$data['iklan'] = $this->IklanModel->getIklanByFilter($seo, $jenis_iklan, $subkategori, $kota, $filter);
			$data['barang_baru'] = sizeof($this->IklanModel->getIklanByFilter($seo, 1, $subkategori, $kota, $filter));
			$data['barang_bekas'] = sizeof($this->IklanModel->getIklanByFilter($seo, 2, $subkategori, $kota, $filter));
			$data['barang_semua'] = sizeof($this->IklanModel->getIklanByFilter($seo, 0, $subkategori, $kota, $filter));
			$this->template_web->display('web/content/subkategoriiklan2', $data);

		}		
	}

	function topwebsite(){
		$data = $this->data;
		$data['iklan'] = $this->IklanModel->getIklanTopWebsite();
		$data['paging'] = null;
		$data['uri1'] = '';
		$data['uri2'] = '';
		$data['uri3'] = '';
		$data['uri4'] = '';
		$data['uri5'] = '';
		$data['uri6'] = '';
		$data['uri7'] = '';
		$data['datakategori'] = '';
		$data['jenisiklan'] = '';
		$this->template_web->display('web/content/topwebsite', $data);
	}

	function search(){
		$data = $this->data;
		$data['provinsi'] = $this->ProvinsiModel->getProvinsi();

		$data_cari = $this->input->get();

		//var_dump(isset($data_cari['kategori']));
		if($data_cari['kategori'] !=''){
			redirect(base_url().'iklan/kategori/'.($data_cari['kategori']).'?'.http_build_query(array_merge(['kota'=>$data_cari['kota']], ['keyword'=>$data_cari['keyword']])));
		}else{
			//$where =['judul_iklan'=>$data_cari["q"]];
			//$this->db->where('iklan', ['status'=>1]);
			//$this->db->like($where);
			//$data['iklan'] = $this->db->get('iklan');
			$data['iklan'] = $this->IklanModel->search($data_cari);
			$data['paging'] = null;
			//var_dump($this->session->userdata('username'));
			$this->template_web->display('web/content/search', $data);
		}

	}

	function laporkan($seo){
		$this->LaporkanModel->insert($seo);
	}

}