<?php
	class Api extends CI_Controller{
		var $api_key='wikiloka_aksamedia';
		function __construct(){
			parent::__construct();
			$this->load->model('ApiModel');

		}
		function like(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->like($this->input->post());
			}else{
				echo "failed";
			}
		}
		function get_merk(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->get_merk($this->input->post());
			}else{
				echo "failed";
			}
		}
		function get_premium(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->get_premium($this->input->post());
			}else{
				echo "failed";
			}
		}
		function unlike(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->unlike($this->input->post());
			}else{
				echo "failed";
			}
		}
		function daftar(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->register($this->input->post());
			}else{
				echo "failed";
			}
		}
		function search(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->search($this->input->post());
			}else{
				echo "failed";
			}
		}
		function kategori(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				//$this->IklanModel->getIklanByFilter($kat[0], $jenis_iklan, $subkategori, $kota, $filter);
				echo $this->ApiModel->iklan_by_filter($this->input->post());
			}else{
				echo "failed";
			}
		}
		function detail(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->detail_iklan($this->input->post());
			}else{
				echo "failed";
			}
		}
		function detail_foto(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->detail_foto_iklan($this->input->post());
			}else{
				echo "failed";
			}
		}
		function txt_kota(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->txt_kota($this->input->post());
			}else{
				echo "failed";
			}
		}
		function txt_prov(){
			$key_awal = md5($this->api_key);
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->txt_prov($this->input->post());
			}else{
				echo "failed";
			}
		}

		function berbayar(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->daftarberbayar($this->input->post());
			}
			else{
				echo "failed";
			}
			
		}
		function ubahaktifkelaku(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				echo $this->ApiModel->ubahaktifkelaku($this->input->post());
			}
			else{
				echo "failed";
			}
		}
		function login(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->login($this->input->post());
				echo $data;
			}else{
				echo "failed";
			}
		}
		function pasangiklan(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->pasangiklan($this->input->post());
				echo "Success";
			}else
			{
				echo "failed";
			}
		}
		
		function get_profil(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->get_profil($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}
		function updateprofil(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->updateprofil($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}
		function all_iklan($jenis=null){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->all_iklan($jenis);
				echo $data;
			}else
			{
				echo "failed";
			}
		}
		function all_iklan_username_aktif(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->all_iklanaktif_by_username($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}
		function all_iklan_username_ditolak(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->all_iklanditolak_by_username($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}

		function all_iklan_username_premium(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->all_iklanpremium_by_username($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}

		function all_iklan_nopremium(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->all_iklan_nopremium($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}

		function all_iklan_username_laku(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->all_iklanlaku_by_username($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}

		function all_iklan_username_tidakaktif(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->all_iklantidakaktif_by_username($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}
		function kategori_utama(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->get_kategori_utama($this->input->post());
				echo $data;
			}else
			{
				echo "failed";
			}
		}
		function sub_kategori_satu(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data=$this->ApiModel->sub_kategori_satu($this->input->post());
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}
		function sub_kategori_dua(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->sub_kategori_dua($this->input->post());
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}
		function provinsi(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->get_provinsi();
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}
		function provinsi_id(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->get_provinsi_id($this->input->post());
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}

		function beli_poin(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->beli_poin($this->input->post());
				echo $data;
			}else{
				echo "failed";
			}
		}
		function get_poin(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->get_poin();
				echo $data;
			}else{
				echo "failed";
			}
		}

		function beli_premium(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->beli_premium($this->input->post());
				echo $data;
			}else{
				echo "failed";
			}
		}
		function daftar_premium(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->daftar_premium($this->input->post());
				echo $data;
			}else{
				echo "failed";
			}
		}
		function laporkan(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->laporkan($this->input->post());
				echo $data;
			}else{
				echo "failed";
			}
		}

		function kota(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->get_kota($this->input->post());
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}
		function kota_id(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->get_kota_id($this->input->post());
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}
		function tambahfavorit(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->tambahfavorit($this->input->post());
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}
		function hapusfavorit(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->hapusfavorit($this->input->post());
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}
		function hapusiklan(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$this->ApiModel->hapusiklan($this->input->post());
				echo "sukses";
			}
			else
			{
				echo "failed";
			}
		}
		function bacafavorit(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$data= $this->ApiModel->get_favorit_iklan($this->input->post());
				echo $data;
			}
			else
			{
				echo "failed";
			}
		}
		function getSubKategori(){
			echo json_encode($this->ApiModel->getSubKategori());
		}
		function getSub2Kategori(){
			echo json_encode($this->ApiModel->getSub2Kategori());
		}
		function getKota(){
			echo json_encode($this->ApiModel->getKota());
		}
		function get_upgrade($aksi='index'){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				switch ($aksi) {
					case 'index':
						$data= $this->ApiModel->getUpgradeByUsername($this->input->post());
						echo $data;
					break;
					case 'upload10':
						$this->ApiModel->upgradeUpload10($this->input->post());
						echo "sukses";
						break;
					case 'upload25':
						$this->ApiModel->upgradeUpload25($this->input->post());
						echo "sukses";
						break;
					case 'upload50':
						$this->ApiModel->upgradeUpload50($this->input->post());
						echo "sukses";
						break;
					case 'uploadunl':
						$this->ApiModel->upgradeUploadUnl($this->input->post());
						echo "sukses";
						break;
					case 'pembaruan':
						$this->ApiModel->upgradePembaruan($this->input->post());
						echo "sukses";
						break;
					case 'domainsendiri':
						$this->ApiModel->upgradeDomainSendiri($this->input->post());
						echo "sukses";
						break;
					case 'multikategori':
						$this->UpgradeModel->upgradeMultiKategori($this->input->post());
						echo "sukses";
						break;
					default:
						break;
				}
				
			}
			else{
				echo "failed";
			}

		}
		public function renew(){
			$key_awal = md5($this->api_key);	
			$key = $this->input->post('key');
			if ($key == $key_awal) {
				$this->ApiModel->renew($this->input->post());
				echo 'sukses';
			}
			else
			{
				echo "failed";
			}	
		}
	}