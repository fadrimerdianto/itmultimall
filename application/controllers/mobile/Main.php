<?php 
class Main extends CI_Controller{
	function __construct(){
		parent::__construct();

		// $this->load->library('user_agent');

		// if ($this->agent->is_browser())
		// {
		// 	//$agent = $this->agent->browser().' '.$this->agent->version();
		// 	redirect(base_url());
		// }
		// elseif ($this->agent->is_robot())
		// {
		// 	$agent = $this->agent->robot();
		// }
		// elseif ($this->agent->is_mobile())
		// {
		// 	$agent = $this->agent->mobile();
		// }
		// else
		// {
		// 	$agent = 'Unidentified User Agent';
		// }

		$this->load->library("template_mobile");
		$this->load->model('KategoriModel');
		$this->load->model('UserModel');
		$this->data['kategori'] = $this->KategoriModel->getKategori();

		$this->data['filter_kategori'] = false;
	}
	function index(){
		$data = $this->data;
		$this->template_mobile->display('mobile/content/index', $data);
	}
	// function login(){
	// 	$this->template_mobile->display('mobile/content/login');
	// }
	function register($aksi = 'index'){
		$data = $this->data;
		if($this->session->userdata('tipe_user') == 'member'){
			redirect(base_url().'mobile/iklanku');
		}
		switch ($aksi) {
			case 'index':
			$this->template_mobile->display('mobile/content/register', $data);
			break;
			case 'do':
			$register = $this->UserModel->register();
			if($register == true){
				redirect(base_url().'mobile/main/registered');
			}else if($register == false){
				redirect(base_url().'mobile/main/register');
			}
			break;
			default:
					# code...
			break;
		}
	}
	function login($aksi='index'){
		$data = $this->data;
		if($this->session->userdata('tipe_user') == 'member'){
			redirect(base_url().'mobile/iklanku');
		}
		switch ($aksi) {
			case 'index':
			$this->template_mobile->display('mobile/content/login', $data);
			break;
			case 'do':
			$login = $this->input->post();
			$this->do_login($login);
			break;
			default:
					# code...
			break;
		}
	}
	function do_login($login){
		$cek_login  = $this->db->get_where('member_akun', ['username'=>$login['username'], 'password'=>md5($login['password'])]);
		if($cek_login->num_rows() == 1){
			$data_user = $cek_login->row();
			if($data_user->status != 0){
				$newsession = [
				'username' => $data_user->username,
				'tipe_user' => 'member'
				];
				$this->session->set_userdata($newsession);
				redirect(base_url().'mobile/iklanku');
			}else{
				$newsession = [
				'username' => $data_user->username,
				'tipe_user' => 'member'
				];
				$this->session->set_userdata($newsession);
				redirect(base_url().'mobile/iklanku');
			}

		}else{
			$this->session->set_flashdata('login_gagal', 'Username atau Password salah');
			redirect(base_url().'mobile/main/login');
		}
	}

	function registered(){
		$data = $this->data;
		$this->template_mobile->display('mobile/content/registered', $data);
	}
}