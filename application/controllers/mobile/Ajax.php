<?php
	class Ajax extends CI_Controller{
		function subkategori(){
			$this->load->model('SubKategoriModel');
			$kategori = $this->input->get();
			$data['subkategori'] = $this->SubKategoriModel->getSub1Kategori($kategori['kategori']);
			$data['sub'] = 1;
			$this->load->view('utility/mobile/form-pasang', $data);
		}
		function filter_subkategori(){
			$this->load->model('SubKategoriModel');
			$kategori = $this->input->get();
			$datakategori = $this->db->get_where('kategori', ['seo_kategori'=>$kategori['kategori']])->row();
			$data['subkategori'] = $this->SubKategoriModel->getSub1Kategori($datakategori->id_kategori);
			echo json_encode($data['subkategori']);
			//$this->load->view('utility/mobile/form-pasang', $data);
		}
		function filter_subkategori2(){
			$this->load->model('SubKategoriModel');
			$kategori = $this->input->get();
			$datasubkategori = $this->db->get_where('sub1_kategori', ['seo_sub1_kategori'=>$kategori['subkategori']])->row();
			$data['subkategori2'] = $this->SubKategoriModel->getSub2Kategori($datasubkategori->id_sub1_kategori);
			echo json_encode($data['subkategori2']);
		}
		function sub2kategori(){
			$this->load->model('SubKategoriModel');
			$subkategori = $this->input->get();
			$data['sub2kategori'] = $this->SubKategoriModel->getSub2Kategori($subkategori['subkategori']);
			$data['sub'] = 2;
			$this->load->view('utility/mobile/form-pasang', $data);
		}
		function formfilter(){
			$sub2kategori = $this->input->get();
			$data['sub'] = $sub2kategori['subkategori'];
			if($data['sub'] == 8){
				$this->load->model('TipeMobilModel');
					$data['tipe_mobil'] = $this->TipeMobilModel->getTipeByMerk($sub2kategori['sub2kategori']);
			}
			$this->load->view('utility/mobile/form-filter', $data);
		}
		function kota(){
			$this->load->model('ProvinsiModel');
			$provinsi = $this->input->get();
			$data['kota'] = $this->ProvinsiModel->getKotaFromProvinsi($provinsi['provinsi']);

			$this->load->view('utility/mobile/form-kota', $data);
		}
		function filter_spesial(){
			//$data['subkategori'] = $this->input->get('subkategori');
			$datasubkategori = $this->db->get_where('sub1_kategori', ['seo_sub1_kategori'=>$this->input->get('subkategori')])->row();
			$data['subkategori'] = $datasubkategori->id_sub1_kategori;
			if($datasubkategori->id_sub1_kategori == 8){
				$sub2kategori = $this->db->get_where('sub2_kategori', ['seo_sub2_kategori'=>$this->input->get('sub2kategori')])->row();
				$data['tipe'] = $this->db->get_where('tipe_mobil', ['id_merk'=>$sub2kategori->id_sub2_kategori])->result();
			}

			$this->load->view('utility/mobile/filter-khusus', $data);
		}
	}