<?php
class Iklanku extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('UserModel');
		if($this->session->userdata('tipe_user') != 'member'){
			$this->session->set_flashdata('login_first', 'Harap login terlebih dahulu');
			redirect(base_url().'mobile/login');
		}

		$this->profil = $this->UserModel->getProfil($this->session->userdata('username'));

		$this->load->library('template_mobile');
		$this->load->model('ProvinsiModel');
		$this->load->model('UserModel');
		$this->load->model('KategoriModel');
		$this->load->model('PoinModel');
		$this->load->model('PremiumIklanModel');
		$this->data['kategori'] = $this->KategoriModel->getKategori();

		$this->data['filter_kategori'] = false;
	}

	function index(){
		if($this->profil->status == 0){
			redirect(base_url().'mobile/iklanku/pengaturan');
		}

		$data = $this->data;

		$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
		$data['iklan_off'] = $this->IklanModel->getIklanByUserOff($this->session->userdata('username'));
		$data['iklan_laku'] = $this->IklanModel->getIklanByUserLaku($this->session->userdata('username'));
		$data['iklan_ditolak'] = $this->IklanModel->getIklanByUserDitolak($this->session->userdata('username'));
		$data['iklan_premium'] = $this->IklanModel->getIklanByUserPremium($this->session->userdata('username'));

		$this->template_mobile->display('mobile/content/member/home', $data);
	}

	function tidakaktif($aksi='index', $id=null){
		if($this->profil->status == 0){
			redirect(base_url().'mobile/iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
			$data['iklan_off'] = $this->IklanModel->getIklanByUserOff($this->session->userdata('username'));
			$data['iklan_laku'] = $this->IklanModel->getIklanByUserLaku($this->session->userdata('username'));
			$data['iklan_ditolak'] = $this->IklanModel->getIklanByUserDitolak($this->session->userdata('username'));
			$data['iklan_premium'] = $this->IklanModel->getIklanByUserPremium($this->session->userdata('username'));	

			$this->template_mobile->display('mobile/content/member/iklan_off', $data);
			break;
			case 'renew':
			$this->IklanModel->renew($id);
			break;
			default:
				# code...
			break;
		}
	}

	function laku($aksi='index', $id=null){
		if($this->profil->status == 0){
			redirect(base_url().'mobile/iklanku/pengaturan');
		}
		switch ($aksi) {
			case 'index':
			$data = $this->data;

			$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
			$data['iklan_off'] = $this->IklanModel->getIklanByUserOff($this->session->userdata('username'));
			$data['iklan_laku'] = $this->IklanModel->getIklanByUserLaku($this->session->userdata('username'));
			$data['iklan_ditolak'] = $this->IklanModel->getIklanByUserDitolak($this->session->userdata('username'));
			$data['iklan_premium'] = $this->IklanModel->getIklanByUserPremium($this->session->userdata('username'));
			
			$this->template_mobile->display('mobile/content/member/iklan_laku', $data);
			break;
			case 'do':
			$this->IklanModel->ubahAktifKeLaku($id);
			redirect('mobile/iklanku/laku');
			break;
			default:
					# code...
			break;
		}
	}

	function pengaturan($aksi='index'){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$username = $this->session->userdata('username');
			$profil = $this->UserModel->getProfil($username);
			$data['profil'] = $profil;
			$data['kota'] = $this->ProvinsiModel->getKotaFromProvinsi($profil->provinsi_member);
			$this->template_mobile->display('mobile/content/member/pengaturan', $data);
			break;
			case 'update':
			$this->session->set_flashdata('update_profil', 'Profil berhasil di ubah');
			$this->UserModel->updateProfil($this->session->userdata('username'));
			redirect(base_url().'mobile/iklanku');
			break;
			default:
					# code...
			break;
		}
	}
	function pasang($aksi='index'){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['member'] = $this->UserModel->getProfil($this->session->userdata('username'));
			$data['provinsi'] = $this->ProvinsiModel->getProvinsi();
			$this->template_mobile->display('mobile/content/member/pasang', $data);
			break;
			case 'do':
			$this->IklanModel->insert_iklan();
			redirect(base_url().'mobile/iklanku');
			break;
			default:
				# code...
			break;
		}
		
	}
	function belipoin($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'mobile/iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['poin'] = $this->PoinModel->getAll();
			$this->template_mobile->display('mobile/content/member/belipoin', $data);
			break;
			case 'beli':
			$this->session->set_flashdata('belipoin', 'Terima Kasih telah membeli poin. Segera Cek Email Anda untuk melihat invoice pembayaran');
			$this->TransaksiBeliPoinModel->insert($this->session->userdata('username'));
			redirect(base_url().'mobile/iklanku');
			break;
			default:
					# code...
			break;
		}
	}
	function upgrade($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'mobile/iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$this->template_mobile->display('mobile/content/member/upgrade', $data);
			break;
			case 'upgrade-upload10':
			$this->UpgradeModel->upgradeUpload10($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-upload25':
			$this->UpgradeModel->upgradeUpload25($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-upload50':
			$this->UpgradeModel->upgradeUpload50($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-uploadunl':
			$this->UpgradeModel->upgradeUploadUnl($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-pembaruan':
			$this->UpgradeModel->upgradePembaruan($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-domainsendiri':
			$this->UpgradeModel->upgradeDomainSendiri($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			case 'upgrade-multikategori':
			$this->UpgradeModel->upgradeMultiKategori($this->session->userdata('username'));
			redirect(base_url().'iklanku');
			break;
			default:
				# code...
			break;
		}
	}

	function buatwebiste(){

	}

	function premium(){

	}

	function berbayar($aksi='index'){
		if($this->profil->status == 0){
			redirect(base_url().'mobile/iklanku/pengaturan');
		}
		$data = $this->data;
		switch ($aksi) {
			case 'index':
			$data['premium'] = $this->PremiumIklanModel->getAll();
			//$data['web_replika'] = $this->WebsiteReplikaModel->getWebsiteByUsername($this->session->userdata('username'));
			$this->template_mobile->display('mobile/content/member/berbayar', $data);
			break;
			case 'iklan':
			$data['iklan'] = $this->IklanModel->getIklanByUser($this->session->userdata('username'));
			$this->template_mobile->display('mobile/content/member/berbayariklan', $data);
			break;
			case 'daftar':
			$this->session->set_flashdata('berbayar', 'Iklan anda telah terdaftar sebagai iklan pemium');
			$this->TransaksiPremiumModel->daftar();
			redirect(base_url().'iklanku');
			break;
			default:
					# code...
			break;
		}
	}
	function favorit(){

	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url().'mobile');
	}

}