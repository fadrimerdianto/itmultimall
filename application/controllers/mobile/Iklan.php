<?php
class Iklan extends CI_Controller{
	function __construct(){
		parent::__construct();

		$this->load->model('UserModel');
		$this->load->library('template_mobile');
		$this->load->library('Aksa_pagination');
		$this->load->model('IklanModel');

		$this->load->model('FavoritModel');
		$this->load->model('SubKategoriModel');
		$this->load->model('ProvinsiModel');
		$this->load->model('KotaModel');

		$this->load->model('KomentarModel');
		$this->load->model('LaporkanModel');
		$this->data['favorit'] = $this->FavoritModel->getFavoritByUsername($this->session->userdata('username'));
		$this->load->model('KategoriModel');
		$this->data['kategori'] = $this->KategoriModel->getKategori();
		$this->data['profil'] = $this->UserModel->getProfil($this->session->userdata('username'));
		

		$this->data['harga_mobil'] = [50000000, 60000000, 70000000, 80000000, 90000000, 100000000,

									110000000, 120000000, 130000000, 140000000, 150000000, 20000000, 250000000];
		
		
		$this->data['filter_kategori'] = false;
		$this->data['tipe_filter'] = null;
	}

	function detail($seo){
		$data = $this->data;
		$this->IklanModel->dilihat($seo);
		$data['detail'] = $this->IklanModel->detail_iklan($seo);
		$data['kota'] = $this->KotaModel->getKotaFromId($data['detail']->kota);
		$data['kota_member'] = $this->KotaModel->getKotaFromId($data['detail']->kota_member);
		$data['related'] = $this->IklanModel->getIklanByKota($data['detail']->kota);
		$data['komentar'] = $this->KomentarModel->getKomentarByIklan($data['detail']->id_iklan);
		$this->template_mobile->display('mobile/content/detailiklan', $data);
	}

	function kategori($kategori, $subkategori='semua', $sub2kategori='semua'){
		$data = $this->data;

		$data['filter_kategori'] = true;
		$data['provinsi'] = $this->ProvinsiModel->getProvinsiWithKota();

		$data['paging'] = null;
		$kat = explode('-', $kategori);
		$jenis_iklan = 0;
		$seo = '';
		
		if(sizeof($kat) == 1){
			$seo = $kat[0];
		}elseif (sizeof($kat) == 2) {
			if($kat[sizeof($kat)-1] == 'bekas'){
				$jenis_iklan = 2;
				$seo = $kat[0];
			}elseif ($kat[sizeof($kat)-1] == 'baru') {
				$jenis_iklan = 1;
				$seo = $kat[0];
			}else{
				$seo = $kat[0].'-'.$kat[1];
			}
		}elseif (sizeof($kat) == 3) {
			if($kat[sizeof($kat)-1] == 'bekas'){
				$jenis_iklan = 2;
				$seo = $kat[0].'-'.$kat[1];
			}elseif ($kat[sizeof($kat)-1] == 'baru') {
				$jenis_iklan = 1;
				$seo = $kat[0].'-'.$kat[1];
			}else{
				$seo = $kat[0].'-'.$kat[1];
			}
		}
		$data['jenisiklan'] = $jenis_iklan;
		$filter = $this->input->get();

		if($subkategori == 'pilih'){
			$this->template_web->display('web/content/subkategori', $data);
		}else{
			//$data['paging'] = $this->Aksa_pagination->paginate();
			// $data['uri1'] = $this->uri->segment(0);
			// $data['uri2'] = $this->uri->segment(1);

			// $data['uri3'] = $this->uri->segment(2);

			// $data['uri4'] = $this->uri->segment(3);
			// //subkategori
			// $data['uri5'] = $this->uri->segment(4);
			//kota
			if(isset($filter['kota'])){
				$data['kota'] = $this->db->get_where('kota',['id_kota'=>$filter['kota']])->row();
				$kota = $filter['kota'];
			}else{
				$kota = 0;
			} 
			// $data['uri6'] = $this->uri->segment(5);
			// $data['uri7'] = $this->uri->segment(6);
			// $data['uri8'] = $this->uri->segment(7);

			//ambil data per kategori
			$data['datakategori'] = $this->KategoriModel->getKategoriBySeo($seo);
			$id_kategori = $data['datakategori']->id_kategori;
			//ambil data per sub1 kategori
			//$data['sub1_kategori'] = $this->SubKategoriModel->getSubKategoriBySeo($subkategori);
			$data['sub1_kategori'] = $this->db->query("SELECT * FROM sub1_kategori WHERE seo_sub1_kategori = '$subkategori' AND id_kategori = '$id_kategori'")->row();
			$data['subkategori'] = $this->SubKategoriModel->getSub1Kategori($data['datakategori']->id_kategori);
			@$data['sub2kategori'] = $this->SubKategoriModel->getSub2Kategori($data['sub1_kategori']->id_sub1_kategori);
			if(isset($filter['filter'])){
				$this->load->model('TipeMobilModel');
				$this->load->model('TipeMotorModel');
				//ambil per sub2 kategori
				$sub2_kategori = $this->SubKategoriModel->getSubKategoriBySeo2($filter['filter']);
				$data['sub2_kategori'] = $sub2_kategori;
				$data['tipe'] = $this->TipeMobilModel->getTipeByMerk($sub2_kategori->id_sub2_kategori);
				$data['tipe_motor'] = $this->TipeMotorModel->getTipeByMerk($sub2_kategori->id_sub2_kategori);
			}
			$data['iklan'] = $this->IklanModel->getIklanByFilter($seo, $jenis_iklan, $subkategori, $kota, $filter);
			$data['barang_baru'] = sizeof($this->IklanModel->getIklanByFilter($seo, 1, $subkategori, $kota, $filter));
			$data['barang_bekas'] = sizeof($this->IklanModel->getIklanByFilter($seo, 2, $subkategori, $kota, $filter));
			$data['barang_semua'] = sizeof($this->IklanModel->getIklanByFilter($seo, 0, $subkategori, $kota, $filter));
			$this->template_mobile->display('mobile/content/listiklan', $data);

		}		
	}
}