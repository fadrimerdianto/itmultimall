<?php
class Ajax extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('ProvinsiModel');
		$this->load->model('KomentarModel');
		$this->load->model('IklanModel');


	}
	function getKategoriIklanByUrutan(){
		echo "gasdhgashdg";
	}
	function getKota($provinsi){
		echo json_encode($this->ProvinsiModel->getKotaFromProvinsi($provinsi));
	}
	function reply(){
		//var_dump($this->input->post());
		$reply = $this->KomentarModel->postReply();
		$profil = $this->db->get_where('profil_akun', ['username'=>$this->session->userdata('username')])->row();
		$reply->profil = $profil;
		echo json_encode($reply);
	}
	function komentar(){
		$komentar = $this->KomentarModel->postKomentar();
		$profil = $this->db->get_where('profil_akun', ['username'=>$this->session->userdata('username')])->row();
		$komentar->profil = $profil;
		echo json_encode($komentar);
	}
	function tampil_reply($id_komentar){
		$reply = $this->KomentarModel->getReply($id_komentar);
		echo json_encode($reply);
	}
	function like($id_komentar){
		echo json_encode($this->IklanModel->like($id_komentar));
	}
	function unlike($id_komentar){
		echo json_encode($this->IklanModel->unlike($id_komentar));
	}
}