<?php
	class Main extends CI_Controller{
		var $data;
		function __construct(){
			parent::__construct();
			$this->load->library('template_web');
			$this->load->model('UserModel');
			$this->load->model('FavoritModel');
			$this->load->model('KategoriModel');
			$this->load->model('ProvinsiModel');
			$this->load->model('MallModel');
			$this->load->model('IklanModel');
			$this->load->model('TemplateModel');
			$this->load->model('WebsiteReplikaModel');
			$this->load->model('SliderReplikaModel');
			$this->data['favorit'] = $this->FavoritModel->getFavoritByUsername($this->session->userdata('username'));
			$this->data['kategori'] = $this->KategoriModel->getKategori();
			$this->data['mall'] = $this->MallModel->getMall();
			$this->data['profil'] = $this->UserModel->getProfil($this->session->userdata('username'));

		}
		function index(){
			$data = $this->data;
			$data['sub1kategori'] = $this->KategoriModel->getSub1Kategori();
			$data['provinsi'] = $this->ProvinsiModel->getProvinsi();
			$data['featured'] = $this->IklanModel->getFeatured();
			$this->template_web->display('web/content/home3', $data);
		}
		
		function website($alamat, $aksi='index', $seo=null){
			$data = $this->data;
			$website = $this->WebsiteReplikaModel->getWebsiteByAlamat($alamat);
			if($website->langkah_pembuatan == 3){
				$this->db->update('website_replika', ['langkah_pembuatan'=>4], ['id_website'=>$website->id_website]);
			}
			$template = $this->TemplateModel->getTemplateByUsername($website->username);
			$data['website'] = $website;
			switch ($aksi) {
				case 'index':
					$data['iklan'] = $this->IklanModel->getIklanByUser($website->username);
					$data['slider'] = $this->SliderReplikaModel->getSliderByWebsite($website->id_website);
					if($template->template == 1){
						$this->load->view('replika/template/1/header', $data);
						$this->load->view('replika/template/1/index', $data);
						$this->load->view('replika/template/1/footer');
					}elseif ($template->template == 2) {
						$this->load->view('replika/template/2/header', $data);
						$this->load->view('replika/template/2/index', $data);
						$this->load->view('replika/template/2/footer');
					}
					break;
				case 'about-us':
					if($template->template == 1){
						$this->load->view('replika/template/1/header', $data);
						$this->load->view('replika/template/1/aboutus', $data);
						$this->load->view('replika/template/1/footer');
					}elseif ($template->template == 2) {
						$this->load->view('replika/template/2/header', $data);
						$this->load->view('replika/template/2/kontakkami', $data);
						$this->load->view('replika/template/2/footer');
					}
					break;
				case 'detail':
					$data['iklan'] = $this->IklanModel->detail_iklan($seo);
					//$data['gambar'] = 
					if($template->template == 1){
						$this->load->view('replika/template/1/header', $data);
						$this->load->view('replika/template/1/detail', $data);
						$this->load->view('replika/template/1/footer');
					}elseif ($template->template == 2) {
						$this->load->view('replika/template/2/header', $data);
						$this->load->view('replika/template/2/detail', $data);
						$this->load->view('replika/template/2/footer');
					}
					break;
				case 'produk-kami':
					$data['iklan'] = $this->IklanModel->getIklanByUser($website->username);
					if($template->template == 1){
						$this->load->view('replika/template/1/header', $data);
						$this->load->view('replika/template/1/produkkami', $data);
						$this->load->view('replika/template/1/footer');
					}elseif ($template->template == 2) {
						$this->load->view('replika/template/2/header', $data);
						$this->load->view('replika/template/2/produkkami', $data);
						$this->load->view('replika/template/2/footer');
					}
					break;
				case 'hubungi-kami':
					if($template->template == 1){
						$this->load->view('replika/template/1/header', $data);
						$this->load->view('replika/template/1/hubungikami', $data);
						$this->load->view('replika/template/1/footer');
					}elseif ($template->template == 2) {
						$this->load->view('replika/template/2/header', $data);
						$this->load->view('replika/template/2/hubungikami', $data);
						$this->load->view('replika/template/2/footer');
					}
					break;
				default:
					# code...
					break;
			}
		}

		function register($aksi = 'index'){
			$data = $this->data;
			if($this->session->userdata('tipe_user') == 'member'){
				redirect(base_url().'iklanku');
			}
			switch ($aksi) {
				case 'index':
					$this->template_web->display('web/content/register', $data);
					break;
				case 'do':
					$this->UserModel->register();

					break;
				default:
					# code...
					break;
			}
		}

		function registered(){
			$data = $this->data;
			$this->template_web->display('web/content/member/registered', $data);
		}
		function about(){
			$data = $this->data;
			$this->template_web->display('web/content/about us', $data);
		}
		function blog(){
			$data = $this->data;
			$this->template_web->display('web/content/blog', $data);
		}
		function contact(){
			$data = $this->data;
			$this->template_web->display('web/content/contact us', $data);
		}
		function konfirmasi($token){
			$data = $this->data;
			$this->UserModel->getUserByToken($token);
		}

		function login($aksi='index'){
			$data = $this->data;
			if($this->session->userdata('tipe_user') == 'member'){
				redirect(base_url().'iklanku');
			}
			switch ($aksi) {
				case 'index':
					$this->template_web->display('web/content/login', $data);
					break;
				case 'do':
					$login = $this->input->post();
					$this->do_login($login);
					break;
				default:
					# code...
					break;
			}
		}
		function aksa_login($aksi='index'){
			$data = $this->data;
			switch ($aksi) {
				case 'index':
					$this->load->view('admin/content/login');
					break;
				case 'do':
					$this->UserModel->loginadmin();
					//redirect(base_url().'aksa_admin');
					break;
				default:
					# code...
					break;
			}
		}
		function do_login($login){
			$cek_login  = $this->db->get_where('member_akun', ['username'=>$login['username'], 'password'=>md5($login['password'])]);
			if($cek_login->num_rows() == 1){
				$data_user = $cek_login->row();
				if($data_user->status != 0){
					$newsession = [
						'username' => $data_user->username,
						'tipe_user' => 'member'
					];
					$this->session->set_userdata($newsession);
					redirect(base_url().'iklanku');
				}else{
					$newsession = [
						'username' => $data_user->username,
						'tipe_user' => 'member'
					];
					$this->session->set_userdata($newsession);
					redirect(base_url().'iklanku');
				}
				
			}else{
				$this->session->set_flashdata('login_gagal', 'Username atau Password tidak terdaftar');
				redirect(base_url().'main/login');
			}
		}

		function expired($token){
			if($this->session->userdata('username')){
				redirect(base_url().'iklanku');
			}else{
				$data = $this->data;
				$this->template_web->display('web/content/expiredtoken', $data);
			}
			
			//echo "Token has expired";
		}


		function logout(){
			$this->session->sess_destroy();
			redirect(base_url());
		}
	}