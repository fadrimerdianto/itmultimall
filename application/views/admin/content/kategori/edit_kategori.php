		<!-- BEGIN CONTENT -->
		<?php $kategori = $kategori->row(); ?>
		<div class="page-content-wrapper">
			<div class="page-content">
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Kategori <small>Edit</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Kategori</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Edit</a>
						</li>
					</ul>
					
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					
					<div class="col-md-12 ">
						<!-- BEGIN SAMPLE FORM PORTLET-->
						<div class="portlet box green ">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gift"></i> Edit Kategori
								</div>
								
							</div>
							<div class="portlet-body form">
								<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'aksa_admin/kategori/update/'.$kategori->id_kategori; ?>" enctype="multipart/form-data">
									<div class="form-body">
										<div class="form-group">
											<label class="col-md-3 control-label">Kategori</label>
											<div class="col-md-9">
												<input required name="kategori" type="text" class="form-control input-inline input-medium" placeholder="Masukkan Kategori" value="<?php echo $kategori->kategori;?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Icon</label>
											<div class="col-md-9">
												<img src="<?php echo base_url().'images/kategori/'.$kategori->icon;?>" width="100">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label" for="exampleInputFile1">Ganti Icon</label>
											<div class="col-md-9">
												<input type="file" id="exampleInputFile1" name="foto_kategori">
												<p class="help-block">
													Icon JPEG, JPG, PNG.
												</p>
											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" class="btn green">Update</button>
												<button type="button" class="btn default" onclick=self.history.back()>Cancel</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- END SAMPLE FORM PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->