	<?php $sub = $sub2kategori->row();?>
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Sub Kategori <small>Edit</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Sub Kategori</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Edit</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> Edit Sub Kategori
							</div>
							
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'aksa_admin/kategori/updatesub2/'.$sub->id_sub2_kategori;?>">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Sub Kategori</label>
										<div class="col-md-9">
										<select class="form-control input-inline input-medium" name="id_sub1_kategori">
								<?php 
								$i = 1;
								foreach ($sub1kategori as $q_kategori) {
									if ($q_kategori->id_sub1_kategori == $sub->id_sub1_kategori) {
										echo '<option Value="'.$q_kategori->id_sub1_kategori.'" selected>'.$q_kategori->kategori.' -> ' .$q_kategori->sub1_kategori.' </option>';
									} else {
										echo '<option Value="'.$q_kategori->id_sub1_kategori.'" >'.$q_kategori->kategori.' -> ' .$q_kategori->sub1_kategori.' </option>';
									}
								}
								?>
										</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Sub 2 Kategori</label>
										<div class="col-md-9">
											<input required name="sub2_kategori" type="text" class="form-control input-inline input-medium" placeholder="Masukkan Sub Kategori" value="<?php echo $sub->sub2_kategori;?>">
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Update</button>
											<button type="button" class="btn default" onclick=self.history.back()>Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>