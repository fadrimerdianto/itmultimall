		<div class="page-content-wrapper">
			<div class="page-content">
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Kategori <small>Data Kategori</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Kategori</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Data Sub 1 Kategori</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<?php 
					if($this->session->userdata('approve_iklan')){
				?>
					<div class="alert alert-success alert-dismissable">
						<strong>Berhasil!</strong> <?php echo $this->session->userdata('approve_iklan') ?>
					</div>
				<?php
					}
				?>
				
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-cogs"></i>Data Sub 1 Kategori
								</div>
								
							</div>
							
							<div class="portlet-body">
								<div>
									<a href="<?php echo base_url(); ?>aksa_admin/kategori/tambahsub1"><button class="btn btn-primary">Tambah</button></a>
								</div>
								<div class="table-scrollable">
									<table class="table table-hover">
									<thead>
									<tr>
										<th> NO. </th>
										<th> Sub Kategori </th>
										<th> Kategori </th>
										<th> Aksi </th>
									</tr>
									</thead>
									<tbody>
									<?php 
										$i = 1;
										foreach ($subkategori as $q_kategori) {
											$ka = $this->KategoriModel->KategoriById($q_kategori->id_kategori);
											echo '
												<tr>
													<td> '.$i.' </td>
													<td> '.$q_kategori->sub1_kategori.' </td>
													<td> '.$ka->row()->kategori.' </td>												
													<td><a href="'.base_url().'aksa_admin/kategori/editsub1/'.$q_kategori->id_sub1_kategori.'"><button class="btn btn-success">Edit</button></a>
														<a href="'.base_url().'aksa_admin/kategori/deletesub1/'.$q_kategori->id_sub1_kategori.'" onClick="return confirm(\'Apakah Anda benar-benar mau menghapusnya?\')"><button class="btn btn-warning">Delete</button></a>
													</td>
												</tr>
											';
											$i++;
										}
									?>
									</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>