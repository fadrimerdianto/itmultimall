		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Sub 2 Kategori <small>Tambah</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Sub 2 Kategori</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Tambah</a>
						</li>
					</ul>
					
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					
					<div class="col-md-12 ">
						<!-- BEGIN SAMPLE FORM PORTLET-->
						<div class="portlet box green ">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gift"></i> Tambah Sub 2 Kategori
								</div>
								
							</div>
							<div class="portlet-body form">
								<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>aksa_admin/kategori/insert_sub2">
									<div class="form-body">
										<div class="form-group">
											<label class="col-md-3 control-label">Sub 1 Kategori</label>
											<div class="col-md-9">
											<select class="form-control input-inline input-medium" name="id_sub1_kategori">
									<?php 
										$i = 1;
										foreach ($sub1kategori as $q_kategori) {
											echo '<option Value="'.$q_kategori->id_sub1_kategori.'" >'.$q_kategori->kategori.' -> ' .$q_kategori->sub1_kategori.' </option>';
										}
									?>
										
											</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Nama Sub 2 Kategori</label>
											<div class="col-md-9">
												<input required name="sub2_kategori" type="text" class="form-control input-inline input-medium" placeholder="Masukkan Sub 2 Kategori">
											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" class="btn green">Submit</button>
												<button type="button" class="btn default">Cancel</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- END SAMPLE FORM PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->