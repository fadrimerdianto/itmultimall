<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN STYLE CUSTOMIZER -->
				
				<!-- END STYLE CUSTOMIZER -->
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Basic Datatables <small>basic datatable samples</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Data Tables</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Basic Datatables</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet red box form">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-newspaper-o"></i>Detail Iklan
								</div>
								
							</div>
							<div class="portlet-body">
								<div class="row static-info">
									<div class="col-md-3 name">
										 Judul Iklan
									</div>
									<div class="col-md-9 value">
										<?php echo $iklan->judul_iklan; ?>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-3 name">
										Deskripsi Iklan
									</div>
									<div class="col-md-9 value">
										<?php echo $iklan->deskripsi_iklan; ?>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-3 name">
										Owner
									</div>
									<div class="col-md-9 value">
										<?php echo $iklan->owner; ?>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-3 name">
										Harga
									</div>
									<div class="col-md-9 value">
										Rp. <?php echo number_format($iklan->harga_iklan); ?>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-3 name">
										Website
									</div>
									<div class="col-md-9 value">
										<a target="_blank" href="http://<?php echo $iklan->web_iklan; ?>"><?php echo $iklan->web_iklan; ?></a>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-3 name">
										Gambar Iklan
									</div>
									<div class="col-md-9 value">
										<?php 
											foreach ($iklan->gambar as $q_gambar) {
												echo '
													<div class="col-md-3">
														<img class="img-responsive img-thumbnail" src="'.base_url().'images/iklan/'.$q_gambar->photo.'" />
													</div>
													
												';
											}
										?>
									</div>
								</div>
								<div class="form-actions">
									<a href="<?php echo base_url(); ?>aksa_admin/iklan/approve/<?php echo $iklan->id_iklan; ?>"><button type="submit" class="btn btn-success">Approve</button></a>
									<button type="button" class="btn btn-danger">Decline</button>
								</div>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>