		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN STYLE CUSTOMIZER -->
				
				<!-- END STYLE CUSTOMIZER -->
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Iklan <small>Baru</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Iklan</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Iklan Baru</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<?php 
					if($this->session->userdata('approve_iklan')){
				?>
					<div class="alert alert-success alert-dismissable">
						<strong>Berhasil!</strong> <?php echo $this->session->userdata('approve_iklan') ?>
					</div>
				<?php
					}
				?>
				
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-cogs"></i>Iklan Baru
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse">
									</a>
									<a href="#portlet-config" data-toggle="modal" class="config">
									</a>
									<a href="javascript:;" class="reload">
									</a>
									<a href="javascript:;" class="remove">
									</a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-scrollable">
									<table class="table table-hover">
									<thead>
									<tr>
										<th>
											 #
										</th>
										<th>
											 Judul Iklan
										</th>
										<th>
											 Owner
										</th>
										<th>
											 Aksi
										</th>
									</tr>
									</thead>
									<tbody>
									<?php 
										$i = 1;
										foreach ($newiklan as $q_newiklan) {
											echo '
												<tr>
													<td>
														 '.$i.'
													</td>
													<td>
														'.$q_newiklan->judul_iklan.'
													</td>
													
													<td>
														 '.$q_newiklan->owner.'
													</td>
													<td>
														<a href="'.base_url().'aksa_admin/iklan/approve/'.$q_newiklan->id_iklan.'"><button class="btn btn-success">Approve</button></a>
														<button class="btn btn-danger">Decline</button>
														<a href="'.base_url().'aksa_admin/iklan/detail/'.$q_newiklan->seo_iklan.'"><button class="btn btn-primary">Detail</button>
													</td>
												</tr>
											';
											$i++;
										}
									?>
									</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>