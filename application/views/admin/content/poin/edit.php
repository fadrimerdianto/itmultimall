	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Poin <small>Edit</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url(); ?>aksa_admin">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>aksa_admin/poin">Poin</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Edit</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> Edit Poin
							</div>
							
						</div>
						<div class="portlet-body form">
							<form enctype="multipart/form-data" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'aksa_admin/poin/update/'.$poin->id_poin; ?>">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Nama Poin</label>
										<div class="col-md-9">
											<input required name="poin" type="text" class="form-control input-inline input-medium" value="<?php echo $poin->nama_poin; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Jumlah</label>
										<div class="col-md-9">
											<input required name="jumlah_poin" type="text" class="form-control input-inline input-medium"  value="<?php echo $poin->jumlah_poin; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Harga</label>
										<div class="col-md-9">
											<input required name="harga" type="text" class="form-control input-inline input-medium" value="<?php echo $poin->harga_poin; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Gambar Poin</label>
										<div class="col-md-9">
											<?php 
												if($poin->gambar_poin != '' AND $poin->gambar_poin != NULL){
													echo '<img style="width:100px" src="'.base_url().'images/poin/'.$poin->gambar_poin.'">';
												}
											?>
											<input type="file" name="gambar_poin"/>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="button" onclick="self.history.back()" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>