	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Toko 
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url(); ?>aksa_admin">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Toko</a>
					</li>
				</ul>
				
			</div>
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Toko
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-hover">
								<thead>
								<tr>
									<th> No </th>
									<th> Nama Toko </th>
									<th> Lokasi Toko </th>
									<th> Kontak Toko </th>
									<th> Status </th>
									<th> Aksi </th>
								</tr>
								</thead>
								<tbody>
								<?php 
									$i = 1;
									foreach ($toko as $q_result) {
										echo '
											<tr>
												<td>'.$i.'</td>
												<td>'.$q_result->nama_toko.'</td>
												<td>'.$q_result->alamat_toko.'</td>
												<td>'.$q_result->kontak_toko.'</td>
												<td>';
												if($q_result->status==0)
												{

												echo 'Belum terverifikasi';
												}

												else echo 'Sudah terverifikasi';?>
												</td>
												<td>
													<a href="#"><button class="btn btn-primary">Edit</button>
													<a href="#"><button class="btn btn-danger" onClick="return confirm(\'Apakah Anda benar-benar mau menghapusnya?\')">Hapus</button></a>
												</td>
											</tr>
										<?php
										$i++;
									}
								?>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>