	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Berita <small>Tambah</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url(); ?>aksa_admin">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>aksa_admin/berita">Berita</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Tambah</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> Tambah Berita
							</div>
							
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>aksa_admin/berita/insert">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Judul</label>
										<div class="col-md-9">
											<input required name="judul" type="text" class="form-control input-inline input-medium" placeholder="Judul Berita">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Sumber</label>
										<div class="col-md-9">
											<input required name="sumber_berita" type="text" class="form-control input-inline input-medium" placeholder="http://">
										</div>
									</div>
									<div class="form-group last">
										<label class="col-md-3 control-label">Isi Berita</label>
										<div class="col-md-8">
											<textarea class="wysihtml5 form-control" rows="6" name="isi" ></textarea>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="button" onclick="self.history.back()" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>