	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Iklan 
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url(); ?>aksa_admin">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Berita</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<?php 
				if($this->session->userdata('approve_iklan')){
			?>
				<div class="alert alert-success alert-dismissable">
					<strong>Berhasil!</strong> <?php echo $this->session->userdata('approve_iklan') ?>
				</div>
			<?php
				}
			?>
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Berita
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-hover">
								<thead>
								<tr>
									<th> No </th>
									<th> Judul Berita </th>
									<th> Sumber </th>
									<th> Aksi </th>
								</tr>
								</thead>
								<tbody>
								<?php 
									$i = 1;
									foreach ($berita as $q_result) {
										echo '
											<tr>
												<td>'.$i.'</td>
												<td>'.$q_result->judul_berita.'</td>
												<td><a href="'.$q_result->sumber_berita.'" target="_blank"> '.$q_result->sumber_berita.'</a>
												</td>
												<td>
													<a href="'.base_url().'aksa_admin/berita/edit/'.$q_result->seo_berita.'"><button class="btn btn-primary">Edit</button>
													<a href="'.base_url().'aksa_admin/berita/delete/'.$q_result->id_berita.'"><button class="btn btn-danger" onClick="return confirm(\'Apakah Anda benar-benar mau menghapusnya?\')">Hapus</button></a>
												</td>
											</tr>
										';
										$i++;
									}
								?>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>