	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Poin <small>Tambah</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url(); ?>aksa_admin">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>aksa_admin/poin">Poin</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Tambah</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> Tambah Poin
							</div>
							
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>aksa_admin/poin/tambah">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Nama Poin</label>
										<div class="col-md-9">
											<input required name="poin" type="text" class="form-control input-inline input-medium" placeholder="Nama Poin">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Jumlah Poin</label>
										<div class="col-md-9">
											<input required name="jumlah_poin" type="text" class="form-control input-inline input-medium" placeholder="Jumlah Poin">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Harga Poin</label>
										<div class="col-md-9">
											<input required name="harga" type="text" class="form-control input-inline input-medium" placeholder="Harga Poin">
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="button" onclick="self.history.back()" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>