		<div class="page-content-wrapper">
			<div class="page-content">
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
					Poin <small>Transaksi Pembelian Poin</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Poin</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Transaksi Pembelian Poin</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<?php 
					if($this->session->userdata('approve_transaksi')){
				?>
					<div class="alert alert-success alert-dismissable">
						<strong>Berhasil!</strong> <?php echo $this->session->userdata('approve_transaksi') ?>
					</div>
				<?php
					}
				?>
				
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-cogs"></i>Transaksi Pembelian Poin
								</div>
								
							</div>
							<div class="portlet-body">
								<div class="table-scrollable">
									<table class="table table-hover">
									<thead>
									<tr>
										<th>
											 #
										</th>
										<th>
											Nama Member
										</th>
										<th>
											Tanggal Transaksi
										</th>
										<th>
											Detail Transaksi
										</th>
									</tr>
									</thead>
									<tbody>
									<?php 
										$i = 1;
										foreach ($transaksi as $q_transaksi) {
											echo '
												<tr>
													<td>
														 '.$i.'
													</td>
													<td>
														'.$q_transaksi->nama_member.'
													</td>
													<td>
														'.$q_transaksi->tanggal_transaksi.'
													</td>
													<td>
														<a href="'.base_url().'aksa_admin/poin/detailtransaksi/'.$q_transaksi->id_transaksi_poin.'"><button class="btn btn-primary">Detail Transaksi</button></a>
													</td>
												</tr>
											';
											$i++;
										}
									?>
									</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>