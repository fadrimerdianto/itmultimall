<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Poin <small>Detail Transaksi Poin</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Poin</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Transaksi</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Detail</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet red box form">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-newspaper-o"></i>Detail Transaksi
								</div>
								
							</div>
							<div class="portlet-body">
								<div class="row static-info">
									<div class="col-md-3 name">
										Tanggal Transaksi
									</div>
									<div class="col-md-9 value">
										<?php echo convertDateTime($transaksi->tanggal_transaksi); ?>
									</div>
								</div>

								<div class="row static-info">
									<div class="col-md-3 name">
										Nama Member
									</div>
									<div class="col-md-9 value">
										<?php echo $transaksi->nama_member; ?>
									</div>
								</div>

								<div class="row static-info">
									<div class="col-md-3 name">
										Nama Poin
									</div>
									<div class="col-md-9 value">
										<?php echo $transaksi->nama_poin; ?>
									</div>
								</div>

								<div class="row static-info">
									<div class="col-md-3 name">
										Nama Poin
									</div>
									<div class="col-md-9 value">
										<?php echo number_format($transaksi->harga_poin); ?>
									</div>
								</div>

								<div class="row static-info">
									<div class="col-md-3 name">
										Biaya Yang Dibayar
									</div>
									<div class="col-md-9 value">
										<?php echo number_format($transaksi->biaya_transaksi); ?>
									</div>
								</div>
								<form method="post" action="<?php echo base_url(); ?>aksa_admin/poin/approve">
									<input name="id_transaksi" type="hidden" value="<?php echo $transaksi->id_transaksi_poin; ?>" />
									<div class="form-actions">
										<button type="submit" class="btn btn-success">Approve</button>
									</div>
								</form>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>