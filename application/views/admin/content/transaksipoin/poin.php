		<div class="page-content-wrapper">
			<div class="page-content">
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
					Poin <small>data Poin</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Poin</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<?php 
					if($this->session->userdata('approve_poin')){
				?>
					<div class="alert alert-success alert-dismissable">
						<strong>Berhasil!</strong> <?php echo $this->session->userdata('approve_poin') ?>
					</div>
				<?php
					}
				?>
				
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-cogs"></i>Data Poin
								</div>
								
							</div>
							<div class="portlet-body">
								<div class="table-scrollable">
									<table class="table table-hover">
									<thead>
									<tr>
										<th>
											 #
										</th>
										<th>
											Poin
										</th>
										<th>
											Jumlah
										</th>
										<th>
											Harga
										</th>
									</tr>
									</thead>
									<tbody>
									<?php 
										$i = 1;
										foreach ($poin->result() as $q_poin) {
											echo '
												<tr>
													<td>
														 '.$i.'
													</td>
													<td>
														'.$q_poin->nama_poin.'
													</td>
													<td>
														'.$q_poin->jumlah_poin.'
													</td>
													<td>
													<a href="'.base_url().'aksa_admin/poin/edit/'.$q_poin->id_poin.'"><button class="btn btn-primary">Edit</button>
													<a href="'.base_url().'aksa_admin/poin/delete/'.$q_poin->id_poin.'"><button class="btn btn-danger" onClick="return confirm(\'Apakah Anda benar-benar mau menghapusnya?\')">Hapus</button></a>
													</td>
												</tr>
											';
											$i++;
										}
									?>
									</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>