	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Premium Iklan <small>Data Premium Iklan</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Premium Iklan</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Data Premium Iklan</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<?php 
				if($this->session->userdata('tambah_iklan')){
			?>
				<div class="alert alert-success alert-dismissable">
					<strong>Berhasil!</strong> <?php echo $this->session->userdata('tambah_iklan') ?>
				</div>
			<?php
				}
			?>
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Data Premium Iklan
							</div>
							
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-hover">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th> Premium Iklan </th>
									<th> Masa Aktif </th>
									<th> Aksi </th>
								</tr>
								</thead>
								<tbody>
								<?php 
									$i = 1;
									foreach ($premium as $q_premium) {
									echo '
										<tr>
											<td> '.$i.' </td>
											<td> '.$q_premium->nama_premium.' </td>
											<td> '.$q_premium->masa_aktif.' hari</td>
											<td>
												<a href="'.base_url().'aksa_admin/premiumiklan/edit/'.$q_premium->id_premium.'"><button class="btn btn-success">Edit</button></a>
												<a href="'.base_url().'aksa_admin/premiumiklan/delete/'.$q_premium->id_premium.'" onClick="return confirm(\'Apakah Anda benar-benar mau menghapusnya?\')"><button class="btn btn-warning">Delete</button></a>
											</td>
										</tr>
									';
										$i++;
									}
								?>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>