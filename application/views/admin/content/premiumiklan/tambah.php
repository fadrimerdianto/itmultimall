	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Premium Iklan <small>Tambah</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url(); ?>aksa_admin">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>aksa_admin/premium">Premium Iklan</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Tambah</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> Tambah Premium Iklan
							</div>
							
						</div>
						<div class="portlet-body form">
							<form enctype="multipart/form-data" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>aksa_admin/premiumiklan/insert">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Premium Iklan</label>
										<div class="col-md-9">
											<input required name="nama" type="text" class="form-control input-inline input-medium" placeholder="Nama Premium Iklan">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Harga Premium Iklan</label>
										<div class="col-md-9">
											<input required name="harga" type="text" class="form-control input-inline input-medium" placeholder="Harga Premium Iklan">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Masa Aktif Iklan</label>
										<div class="col-md-9">
											<input required name="masa" type="text" class="form-control input-inline input-medium" placeholder="Masa Aktif Iklan"> Hari
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Gambar Premium Iklan</label>
										<div class="col-md-9">
											<input type="file" name="gambar" required />
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="button" onclick="self.history.back()" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>