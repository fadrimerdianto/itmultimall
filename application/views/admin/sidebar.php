<div class="page-sidebar-wrapper">
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
				<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
				<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
				<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
				<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
				<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
				<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
					<!-- <li class="start ">
						<a href="<?php echo base_url(); ?>aksa_admin">
						<i class="icon-basket"></i>
						<span class="title">eCommerce</span>
						</a>
					</li> -->
					<li class="active open">
						<a href="<?php echo base_url(); ?>aksa_admin">
						<i class="icon-home"></i>
						<span class="title">Dashboard</span>
						<span class="selected"></span>
						<span class="arrow open"></span>
						</a>
						<!-- <ul class="sub-menu">
							<li class="active">
								<a href="#">
								<i class="icon-home"></i>
								Dashboard</a>
							</li>
							<li>
								<a href="#">
								<i class="icon-basket"></i>
								Orders</a>
							</li>
							<li>
								<a href="ecommerce_orders_view.html">
								<i class="icon-tag"></i>
								Order View</a>
							</li>
							<li>
								<a href="ecommerce_products.html">
								<i class="icon-handbag"></i>
								Products</a>
							</li>
							<li>
								<a href="ecommerce_products_edit.html">
								<i class="icon-pencil"></i>
								Product Edit</a>
							</li>
						</ul> -->
					</li>

					<li>
						<a href="javascript:;">
						<i class="icon-paper-clip "></i>
						<span class="title">Kategori</span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/kategori">
								<i class="icon-home"></i>
								Data Kategori</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/kategori/tambah">
								<i class="icon-basket"></i>
								Tambah Kategori</a>
							</li>

							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/kategori/sub1">
								<i class="icon-home"></i>
								Data Sub 1 Kategori</a>
							</li>

							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/kategori/tambahsub1">
								<i class="icon-basket"></i>
								Tambah Sub 1 Kategori</a>
							</li>

							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/kategori/sub2">
								<i class="icon-home"></i>
								Data Sub 2 Kategori</a>
							</li>

							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/kategori/tambahsub2">
								<i class="icon-basket"></i>
								Tambah Sub 2 Kategori</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:;">
						<i class="icon-docs"></i>
						<span class="title">Berita</span>
						<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li class="active">
								<a href="<?php echo base_url(); ?>aksa_admin/berita">
								<i class="icon-home"></i>
								Data</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/berita/tambah">
								<i class="icon-basket"></i>
								Tambah</a>
							</li>
						</ul>
					</li>

					<li>
						<a href="javascript:;">
						<i class="icon-grid "></i>
						
						<span class="title">Iklan <span class="badge badge-roundless badge-danger">baru</span></span>
						</a>
						<ul class="sub-menu">
							<li class="active">
								<a href="<?php echo base_url(); ?>aksa_admin/iklan/all">
								<i class="icon-home"></i>
								Data</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/iklan/baru">
								<i class="icon-basket"></i>
								Baru</a>
							</li>
							<li>
								<a href="#">
								<i class="icon-tag"></i>
								Order View</a>
							</li>
							<li>
								<a href="#">
								<i class="icon-handbag"></i>
								Products</a>
							</li>
							<li>
								<a href="#">
								<i class="icon-pencil"></i>
								Product Edit</a>
							</li>
						</ul>
					</li>

					<li>
						<a href="javascript:;">
						<i class="icon-docs"></i>
						<span class="title">Mall</span>
						<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li class="active">
								<a href="<?php echo base_url(); ?>aksa_admin/mall">
								<i class="icon-home"></i>
								Data</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/mall/tambah">
								<i class="icon-basket"></i>
								Tambah</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:;">
						<i class="icon-users "></i>
						<span class="title">Member</span>
						</a>
						<ul class="sub-menu">
							<li class="active">
								<a href="<?php echo base_url(); ?>aksa_admin/member">
								<i class="icon-home"></i>
								Data</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/member/baru">
								<i class="icon-basket"></i>
								Baru</a>
							</li>
						</ul>
					</li>

					<li>
						<a href="javascript:;">
						<i class="icon-users "></i>
						<span class="title">Poin</span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/poin">
								<i class="icon-home"></i>
								Poin</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/poin/tambah">
								<i class="icon-home"></i>
								Tambah Poin</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/poin/konfirmasi">
								<i class="icon-basket"></i>
								Transaksi Poin</a>
							</li>
						</ul>
					</li>

					<li>
						<a href="javascript:;">
						<i class="icon-users "></i>
						<span class="title">Iklan Premium</span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/premiumiklan">
								<i class="icon-home"></i>
								Data</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>aksa_admin/premiumiklan/tambah">
								<i class="icon-basket"></i>
								Tambah Iklan</a>
							</li>
						</ul>
					</li>
					<!-- <li class="last ">
						<a href="javascript:;">
						<i class="icon-pointer"></i>
						<span class="title">Maps</span>
						<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="maps_google.html">
								Google Maps</a>
							</li>
							<li>
								<a href="maps_vector.html">
								Vector Maps</a>
							</li>
						</ul>
					</li> -->
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>