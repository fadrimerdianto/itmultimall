<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>IT Multi Mall</title>
    <meta name="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="wikiloka.com">
    <?php
        if($this->uri->segment(1) == 'iklan' AND $this->uri->segment(2) == 'detail'){
            $d= $detail;
            $gbr = $this->IklanModel->gbr_iklan($d->id_iklan);
            $gbr1 = $gbr->row(); 
    ?> 
    <meta property="og:url" content="<?php echo base_url() ?>iklan/detail/<?php echo $detail->seo_iklan ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo $detail->judul_iklan ?>" />
    <meta property="og:description" content="<?php echo strip_tags($detail->deskripsi_iklan) ?>" />
    <?php 
        }
    ?>
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/files/favicon.png">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon--> 
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon--> 
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css">
    <!-- =============== Google fonts =============== -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <style type="text/css">
        .notifyjs-foo-base {
            z-index: 10001;
        }
    </style>

    <!-- Font Awesome CSS -->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/social-button.css" rel="stylesheet">

    <!-- Flexslider -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/flexslider/css/flexslider.css" type="text/css" media="screen" />

    <!-- Nivo Slider CSS -->
    <link href="<?php echo base_url(); ?>assets/css/nivo-slider.css" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="<?php echo base_url(); ?>assets/css/jquery.bxslider.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/cloud-zoom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/owl.carousel.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <!-- Boxed Style CSS -->
    <link href="<?php echo base_url(); ?>assets/css/style-boxed.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <?php echo $_javascript; ?>
    </head>
    <body onscroll="nggantung(event);">
        <!-- ========> Facebook HTML5 script: <========= -->
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>


      <?php echo $_header; ?>

      <?php echo $_content; ?>

      <?php 
    //   if($this->uri->segment(1) != '' AND $this->uri->segment(1) != 'login' AND ($this->uri->segment(1) != 'main' AND $this->uri->segment(2) != 'login')){
    //     echo $_footer;
    // }
      echo $_footer;
    ?>


</body>
</html>
