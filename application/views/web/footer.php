 <div class="footer">
        	<div class="container">
            	<div class="footer-top">
                	<div class="row">
                    	<div class="col-md-12  ">
                        	<ul class="social">
                                <li><a href="#" class="face">facebook</a></li>
                                <li><a href="#" class="google">google</a></li>
                                <li><a href="#" class="twitter">twitter</a></li>
                                <li><a href="#" class="youtube">youtube</a></li>
                                <li><a href="#" class="linkedin">linkedin</a></li>
                            </ul>
                        </div>
                    </div>
            	</div><!-- /.footer-top -->
            	<div class="footer-middle">
                	<div class="row">
                    	<div class="col-sm-3">
                        	<div class="footer-title">
                            	<h2>contact us</h2>
                            </div>
                            <div class="footer-content">
                            	<div class="email add">
                            		<p>Support@gmail.com</p>
                            	</div>
                            	<div class="phone add">
                            		<p>(800) 0123 456 789</p>
                            	</div>
                            	<div class="address add">Address: 
                            		<p>1234 - Jl. Dr Ir, Soekarno. Surabaya</p>
                            	</div>
                            	<div class="contact-link"><a href="#" class="btn btn-default">Open in Google Maps</a></div>
                            </div>
                        </div>
                    	<div class="col-sm-3">
                        	<div class="footer-title">
                            	<h2>Footer</h2>
                            </div>
                            <div class="footer-content">
                                <ul>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="col-sm-3">
                        	<div class="footer-title">
                            	<h2>Second &amp; Footer</h2>
                            </div>
                            <div class="footer-content">
                                <ul>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="col-sm-3">
                        	<div class="footer-title">
                            	<h2>Third Footer</h2>
                            </div>
                            <div class="footer-content">
                                <ul>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                    <li><a href="#">Sub Footer</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
            	</div>
            	<div class="footer-bottom">
                	<div class="row">
                    	<div class="col-sm-6">
                        	<div class="copy">Copyright &copy; 2016 Jasa pembuatan Website. All Rights Reserved</div>
                        </div>
                    	<div class="col-sm-6">
                        	<div class="payment"><img src="images/payment.png" alt="" class="img-responsive"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.footer -->