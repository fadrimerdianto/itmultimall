
        <div class="main">
        	<div class="container">
            	<div class="row">  
                    <div class="col-sm-3 col-left">
                    </div><!-- /.col-left -->
                	<div class="col-sm-9">
                    	<ol id="products-list" class="blog-list">
                            <li class="item">
                                <div class="item-inner">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="images-container">
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/blog/blog-03.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <h2 class="product-name"><a title="" href="#">Fusce aliquam</a></h2>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div style="width:67%" class="rating"></div>
                                                </div>
                                                <span class="amount"><a href="#">1 Review(s)</a></span>
                                                <span class="separator">|</span>
                                                <span class="comment-amount"><a href="#">4 comment</a></span>
                                            </div>
                                            <div class="blog-attr">
                                            	<span>Post by <a href="#">Admin</a></span>
                                                <span class="separator">|</span>
                                                <span>On February 09, 2015</span>
                                            </div>
                                            <div class="desc">
                                               Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                                            </div>
                                            <a href="#" class="btn btn-default btn-readmore">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item">
                                <div class="item-inner">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="images-container">
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/blog/blog-04.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <h2 class="product-name"><a title="" href="#">Fusce aliquam</a></h2>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div style="width:67%" class="rating"></div>
                                                </div>
                                                <span class="amount"><a href="#">1 Review(s)</a></span>
                                                <span class="separator">|</span>
                                                <span class="comment-amount"><a href="#">4 comment</a></span>
                                            </div>
                                            <div class="blog-attr">
                                            	<span>Post by <a href="#">Admin</a></span>
                                                <span class="separator">|</span>
                                                <span>On February 09, 2015</span>
                                            </div>
                                            <div class="desc">
                                               Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                                            </div>
                                            <a href="#" class="btn btn-default btn-readmore">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item">
                                <div class="item-inner">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="images-container">
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/blog/blog-05.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <h2 class="product-name"><a title="" href="#">Fusce aliquam</a></h2>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div style="width:67%" class="rating"></div>
                                                </div>
                                                <span class="amount"><a href="#">1 Review(s)</a></span>
                                                <span class="separator">|</span>
                                                <span class="comment-amount"><a href="#">4 comment</a></span>
                                            </div>
                                            <div class="blog-attr">
                                            	<span>Post by <a href="#">Admin</a></span>
                                                <span class="separator">|</span>
                                                <span>On February 09, 2015</span>
                                            </div>
                                            <div class="desc">
                                               Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                                            </div>
                                            <a href="#" class="btn btn-default btn-readmore">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item">
                                <div class="item-inner">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="images-container">
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/blog/blog-06.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <h2 class="product-name"><a title="" href="#">Fusce aliquam</a></h2>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div style="width:67%" class="rating"></div>
                                                </div>
                                                <span class="amount"><a href="#">1 Review(s)</a></span>
                                                <span class="separator">|</span>
                                                <span class="comment-amount"><a href="#">4 comment</a></span>
                                            </div>
                                            <div class="blog-attr">
                                            	<span>Post by <a href="#">Admin</a></span>
                                                <span class="separator">|</span>
                                                <span>On February 09, 2015</span>
                                            </div>
                                            <div class="desc">
                                               Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                                            </div>
                                            <a href="#" class="btn btn-default btn-readmore">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item">
                                <div class="item-inner">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="images-container">
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/blog/blog-07.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <h2 class="product-name"><a title="" href="#">Fusce aliquam</a></h2>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div style="width:67%" class="rating"></div>
                                                </div>
                                                <span class="amount"><a href="#">1 Review(s)</a></span>
                                                <span class="separator">|</span>
                                                <span class="comment-amount"><a href="#">4 comment</a></span>
                                            </div>
                                            <div class="blog-attr">
                                            	<span>Post by <a href="#">Admin</a></span>
                                                <span class="separator">|</span>
                                                <span>On February 09, 2015</span>
                                            </div>
                                            <div class="desc">
                                               Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                                            </div>
                                            <a href="#" class="btn btn-default btn-readmore">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item">
                                <div class="item-inner">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="images-container">
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/blog/blog-08.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <h2 class="product-name"><a title="" href="#">Fusce aliquam</a></h2>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div style="width:67%" class="rating"></div>
                                                </div>
                                                <span class="amount"><a href="#">1 Review(s)</a></span>
                                                <span class="separator">|</span>
                                                <span class="comment-amount"><a href="#">4 comment</a></span>
                                            </div>
                                            <div class="blog-attr">
                                            	<span>Post by <a href="#">Admin</a></span>
                                                <span class="separator">|</span>
                                                <span>On February 09, 2015</span>
                                            </div>
                                            <div class="desc">
                                               Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                                            </div>
                                            <a href="#" class="btn btn-default btn-readmore">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                    	</ol>
                        <nav>
                          <ul class="pagination">
                            <li><a href="#" aria-label="Previous">Prev</a></li>
                            <li class="divider"><span>|</span></li>
                            <li><a href="#">1</a></li>
                            <li class="divider"><span>|</span></li>
                            <li><a href="#">2</a></li>
                            <li class="divider"><span>|</span></li>
                            <li><a href="#" aria-label="Next">Next</a></li>
                          </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div><!-- /.main -->