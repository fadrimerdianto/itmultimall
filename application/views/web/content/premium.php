<div class="gap130"></div>

        <?php
            // $nama_jenis_iklan = ''; 
            // $baru_aktif = '';
            // $bekas_aktif = '';
            // $semua_aktif = '';
            // if($jenisiklan == 1){
            //     $baru_aktif = 'active';
            //     $nama_jenis_iklan = 'Baru';
            // }elseif($jenisiklan == 2){
            //     $bekas_aktif = 'active';
            //     $nama_jenis_iklan = 'Bekas';
            // }else{
            //     $semua_aktif = 'active';
            // }
        ?>

        <!-- <div class="container">
            <div class="breadcrumbs">
                <ul class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <?php 
                    if($datakategori != null){
                        ?>
                        <li class="active"><?php echo $datakategori->kategori ?> </li>
                        <?php
                    }
                    ?>    
                </ul>
            </div>
        </div> -->
        
        <div class="container container-filter">
            <div class="row">
             <div class="iklan-tengen">
                <img src="<?php echo base_url(); ?>assets/images/iklan-sih-tengen.png">
            </div>
            <div class="iklan-kiwo">
                <img src="<?php echo base_url(); ?>assets/images/iklan-sih-tengen.png">
            </div>
            <div class="col-md-6">
                <!-- <div class="kategori-box">
                    <div class="logo-kategori">
                        <?php 
                        if($datakategori != null){
                            ?>
                            <img src="<?php echo base_url(); ?>images/kategori/<?php echo $datakategori->icon ?>">
                            <?php
                        }else{
                            ?>
                            <img src="<?php echo base_url(); ?>assets/files/favicon.png ?>">
                            <?php
                        }
                        ?>
                    </div>
                    <div class="name-kategori">
                        <?php 
                        if($datakategori != null){
                            ?>
                            <div><?php echo $datakategori->kategori ?> <?php echo $nama_jenis_iklan ?></div>
                            <?php
                        }else{
                            ?>
                            <div>Semua</div>
                            <?php
                        }
                        ?>
                    </div>
                </div> -->
            </div>
            <div class="col-md-6">
                <div class="filterbox">
                    <div class="kategoribox">

                    </div>
                    <div class="filtertool">
                        <!-- <div class="col-md-6">
                            <select class="form-control">
                                <option>Sub Kategori</option>
                            </select>
                        </div> -->
                        <div class="col-md-6">
                            <input type="text" class="form-control input-sm" />

                            <?php 
                            if(sizeof($this->input->get() > 0)){
                                ?>
                                <select onchange="window.location.href=this.value" class="form-control">
                                    <?php
                                }else{
                                    ?>
                                    <select onchange="window.location.href=this.value" class="form-control input-sm">\
                                        <?php
                                    }
                                    ?>
                                    <option selected disabled>Urutan</option>
                                    <?php
                                    $termurah = '';
                                    $terbaru = ''; 
                                    if($this->input->get('sort') == 'termurah'){
                                        $termurah = 'selected';
                                    }elseif($this->input->get('sort') == 'terbaru'){
                                        $terbaru = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $termurah ?> value="?sort=termurah">Termurah</option>
                                    <option <?php echo $terbaru ?> value="?sort=terbaru">Terbaru</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="main">
                <div class="row">
                    <div class="col-sm-12">
\
                    <ol id="products-list" class="products-list">
                        <?php foreach($iklan as $i) { 

                            $gbr = $this->IklanModel->gbr_iklan($i->id_iklan);
                            $gbr = $gbr->row();
                            $isi_berita=strip_tags($i->deskripsi_iklan);
                        $isi = substr($isi_berita,0,100); // ambil sebanyak 00 karakter
                        $isi = substr($isi_berita,0,strrpos($isi," "));

                        $tgl = $i->tanggal_post; 
                        $tanggal = substr($tgl,8,2);
                        $bulan = substr($tgl,5,2);
                        $tahun = substr($tgl,0,4);
                        $jam   = substr($tgl,11,2);
                        $menit = substr($tgl,14,2);
                        $detik = substr($tgl,17,2);
                        $thn_sekarang = date('Y');
                        $bln_sekarang = date('m');

                        // $time = mktime(12, 40, 33, 6, 10, 2009); // 10 July 2009 12:40:33
                        $time = mktime($jam, $menit, $detik, $bulan, $tanggal, $tahun); // 10 July 2009 12:40:33 
                        $timediff = time() - $time; 
                        $tanggal_skr = timeInSentence($timediff, 'id', 1);
                        $tgl_aktif = explode(" ",$tanggal_skr);
                        if ($tahun < $thn_sekarang || $bulan < $bln_sekarang) {
                            $tglnya = tgl_indo($tgl);
                        }else {
                            $tglnya = $tanggal_skr;
                        }

                        if($i->jenis_iklan == 1){
                            $jenis_iklan = 'Baru';
                        }else{
                            $jenis_iklan = 'Bekas';
                        }
                        echo '
                        <li class="item">
                            <div class="item-inner">
                                <div class="row iqbal list-iklan-depan">
                                    <div class="col-sm-2 boby nopadding">
                                        <div class="images-container">
                                            <div class="product_icon nadif">
                                                ';
                                                if($i->status == 1){
                                                    echo '<button style="width:36px; height:36px; padding:0px" title="Hapus Favorit" isFavorit="1" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-unfavorit"><i class="fa fa-star"></i></button>';
                                                }else{
                                                    echo '<button style="width:36px; height:36px;padding:0px" title="Favorit" isFavorit="0" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-favorit"><i class="fa fa-star"></i></button>';
                                                }
                                                echo '
                                                <!--div class="favorit-icon"><a class="disabled-favorit" id_iklan="'.$i->id_iklan.'" onclick="favorit(this)"></a></div-->
                                            </div>
                                            <div class="product_icon fadel">
                                                <div class="label-premium">
                                                    <div class="text-premium">Top 25</div>
                                                </div>
                                                <!--<div class="label-istimewa" style="background: url('.base_url().'assets/files/istimewa.png) no-repeat;"><a href="#"></a></div>-->
                                            </div>
                                            <a class="product-image" style="object-fit:cover/contain;height:150px;" title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'" rel="'.$i->owner.'"><img class="img-cover-list" alt="'.$i->judul_iklan.'" src="'.base_url().'images/iklan/'.$gbr->photo.'"></a>
                                        </div>
                                    </div>
                                    <div class="product-shop col-sm-7 col-sms-4 col-smb-12">
                                        <div class="f-fix">
                                            <h5 class="product-name"><a title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'">'.$i->judul_iklan.'</a><span class="jenis_iklan"><i style="color: rgb(240, 133, 25);" class="fa fa-bookmark"></i> '.$jenis_iklan.'</span></h5>
                                            <div class="list-kategori-iklan">
                                                <label class="label label-warning bck-org"><i class="fa fa-tags"></i> '.$i->kategori->kategori.'</label>
                                                <i class="fa fa-angle-double-right"></i> '.$i->sub1->sub1_kategori.'
                                            </div>
                                            <div style="margin-top:15px">
                                                <i class="fa fa-clock-o icon-interval-time"></i> <div class="text-interval-time">'.$tglnya.'</div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-shop col-sm-3 bar-right nopadding">
                                        <h4 class="nomargin pad-8 bck-org text-center">
                                            <i class="fa fa-tag"></i> 
                                            <label style="color:#000">Rp '.number_format($i->harga_iklan,0,',','.').'</label>
                                        </h4>
                                        <div class="pad-8" style="font-size:14px;">
                                            <ul>
                                                <li>
                                                    <span class="label label-success">
                                                        <i class="fa fa-calendar"></i> &nbsp;'.tgl_indo($tgl).'
                                                    </span>
                                                </li>
                                                <li>
                                                    <a href="'.$i->web_iklan.'">
                                                        <span class="label label-primary">
                                                            <i class="fa fa-chain"></i> Visit Website
                                                        </span>
                                                    </a>
                                                </li>
                                                <li style="border-top:1px dashed#d3d3d3;padding-top:5px;margin-top:10px;">
                                                    <i class="text-small" style="font-size:10px;color:#000">
                                                        <i class="fa fa-map-marker"></i> &nbsp;
                                                        '.$i->kota->nama_area.', '.$i->provinsi->nama_provinsi.' 
                                                    </i>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        ';
                    } ?>
                            <!-- biasa 
                            <li class="item" style="text-align: center;">
                                <a class="" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/contoh-iklan.jpg"></a>
                            </li>
                            <li class="item faisal">
                                <div class="item-inner">
                                    <div class="row iqbal">
                                        <div class="col-sm-2 col-sms-3 col-smb-12 boby">
                                            <div class="images-container">
                                                <div class="product_icon nadif">
                                                    <div class="favorit-icon"><a href="#"></a></div>
                                                </div>
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/products/53448763_1_644x461_alphard-g-atpm-hitam-2012-km-20-ribuan-jakarta-selatan.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-8 col-sms-6 col-smb-12">
                                            <div class="f-fix">
                                                <h5 class="product-name"><a title="Lenovo Yoga" href="detail.html">Lenovo Yoga 3 Core i7 Nvidia 940M 2gb</a></h5>
                                                <p>
                                                    Premium Lenovo Yoga3 dengan Procesor terbaru Core i7 BroadWell Yg super cepat. 
                                                </p>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-2 col-sms-2 col-smb-12">
                                            <div class="f-fix">
                                                    <p class="special-price"><strong>
                                                        <span id="product-price-1" class="price">Rp 10.500.000</span></strong>
                                                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>-->
                        </ol>

                        <nav>
                          <ul class="pagination">
                              <?php echo $paging;?>
                          </ul>
                      </nav>
                  </div><!-- /.col-right -->
              </div>
          </div>
      </div><!-- /.main -->
      <script type="text/javascript">
        function favorit (that) {
            var id = $(that).attr('id_iklan');
            if($(that).attr('isFavorit') === '0'){
                $.ajax({
                    'method':'POST',
                    'url':'<?php echo base_url(); ?>iklanku/favorit/add',
                    'data':{id_iklan:id},
                    'success':showAlert
                }).done(function(data){
                    console.log('data ->' + data);
                    var result = JSON.parse(data);
                    if(result['status'] === 'success'){
                        $(that).removeClass('btn-favorit');
                        $(that).addClass("btn-unfavorit");
                        $(that).attr('isFavorit', '1');
                        var jumlah_lama = $('#jumlah-favorit').text();
                        $('#jumlah-favorit').text(parseInt(jumlah_lama)+1);
                        console.log($(that));
                    }
                });
            }else{
               $.ajax({
                'method':'POST',
                'url':'<?php echo base_url(); ?>iklanku/favorit/remove',
                'data':{id_iklan:id},
                'success':showAlertRemove
            }).done(function(data){
                var result = JSON.parse(data);
                if(result['status'] === 'success'){
                    $(that).removeClass('btn-unfavorit');
                    $(that).addClass("btn-favorit");
                    $(that).attr('isFavorit', '0');
                    var jumlah_lama = $('#jumlah-favorit').text();
                    $('#jumlah-favorit').text(parseInt(jumlah_lama)-1);
                }
            });
        }
    }
</script>