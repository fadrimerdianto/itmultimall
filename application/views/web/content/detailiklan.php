
<style type="text/css">
    /* aku wedi ngganggu global css dadi tak delek kene mboh sembarang sampean pindah ndi ttd hengki */
    .flex-direction-nav .flex-nav-prev a{
        background: transparent !important;
        transition: none;
        opacity: 1;
        z-index: 1000;
        left: 3px;
    }


    #carousel.flexslider .slides img:hover {

        margin-top: -10px;
        box-shadow: 2px 2px 5px #6D6D6D;

    }

    .flexslider .slides {
        overflow-y:show;
    }
    .flexslider .slides img {
        margin-right: auto;
        margin-left: auto;
        transition: all .5s;
        -wenkit-transition: all .5s;
        -moz-transition: all .5s;
        -o-transition: all .5s;
        -ms-transition: all .5s;
    }

    #carousel .flex-direction-nav a.flex-disabled {
       display:none;
   }
   .overflow {
      overflow-x: hidden;
  }
</style>

<?php 
$d= $detail;
$gbr = $this->IklanModel->gbr_iklan($d->id_iklan);
$gbr1 = $gbr->row();
?> 
<style type="text/css">
    .product-name{
        font-size: 22px;
        font-weight: 600;
        color: black;
    }
</style>
<div class="gap130"></div>
<?php 
if($this->session->flashdata('laporan')){
    echo '
    <div class="alert alert-warning alert-dismissible" role="alert" style="margin-left:15%; margin-right:15%">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>Info!</strong> '.$this->session->flashdata('laporan').'
  </div>
  ';
}
?>

<div class="container">
    <div class="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="<?php echo base_url();?>iklan">Iklan</a></li>
            <li class="active"><?php echo $d->judul_iklan; ?></li>
        </ul>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-8">
                <div class="product-name">
                    <h2><strong><?php echo $d->judul_iklan; ?></strong></h2>
                    <ul class="list-detail-iklan list-inline">
                     <!--    <li>
                            <div class="ratings">
                                <div class="rating-box">
                                    <div style="width:67%" class="rating"></div>
                                </div>
                                <span class="amount"><a href="#">1 Review(s)</a></span>
                            </div>
                        </li> -->
                        <li>
                            <div class="ratings">
                                <span class="price"><img src="<?php echo base_url();?>assets/images/jasa-pembuatan-website-direction.png" width="20"> <?php echo $kota->nama_area ?>, <?php echo $kota->provinsi->nama_provinsi ?></span>
                            </div>
                        </li>
                        <li>
                            <div class="ratings">
                                <span class="price">
                                    <i class="fa fa-eye" style="color:rgb(235, 47, 10); font-size:18px;"></i> Dilihat <?php echo $d->dilihat ?> Kali
                                </span>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-sm-8 col-right">
                <div class="product-view">
                    <div  class="col-md-12 overflow">
                        <section class="slider">
                            <div id="slider" class="flexslider" style="margin:0">
                              <ul class="slides">
                                <?php 
                                foreach ($gbr->result() as $g) {

                                    echo '
                                    <li>
                                        <img src="'.base_url().'images/iklan/'.$g->photo.'" alt="'.$d->judul_iklan.'" />
                                    </li>
                                    ';
                                }
                                ?>
                            </ul>
                        </div>
                        <div id="carousel" class="flexslider" style="overflow:visible">
                          <ul class="slides" >
                            <?php 
                            foreach ($gbr->result() as $g) {
                                echo '
                                <li >
                                    <a class="koko" href="#">
                                        <img class="img-rounded img-cover" src="'.base_url().'images/iklan/'.$g->photo.'" alt="'.$d->judul_iklan.'" />
                                    </a>
                                </li>
                                ';
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </section>
                <section class="detail-extra">
                    <?php
                    //rumah
                    if($d->sub1_kategori == 17){
                        ?>
                        <div>
                            <h4>Spesifikasi</h4>
                            <hr>
                            <div class="col-md-6">
                                Luas Tanah : <?php echo $d->luas_tanah ?> m<sup>2</sup>
                            </div>
                            <div class="col-md-6">
                                Luas Bangunan : <?php echo $d->luas_bangunan ?> m<sup>2</sup>
                            </div>
                            <div class="col-md-6">
                                Lantai : <?php echo $d->lantai ?>
                            </div>
                            <div class="col-md-6">
                                Kamar Tidur : <?php echo $d->kamar_tidur ?>
                            </div>
                            <div class="col-md-6">
                                Kamar Mandi : <?php echo $d->kamar_mandi ?>
                            </div>
                            <div class="col-md-6">
                                <?php 
                                if($d->sertifikasi == 1){
                                    $sertifikasi = "SHM (Sertifikat Hak Milik)";
                                }elseif ($d->sertifikasi == 2) {
                                    $sertifikasi = "HGB (Hak Guna Bangunan)";
                                }elseif ($d->sertifikasi == 3) {
                                    $sertifikasi = "Lainnya (PPBJ, Girik, Adat, dll)";
                                }
                                ?>
                                Sertifikasi : <?php echo $sertifikasi; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Lokasi</h4>
                            <hr>
                            <div class="col-md-12">
                                Alamat : <?php echo $d->alamat_lokasi ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Fasilitas</h4>
                            <hr>
                            <?php 
                            if($d->ac == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> AC
                                </div>
                                <?php
                            }
                            if($d->swimming_pool == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Swimmping Pool
                                </div>
                                <?php
                            }
                            if($d->garasi == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Garasi
                                </div>
                                <?php
                            }
                            if($d->refrigerator == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Refrigerator
                                </div>
                                <?php
                            }
                            if($d->fire_extenguisher == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Fire Extenguiser
                                </div>
                                <?php
                            }
                            if($d->telephone == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Telephone
                                </div>
                                <?php
                            }
                            if($d->stove == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Stove
                                </div>
                                <?php
                            }
                            if($d->gordyn == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Gordyn
                                </div>
                                <?php
                            }
                            if($d->carport == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Carport
                                </div>
                                <?php
                            }
                            if($d->pam == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> PAM
                                </div>
                                <?php
                            }
                            if($d->microwave == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Microwave
                                </div>
                                <?php
                            }
                            if($d->garden == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Garden
                                </div>
                                <?php
                            }
                            if($d->water_heater == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Water Heater
                                </div>
                                <?php
                            }
                            if($d->oven == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Oven
                                </div>
                                <?php
                            }


                            ?>
                            
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    //apartemen
                    }elseif ($d->sub1_kategori == 18) {
                        ?>
                        <div>
                            <h4>Spesifikasi</h4>
                            <hr>
                            <div class="col-md-6">
                                Luas Bangunan : <?php echo $d->luas_bangunan ?> m<sup>2</sup>
                            </div>
                            <div class="col-md-6">
                                Lantai : <?php echo $d->lantai ?>
                            </div>
                            <div class="col-md-6">
                                Kamar Tidur : <?php echo $d->kamar_tidur ?>
                            </div>
                            <div class="col-md-6">
                                <?php 
                                if($d->sertifikasi == 1){
                                    $sertifikasi = "SHM (Sertifikat Hak Milik)";
                                }elseif ($d->sertifikasi == 2) {
                                    $sertifikasi = "HGB (Hak Guna Bangunan)";
                                }elseif ($d->sertifikasi == 3) {
                                    $sertifikasi = "Lainnya (PPBJ, Girik, Adat, dll)";
                                }
                                ?>
                                Sertifikasi : <?php echo $sertifikasi; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Lokasi</h4>
                            <hr>
                            <div class="col-md-12">
                                Alamat : <?php echo $d->alamat_lokasi ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Fasilitas</h4>
                            <hr>
                            <?php 
                            if($d->ac == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> AC
                                </div>
                                <?php
                            }
                            if($d->swimming_pool == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Swimmping Pool
                                </div>
                                <?php
                            }
                            if($d->garasi == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Garasi
                                </div>
                                <?php
                            }
                            if($d->refrigerator == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Refrigerator
                                </div>
                                <?php
                            }
                            if($d->fire_extenguisher == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Fire Extenguiser
                                </div>
                                <?php
                            }
                            if($d->telephone == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Telephone
                                </div>
                                <?php
                            }
                            if($d->stove == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Stove
                                </div>
                                <?php
                            }
                            if($d->gordyn == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Gordyn
                                </div>
                                <?php
                            }
                            if($d->carport == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Carport
                                </div>
                                <?php
                            }
                            if($d->pam == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> PAM
                                </div>
                                <?php
                            }
                            if($d->microwave == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Microwave
                                </div>
                                <?php
                            }
                            if($d->garden == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Garden
                                </div>
                                <?php
                            }
                            if($d->water_heater == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Water Heater
                                </div>
                                <?php
                            }
                            if($d->oven == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Oven
                                </div>
                                <?php
                            }


                            ?>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    //
                    }elseif ($d->sub1_kategori == 19) {
                        ?>
                        <div>
                            <h4>Spesifikasi</h4>
                            <hr>
                            <div class="col-md-6">
                                Luas Bangunan : <?php echo $d->luas_bangunan ?> m<sup>2</sup>
                            </div>

                            <div class="col-md-6">
                                Kamar Mandi : <?php echo $d->kamar_mandi ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Lokasi</h4>
                            <hr>
                            <div class="col-md-12">
                                Alamat : <?php echo $d->alamat_lokasi ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Fasilitas</h4>
                            <hr>
                            <?php 
                            if($d->ac == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> AC
                                </div>
                                <?php
                            }
                            if($d->swimming_pool == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Swimmping Pool
                                </div>
                                <?php
                            }
                            if($d->garasi == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Garasi
                                </div>
                                <?php
                            }
                            if($d->refrigerator == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Refrigerator
                                </div>
                                <?php
                            }
                            if($d->fire_extenguisher == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Fire Extenguiser
                                </div>
                                <?php
                            }
                            if($d->telephone == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Telephone
                                </div>
                                <?php
                            }
                            if($d->stove == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Stove
                                </div>
                                <?php
                            }
                            if($d->gordyn == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Gordyn
                                </div>
                                <?php
                            }
                            if($d->carport == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Carport
                                </div>
                                <?php
                            }
                            if($d->pam == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> PAM
                                </div>
                                <?php
                            }
                            if($d->microwave == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Microwave
                                </div>
                                <?php
                            }
                            if($d->garden == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Garden
                                </div>
                                <?php
                            }
                            if($d->water_heater == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Water Heater
                                </div>
                                <?php
                            }
                            if($d->oven == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Oven
                                </div>
                                <?php
                            }


                            ?>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    //
                    }elseif ($d->sub1_kategori == 20) {
                        ?>
                        <div>
                            <h4>Spesifikasi</h4>
                            <hr>
                            <div class="col-md-6">
                                Luas Tanah : <?php echo $d->luas_tanah ?> m<sup>2</sup>
                            </div>
                            <div class="col-md-6">
                                Luas Bangunan : <?php echo $d->luas_bangunan ?> m<sup>2</sup>
                            </div>
                            <div class="col-md-6">
                                <?php 
                                if($d->sertifikasi == 1){
                                    $sertifikasi = "SHM (Sertifikat Hak Milik)";
                                }elseif ($d->sertifikasi == 2) {
                                    $sertifikasi = "HGB (Hak Guna Bangunan)";
                                }elseif ($d->sertifikasi == 3) {
                                    $sertifikasi = "Lainnya (PPBJ, Girik, Adat, dll)";
                                }
                                ?>
                                Sertifikasi : <?php echo $sertifikasi; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Lokasi</h4>
                            <hr>
                            <div class="col-md-12">
                                Alamat : <?php echo $d->alamat_lokasi ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Fasilitas</h4>
                            <hr>
                            <?php 
                            if($d->ac == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> AC
                                </div>
                                <?php
                            }
                            if($d->swimming_pool == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Swimmping Pool
                                </div>
                                <?php
                            }
                            if($d->garasi == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Garasi
                                </div>
                                <?php
                            }
                            if($d->refrigerator == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Refrigerator
                                </div>
                                <?php
                            }
                            if($d->fire_extenguisher == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Fire Extenguiser
                                </div>
                                <?php
                            }
                            if($d->telephone == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Telephone
                                </div>
                                <?php
                            }
                            if($d->stove == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Stove
                                </div>
                                <?php
                            }
                            if($d->gordyn == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Gordyn
                                </div>
                                <?php
                            }
                            if($d->carport == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Carport
                                </div>
                                <?php
                            }
                            if($d->pam == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> PAM
                                </div>
                                <?php
                            }
                            if($d->microwave == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Microwave
                                </div>
                                <?php
                            }
                            if($d->garden == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Garden
                                </div>
                                <?php
                            }
                            if($d->water_heater == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Water Heater
                                </div>
                                <?php
                            }
                            if($d->oven == 1){
                                ?>
                                <div class="col-md-4">
                                    <i class="fa fa-dot-circle-o clr-green"></i> Oven
                                </div>
                                <?php
                            }


                            ?>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    //
                    }elseif ($d->sub1_kategori == 21) {
                        ?>
                        <div>
                            <h4>Spesifikasi</h4>
                            <hr>
                            <div class="col-md-6">
                                Luas Tanah : <?php echo $d->luas_tanah ?> m<sup>2</sup>
                            </div>
                            <div class="col-md-6">
                                <?php 
                                if($d->sertifikasi == 1){
                                    $sertifikasi = "SHM (Sertifikat Hak Milik)";
                                }elseif ($d->sertifikasi == 2) {
                                    $sertifikasi = "HGB (Hak Guna Bangunan)";
                                }elseif ($d->sertifikasi == 3) {
                                    $sertifikasi = "Lainnya (PPBJ, Girik, Adat, dll)";
                                }
                                ?>
                                Sertifikasi : <?php echo $sertifikasi; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <h4>Lokasi</h4>
                            <hr>
                            <div class="col-md-12">
                                Alamat : <?php echo $d->alamat_lokasi ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    //mobil
                    }elseif ($d->sub1_kategori == 8) {
                        ?>
                        <div>
                            <h4>Spesifikasi</h4>
                            <hr>
                            <div class="col-md-6">
                                Tipe Kendaraan : <?php echo $d->tipe; ?>
                            </div>
                            <div class="col-md-6">
                                Transmisi : <?php echo $d->transmisi; ?>
                            </div>
                            <div class="col-md-6">
                                Tahun : <?php echo $d->tahun; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    //motor
                    }elseif ($d->sub1_kategori == 13) {
                        ?>
                        <div>
                            <h4>Spesifikasi</h4>
                            <hr>
                            <div class="col-md-6">
                                Tipe Kendaraan : 
                            </div>
                            <div class="col-md-6">
                                Transmisi : Manual
                            </div>
                            <div class="col-md-6">
                                Tahun : 2016
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    }
                    ?>
                </section>
                <div style="margin-bottom:40px;">
                    <img src="<?php echo base_url().'assets/produk/a/1.jpg'?>">
                </div>
                <section class="like">
                    <button onclick="like(<?php echo $d->id_iklan ?>)" class="btn btn-primary"><i class="fa fa-thumbs-up"></i></button><span id="like_total"> <?php echo $d->like ?> Suka</span>
                    <button onclick="unlike(<?php echo $d->id_iklan ?>)" class="btn btn-primary"><i class="fa fa-thumbs-down"></i></button><span id="unlike_total"> <?php echo $d->unlike ?> Tidak Suka</span>
                </section>
                <div class="share">
                  <div>
                      Share di :
                  </div>
                  <div class="share-button">
                    <a target="_new" href="http://www.facebook.com/sharer.php?u=<?php echo base_url(); ?>iklan/detail/<?php echo $d->seo_iklan?>" class="btn btn-social-icon btn-facebook btn-lg"><i class="fa fa-facebook"></i></a>
                    <a href="https://plus.google.com/share?url=<?php echo base_url(); ?>iklan/detail/<?php echo $d->seo_iklan?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="btn btn-social-icon btn-google-plus btn-lg"><i class="fa fa-google-plus"></i></a>
                    <!-- <a href="whatsapp://send?text=<?php echo base_url(); ?>iklan/detail/<?php echo $d->seo_iklan?>" class="btn btn-social-icon btn-whatsapp btn-lg" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i></a> -->
                    <!-- <a class="btn btn-social-icon btn-tumblr btn-lg"><i class="fa fa-tumblr"></i></a> -->
                    <a href="https://twitter.com/intent/tweet?url=<?php echo base_url(); ?>iklan/detail/<?php echo $d->seo_iklan?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="btn btn-social-icon btn-twitter btn-lg"><i class="fa fa-twitter"></i></a>
                </div>
            </div>
            <h3><span style="color:#5CB85C"><i class="fa fa-th-large"></i> Deskripsi </span> <span style="color:#F08519">Iklan</span></h3>
            <hr>
            <section class="detail-iklan">
              <?php echo $d->deskripsi_iklan; ?>
          </section>
          <hr>
      </div>

  </div><!-- /.product-view -->
</div><!-- /.col-right -->
<div class="col-sm-4 col-md-4 col-left">

    <div>
        <div class="harga-iklan">
            <div class="harga">
                <div class="harga-icon col-md-12">
                    <p style="font-size:22px;" class="harga-detail">
                        <i class="fa fa-tags"></i> RP. <?php echo $this->cart->format_number($d->harga_iklan); ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="owner-iklan">
            <div class="owner-container">
                <div class="user-icon col-md-12" style="margin-bottom:8px; font-size:16px; margin-top:10px;" >
                    <p class="owner-name"><i class="fa fa-user" style="font-size:22px;"></i>   &nbsp&nbsp<?php echo $d->nama_member; ?></p>
                </div>
                <div class="ratings col-md-12" style="margin-bottom:8px; font-size:16px; margin-top:10px;">
                    <span class="price" style="color:black"><img src="<?php echo base_url();?>assets/images/jasa-pembuatan-website-direction.png" width="20">  &nbsp <?php echo $kota_member->nama_area ?>, <?php echo $kota_member->provinsi->nama_provinsi ?></span>
                </div>
                <div class="laporkan-penjual col-md-12" style="margin-bottom:8px; font-size:16px; margin-top:10px;">
                    <a href="#" data-toggle="modal" data-target="#modal_lapor"><i class="fa fa-ban" style="font-size:22px;"></i>  &nbsp&nbsp Laporkan Penjual</a>
                </div>
                <div class="laporkan-penjual col-md-12" style="margin-bottom:8px; font-size:16px; margin-top:10px;">
                    <a href="#" data-toggle="modal"><i class="fa fa-reorder" style="font-size:22px;"></i> &nbsp&nbsp <?php echo $mall_id->nama_mall;?></a>
                </div>
                <div class="laporkan-penjual col-md-12" style="margin-bottom:8px; font-size:16px; margin-top:10px;">
                    <a href="#" data-toggle="modal"><i class="fa fa-home" style="font-size:22px;"></i> &nbsp&nbsp <?php echo $toko->nama_toko." - ". $toko->alamat_toko;?></a>
                </div>
                <div class="laporkan-penjual col-md-12" style="margin-bottom:8px; font-size:16px; margin-top:10px;">
                    <a href="#" data-toggle="modal"><i class="fa fa-phone" style="font-size:22px;"></i> &nbsp&nbsp <?php 
                    if($toko->kontak_toko!=null)echo $toko->kontak_toko;
                    else echo 'Tidak Ada Kontak Toko';?></a>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="">
                <span class="label label-warning" style="display:block; padding:5px; margin-bottom:5px;font-size:15px;"><?php echo '<i class="fa fa-calendar"></i> &nbsp; Sejak '.tgl_indo($d->tanggal_post) ?></span>
            </div>
        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="tips">
            <div class="tips-judul">
                <blockquote class="bg-primary"><i class="fa fa-exclamation-circle fa-2x"></i> &nbsp &nbspTips untuk membeli</blockquote>
            </div>
            <ul class="tips-list">
                <li><i class="fa fa-check"></i>Ketemuan di tempat aman</li>
                <li><i class="fa fa-check"></i>Teliti sebelum membeli </li>
                <li><i class="fa fa-check"></i>Bayar setelah barang diterima</li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <hr>
        <div class="kontak-iklan">
            <ul class="kontak-list">
                <li>
                    <div class="list-icon">
                        <img src="<?php echo base_url() ?>assets/images/phone.png">
                        <div class="kontak-hp"><?php echo $d->telepon_member ?></div>
                        <?php 
                        if($d->wa_available == 1){
                            ?>
                            <i style="color:green" class="fa fa-whatsapp"></i> Ada Whatsapp
                            <?php
                        }else{
                            ?>
                            <i style="color:red" class="fa fa-whatsapp"></i> Tidak Ada Whatsapp
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </li>

                <li>
                    <div class="list-icon">
                        <img src="<?php echo base_url() ?>assets/images/phone.png">
                        <div class="kontak-hp"><?php echo $d->telepon_member2 ?></div>
                    </div>
                    <div class="clearfix"></div>
                </li>

                <li>
                    <div class="list-icon">
                        <img src="<?php echo base_url() ?>assets/images/bbm.png">
                        <?php echo $d->bbm_member ?>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="list-icon">
                        <img src="<?php echo base_url() ?>assets/images/profil.png">
                        <?php echo $d->id_line ?>
                    </div>
                    <div class="clearfix"></div>
                </li>

                <!-- <li>
                    <div class="list-icon">
                        <img src="<?php echo base_url() ?>assets/images/email.png">
                    </div>
                    <div class="kontak-email">
                        <?php echo $d->email_member ?>
                    </div>
                    <div class="clearfix"></div>
                </li> -->
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

</div><!-- /.compare -->

<div class="banner-left"><a href="#"><img src="<?php echo base_url(); ?>assets/images/ads/ads-01.jpg" alt=""></a>
    <div class="banner-content">
        <h2>Banner</h2>
    </div>
</div>

</div><!-- /.col-left -->

<div class="row">
    <div class="col-md-8">
        <div class="review">
         <ul class="media-list">
             <?php 
             if(sizeof($komentar) < 1){
                echo '<center><h4>Beri tanggapan pertama <i class="fa fa-exclamation-triangle"></i></h4></center>';
            }
            foreach ($komentar as $q_komentar) {
                echo '
                <li class="media"> 
                    <div class="media-left"> 
                        <a href="#"> 
                            <img class="img-circle" data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTRiOTAwMzY5YiB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NGI5MDAzNjliIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" style="width: 64px; height: 64px;" class="media-object" data-src="holder.js/64x64" alt="64x64"> </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">'.$q_komentar->member->nama_member.'</h4> 
                            <p>'.$q_komentar->komentar.'</p>  
                            ';
                            if(sizeof($q_komentar->reply) > 0){
                                echo '<a onclick="tampil_reply(this, '.$q_komentar->id_komentar.')" href="javascript:void(0)"><div class="lihat-komentar">Lihat Komentar ('.sizeof($q_komentar->reply).')</div></a>';
                                echo '<ul id="media-reply">';
                                // foreach ($q_komentar->reply as $q_reply) {
                                //     echo '
                                //     <li class="media"> 
                                //         <div class="media-left"> 
                                //             <a href="#"> <img class="img-circle" data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTRiOTAwM2E5NCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NGI5MDAzYTk0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" style="width: 32px; height: 32px;" class="media-object" data-src="holder.js/64x64" alt="64x64"> </a> 
                                //         </div> 
                                //         <div class="media-body"> 
                                //             <h5 class="media-heading">'.$q_reply->member->nama_member.'</h5> 
                                //             <div>'.$q_reply->reply.'</div>  
                                //         </div> 
                                //     </li>
                                //     ';
                                // }
                                echo '</ul>';
                                echo '<div class="media"></div>';
                                echo '

                            </div> 
                        </li> 
                        ';
                    }else{
                        echo '<a onclick="tampil_reply(this, '.$q_komentar->id_komentar.')" href="javascript:void(0)"><div class="lihat-komentar">Balas Komentar</div></a>';
                        echo '<ul id="media-reply">';
                        echo '</ul>';
                        echo '<div class="media"></div>';
                        // echo '
                        // <div class="media-left">
                        //     <a href="#"> <img class="img-circle" data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTRiOTAwM2E5NCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NGI5MDAzYTk0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" style="width: 32px; height: 32px;" class="media-object" data-src="holder.js/64x64" alt="64x64"> </a>
                        // </div> 
                        // <div class="media-body"> 
                        //     <form class="form" method="post" id="reply_form">
                        //         <div class="form-group">
                        //             <div>
                        //                 <input id="reply_komentar" name="reply_komentar" class="form-control input-sm" />
                        //                 <input type="hidden" name="id_komentar" value="'.$q_komentar->id_komentar.'"/>
                        //             </div>      
                        //         </div>
                        //     </form>
                        // </div>
                        // ';
                        
                    }
                }
                ?> 

            </ul>
            <div class="review-box">
                <form class="form-horizontal" method="post" id="komentar_form">
                    <div>
                        <textarea id="komentar" name="komentar" class="form-control input-sm"></textarea>
                        <input name="id_iklan" type="hidden" value="<?php echo $d->id_iklan ?>" />
                    </div>
                    <div>
                        <input type="submit" class="btn btn-success" value="Komentar" />
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<div class="row" style="margin-top:30px;margin-bottom:40px;">
    <div class="col-md-12">
        <div style="padding-left:30px;padding-right:30px;">
            <h3><span style="color:#5CB85C"><i class="fa fa-th-large"></i> Iklan </span> <span style="color:#F08519">Sama Lainnya</span></h3>
            <hr>
            <div class="related-iklan">
                <div class="title">

                </div>
                <div class="list">

                    <ol id="products-list" class="products-list">
                        <?php foreach($related as $i) { 

                            $gbr = $this->IklanModel->gbr_iklan($i->id_iklan);
                            $gbr = $gbr->row();
                            $isi_berita=strip_tags($i->deskripsi_iklan);
                            $isi = substr($isi_berita,0,100); // ambil sebanyak 00 karakter
                            $isi = substr($isi_berita,0,strrpos($isi," "));

                            $tgl = $i->tanggal_post; 
                            $tanggal = substr($tgl,8,2);
                            $bulan = substr($tgl,5,2);
                            $tahun = substr($tgl,0,4);
                            $jam   = substr($tgl,11,2);
                            $menit = substr($tgl,14,2);
                            $detik = substr($tgl,17,2);
                            $thn_sekarang = date('Y');
                            $bln_sekarang = date('m');

                                // $time = mktime(12, 40, 33, 6, 10, 2009); // 10 July 2009 12:40:33
                            $time = mktime($jam, $menit, $detik, $bulan, $tanggal, $tahun); // 10 July 2009 12:40:33 
                            $timediff = time() - $time; 
                            $tanggal_skr = timeInSentence($timediff, 'id', 1);
                            $tgl_aktif = explode(" ",$tanggal_skr);
                            if ($tahun < $thn_sekarang || $bulan < $bln_sekarang) {
                                $tglnya = tgl_indo($tgl);
                            }else {
                                $tglnya = $tanggal_skr;
                            }

                            if($i->jenis_iklan == 1){
                                $jenis_iklan = 'Baru';
                            }else{
                                $jenis_iklan = 'Bekas';
                            }

                            echo '
                            <li class="item">
                                <div class="item-inner">
                                    <div class="row iqbal list-iklan-depan">
                                        <div class="col-sm-2 boby nopadding">
                                            <div class="images-container">
                                                <div class="product_icon nadif">
                                                    ';
                                                    if($i->status == 1){
                                                        echo '<button style="width:36px; height:36px; padding:0px" title="Hapus Favorit" isFavorit="1" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-unfavorit"><i class="fa fa-star"></i></button>';
                                                    }else{
                                                        echo '<button style="width:36px; height:36px;padding:0px" title="Favorit" isFavorit="0" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-favorit"><i class="fa fa-star"></i></button>';
                                                    }
                                                    echo '
                                                    <!--div class="favorit-icon"><a class="disabled-favorit" id_iklan="'.$i->id_iklan.'" onclick="favorit(this)"></a></div-->
                                                </div>
                                                <div class="product_icon fadel">
                                                    <div class="label-istimewa" style="background: url('.base_url().'assets/files/istimewa.png) no-repeat;"><a href="#"></a></div>
                                                </div>
                                                <a class="product-image" style="object-fit:cover/contain;height:150px;" title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'" rel="'.$i->owner.'"><img class="img-cover-list" alt="'.$i->judul_iklan.'" src="'.base_url().'images/iklan/'.$gbr->photo.'"></a>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-7 col-sms-4 col-smb-12">
                                            <div class="f-fix">
                                                <h5 class="product-name"><a title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'">'.$i->judul_iklan.'</a><span class="jenis_iklan"><i style="color: rgb(240, 133, 25);" class="fa fa-bookmark"></i> '.$jenis_iklan.'</span></h5>
                                                <div class="list-kategori-iklan">
                                                    <label class="label label-warning bck-org"><i class="fa fa-tags"></i> '.$i->kategori->kategori.'</label>
                                                    <i class="fa fa-angle-double-right"></i> '.$i->sub1->sub1_kategori.'
                                                </div>
                                                <div style="margin-top:15px">
                                                    <i class="fa fa-clock-o icon-interval-time"></i> <div class="text-interval-time">'.$tglnya.'</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-3 bar-right nopadding">
                                            <h4 class="nomargin pad-8 bck-org text-center">
                                                <i class="fa fa-tag"></i> 
                                                <label style="color:#000">Rp '.number_format($i->harga_iklan,0,',','.').'</label>
                                            </h4>
                                            <div class="pad-8" style="font-size:14px;">
                                                <ul>
                                                    <li>
                                                        <span class="label label-success">
                                                            <i class="fa fa-calendar"></i> &nbsp;'.tgl_indo($tgl).'
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <a href="'.$i->web_iklan.'">
                                                            <span class="label label-primary">
                                                                <i class="fa fa-chain"></i> Visit Website
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li style="border-top:1px dashed#d3d3d3;padding-top:5px;margin-top:10px;">
                                                        <i class="text-small" style="font-size:10px;color:#000">
                                                            <i class="fa fa-map-marker"></i> &nbsp;
                                                            '.$i->kota->nama_area.', '.$i->provinsi->nama_provinsi.' 
                                                        </i>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            ';
                        } ?>
                    </ol>

                </div>
            </div>

        </div>
    </div>
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_lapor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-ban"></i> Laporkan Penjual</h4>
    </div>
    <div class="modal-body">
        <div>
            Jika menurut Anda konten pada halaman ini tidak layak tampil atau menyalahi aturan, silakan isi formulir di bawah ini. 
        </div>
        <form action="<?php echo base_url(); ?>iklan/laporkan/<?php echo $d->seo_iklan ?>" method="post">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Nama Anda">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Email Anda">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Pesan</label>
                <textarea name="pesan" class="form-control"></textarea>
            </div>
            <div class="col-md-3">

            </div>
            <div class="col-md-9">
                <div class="g-recaptcha" data-sitekey="6LcGpSATAAAAAIoELZiGncvdmL_Lw2yyCJCjhrvP"></div>
            </div>
            <div class="clearfix"></div>
            <div style="margin-top:10px;">
                <button type="submit" class="btn btn-success btn-block">Kirim</button>
            </div>

        </form>
    </div>
</div>
</div>
</div>
