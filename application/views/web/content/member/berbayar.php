<div class="gap130"></div>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="berbayar-page">
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar; ?>
				</div>
				
				<form method="post" action="<?php echo base_url(); ?>iklanku/berbayar/daftar">
					<input required id="pilihpremium" name="pilihpremium" type="hidden" />
					<input type="hidden" name="harga">
					<div class="col-md-9">
						<?php 
						if($this->session->flashdata('poin_kurang')){
							?>
							<div class="alert alert-warning">
								Poin anda kurang untuk mendaftar premium iklan
							</div>
							<?php
						}
						?>
						
						<!-- <div class="header-member-page">
							<div class="label label-primary label-poin-member">
								<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>
							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->
						<div style="padding-bottom:80px;"></div>
						<div style="text-align: center;font-size: 20px;font-weight: 700;">
							Pilih iklan berbayarmu
						</div>

						<div class="pilih-premium">
							<div class="col-md-10 col-md-offset-1">
								<?php 
								foreach ($premium as $q_premium) {
									?>		
									<?php 
									if($q_premium->id_premium == 4 AND $web_replika == null){
										?>
										<div class="poin pull-left">
											<div class="btn btn-info btn-poin" disabled>
												<?php
											}else{
												?>
												<div class="poin pull-left" onclick="pilihpremium(this, <?php echo $q_premium->id_premium ?>, <?php echo $q_premium->harga_premium ?>)">
													<div class="btn btn-info btn-poin">
														<?php
													}
													?>
													<div class="poin-detail">
														<img class="img-responsive" src="<?php echo base_url().'images/premium/'.$q_premium->gambar_premium; ?>">
														<div class="nama-poin"><?php echo $q_premium->nama_premium ?></div>

													</div>
												</div>
												<?php 
												if($q_premium->id_premium == 4 AND $web_replika == null){
													echo '<div>Anda Belum Punya Website</div>';
												}
												?>
											</div>
											<?php
										}
										?>
									</div>
								</div>
								<div class="clearfix"></div>
								<div id="pilih"></div>
								<div class="clearfix"></div>

							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			var poin = <?php echo $profil->poin_member ?>;
			var hargapremium = 0;
			var biaya = 0;

			function cekPoin(){
				$('.checkbox').change(function(){
					if(this.checked){
						console.log(hargapremium);
					}
				})
			}

			var old_that = null;
			function pilihpremium(that, kodepremium, harga){
				hargapremium = harga;
		//alert(hargapremium);
		$(old_that).children().css('background-color', '');
		//console.log(old_that);
		old_that = that;
		console.log(kodepremium);
		// $.ajax({
		// 	'url':'<?php echo base_url() ?>iklanku/iklannopremium/' + kodepremium,
		// 	'method':'post',
		// 	.done(function(data){
		// 		$('#pilih').load(data);
		// 	})
		// })
		$.ajax({
			url:'<?php echo base_url() ?>iklanku/iklannopremium/'+kodepremium,
			method: "get",
			beforeSend:function(){
				$('.container').block({ message: '<h1 style="color:#fff"><i class="fa fa-refresh fa-spin"></i> </h1><b style="color:#fff">Loading</b>' })
			}
		})
		.done(function(data){
			$("#pilih").html(data);
			$('.container').unblock();
		})
		/*$('#pilih').load( "<?php echo base_url() ?>iklanku/iklannopremium/"+kodepremium, function(){
			.done(fun)
		});*/
		$('#pilihpremium').val(kodepremium);
		//console.log($('#pilihpremium'));
		$(that).children().first().css("background-color", "#5cb85c");
		$('#pilih-iklan-premium').css('display', 'block');
		cekPoin();
	}

</script>
