<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="gap130"></div>
<?php 
if($this->session->flashdata('update-about')){
	?>
	<div class="container" style="background:none">
		<div class="alert alert-success">Updated</div>

	</div>
	<?php
}
?>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="home-member-page">
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar ?>
				</div>
				
				<div class="col-md-9">
						<div style="padding-bottom:80px;"></div>
					<!-- <div class="header-member-page">
						<div class="label label-primary label-poin-member">
							<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->
						<div>
							<ul class="nav nav-tabs nav-tabs-ku">
								<li><a href="#">Beranda</a></li>
								<li><a href="<?php echo base_url() ?>iklanku/kelola/tampilan">Tampilan</a></li>
								<li class="active"><a href="#">About Us</a></li>
							</ul>

							<div class="tab-content" style="padding-top:20px;">
								<div id="iklanAktif" class="tab-pane fade in active">
									<div>
										<form class="form-horizontal" method="post" action="<?php echo base_url() ?>iklanku/kelola/update-about-us" enctype="multipart/form-data">
											<div class="form-group">
												<label class="col-sm-2 control-label">Foto</label>
												<div class="col-md-4">
													<?php 
													if($website->about_foto != ''){
														echo '<img class="img-responsive" style="width:150px;" src="'.base_url().'images/about_replika/'.$website->about_foto.'" />';
													}
													?>
													<input type="file" name="aboutfoto" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">About</label>
												<div class="col-md-10">
													<textarea id="about" name="aboutdesc" class="form-control"><?php echo $website->about ?></textarea>
												</div>
											</div>
											<hr>
											<div class="form-group">
												<button type="submit" class="btn btn-info col-md-offset-10">Simpan</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		CKEDITOR.replace('about');
	</script>