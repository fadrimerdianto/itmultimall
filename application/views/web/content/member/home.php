

<div class="container gap130">
	<div class="main">
		<div class="col-main">
			<div class="home-member-page">
				<?php
				if($this->session->userdata('update_profil')){
					?>
					<div class="alert alert-info">
						<?php echo $this->session->userdata('update_profil');  ?>
					</div>
					<?php
				}
				?>

				<?php
				if($this->session->userdata('belipoin')){
					?>
					<div class="alert alert-info">
						<?php echo $this->session->userdata('belipoin');  ?>
					</div>
					<?php
				}
				?>

				<?php
				if($this->session->userdata('berbayar')){
					?>
					<div class="alert alert-info">
						<?php echo $this->session->userdata('berbayar');  ?>
					</div>
					<?php
				}
				?>

				<?php
				if($this->session->userdata('hapus_iklan')){
					?>
					<div class="alert alert-info">
						<?php echo $this->session->userdata('hapus_iklan');  ?>
					</div>
					<?php
				}
				?>

				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar ?>
				</div>
				
				<div class="col-md-9">
					<?php 
					if(sizeof($iklan) + sizeof($iklan_off) == 0){
						?>
						<div class="member-iklan">
							<div style="text-align:center; margin-top:100px;">
								<img src="<?php echo base_url(); ?>assets/images/pin.png">
							</div>
							<div class="button-tambah-iklan">
								<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Tambah Iklanmu</button></a>
							</div>
						</div>
						<?php
					}else{
						?>
						<div style="padding-bottom:80px;"></div>
						<!-- <div class="header-member-page">
 -->							<!-- <div class="label label-primary label-poin-member">
								<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>							
								<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
								<div class="clearfix"></div>
							</div> -->
							<div>
								<ul class="nav nav-tabs nav-tabs-ku">
									<li class="active"><a href="#">Iklan Aktif &nbsp;<span class="label label-success"><?php echo sizeof($iklan) ?></span></a></li>
									<li><a href="<?php echo base_url() ?>iklanku/tidakaktif">Iklan Tidak Aktif &nbsp;<span class="label label-success"><?php echo sizeof($iklan_off) ?></span></a></li>
									<li><a href="<?php echo base_url() ?>iklanku/laku">Iklan Laku &nbsp;<span class="label label-success"><?php echo sizeof($iklan_laku) ?></span></a></li>
									<li><a href="<?php echo base_url() ?>iklanku/ditolak">Iklan Ditolak &nbsp;<span class="label label-success"><?php echo sizeof($iklan_ditolak) ?></span></a></li>
									<li><a href="<?php echo base_url() ?>iklanku/premium">Iklan Premium &nbsp;<span class="label label-success"><?php echo sizeof($iklan_premium) ?></span></a></li>

								</ul>

								<div class="tab-content" style="padding-top:20px;">
									<div id="iklanAktif" class="tab-pane fade in active">
										<table class="datatable-ku table custom-table">

									<a class="btn btn-primary" href="<?php echo base_url(); ?>iklanku/pasang" ><i class="fa fa-plus"></i> Iklan Baru &nbsp;</a>
											<thead>
												<tr class="first last">
													<th  width="22%">Tanggal</th>
													<th >Judul Iklan</th>
													<th> Gambar</th>
													<th> Harga</th>
													<th> Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												foreach ($iklan as $q_iklan) {
													echo '
													<tr>
														<td>
															<center style="margin-bottom:5px;">'.tgl_indo($q_iklan->tanggal_post).'</center>
															';
															if($q_iklan->tanggal_aktif_premium > date('Y-m-d')){
																//echo '<center><span class="label label-success">Aktif Premium '.tgl_indo($q_iklan->tanggal_aktif_premium).'</span></center>';
															}
															echo '

														</td>
														<td>
															<div class="link-judul-iklan"><a href="'.base_url().'iklan/detail/'.$q_iklan->seo_iklan.'" title="'.$q_iklan->judul_iklan.'">'.read_more($q_iklan->judul_iklan,30).'</a></div>
															<div class="text-small" style="font-size:10px;"><i class="fa fa-eye"></i> Dilihat '.$q_iklan->dilihat.' Kali</div>
														</td>
														<td><a class="product-image" title="'.$q_iklan->judul_iklan.'" href="#">
															<center>'.
																// echo '<img style="width:50px;" alt="'.$q_iklan->judul_iklan.'" src="'.base_url().'images/iklan/'.$q_iklan->gambar->photo.'">
															'</center>
															<center>
																';
																// if($q_iklan->tanggal_aktif_premium > date('Y-m-d')){
																// 	if($q_iklan->id_premium == 1){
																// 		echo '<div class="label label-info">Top 25</div>';
																// 	}else if ($q_iklan->id_premium == 2) {
																// 		echo '<div class="label label-info">Top Shop</div>';
																// 	}else if($q_iklan->id_premium == 3){
																// 		echo '<div class="label label-info">Recommended</div>';
																// 	}else if($q_iklan->id_premium == 4){
																// 		echo '<div class="label label-info">Terlaris</div>';
																// 	}
																// }
																
																echo '
																</center>
														</a></td>


														<td class="subtotal">Rp. '.number_format($q_iklan->harga_iklan).'</td>
														<td>
															<center>
																<a href="'.base_url().'iklanku/laku/do/'.$q_iklan->id_iklan.'"><button onClick="return confirm(\'Apakah anda yakin mengubah status iklan menjadi laku ?\')" class="btn btn-success btn-sm btn-block">Sudah Laku</button></a>
																<a href="'.base_url().'iklanku/hapus/'.$q_iklan->id_iklan.'"><button onClick="return confirm(\'Apakah Anda yakin untuk menghapus ?\')" class="btn btn-warning btn-sm btn-block">Hapus</button></a>
															</center>
														</td>
													</tr>
													';
												}
												?>

											</tbody>
										</table>
									</div>

								</div>
								<?php
							}
							?>
						</div>
				<!--<div class="col-sm-4">
                    	<div class="block block-layered-nav">
                            <div class="block-content">
                                <h2>Menu</h2>
                                <ol>
                                    <li class="active"><a href="#">Iklan</a></li>
                                    <li><a href="#">Account Information</a></li>
                                    <li><a href="#">Address Book</a></li>
                                    <li><a href="<?php echo base_url(); ?>iklanku/pengaturan">Pengaturan</a></li>
                                   
                                </ol>
                            </div>
                        </div>--><!-- /.block - Comments -->
                        <!--</div>-->
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>