<div class="gap130"></div>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="home-member-page">
				<?php
				if($this->session->userdata('update_profil')){
					?>
					<div class="alert alert-info">
						<?php echo $this->session->userdata('update_profil');  ?>
					</div>
					<?php
				}
				?>
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar ?>
				</div>
				<div class="col-md-9">
					<?php 
					if(sizeof($iklan_off + $iklan) == 0){
						?>
						<div class="member-iklan">
							<div style="text-align:center; margin-top:100px;">
								<img src="<?php echo base_url(); ?>assets/images/pin.png">
							</div>
							<div class="button-tambah-iklan">
								<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Tambah Iklanmu</button></a>
							</div>
						</div>
						<?php
					}else{
						?>
						<div style="padding-bottom:80px;"></div>
						<!-- <div class="header-member-page">
							<div class="label label-primary label-poin-member">
								<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>
							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->
						<div>
							<ul class="nav nav-tabs nav-tabs-ku">
									<li><a href="<?php echo base_url(); ?>iklanku/">Iklan Aktif <span class="label label-success"><?php echo sizeof($iklan) ?></span></a></li>
									<li class="active"><a href="#">Iklan Tidak Aktif &nbsp;<span class="label label-success"><?php echo sizeof($iklan_off) ?></span></a></li>
									<li><a href="<?php echo base_url() ?>iklanku/laku">Iklan Laku &nbsp;<span class="label label-success"><?php echo sizeof($iklan_laku) ?></span></a></li>
									<li><a href="<?php echo base_url() ?>iklanku/ditolak">Iklan Ditolak &nbsp;<span class="label label-success"><?php echo sizeof($iklan_ditolak) ?></span></a></li>
									<li><a href="<?php echo base_url() ?>iklanku/premium">Iklan Premium &nbsp;<span class="label label-success"><?php echo sizeof($iklan_premium) ?></span></a></li>

								</ul>
							<div class="table-responsive" style="padding-top:20px;">
								<table class="datatable-ku table custom-table">
									<a class="btn btn-primary" href="<?php echo base_url(); ?>iklanku/pasang" ><i class="fa fa-plus"></i> Iklan Baru &nbsp;</a>
									<thead>
										<tr class="first last">
											<th>Tanggal</th>
											<th>Judul Iklan</th>
											<th>Gambar</th>
											<th>Harga</th>
											<th>Status</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach ($iklan_off as $q_iklan) {
											$dif_time = strtotime(date('Y-m-d H:i:s')) - strtotime($q_iklan->tanggal_post);
											$days = (floor($dif_time / (60*60*24)));
											echo '
											<tr>
												<td>
													<center>'.tgl_indo($q_iklan->tanggal_post).'></center>

												</td>
												<td>
													<div class="link-judul-iklan"><a href="#">'.$q_iklan->judul_iklan.'</a></div>
													<div><i class="fa fa-eye"></i> Dilihat '.$q_iklan->dilihat.' Kali</div>
												</td>
												<td><a class="product-image" title="'.$q_iklan->judul_iklan.'" href="#">
													<center>';
													if(isset($q_iklan->gambar[0]->photo))  echo '<img class="img-thumbnail" style="width:70px;" alt="'.$q_iklan->judul_iklan.'" src="'.base_url().'images/iklan/'.$q_iklan->gambar[0]->photo.'">';
													echo '</center>
												</a></td>
												
												
												<td class="subtotal">Rp. '.number_format($q_iklan->harga_iklan).'</td>
												
												<td>
													';
													//var_dump(date('Y-m-d') - (date('Y-m-d', strtotime($q_iklan->tanggal_post))));
													
													if($q_iklan->status == 0){
														echo '<div class="label label-info">Validasi</div>';
													}elseif ($q_iklan->status == 2) {
														echo '<div class="label label-danger">Ditolak</div>';
													}elseif($days > 60){
														echo '<div class="label label-danger">Expired</div>';
													}
													
													echo '
												</td>
												<td>
													';
													if($days > 60){
														if($upgrade->pembaruan == 1){
															echo '<a href="'.base_url().'iklanku/tidakaktif/renew/'.$q_iklan->id_iklan.'"><button class="btn btn-info btn-sm">Renew</button></a>';
														}else{
															echo '<a href="'.base_url().'iklanku/upgrade/"><button class="btn btn-info btn-sm">Renew</button></a>';
														}
													}
													echo '
													<a href="'.base_url().'iklanku/hapus/'.$q_iklan->id_iklan.'"><button onClick="return confirm(\'Apakah Anda yakin untuk menghapus ?\')" class="btn btn-warning btn-sm btn-block">Hapus</button></a>
												</td>
											</tr>
											';
										}
										?>
										
									</tbody>
								</table>
							</div>
						</div>
						<?php
					}
					?>
				</div>
				<!--<div class="col-sm-4">
                    	<div class="block block-layered-nav">
                            <div class="block-content">
                                <h2>Menu</h2>
                                <ol>
                                    <li class="active"><a href="#">Iklan</a></li>
                                    <li><a href="#">Account Information</a></li>
                                    <li><a href="#">Address Book</a></li>
                                    <li><a href="<?php echo base_url(); ?>iklanku/pengaturan">Pengaturan</a></li>
                                   
                                </ol>
                            </div>
                        </div>--><!-- /.block - Comments -->
                        <!--</div>-->
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>