<style type="text/css">
	#selected-kategori{
		float: left;
		font-size: 14px;
		margin-top: auto;
		margin-bottom: auto;
		padding: 6px;
		margin-right: 10px;
		font-weight: 800;
	}
	.header-kategori{
		padding: 10px;
		background: rgb(234, 161, 34) none repeat scroll 0% 0%;
		margin: 2px;
	}
</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="container gap130">
	<div class="main">
		<div class="col-main">
			<div class="pasang-page">
				<div class="panel panel-primary"> <div class="panel-heading"> 
					<h3 class="panel-title"><i class="fa fa-newspaper-o"></i> Pasang Iklan</h3> 
				</div> 
				<div class="panel-body">
					<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>iklanku/pasang/do">
						<input type="hidden" id="id_kategori" name="id_kategori" />
						<input type="hidden" id="id_sub1_kategori" name="id_sub1_kategori" />
						<input type="hidden" id="id_sub2_kategori" name="id_sub2_kategori" />

						<div class="form-group">
							<label class="col-sm-2 control-label">Judul Iklan <em>*</em></label>
							<div class="col-sm-4">
								<input name="judul_iklan" class="form-control" required/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">Kategori <em>*</em></label>
							<div class="col-sm-10">
								<!-- Trigger the modal with a button -->
								<button type="button" class="btn btn-info" onclick="$('#myModal').modal('show')">Pilih Kategori</button>

								<div id="selected-kategori">

								</div>
							</div>
						</div>
					
						<div class="form-group">
							<label class="col-sm-2 control-label">Jenis Barang <em>*</em></label>
							<div class="col-sm-3">
								<select required name="jenis_iklan" class="form-control">
									<option value="1">Barang Baru</option>
									<option value="2">Barang Bekas</option>
								</select>
							</div>
						</div>
							<!-- <div class="form-group">
								<label class="col-sm-2 control-label">Kategori <em>*</em></label>
							    <div class="col-sm-3">
							    	<select name="kategori" id="kategori" class="form-control" onchange="select_kat(this)">
							    		<option selected disabled>Pilih Kategori</option>
							    		<?php 
							    			foreach ($kategori as $q_kategori) {
							    				echo '<option value="'.$q_kategori->id_kategori.'">'.$q_kategori->kategori.'</option>';
							    			}
							    		?>
							    	</select>
							    	
							    </div>
							</div> -->
							<hr>
							<div class="form-group">
								<label class="col-sm-2 control-label">Harga <em>*</em></label>
								<div class="col-sm-2">
									<input name="harga_iklan" type="number" class="form-control" required/>
								</div>
							</div>
							<div id="load-filter-form"></div>
							<hr>
							<div class="form-group">
								<div class="col-md-2 col-sm-2">
									<label>Upload Images</label>
								</div>
								<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
									<div class="image-placeholder">
										<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
									</div>
									<div style="padding:0; text-align:center">
										<input type="file" name="userfile[]" style="display:none" onchange="readURL(this);" required>
										<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
									</div>
									<div class="clearfix"></div>
								</div>

								<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
									<div class="image-placeholder">
										<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
									</div>
									<div style="padding:0; text-align:center">
										<input type="file" name="userfile[]" style="display:none" onchange="readURL(this);">
										<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
									<div class="image-placeholder">
										<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
									</div>
									<div style="padding:0; text-align:center">
										<input type="file" name="userfile[]" style="display:none" onchange="readURL(this);">
										<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
									<div class="image-placeholder">
										<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
									</div>
									<div style="padding:0; text-align:center">
										<input type="file" name="userfile[]" style="display:none" onchange="readURL(this);">
										<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
									<div class="image-placeholder">
										<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
									</div>
									<div style="padding:0; text-align:center">
										<input type="file" name="userfile[]" style="display:none" onchange="readURL(this);">
										<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<hr>
							<!-- <div class="form-group">
								<label class="col-sm-2 control-label">Kondisi <em>*</em></label>
							    <div class="col-sm-2">
							    	<select class="form-control">
							    		<option>Baru</option>
							    		<option>Bekas</option>
							    	</select>
							    </div>
							</div> -->

							<div class="form-group">
								<label class="col-sm-2 control-label">Deskripsi <em>*</em></label>
								<div class="col-sm-8">
									<textarea type="hidden" name="deskripsi_iklan" id="deskripsi_iklan"></textarea>
									<!-- <div name="deskripsi_iklan" class="summernote"></div> -->
								</div>
							</div>
							<hr>
							<div class="form-group">
								<label class="col-sm-2 control-label">Provinsi <em>*</em></label>
								<div class="col-sm-3">
									<select required class="form-control" name="provinsi" onchange="getKota(this)">
										<option disabled selected>Pilih Provinsi</option>
										<?php 
										foreach ($provinsi as $q_provinsi) {
											echo '<option value="'.$q_provinsi->id_provinsi.'">'.$q_provinsi->nama_provinsi.'</option>';
										}
										?>
									</select>
								</div>
							</div>
							<hr>
							<div class="form-group">
								<label class="col-sm-2 control-label">Mall <em>*</em></label>
								<div class="col-sm-3">
									<select required class="form-control" name="mall">
										<option disabled selected>Pilih Mall</option>
										<?php 
										foreach ($mall as $q_mall) {
											echo '<option value="'.$q_mall->id_mall.'">'.$q_mall->nama_mall.'</option>';
										}
										?>
									</select>
								</div>
							</div>
							<hr>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama <em>*</em></label>
								<div class="col-sm-6">
									<?php 
									if($member->nama_member != ''){
										echo '<input readonly value="'.$member->nama_member.'" class="form-control" name="nama" />';
									}else{
										echo '<input class="form-control" name="nama" />';
									}
									?>

								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Email <em>*</em></label>
								<div class="col-sm-6">
									<input readonly value="<?php echo $member->email; ?>" type="email" class="form-control" name="email" />
									<div class="checkbox">
										<label>
											<input value="1" name="hide_email" type="checkbox"> Jangan Tampilkan Email
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nomer Telepon /  HP <em>*</em></label>
								<div class="col-sm-3">
									<input readonly value="<?php echo $member->telepon_member; ?>" class="form-control" name="telepon" required/>
									<div class="checkbox">
										<label>
											<input value="1" name="wa_available" type="checkbox"> Whatsapp Available
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nomer Telepon /  HP 2<em>*</em></label>
								<div class="col-sm-3">
									<input readonly value="<?php echo $member->telepon_member2; ?>" class="form-control" name="telepon" required/>
									<div class="checkbox">
										<label>
											<input value="1" name="wa_available2" type="checkbox"> Whatsapp Available
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">LINE ID </label>
								<div class="col-sm-3">
									<input readonly value="<?php echo $member->id_line; ?>" class="form-control" name="pinbb" />
									<div class="checkbox">
								    <label>
								      <input value="1" name="hide_line" type="checkbox"> Sembuyikan LINE ID
								    </label>
								  </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">PIN BB </label>
								<div class="col-sm-3">
									<input readonly value="<?php echo $member->bbm_member; ?>" class="form-control" name="pinbb" />
									<div class="checkbox">
								    <label>
								      <input value="1" name="hide_bbm" type="checkbox"> Sembuyikan PIN BBM
								    </label>
								  </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Website </label>
								<div class="col-sm-4">
									<input readonly class="form-control" name="web" />
								</div>
							</div>
							<hr>

							<button class="btn btn-success pull-right" type="submit">Simpan</button>
						</form>						
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<?php
	$arr = array(
	    " & " => "",
	    " " => "",);
?>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width:60%">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header head_modal">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-th"></i> &nbsp;Pilih Kategori Iklan</h4>
			</div>
			<div class="modal-ku modal-body">
				<div class="col-md-4 kategori-bck scroll">
					<h4 class="head-kategori">
						<i class="fa fa-arrow-circle-right"></i>
						&nbsp; Main Category
					</h4>
					<ul class="list-group kategori-parent " id="list-kat">
						<?php
						foreach ($kategori as $q_kategori) {
							echo '<li onclick="aksiKategori(this)" class="list-group-item " kategori-id="'.$q_kategori->id_kategori.'">
							<a data-toggle="tab" href="#'.strtolower(strtr($q_kategori->kategori,$arr)).'"><i class="fa '.$q_kategori->icon.'"></i> &nbsp;'.$q_kategori->kategori.'</a>
						</li>';

					} 

					?>
				</ul>
			</div>
			<div class="col-md-4 kategori-bck scroll">
				<h4 class="head-kategori">
					<i class="fa fa-arrow-circle-right"></i>
					&nbsp; Sub Kategori
				</h4>
				<div class="tab-content">
					<?php 
					foreach ($kategori as $q_kategori) {
						echo '
						<div id="'.strtolower(strtr($q_kategori->kategori,$arr)).'" class="tab-pane fade in">
							<ul class="list-group  kategori-parent-sub" id="sub1">
								';
								foreach ($q_kategori->sub1_kategori as $q_sub1) {
							    	//terpaksa query di view
									$subkat2 = $this->db->get_where('sub2_kategori',['id_sub1_kategori'=>$q_sub1->id_sub1_kategori]);
									if($subkat2->num_rows() > 0){
										echo '<li sub1-id="'.$q_sub1->id_sub1_kategori.'" class="list-group-item"><a data-toggle="tab" href="#'.strtolower(str_replace(" ", "", $q_sub1->id_kategori.'-'.$q_sub1->sub1_kategori)).'">'.$q_sub1->sub1_kategori.'</a></li>';
									}else{
										echo '<li onclick="select(this, 1)" sub1-id="'.$q_sub1->id_sub1_kategori.'" class="list-group-item"><a data-toggle="tab" href="#'.strtolower(str_replace(" ", "", $q_sub1->sub1_kategori)).'">'.$q_sub1->sub1_kategori.'</a></li>';
									}
								}
								echo '
							</ul>
						</div>
						';
					}
					?>
				</div>
			</div>
			<div class="col-md-4 kategori-bck scroll">
				<h4 class="head-kategori">
					<i class="fa fa-arrow-circle-right"></i>
					&nbsp; Sub Kategori 2
				</h4>
				<div class="tab-content" id="sub2-content">
					<?php 
					foreach ($sub1kategori as $q_sub1) {
						echo '<div id="'.strtolower(str_replace(" ", "", $q_sub1->id_kategori.'-'.$q_sub1->sub1_kategori)).'" class="tab-pane fade in sub2">
						<ul class="list-group kategori-parent-sub-sub" id="sub2">';
							foreach ($q_sub1->sub2_kategori as $q_sub2) {
								echo '<li onclick="select(this, 2)" sub1-id="'.$q_sub1->id_sub1_kategori.'" sub2-id="'.$q_sub2->id_sub2_kategori.'" class="list-group-item"><a data-toggle="tab" href="#'.strtolower(str_replace(" ", "", $q_sub2->sub2_kategori)).'">'.$q_sub2->sub2_kategori.'</a></li>';
							}
							echo '
						</ul>
					</div>
					';
				}
				?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="modal-footer modal-footer-ku">
		<article class="text-center">
			ITMultimall
		</article>
	</div>
</div>

</div>
</div>
<script type="text/javascript">
	CKEDITOR.replace('deskripsi_iklan', {
		removePlugins: 'toolbar'
	});
</script>
<script type="text/javascript">
	var id_kategori = 0;
	var kategori;
	var sub1kategori;
	var sub2kategori;
	function aksiKategori(that){
	//ambil kategori yang di pilih
	if(id_kategori != $(that).attr('kategori-id')){
		id_kategori = $(that).attr('kategori-id');
		$('#sub2-content .sub2').removeClass('active');
	}else{

	}
}

function select(that, sub){
	$('#id_kategori').val('');
	$('#id_sub1_kategori').val('');
	$('#id_sub2_kategori').val('');
	//$('#selected-kategori').append('');
	var id_sub = 0;
	
	//console.log("text -> " + $(that).text());
	//console.log("Kategori ->" + $('#list-kat li.active').text());
	if(sub === 1){
		//console.log("sub -> " + sub);
		$('#selected-kategori').text($('#list-kat li.active').text()+" >> " +$(that).text()+"");

		$('#id_kategori').val(id_kategori);
		$('#id_sub1_kategori').val($(that).attr('sub1-id'));
		
	}else{
		var sub1 = $('#sub1 li.active').text();
		//console.log(sub1);
		$('#selected-kategori').text($('#list-kat li.active').text()+" >> "+sub1+" >> " +$(that).text()+"");
		//console.log($('#sub1 li.active').attr('sub1-id'));
		$('#id_kategori').val(id_kategori);
		$('#id_sub1_kategori').val($('#sub1 li.active').attr('sub1-id'));
		$('#id_sub2_kategori').val($(that).attr('sub2-id'));

	}

	$('#myModal').modal('hide');
	$('body').removeClass('modal-open');
	$('.modal-backdrop').remove();
	
	//console.log();
	select_subkat2($(that).attr('sub1-id'));
}
</script>

<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$(input).parent().prev().find("img").attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	function openUpload(me){
		$(me).parent().find("input[type=file]").click();
	}
	function select_kat(that){
		$.ajax({
			'method':'POST',
			'url':'<?php echo base_url() ?>api/getSubKategori',
			'data':{id_kategori:that.value}
		}).done(function(data){
			var array = JSON.parse(data);
			if(array.length > 0){
				$('#kategori').after("<select name=\"sub1_kategori\" onchange=\"select_subkat(this)\" class=\"form-control\" id=\"sub1_kategori\"></select>");
				$('#sub1_kategori').append("<option selected disabled>Pilih Sub Kategori</option>")
				for (var i = 0; i <= array.length - 1; i++) {
					$('#sub1_kategori').append("<option value="+array[i].id_sub1_kategori+">"+array[i].sub1_kategori+"</option");
				};
			}else{
				$('#sub1_kategori').remove();
				$('#sub2_kategori').remove();
			}
		});
	}
	function select_subkat(that){
		$.ajax({
			'method':'POST',
			'url':'<?php echo base_url(); ?>api/getSub2Kategori',
			'data':{id_sub1_kategori:that.value}
		}).done(function(data){
			var array = JSON.parse(data);
			if(array.length > 0){
				$('#sub1_kategori').after("<select name=\"sub2_kategori\" class=\"form-control\" id=\"sub2_kategori\"></select>");
				$('#sub2_kategori').append("<option selected disabled>Pilih Sub 2 Kategori</option>")
				for (var i = 0; i <= array.length; i++) {
					$('#sub2_kategori').append("<option value="+array[i].id_sub2_kategori+">"+array[i].sub2_kategori+"</option>")
				};
			}else{
				$('#sub2_kategori').remove();
			}
		});
	}

	function select_subkat2(sub1_id){
		$( "#load-filter-form" ).load( "<?php echo base_url() ?>iklanku/filter_form/"+sub1_id);
	}

</script>
<?php 
	if($jumlah_iklan >= $maks_iklan){
	?>
	<script type="text/javascript">
		alert("Anda telah melampaui batas maksimal dari upload iklan, silahkan upgrade");
		window.location. =  history.go(-1);
	</script>
	<?php
	}
?>
