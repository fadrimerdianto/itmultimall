<div class="gap130"></div>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="home-member-page">
				<?php
					if($this->session->userdata('update_profil')){
				?>
					<div class="alert alert-info">
						<?php echo $this->session->userdata('update_profil');  ?>
					</div>
				<?php
					}
				?>
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar ?>
				</div>
				<div class="col-md-9">
					<?php 
						if(sizeof($iklan) == 0){
					?>
						<div class="member-iklan">
							<div style="text-align:center; margin-top:100px;">
								<img src="<?php echo base_url(); ?>assets/images/pin.png">
							</div>
							<!-- <div class="button-tambah-iklan">
								<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Tambah Iklanmu</button></a>
							</div> -->
						</div>
					<?php
						}else{
					?>
						<!-- <div class="header-member-page">
							<div class="label label-primary label-poin-member">
								<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>
							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->
						<div>
							<ul class="nav nav-tabs nav-tabs-ku">
							  <li role="presentation" class="active"><a href="#">Iklan Favorit <div class="label label-success"><?php echo sizeof($favorit); ?></div></a></li>
							</ul>
							<div class="tab-content" style="padding-top:20px;">
							  <div id="iklanAktif" class="tab-pane fade in active">
			                	<table class="datatable-ku table custom-table">
			                        <thead>
			                            <tr class="first last">
			                                <th>Judul Iklan</th>
			                                <th>Gambar Iklan</th>
			                                <th>Harga</th>
			                                <th></th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        	<?php 
			                        		foreach ($iklan as $q_iklan) {
			                        			echo '
			                        				<tr>
			                        					<td>
						                                	<div><a href="'.base_url().'iklan/detail/'.$q_iklan->seo_iklan.'">'.$q_iklan->judul_iklan.'</a></div>
						                                </td>
						            					<td>
						            						<a class="product-image" title="'.$q_iklan->judul_iklan.'" href="#">
						                                		<center><img style="width:50px;" alt="'.$q_iklan->judul_iklan.'" src="'.base_url().'images/iklan/'.$q_iklan->gambar[0]->photo.'"></center>
						                                	</a>
						                                </td>
						            					
						                        		
						                        		<td class="subtotal">Rp. '.number_format($q_iklan->harga_iklan).'</td>
						                        		<td>
						                        			<button class="btn btn-warning">Hapus Favorit</button>
						                        		</td>
						        					</tr>
			                        			';
			                        		}
			                        	?>
			                        	
			                    	</tbody>
			                    </table>
		                    </div>
		                  </div>
						</div>
					<?php
						}
					?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>