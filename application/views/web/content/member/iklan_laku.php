<div class="gap130"></div>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="home-member-page">
				<?php
				if($this->session->userdata('update_profil')){
					?>
					<div class="alert alert-info">
						<?php echo $this->session->userdata('update_profil');  ?>
					</div>
					<?php
				}
				?>
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar ?>
				</div>
				<div class="col-md-9">
					<div style="padding-bottom:80px;"></div>
					<?php 
					if(sizeof($iklan_laku) == 0){
						?>
						<div>
							<ul class="nav nav-tabs nav-tabs-ku">
								<li><a href="<?php echo base_url(); ?>iklanku/">Iklan Aktif <span class="label label-success"><?php echo sizeof($iklan) ?></span></a></li>
								<li><a href="<?php echo base_url(); ?>iklanku/tidakaktif">Iklan Tidak Aktif <span class="label label-success"><?php echo sizeof($iklan_off) ?><span class="label label-success"></a></li>
								<li class="active"><a href="#">Iklan Laku <span class="label label-success"><?php echo sizeof($iklan_laku) ?><span class="label label-success"></a></li>
								<li><a href="<?php echo base_url() ?>iklanku/ditolak">Iklan Ditolak &nbsp;<span class="label label-success"><?php echo sizeof($iklan_laku) ?></span></a></li>
								<li><a href="<?php echo base_url() ?>iklanku/premium">Iklan Premium &nbsp;<span class="label label-success"><?php echo sizeof($iklan_premium) ?></span></a></li>
							</ul>
							<div class="table-responsive" style="padding-top:20px;">
								<table class="datatable-ku table custom-table">
									<a class="btn btn-primary" href="<?php echo base_url(); ?>iklanku/pasang" ><i class="fa fa-plus"></i> Iklan Baru &nbsp;</a>
									<thead>
										<tr class="first last">
											<th>Tanggal</th>
											<th style="width:35%">Judul Iklan</th>
											<th></th>
											<th>Harga</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
						<?php
					}else{
						?>
						<div style="padding-bottom:80px;"></div>
						<!-- <div class="header-member-page">
							<div class="label label-primary label-poin-member">
								<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>
							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->
						<div>
							<ul class="nav nav-tabs nav-tabs-ku">
								<li><a href="<?php echo base_url(); ?>iklanku/">Iklan Aktif <span class="label label-success"><?php echo sizeof($iklan) ?></span></a></li>
								<li><a href="<?php echo base_url(); ?>iklanku/tidakaktif">Iklan Tidak Aktif <span class="label label-success"><?php echo sizeof($iklan_off) ?><span class="label label-success"></a></li>
								<li class="active"><a href="#">Iklan Laku <span class="label label-success"><?php echo sizeof($iklan_laku) ?><span class="label label-success"></a></li>
								<li><a href="<?php echo base_url() ?>iklanku/ditolak">Iklan Ditolak &nbsp;<span class="label label-success"><?php echo sizeof($iklan_ditolak) ?></span></a></li>
								<li><a href="<?php echo base_url() ?>iklanku/premium">Iklan Premium &nbsp;<span class="label label-success"><?php echo sizeof($iklan_premium) ?></span></a></li>
							</ul>
							<div class="table-responsive" style="padding-top:20px;">
								<table class="datatable-ku table custom-table">
									<thead>
										<tr class="first last">
											<th>Tanggal</th>
											<th style="width:35%">Judul Iklan</th>
											<th></th>
											<th>Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach ($iklan_laku as $q_iklan) {
											echo '
											<tr>
												<td>
													<center>'.tgl_indo($q_iklan->tanggal_post).'</center>

												</td>
												<td>
													<div class="link-judul-iklan"><a href="#">'.$q_iklan->judul_iklan.'</a></div>
													<div><i class="fa fa-eye"></i> Dilihat '.$q_iklan->dilihat.' Kali</div>
												</td>
												<td><a class="product-image" title="'.$q_iklan->judul_iklan.'" href="#">
													<center><img class="img-thumbnail" style="width:70px;" alt="'.$q_iklan->judul_iklan.'" src="'.base_url().'images/iklan/'.$q_iklan->gambar[0]->photo.'"></center>
												</a></td>
												
												
												<td class="subtotal">Rp. '.number_format($q_iklan->harga_iklan).'</td>
												
												
											</tr>
											';
										}
										?>
										
									</tbody>
								</table>
							</div>
						</div>
						<?php
					}
					?>
				</div>
				<!--<div class="col-sm-4">
                    	<div class="block block-layered-nav">
                            <div class="block-content">
                                <h2>Menu</h2>
                                <ol>
                                    <li class="active"><a href="#">Iklan</a></li>
                                    <li><a href="#">Account Information</a></li>
                                    <li><a href="#">Address Book</a></li>
                                    <li><a href="<?php echo base_url(); ?>iklanku/pengaturan">Pengaturan</a></li>
                                   
                                </ol>
                            </div>
                        </div>--><!-- /.block - Comments -->
                        <!--</div>-->
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>