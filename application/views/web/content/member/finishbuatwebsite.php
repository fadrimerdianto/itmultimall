<div class="gap130"></div>
<div class="container">
  <div class="main">
    <div class="col-main">
      <div class="buatwebsite-member-page">
        <div class="col-md-3">
          <?php echo $sidebar ?>
        </div>

        <div class="col-md-9">
          <div class="row bs-wizard" style="border-bottom:0;">
            <div class="col-xs-3 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum">Langkah 1</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Register Website Gratis</div>
            </div>

            <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Langkah 2</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Pilih Tema</div>
            </div>

            <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Langkah 3</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Tinjau</div>
            </div>

            <div class="col-xs-3 bs-wizard-step active"><!-- active -->
              <div class="text-center bs-wizard-stepnum">Langkah 4</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"> Selesai</div>
            </div>
          </div>
          <div class="step-detail">
            <center>
            	<h2>Website Anda Berhasi Dibuat</h2>
            	<div><a target="_blank" href="<?php echo base_url().$website->alamat_website ?>"><button onclick="window.location.reload()" class="btn btn-success btn-lg">Buka website</button></a></div>
            </center>
            <!-- <div>Cek di <a href="<?php echo base_url(); ?>lanayacatering">Disini</a></div> -->
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  CKEDITOR.replace('about_text');
</script>