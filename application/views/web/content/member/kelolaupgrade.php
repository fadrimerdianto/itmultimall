<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<style type="text/css">
	/* COMMON PRICING STYLES */
	.panel.price,
	.panel.price>.panel-heading{
		border-radius:0px;
		-moz-transition: all .3s ease;
		-o-transition:  all .3s ease;
		-webkit-transition:  all .3s ease;
	}
	.panel.price:hover{
		box-shadow: 0px 0px 30px rgba(0,0,0, .2);
	}
	.panel.price:hover>.panel-heading{
		box-shadow: 0px 0px 30px rgba(0,0,0, .2) inset;
	}


	.panel.price>.panel-heading{
		box-shadow: 0px 5px 0px rgba(50,50,50, .2) inset;
		text-shadow:0px 3px 0px rgba(50,50,50, .6);
	}

	.price .list-group-item{
		border-bottom-:1px solid rgba(250,250,250, .5);
	}

	.panel.price .list-group-item:last-child {
		border-bottom-right-radius: 0px;
		border-bottom-left-radius: 0px;
	}
	.panel.price .list-group-item:first-child {
		border-top-right-radius: 0px;
		border-top-left-radius: 0px;
	}

	.price .panel-footer {
		color: #fff;
		border-bottom:0px;
		background-color:  rgba(0,0,0, .1);
		box-shadow: 0px 3px 0px rgba(0,0,0, .3);
	}


	.panel.price .btn{
		box-shadow: 0 -1px 0px rgba(50,50,50, .2) inset;
		border:0px;
	}

	/* green panel */
	

	.price.panel-green>.panel-heading {
		color: #fff;
		background-color: #57AC57;
		border-color: #71DF71;
		border-bottom: 1px solid #71DF71;
	}


	.price.panel-green>.panel-body {
		color: #fff;
		background-color: #65C965;
	}


	.price.panel-green>.panel-body .lead{
		text-shadow: 0px 3px 0px rgba(50,50,50, .3);
	}

	.price.panel-green .list-group-item {
		color: #333;
		background-color: rgba(50,50,50, .01);
		font-weight:600;
		text-shadow: 0px 1px 0px rgba(250,250,250, .75);
	}

	/* blue panel */
	

	.price.panel-blue>.panel-heading {
		color: #fff;
		background-color: #608BB4;
		border-color: #78AEE1;
		border-bottom: 1px solid #78AEE1;
	}


	.price.panel-blue>.panel-body {
		color: #fff;
		background-color: #73A3D4;
	}


	.price.panel-blue>.panel-body .lead{
		text-shadow: 0px 3px 0px rgba(50,50,50, .3);
	}

	.price.panel-blue .list-group-item {
		color: #333;
		background-color: rgba(50,50,50, .01);
		font-weight:600;
		text-shadow: 0px 1px 0px rgba(250,250,250, .75);
	}

	/* red price */

	
	.price.panel-red>.panel-heading {
		color: #fff;
		background-color: #D04E50;
		border-color: #FF6062;
		border-bottom: 1px solid #FF6062;
	}


	.price.panel-red>.panel-body {
		color: #fff;
		background-color: #EF5A5C;
	}




	.price.panel-red>.panel-body .lead{
		text-shadow: 0px 3px 0px rgba(50,50,50, .3);
	}

	.price.panel-red .list-group-item {
		color: #333;
		background-color: rgba(50,50,50, .01);
		font-weight:600;
		text-shadow: 0px 1px 0px rgba(250,250,250, .75);
	}

	/* grey price */

	
	.price.panel-grey>.panel-heading {
		color: #fff;
		background-color: #6D6D6D;
		border-color: #B7B7B7;
		border-bottom: 1px solid #B7B7B7;
	}


	.price.panel-grey>.panel-body {
		color: #fff;
		background-color: #808080;
	}



	.price.panel-grey>.panel-body .lead{
		text-shadow: 0px 3px 0px rgba(50,50,50, .3);
	}

	.price.panel-grey .list-group-item {
		color: #333;
		background-color: rgba(50,50,50, .01);
		font-weight:600;
		text-shadow: 0px 1px 0px rgba(250,250,250, .75);
	}

	/* white price */

	
	.price.panel-white>.panel-heading {
		color: #333;
		background-color: #f9f9f9;
		border-color: #ccc;
		border-bottom: 1px solid #ccc;
		text-shadow: 0px 2px 0px rgba(250,250,250, .7);
	}

	.panel.panel-white.price:hover>.panel-heading{
		box-shadow: 0px 0px 30px rgba(0,0,0, .05) inset;
	}

	.price.panel-white>.panel-body {
		color: #fff;
		background-color: #dfdfdf;
	}

	.price.panel-white>.panel-body .lead{
		text-shadow: 0px 2px 0px rgba(250,250,250, .8);
		color:#666;
	}

	.price:hover.panel-white>.panel-body .lead{
		text-shadow: 0px 2px 0px rgba(250,250,250, .9);
		color:#333;
	}

	.price.panel-white .list-group-item {
		color: #333;
		background-color: rgba(50,50,50, .01);
		font-weight:600;
	}

</style>
<div class="container gap130">
	<div class="main">
		<div class="col-main">
			<div class="home-member-page">
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar ?>
				</div>
				
				<div class="col-md-9">
						<!-- <div class="header-member-page">
							<div class="label label-primary label-poin-member">
								<i class="fa fa-credit-card">
								</i> <?php echo $profil->poin_member ?>							
							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->
						<div>
							<ul class="nav nav-tabs nav-tabs-ku">
								<li><a href="<?php echo base_url() ?>iklanku/kelola">Beranda</a></li>
								<li><a href="<?php echo base_url() ?>iklanku/kelola/tampilan">Tampilan</a></li>
								<li><a href="<?php echo base_url() ?>iklanku/kelola/about-us">About Us</a></li>
								<li class="active"><a href="#">Upgrade</a></li>

							</ul>

							<div class="tab-content" style="padding-top:20px;">
								<div id="iklanAktif" class="tab-pane fade in active">
										<!-- Plans -->
										<section id="plans">
												<div class="row">
													<!-- item -->
													<div class="col-md-6 text-center">
														<div class="panel panel-danger panel-pricing">
															<div class="panel-heading">
																<h3>MultiKategori</h3>
															</div>
															<div class="panel-body text-center">
																<p><strong>400 Poin / Tahun</strong></p>
															</div>
															<ul class="list-group text-center">
																<li class="list-group-item"><i class="fa fa-check"></i> Personal use</li>
																<li class="list-group-item"><i class="fa fa-check"></i> Unlimited projects</li>
																<li class="list-group-item"><i class="fa fa-check"></i> 27/7 support</li>
															</ul>
															<div class="panel-footer">
																<a class="btn btn-lg btn-block btn-danger" href="#">Beli Sekarang</a>
															</div>
														</div>
													</div>
													<!-- /item -->
												</div>
										</section>
										<!-- /Plans -->
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		CKEDITOR.replace('about');
	</script>