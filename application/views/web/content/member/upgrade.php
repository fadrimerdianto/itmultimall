<div class="gap130"></div>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="belipoin-page">
				<?php
				if($this->session->userdata('poin_kurang')){
					?>
					<div class="alert alert-warning">
						Maaf, poin anda tidak memenuhi harga upgrade.
					</div>
					<?php
				}

				if($this->session->userdata('upgrade_sukses')){
					?>
					<div class="alert alert-success">
						Berhasil, anda berhasil melakukan upgrade.
					</div>
					<?php
				}
				?>
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar; ?>
				</div>
				<div class="col-md-9">
					<!-- <div class="header-member-page">
						<div class="label label-primary label-poin-member">
							<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>
						</div>
						<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
						<div class="clearfix"></div>
					</div> -->
					<div style="padding-bottom:80px;"></div>
					<div class="upgrade paket-poin">
						<?php
							$disable10='';
							$disable25='';
							$disable50='';
							$disableunl='';
							$pembaruan='';
							if($upgrade->pembaruan == 1){
								$pembaruan = 'disabled';
							}
							if($upgrade->upload10 == 1){
								$disable10 = 'disabled';
							}
							if($upgrade->upload25 == 1){
								$disable25 = 'disabled';
							}
							if($upgrade->upload50 == 1){
								$disable50 = 'disabled';
							}
							if($upgrade->uploadunl == 1){
								$disableunl = 'disabled';
							}

						?>

						<div class="per-poin" style="padding-bottom:15px;">
							<!-- <div class="gambar-poin">
								<img class="img-responsive" src="'.base_url().'images/poin/'.$q_poin->gambar_poin.'">
							</div> -->
							<div class="detail-poin" style="width:60%">
								<div class="nama-paket-poin">
									Pembaruan Otomatis
								</div>
								<div class="harga-poin">
									Harga 25 Poin / Tahun
								</div>
								
							</div>
							<form method="post" action="<?php echo base_url() ?>iklanku/upgrade/upgrade-pembaruan">
								<input name="poin" type="hidden" value="" />
								<div class="tombol-beli-poin">
									<button onClick="return confirm('Apakah Anda yakin untuk mengupgrade ?')" type="submit" <?php echo $pembaruan ?> class="btn btn-success"><i class="fa fa-shopping-cart"></i> Upgrade</button>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>

						<div class="per-poin" style="padding-bottom:15px;">
							<!-- <div class="gambar-poin">
								<img class="img-responsive" src="'.base_url().'images/poin/'.$q_poin->gambar_poin.'">
							</div> -->
							<div class="detail-poin" style="width:60%">
								<div class="nama-paket-poin">
									+ 10 Iklan Upload
								</div>
								<div class="harga-poin">
									Harga 50 Poin / Tahun
								</div>
								
							</div>
							<form method="post" action="<?php echo base_url() ?>iklanku/upgrade/upgrade-upload10">
								<input name="poin" type="hidden" value="" />
								<div class="tombol-beli-poin">
									<button onClick="return confirm('Apakah Anda yakin untuk mengupgrade ?')" type="submit" <?php echo $disable10 ?> class="btn btn-success"><i class="fa fa-shopping-cart"></i> Upgrade</button>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>

						<div class="per-poin" style="padding-bottom:15px;">
							<!-- <div class="gambar-poin">
								<img class="img-responsive" src="'.base_url().'images/poin/'.$q_poin->gambar_poin.'">
							</div> -->
							<div class="detail-poin" style="width:60%">
								<div class="nama-paket-poin">
									+ 25 Iklan Upload
								</div>
								<div class="harga-poin">
									Harga 75 Poin / Tahun
								</div>
								
							</div>
							<form method="post" action="<?php echo base_url() ?>iklanku/upgrade/upgrade-upload25">
								<input name="poin" type="hidden" value="" />
								<div class="tombol-beli-poin">
									<button onClick="return confirm('Apakah Anda yakin untuk mengupgrade ?')" type="submit" <?php echo $disable25 ?> class="btn btn-success"><i class="fa fa-shopping-cart"></i> Upgrade</button>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>

						<div class="per-poin" style="padding-bottom:15px;">
							<!-- <div class="gambar-poin">
								<img class="img-responsive" src="'.base_url().'images/poin/'.$q_poin->gambar_poin.'">
							</div> -->
							<div class="detail-poin" style="width:60%">
								<div class="nama-paket-poin">
									+ 50 Iklan Upload
								</div>
								<div class="harga-poin">
									Harga 100 Poin / Tahun
								</div>
								
							</div>
							<form method="post" action="<?php echo base_url() ?>iklanku/upgrade/upgrade-upload50">
								<input name="poin" type="hidden" value="" />
								<div class="tombol-beli-poin">
									<button onClick="return confirm('Apakah Anda yakin untuk mengupgrade ?')" type="submit" <?php echo $disable50 ?> class="btn btn-success"><i class="fa fa-shopping-cart"></i> Upgrade</button>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>

						<div class="per-poin" style="padding-bottom:15px;">
							<!-- <div class="gambar-poin">
								<img class="img-responsive" src="'.base_url().'images/poin/'.$q_poin->gambar_poin.'">
							</div> -->
							<div class="detail-poin" style="width:60%">
								<div class="nama-paket-poin">
									Unlimited Iklan Upload
								</div>
								<div class="harga-poin">
									Harga 150 Poin / Tahun
								</div>
								
							</div>
							<form method="post" action="<?php echo base_url() ?>iklanku/upgrade/upgrade-uploadunl">
								<input name="poin" type="hidden" value="" />
								<div class="tombol-beli-poin">
									<button onClick="return confirm('Apakah Anda yakin untuk mengupgrade ?')" type="submit" <?php echo $disableunl ?> class="btn btn-success"><i class="fa fa-shopping-cart"></i> Upgrade</button>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>

						<div class="per-poin" style="padding-bottom:15px;">
							<!-- <div class="gambar-poin">
								<img class="img-responsive" src="'.base_url().'images/poin/'.$q_poin->gambar_poin.'">
							</div> -->
							<div class="detail-poin" style="width:60%">
								<div class="nama-paket-poin">
									Domain Sendiri (Bonus Multi Kategori)
								</div>
								<div class="harga-poin">
									Harga 600 Poin / Tahun
								</div>
								
							</div>
							<form method="post" action="<?php echo base_url() ?>iklanku/upgrade/upgrade-domainsendiri">
								<input name="poin" type="hidden" value="" />
								<div class="tombol-beli-poin">
									<button onClick="return confirm('Apakah Anda yakin untuk mengupgrade ?')" type="submit" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Upgrade</button>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>

						<div class="per-poin" style="padding-bottom:15px;">
							<!-- <div class="gambar-poin">
								<img class="img-responsive" src="'.base_url().'images/poin/'.$q_poin->gambar_poin.'">
							</div> -->
							<div class="detail-poin" style="width:60%">
								<div class="nama-paket-poin">
									Multi Kategori
								</div>
								<div class="harga-poin">
									Harga 400 Poin / Tahun
								</div>
								
							</div>
							<form method="post" action="<?php echo base_url() ?>iklanku/upgrade/upgrade-multikategori">
								<input name="poin" type="hidden" value="" />
								<div class="tombol-beli-poin">
									<button onClick="return confirm('Apakah Anda yakin untuk mengupgrade ?')" type="submit" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Upgrade</button>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>

					</div>					
				</div>
			</div>
		</div>
	</div>
</div>

