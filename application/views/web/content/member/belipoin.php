<div class="gap130"></div>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="belipoin-page">
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar; ?>
				</div>
				<div class="col-md-9">
					<div style="padding-bottom:80px;"></div>
					<!-- <div class="header-member-page">
						<div class="label label-primary label-poin-member">
							<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>
						</div>
						<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
						<div class="clearfix"></div>
					</div> -->
					<div class="paket-poin">
						<?php 
						foreach ($poin as $q_poin) {
							echo '
							
							<div class="per-poin">
								<div class="gambar-poin">
									<img class="img-responsive" src="'.base_url().'images/poin/'.$q_poin->gambar_poin.'">
								</div>
								<div class="detail-poin">
									<div class="nama-paket-poin">
										'.$q_poin->nama_poin.'
									</div>
									<div class="harga-poin">
										Harga Rp. '.number_format($q_poin->harga_poin).'
									</div>
									<div class="jumlah-poin">
										Untuk '.$q_poin->jumlah_poin.' Poin
									</div>
								</div>
								<form method="post" action="'.base_url().'iklanku/belipoin/beli/">
									<input name="poin" type="hidden" value="'.$q_poin->id_poin.'" />
									<div class="tombol-beli-poin">
										<button onClick="return confirm(\'Apakah Anda yakin untuk membeli ?\')" type="submit" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Beli Sekarang</button>
									</div>
								</form>
								<div class="clearfix"></div>
							</div>
							
							';
						}
						?>
						
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>

