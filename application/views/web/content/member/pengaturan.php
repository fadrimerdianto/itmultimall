<div class="gap130"></div>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="pasang-page">
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar; ?>
				</div>
				<div class="col-md-9">
					<div class="header-member-page">
						<!-- <div class="label label-primary label-poin-member">
							<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->
						<div class="panel panel-primary"> <div class="panel-heading"> 
							<h3 class="panel-title"><i class="fa fa-gear"></i> Pengaturan</h3> 
						</div> 
						<div class="panel-body">
							<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>iklanku/pengaturan/update">
							<!-- <div class="form-group">
								<label class="col-sm-2 control-label">Kategori <em>*</em></label>
							    <div class="col-sm-4">
							    	<input name="kategori" class="form-control" required/>
							    </div>
							</div> -->							
							<!-- <div class="form-group">
								<label class="col-sm-2 control-label">Kondisi <em>*</em></label>
							    <div class="col-sm-2">
							    	<select class="form-control">
							    		<option>Baru</option>
							    		<option>Bekas</option>
							    	</select>
							    </div>
							</div> -->

							
							<!-- <hr>
							<div class="form-group">
								<label class="col-sm-2 control-label">Provinsi <em>*</em></label>
							    <div class="col-sm-6">
							    	<input class="form-control" name="provinsi" />
							    </div>
							</div> -->
							<div class="form-group">
								<label class="col-sm-3 control-label">Nama <em>*</em></label>
								<div class="col-sm-6">
									<input required class="form-control" name="nama" value="<?php echo $profil->nama_member; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Email <em>*</em></label>
								<div class="col-sm-4">
									<input required type="email" class="form-control" name="email" value="<?php echo $profil->email; ?>" required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Nomer Telepon /  HP <em>*</em></label>
								<div class="col-sm-3">
									<input required class="form-control" name="telepon"value="<?php echo $profil->telepon_member; ?>" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Nomer Telepon /  HP 2</label>
								<div class="col-sm-3">
									<input class="form-control" name="telepon2"value="<?php echo $profil->telepon_member2; ?>"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">ID Line</label>
								<div class="col-sm-3">
									<input class="form-control" name="id_line"value="<?php echo $profil->id_line ?>"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">PIN BB </label>
								<div class="col-sm-2">
									<input class="form-control" name="pinbb" value="<?php echo $profil->bbm_member; ?>"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Provinsi <em>*</em></label>
								<div class="col-sm-3">
									<select required class="form-control" name="provinsi" onchange="getKota(this)">
										<option disabled selected>Pilih Provinsi</option>
										<?php 
										$selected = '';
										foreach ($provinsi as $q_provinsi) {
											if($q_provinsi->id_provinsi == $profil->provinsi_member){
												$selected = 'selected';
											}else{
												$selected = '';
											}
											echo '<option '.$selected.' value="'.$q_provinsi->id_provinsi.'">'.$q_provinsi->nama_provinsi.'</option>';
										}
										?>
									</select>
								</div>
							</div>
							<?php 
							if($profil->kota_member != 0){
								?>
								<div class="form-group">
									<label class="col-sm-3 control-label">Kota <em>*</em></label>
									<div class="col-sm-3">
										<select required id="kota" class="form-control" name="kota">
											<option disabled selected>Pilih Kota</option>
											<?php 
											foreach ($kota as $q_kota) {
												if($q_kota->id_kota == $profil->kota_member){
													$selected = 'selected';
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$q_kota->id_kota.'">'.$q_kota->nama_area.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<?php
							}
							?>
							<div class="form-group">
								<label class="col-md-3 control-label">Alamat</label>
								<div class="col-md-6">
									<textarea name="alamat" class="form-control"></textarea>
								</div>
							</div>
							<hr>
							<button class="btn btn-success pull-right" type="submit">Simpan</button>
						</form>						
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>
</div>