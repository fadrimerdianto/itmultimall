<div class="gap130"></div>

<div class="container">
  <div class="main">
    <div class="col-main">
      <div class="home-member-page">
        <div class="col-md-3">
          <?php echo $sidebar ?>
        </div>
        
        <div class="col-md-9">
          <div class="row bs-wizard" style="border-bottom:0;">
            <div class="col-xs-3 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum">Langkah 1</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Register Website Gratis</div>
            </div>

            <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Langkah 2</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Pilih Tema</div>
            </div>

            <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Langkah 3</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Tinjau</div>
            </div>

            <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
              <div class="text-center bs-wizard-stepnum">Langkah 4</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"> Selesai</div>
            </div>
          </div>
          <div class="step-detail">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo base_url() ?>iklanku/buatwebsite/do_step3">
              <div class="form-group">
                <label class="col-sm-2 control-label">Logo</label>
                <div class="col-md-5">
                  <input type="file" name="logo" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Slider</label>
                <div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
                  <div class="image-placeholder">
                    <img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
                  </div>
                  <div style="padding:0; text-align:center">
                  <input type="file" name="slider1" style="display:none" onchange="readURL(this);" required>
                    <a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
                  <div class="image-placeholder">
                    <img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
                  </div>
                  <div style="padding:0; text-align:center">
                    <input type="file" name="slider2" style="display:none" onchange="readURL(this);">
                    <a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
                  <div class="image-placeholder">
                    <img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
                  </div>
                  <div style="padding:0; text-align:center">
                    <input type="file" name="slider3" style="display:none" onchange="readURL(this);">
                    <a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
                  <div class="image-placeholder">
                    <img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
                  </div>
                  <div style="padding:0; text-align:center">
                    <input type="file" name="slider4" style="display:none" onchange="readURL(this);">
                    <a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Warna</label>
                <div class="col-md-4">
                  <div class="pilihan-warna"></div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                 <button type="submit" class="btn btn-default">Simpan</button>
               </div>
             </div>
           </form>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>

<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $(input).parent().prev().find("img").attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  function openUpload(me){
    $(me).parent().find("input[type=file]").click();
  }
</script>