<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="gap130"></div>
<div class="container">
  <div class="main">
    <div class="col-main">
      <div class="buatwebsite-member-page">
        <div class="col-md-3">
          <?php echo $sidebar ?>
        </div>

        <div class="col-md-9">
          <div class="row bs-wizard" style="border-bottom:0;">
            <div class="col-xs-3 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum">Langkah 1</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Register Website Gratis</div>
            </div>

            <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Langkah 2</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Pilih Tema</div>
            </div>

            <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Langkah 3</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Tinjau</div>
            </div>

            <div class="col-xs-3 bs-wizard-step active"><!-- active -->
              <div class="text-center bs-wizard-stepnum">Langkah 4</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"> Kontak</div>
            </div>
          </div>
          <div class="step-detail">
            <form class="form-horizontal" method="post" action="<?php echo base_url() ?>iklanku/buatwebsite/register">

               <div class="form-group">
                <label class="col-sm-3 control-label">Email</label>
                <div class="col-md-4">
                  <input readonly type="email" class="form-control" value="<?php echo $profil->email ?>" />
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Nomer Telepon /  HP <em>*</em></label>
                <div class="col-sm-3">
                  <input readonly value="<?php echo $profil->telepon_member; ?>" class="form-control" name="telepon" required/>
                  <div class="checkbox">
                    <label>
                      <input value="1" name="sembunyi_telepon1" type="checkbox"> Jangan Tampilkan
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Nomer Telepon /  HP 2</label>
                <div class="col-sm-3">
                  <input readonly value="<?php echo $profil->telepon_member2; ?>" class="form-control" name="telepon2" required/>
                  <div class="checkbox">
                    <label>
                      <input value="1" name="sembunyi_telepon2" type="checkbox"> Jangan Tampilkan
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">LINE ID</label>
                <div class="col-sm-3">
                  <input readonly value="<?php echo $profil->id_line; ?>" class="form-control" name="telepon2" required/>
                  <div class="checkbox">
                    <label>
                      <input value="1" name="sembunyi_line" type="checkbox"> Jangan Tampilkan
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Alamat</label>
                <div class="col-md-6">
                  <textarea class="form-control"><?php echo $profil->alamat ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  CKEDITOR.replace('about_text');
</script>