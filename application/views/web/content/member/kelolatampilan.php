<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="gap130"></div>
<?php 
if($this->session->flashdata('update-tampilan')){
	?>
	<div class="container" style="background:none">
		<div class="alert alert-success">Updated</div>

	</div>
	<?php
}
?>

<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="home-member-page">
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar ?>
				</div>
				
				<div class="col-md-9">
						<div style="padding-bottom:80px;"></div>
					<!-- <div class="header-member-page">
						<div class="label label-primary label-poin-member">
							<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->
						<div>
							<ul class="nav nav-tabs nav-tabs-ku">
								<li><a href="<?php echo base_url() ?>iklanku/kelola">Beranda</a></li>
								<li class="active"><a href="#">Tampilan</a></li>
								<li><a href="<?php echo base_url() ?>iklanku/kelola/about-us">About Us</a></li>

							</ul>

							<div class="tab-content" style="padding-top:20px;">
								<div id="iklanAktif" class="tab-pane fade in active">
									<div>
										<form method="post" action="<?php echo base_url(); ?>iklanku/kelola/update-tampilan" class="form-horizontal" enctype="multipart/form-data">

											<div class="form-group">
												<label class="col-sm-2 control-label">Logo</label>
												<div class="col-md-10">
													<?php 
													if($website->logo != ''){
														echo '<img style="width:100px;" src="'.base_url().'images/logo_replika/'.$website->logo.'"/>';
													}
													?>
													<input type="file" name="logo" />
												</div>
											</div>

											<div class="form-group">
												<div class="col-md-2 col-sm-2">
													<label>Slider</label>
												</div>
												<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
													<div class="image-placeholder">
														<?php 
														if($website->slider1 != ''){
															?>
															<img src="<?php echo base_url(); ?>images/slider_replika/<?php echo $website->slider1 ?>" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
															<?php
														}else{
															?>
															<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
															<?php
														}
														?>
													</div>
													<div style="padding:0; text-align:center">
														<input type="file" name="slider1" style="display:none" onchange="readURL(this);">
														<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>

												<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
													<div class="image-placeholder">
														<?php 
														if($website->slider2 != ''){
															?>
															<img src="<?php echo base_url(); ?>images/slider_replika/<?php echo $website->slider2 ?>" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
															<?php
														}else{
															?>
															<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
															<?php
														}
														?>
													</div>
													<div style="padding:0; text-align:center">
														<input type="file" name="slider2" style="display:none" onchange="readURL(this);">
														<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
													<div class="image-placeholder">
														<?php 
														if($website->slider3 != ''){
															?>
															<img src="<?php echo base_url(); ?>images/slider_replika/<?php echo $website->slider3 ?>" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
															<?php
														}else{
															?>
															<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
															<?php
														}
														?>
													</div>
													<div style="padding:0; text-align:center">
														<input type="file" name="slider3" style="display:none" onchange="readURL(this);">
														<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-md-2 col-sm-2 submit-image" style="padding-top:8px;padding-bottom:8px">
													<div class="image-placeholder">
														<?php 
														if($website->slider4 != ''){
															?>
															<img src="<?php echo base_url(); ?>images/slider_replika/<?php echo $website->slider4 ?>" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
															<?php
														}else{
															?>
															<img src="http://www.suplugins.com/podium/images/placeholder-03.png" class="img-responsive" style="margin:0 auto;object-fit: cover;width: 100%;height: 100px">
															<?php
														}
														?>
													</div>
													<div style="padding:0; text-align:center">
														<input type="file" name="slider4" style="display:none" onchange="readURL(this);">
														<a class="custom-upload btn btn-small btn-primary" onclick="openUpload(this)"><i class="fa fa-upload"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label">Warna</label>
												<div class="col-md-10">

												</div>
											</div>

											<hr>

											<div class="form-group">
												<?php
												foreach ($template as $q_template) {
													$select = '';
													if($website->template == $q_template->id_template){
														$select = 'checked';
													}
													echo '
													<div class="col-md-3">
														<img style="object-fit:cover;" class="img-responsive img-thumbnail" src="'.base_url().'images/template/'.$q_template->id_template.'.png" />

														<center><div>'.$q_template->nama_template.'</div>
															<input '.$select.' type="radio" value="'.$q_template->id_template.'" name="template" /></center>
														</div>
														';
													}
													?>
												</div>
												<hr>
												<div class="form-group">
													<button type="submit" class="btn btn-info col-md-offset-10">Simpan</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			CKEDITOR.replace('about');
		</script>
		<script type="text/javascript">
			function readURL(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$(input).parent().prev().find("img").attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				}
			}
			function openUpload(me){
				$(me).parent().find("input[type=file]").click();
			}
		</script>