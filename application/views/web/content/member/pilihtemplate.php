<div class="gap130"></div>

<div class="container">
  <div class="main">
    <div class="col-main">
      <div class="home-member-page">
        <div class="col-md-3">
          <?php echo $sidebar ?>
        </div>
        
        <div class="col-md-9">
          <div class="row bs-wizard" style="border-bottom:0;">
            <div class="col-xs-3 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum">Langkah 1</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Register Website Gratis</div>
            </div>

            <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Langkah 2</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Pilih Tema</div>
            </div>

            <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Langkah 3</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Tinjau</div>
            </div>

            <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
              <div class="text-center bs-wizard-stepnum">Langkah 4</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"> Kontak</div>
            </div>
          </div>
          <div class="step-detail">
            <form class="form-horizontal" method="post" action="<?php echo base_url() ?>iklanku/buatwebsite/pilihtemplate">
              <?php
                foreach ($template as $q_template) {
                  echo '
                    <div class="col-md-3">
                      <img style="object-fit:fit" class="img-responsive" src="'.base_url().'images/template/'.$q_template->id_template.'.png" />
                      
                      <center><div>'.$q_template->nama_template.'</div>
                      <input type="radio" value="'.$q_template->id_template.'" name="template" /></center>
                    </div>
                  ';
                }
               ?>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">Pilih</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
</div>