<div class="gap130"></div>
<div class="container">
	<div class="main">
		<div class="col-main">
			<div class="pasang-page">
				<div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
					<?php echo $sidebar; ?>
				</div>
				<div class="col-md-9">
					<div class="header-member-page">
						<!-- <div class="label label-primary label-poin-member">
							<i class="fa fa-credit-card"></i> <?php echo $profil->poin_member ?>							</div>
							<a href="<?php echo base_url(); ?>iklanku/pasang"><button class="btn btn-success btn-tambah-iklan">Tambah Iklan</button></a>
							<div class="clearfix"></div>
						</div> -->

						<div class="panel panel-primary"> <div class="panel-heading"> 
							<h3 class="panel-title"><i class="fa fa-gear"></i> Toko yang sudah ada</h3> 
						</div> 
						<div class="panel-body">	
						<ul>

							<div class="table-responsive" style="padding-top:20px;">
								<table class="datatable-ku table custom-table">
									<thead>
										<tr class="first last">
											<th>No</th>
											<th>Nama Toko</th>
											<th style="width:35%">Alamat Toko</th>
											<th>Kontak</th>
											<th>Status Validasi</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$i=0;
										foreach ($toko as $q_toko) {
											$i++;
											echo '
											<tr>
												<td>
													'.$i.' 
												</td>
												<td>
													<div class="link-judul-iklan"><a href="#">'.$q_toko->nama_toko.'</a></div>
												</td>
												<td>
													<div> '.$q_toko->alamat_toko.'</div>
												</td>

												<td>
													<div> '.$q_toko->kontak_toko.'</div>
												</td>
												<td>
													<div> '.$q_toko->status.'</div>
												</td>
												<td>
													<center>
														<a href="'.base_url().'iklanku/hapus_toko/'.$q_toko->id_toko.'"><button onClick="return confirm(\'Apakah Anda yakin untuk menghapus ?\')" class="btn btn-warning btn-sm btn-block">Hapus</button></a>
													</center>
												</td>
												
												
											</tr>
											';
										}
										?>
										
									</tbody>
								</table>
							</div>
						</ul>		
						</div> 				
					</div> 
						<div class="panel panel-primary"> <div class="panel-heading"> 
							<h3 class="panel-title"><i class="fa fa-gear"></i> Toko</h3> 
						</div> 
						<div class="panel-body">
							<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>iklanku/toko/new">
							<!-- <div class="form-group">
								<label class="col-sm-2 control-label">Kategori <em>*</em></label>
							    <div class="col-sm-4">
							    	<input name="kategori" class="form-control" required/>
							    </div>
							</div> -->							
							<!-- <div class="form-group">
								<label class="col-sm-2 control-label">Kondisi <em>*</em></label>
							    <div class="col-sm-2">
							    	<select class="form-control">
							    		<option>Baru</option>
							    		<option>Bekas</option>
							    	</select>
							    </div>
							</div> -->

							
							<!-- <hr>
							<div class="form-group">
								<label class="col-sm-2 control-label">Provinsi <em>*</em></label>
							    <div class="col-sm-6">
							    	<input class="form-control" name="provinsi" />
							    </div>
							</div> -->
							<div class="form-group">
								<label class="col-sm-3 control-label">Nama Toko<em>*</em></label>
								<div class="col-sm-6">
									<input required class="form-control" name="nama" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Mall <em>*</em></label>
								<div class="col-sm-3">
									<select required class="form-control" name="mall">
										<option disabled selected>Pilih Mall</option>
										<?php 
										$selected = '';
										foreach ($mall as $q_mall) {
											echo '<option value="'.$q_mall->id_mall.'">'.$q_mall->nama_mall.'</option>';
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Alamat Toko <em>*</em></label>
								<div class="col-sm-8">
								<textarea required="true" name="alamat_toko"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Kontak Toko<em>*</em></label>
								<div class="col-sm-6">
									<input required class="form-control" name="kontak" />
								</div>
							</div>
							<hr>
							<button class="btn btn-success pull-right" type="submit">Simpan</button>
						</form>						
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>
</div>