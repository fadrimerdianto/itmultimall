<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="gap130"></div>
  <div class="container">
    <div class="main">
      <div class="col-main">
        <div class="buatwebsite-member-page">
        <div class="col-md-3" style="border-right: 4px solid rgb(231, 231, 231); margin-top:70px;">
            <?php echo $sidebar ?>
          </div>
          
          <div class="col-md-9">
            <div class="row bs-wizard" style="border-bottom:0;">
              <div class="col-xs-3 bs-wizard-step active">
                <div class="text-center bs-wizard-stepnum">Langkah 1</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center">Register Website Gratis</div>
              </div>

              <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
                <div class="text-center bs-wizard-stepnum">Langkah 2</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center">Pilih Tema</div>
              </div>

              <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
                <div class="text-center bs-wizard-stepnum">Langkah 3</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center">Tinjau</div>
              </div>

              <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                <div class="text-center bs-wizard-stepnum">Langkah 4</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center"> Selesai</div>
              </div>
            </div>
            <div class="step-detail">
              <form id="buatwebsite1" enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo base_url() ?>iklanku/buatwebsite/register">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Alamat Webiste</label>
                  <div class="col-md-2">
                    <div style="font-size:14px; margin-top:8px;">http://www.</div>
                  </div>
                  <div class="col-md-4">
                    <input id="alamat_website" type="text" name="alamat_website" class="form-control" placeholder="Alamat Website">
                  </div>
                  <div class="col-md-2">
                    <div style="font-size:14px; margin-top:8px;">.itmultimall.com</div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Foto About Us</label>
                  <div class="col-md2">
                    <input type="file" name="about_foto">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">About</label>
                  <div class="col-md-10">
                    <textarea required name="about" id="about_text"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-10 col-sm-10">
                    <?php 
                      if($website != null){
                        echo '<a href="'.base_url().'iklanku/buatwebsite/step2"><button class="btn btn-default">Skip</button></a>';
                      }else{
                        echo '<button type="submit" onClick="confirm(\'Apakah anda yakin mendaftar dengan alamat tersebut ? Setelah mendaftar anda tidak bisa merubah alamat website.\')" class="btn btn-default">Daftar</button>';
                      }
                    ?>
                    
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
<script type="text/javascript">
  CKEDITOR.replace('about_text');
</script>