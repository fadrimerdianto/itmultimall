 <div class="">
    <div class="home-background">
        <div class="container container-home">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-md-offset-4">

                    <div class="login-box">
                        <div class="header-login">
                            <img style="width:150px;" src="<?php echo base_url();?>assets/images/logo.png">
                        </div>
                        <div class="main-login">
                            <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>main/login/do">    
                                <div class="form-group">
                                    <label for="username" class="cols-sm-2 control-label">Username</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="cols-sm-2 control-label">Password</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <button type="submit" class="btn btn-success btn-block login-button">Login</button>
                                </div>
                            </form>
                            <hr>
                            <div style="float:left"><a href="<?php echo base_url(); ?>register">Register</a></div>
                            <div style="float:right">Lupa Password</div>
                            <div class="clearfix">
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>

<!--         <div class="container">
            <div class="main">
                <div class="row">
                    <div class="col-md-9 col-md-offset-3">
                        <div class="flexslider ma-nivoslider">
                            <div class="ma-loading"></div>
                            <div id="ma-inivoslider-banner7" class="slides">
                                <img src="<?php echo base_url(); ?>assets/images/slider/slide-01.jpg" class="dn" alt="" title="#banner7-caption1"  />                           
                                <img src="<?php echo base_url(); ?>assets/images/slider/slide-02.jpg" class="dn" alt="" title="#banner7-caption2"  />
                            </div>
                            <div id="banner7-caption1" class="banner7-caption nivo-html-caption nivo-caption">
                                <div class="timethai"></div>
                                <div class="banner7-content slider-1">
                                    <div class="title-container">
                                        <h1 class="title1">headphones az12</h1>
                                        <h2 class="title2" >Typi non habent claritatem insitam; est usus legentis</h2>                                          
                                    </div>
                                    <div class="banner7-des">
                                        <div class="des">
                                            <h1>sale up to!</h1>
                                            <h2>30% off</h2>
                                            <div class="check-box">
                                                <ul class="list-unstyled">
                                                    <li>With all products in shop</li>
                                                    <li>All combos $69.96</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>                                                                                              
                                    <img class="img1" src="<?php echo base_url(); ?>assets/images/slider/img-04.png" alt="" />                                                                              
                                </div>
                            </div>                      
                            <div id="banner7-caption2" class="banner7-caption nivo-html-caption nivo-caption">
                                <div class="timethai"></div>
                                <div class="banner7-content slider-2">
                                    <div class="title-container">
                                        <h1 class="title1">Samsung s5</h1>
                                        <h2 class="title2" >Typi non habent claritatem insitam; est usus legentis</h2>                                          
                                    </div>
                                    <div class="banner7-des">
                                        <div class="des">
                                            <h1>sale up to!</h1>
                                            <h2>50% off</h2>
                                        </div>
                                    </div>                                                                                              
                                    <img class="img1" src="<?php echo base_url(); ?>assets/images/slider/img-05.png" alt="" />                                                                                  
                                </div>
                            </div>
                        </div> --><!-- /.flexslider -->
                        <!-- <form class="form-search">
                            <img src="images/icon1.png">
                            <input type="text" class="input-text" name="q" id="search" placeholder="Lokasi">
                            <input type="text" class="input-text" name="q" id="search" placeholder="Search">
                            <button type="submit" class="btn btn-danger"><span class="fa fa-search"></span></button>
                        </form> -->
                      <!--   <div class="main-container col1-layout gap130">
                            <div class="container">
                                <div class="main">
                                    <div class="col-main">

                                        <div class="account-login">
                                            <div class="page-title">
                                                <h1>Login or Create an Account</h1>
                                            </div>
                                            <div>
                                                <?php 
                                                if($this->session->userdata('login_first')){
                                                    echo '<div class="alert alert-warning" role="alert">'.$this->session->userdata('login_first').'</div>'; 
                                                }
                                                if($this->session->userdata('login_gagal')){
                                                    echo '<div class="alert alert-warning" role="alert">'.$this->session->userdata('login_gagal').'</div>'; 
                                                }
                                                ?>
                                            </div>
                                            <form  method="post" id="login-form" action="<?php echo base_url(); ?>main/login/do">
                                                <input name="form_key" type="hidden" value="6eEWHcxhaD3YbCx2" />
                                                <div class="col2-set">
                                                    <div class="col-1 new-users">
                                                        <div class="content">
                                                            <h2>New Customers</h2>
                                                            <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-2 registered-users">
                                                        <div class="content">
                                                            <h2>Registered Customers</h2>
                                                            <p>If you have an account with us, please log in.</p>
                                                            <ul class="form-list">
                                                                <li>
                                                                    <label for="email" class="required"><em>*</em>Username</label>
                                                                    <div class="input-box">
                                                                        <input required type="text" name="username" value="" id="email" class="input-text required-entry validate-email" title="Email Address" />
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <label for="pass" class="required"><em>*</em>Password</label>
                                                                    <div class="input-box">
                                                                        <input required type="password" name="password" class="input-text required-entry validate-password" id="pass" title="Password" />
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <p class="required">* Required Fields</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2-set">
                                                    <div class="col-1 new-users">
                                                        <div class="buttons-set">
                                                            <button type="button" title="Create an Account" class="button" onclick="window.location='<?php echo base_url(); ?>main/register';"><span><span>Create an Account</span></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="col-2 registered-users">
                                                        <div class="buttons-set">
                                                            <a href="#" class="f-left">Forgot Your Password?</a>
                                                            <button type="submit" class="button" title="Login" name="send" id="send2"><span><span>Login</span></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
 -->