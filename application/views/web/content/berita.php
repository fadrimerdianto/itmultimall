<div class="gap130"></div>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ol id="products-list" class="blog-list">
                    <li class="item">
                        <div class="item-inner">
                            <div class="row">
                                <?php
                                foreach ($berita as $berita ) {
                                    echo '
                                    <div class="col-sm-4">
                                        <div class="images-container">
                                            <a class="product-image" title="'.$berita->judul_berita.'" href="#" rel="author"><img alt="'.$berita->judul_berita.'" src="'.$berita->gambar.'"></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <h2 class="product-name"><a title="'.$berita->judul_berita.'" href="'.$berita->sumber_berita.'">'.$berita->judul_berita.'</a></h2>
                                        
                                        <div class="blog-attr">
                                            <span>Post by <a href="#">Admin</a></span>
                                            <span class="separator">|</span>
                                            <span>On February 09, 2015</span>
                                        </div>
                                        <div class="desc">'.$berita->isi_berita.'
                                        </div>
                                        <a href="'.$berita->sumber_berita.'" target="_blank" class="btn btn-default btn-readmore">Read more</a>
                                    </div>
                                    ';
                                } 
                                ?>
                            </div>
                        </div>
                    </li>
                </ol>
                <nav>
                  <ul class="pagination">
                      <?php echo $paging;?>
                  </ul>
              </nav>
          </div>
      </div>
  </div>
    </div><!-- /.main -->