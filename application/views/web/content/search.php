        <div class="gap130"></div>

        <?php
        $nama_jenis_iklan = ''; 
        $baru_aktif = '';
        $bekas_aktif = '';
        $semua_aktif = '';
        // if($jenisiklan == 1){
        //     $baru_aktif = 'active';
        //     $nama_jenis_iklan = 'Baru';
        // }elseif($jenisiklan == 2){
        //     $bekas_aktif = 'active';
        //     $nama_jenis_iklan = 'Bekas';
        // }else{
        //     $semua_aktif = 'active';
        // }
        ?>

        <div class="container">
            <div class="breadcrumbs">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="active">Search </li>

                </ul>
            </div>
        </div>
        
        <div class="container container-filter">
            <div class="row">
             <div class="iklan-tengen">
                <img src="<?php echo base_url(); ?>assets/images/iklan-sih-tengen.png">
            </div>
            <div class="iklan-kiwo">
                <img src="<?php echo base_url(); ?>assets/images/iklan-sih-tengen.png">
            </div>
            <div class="col-md-5">
                <div class="kategori-box">
                    <div class="logo-kategori">
                        <img src="<?php echo base_url(); ?>assets/files/favicon.png">
                    </div>
                    <div class="name-kategori">
                        
                                <div>Semua 
                                    
                                <span class="pilih-kota-list">
                                    <a onclick="select_kota()">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <?php 
                                        if(isset($kota)){
                                            echo $kota->nama_area;
                                        }else{
                                            echo "Semua Lokasi";
                                        } 
                                        ?> 
                                    </a>

                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="filterbox">
                        <div class="kategoribox">

                        </div>
                        <?php 
                        if(!isset($_GET['top-25']) AND !isset($_GET['top-shop']) AND !isset($_GET['recommended']) AND !isset($_GET['terlaris']) ){
                            ?>
                            <div class="filtertool">
                                <div class="col-md-4">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="">Pilih Kategori</option>
                                        <?php 
                                        foreach ($kategori as $q_kategori) {
                                            echo '
                                            <option value="'.base_url().'iklan/kategori/'.$q_kategori->seo_kategori.'">'.$q_kategori->kategori.'</option>;
                                            ';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control input-sm">
                                        <option>Sub Kategori</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <?php 
                                    if(sizeof($this->input->get() > 0)){
                                        ?>
                                        <select onchange="window.location.href=this.value" class="form-control input-sm">
                                            <?php
                                        }else{
                                            ?>
                                            <select onchange="window.location.href=this.value" class="form-control ">
                                                <?php
                                            }
                                            ?>
                                            <option selected disabled>Urutan</option>
                                            <?php
                                            $termurah = '';
                                            $terbaru = ''; 
                                            if($this->input->get('sort') == 'termurah'){
                                                $termurah = 'selected';
                                            }elseif($this->input->get('sort') == 'terbaru'){
                                                $terbaru = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $termurah ?> value="?<?php echo http_build_query(array_merge($_GET, ['sort'=>'termurah'])) ?>">Termurah</option>
                                            <option <?php echo $terbaru ?> value="?<?php echo http_build_query(array_merge($_GET, ['sort'=>'terbaru'])) ?>">Terbaru</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>

            <div class="container">
                <div class="main">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="toolbar-wikiloka">
                                <ul class="nav nav-tabs nav-tabs-ku nav-tabs-list" style="float:left">
                                  <!-- <li role="presentation" class="<?php echo $baru_aktif ?>"><a href="<?php echo base_url().$uri2.'/'.$uri3.'/'.$namakategori.'-baru/'.$uri5.'?'.http_build_query($this->input->get()); ?>">Barang Baru <span class="label label-success"><?php echo $barang_baru ?></span></a></li>
                                  <li role="presentation" class="<?php echo $bekas_aktif ?>"><a href="<?php echo base_url().$uri2.'/'.$uri3.'/'.$namakategori.'-bekas/'.$uri5.'?'.http_build_query($this->input->get()); ?>">Barang Bekas <span class="label label-success"><?php echo $barang_bekas ?></span></a></li>
 -->                                  <li role="presentation" class="active"><a href="#">Semua <span class="label label-success"><?php echo sizeof($iklan) ?></span></a></li>
                              </ul>
                          <!-- <div class="pager">
                            <div class="sort-by hidden-xs">

                                <select class="form-control input-sm" onchange="urutkan(this)">
                                    <option value="terbaru" selected="selected">Terbaru</option>
                                    <option value="termurah">Termurah</option>
                                </select>
                                <a title="Set Descending Direction" href="#"><span class="fa fa-sort-amount-desc"></span></a>
                            </div>
                        </div> --><!-- /.pager -->
                        <div class="clearfix"></div>
                    </div>
                    <ol id="products-list" class="products-list">
                        <?php foreach($iklan as $i) { 

                            $gbr = $this->IklanModel->gbr_iklan($i->id_iklan);
                            $gbr = $gbr->row();
                            $isi_berita=strip_tags($i->deskripsi_iklan);
                        $isi = substr($isi_berita,0,100); // ambil sebanyak 00 karakter
                        $isi = substr($isi_berita,0,strrpos($isi," "));

                        $tgl = $i->tanggal_post; 
                        $tanggal = substr($tgl,8,2);
                        $bulan = substr($tgl,5,2);
                        $tahun = substr($tgl,0,4);
                        $jam   = substr($tgl,11,2);
                        $menit = substr($tgl,14,2);
                        $detik = substr($tgl,17,2);
                        $thn_sekarang = date('Y');
                        $bln_sekarang = date('m');

                        // $time = mktime(12, 40, 33, 6, 10, 2009); // 10 July 2009 12:40:33
                        $time = mktime($jam, $menit, $detik, $bulan, $tanggal, $tahun); // 10 July 2009 12:40:33 
                        $timediff = time() - $time; 
                        $tanggal_skr = timeInSentence($timediff, 'id', 1);
                        $tgl_aktif = explode(" ",$tanggal_skr);
                        if ($tahun < $thn_sekarang || $bulan < $bln_sekarang) {
                            $tglnya = tgl_indo($tgl);
                        }else {
                            $tglnya = $tanggal_skr;
                        }

                        if($i->jenis_iklan == 1){
                            $jenis_iklan = 'Baru';
                        }else{
                            $jenis_iklan = 'Bekas';
                        }
                        echo '
                        <li class="item">
                            <div class="item-inner">
                                <div class="row iqbal list-iklan-depan">
                                    <div class="col-sm-2 boby nopadding">
                                        <div class="images-container">
                                            <div class="product_icon nadif">
                                                ';
                                                if($i->status == 1){
                                                    echo '<button style="width:36px; height:36px; padding:0px" title="Hapus Favorit" isFavorit="1" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-unfavorit"><i class="fa fa-star"></i></button>';
                                                }else{
                                                    echo '<button style="width:36px; height:36px;padding:0px" title="Favorit" isFavorit="0" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-favorit"><i class="fa fa-star"></i></button>';
                                                }
                                                echo '
                                                <!--div class="favorit-icon"><a class="disabled-favorit" id_iklan="'.$i->id_iklan.'" onclick="favorit(this)"></a></div-->
                                            </div>
                                            <div class="product_icon fadel">
                                                ';
                                                if($i->id_premium == 1){
                                                    echo '<div class="label-premium">
                                                    <div class="text-premium">Top 25</div>
                                                </div>';
                                            }elseif ($i->id_premium == 2) {
                                                echo '<div class="label-premium premium-top-shop">
                                                <div class="text-premium">Top Shop</div>
                                            </div>';
                                        }elseif ($i->id_premium == 3) {
                                            echo '<div class="label-premium premium-recommended">
                                            <div class="text-premium">Recommended</div>
                                        </div>';
                                    }elseif($i->id_premium == 4){
                                        echo '<div class="label-premium premium-terlaris">
                                        <div class="text-premium">Terlaris</div>
                                    </div>';
                                }
                                echo '
                                <!--<div class="label-istimewa" style="background: url('.base_url().'assets/files/istimewa.png) no-repeat;"><a href="#"></a></div>-->
                            </div>
                            <a class="product-image" style="object-fit:cover/contain;height:150px;" title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'" rel="'.$i->owner.'"><img class="img-cover-list" alt="'.$i->judul_iklan.'" src="'.base_url().'images/iklan/'.$gbr->photo.'"></a>
                        </div>
                    </div>
                    <div class="product-shop col-sm-7 col-sms-4 col-smb-12">
                        <div class="f-fix">
                            <h5 class="product-name"><a title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'">'.$i->judul_iklan.'</a><span class="jenis_iklan"><i style="color: rgb(240, 133, 25);" class="fa fa-bookmark"></i> '.$jenis_iklan.'</span></h5>
                            <div class="list-kategori-iklan">
                                <label class="label label-warning bck-org"><i class="fa fa-tags"></i> '.$i->kategori->kategori.'</label>
                                <i class="fa fa-angle-double-right"></i> '.$i->sub1->sub1_kategori.'
                            </div>
                            <div style="margin-top:15px">
                                <i class="fa fa-clock-o icon-interval-time"></i> <div class="text-interval-time">'.$tglnya.'</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-shop col-sm-3 bar-right nopadding">
                        <h4 class="nomargin pad-8 bck-org text-center">
                            <i class="fa fa-tag"></i> 
                            <label style="color:#000">Rp '.number_format($i->harga_iklan,0,',','.').'</label>
                        </h4>
                        <div class="pad-8" style="font-size:14px;">
                            <ul>
                                <li>
                                    <span class="label label-success">
                                        <i class="fa fa-calendar"></i> &nbsp;'.tgl_indo($tgl).'
                                    </span>
                                </li>
                                <li>
                                    <a href="'.$i->web_iklan.'">
                                        <span class="label label-primary">
                                            <i class="fa fa-chain"></i> Visit Website
                                        </span>
                                    </a>
                                </li>
                                <li style="border-top:1px dashed#d3d3d3;padding-top:5px;margin-top:10px;">
                                    <i class="text-small" style="font-size:10px;color:#000">
                                        <i class="fa fa-map-marker"></i> &nbsp;
                                        '.$i->kota->nama_area.', '.$i->provinsi->nama_provinsi.' 
                                    </i>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        ';
    } ?>
                            <!-- biasa 
                            <li class="item" style="text-align: center;">
                                <a class="" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/contoh-iklan.jpg"></a>
                            </li>
                            <li class="item faisal">
                                <div class="item-inner">
                                    <div class="row iqbal">
                                        <div class="col-sm-2 col-sms-3 col-smb-12 boby">
                                            <div class="images-container">
                                                <div class="product_icon nadif">
                                                    <div class="favorit-icon"><a href="#"></a></div>
                                                </div>
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/products/53448763_1_644x461_alphard-g-atpm-hitam-2012-km-20-ribuan-jakarta-selatan.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-8 col-sms-6 col-smb-12">
                                            <div class="f-fix">
                                                <h5 class="product-name"><a title="Lenovo Yoga" href="detail.html">Lenovo Yoga 3 Core i7 Nvidia 940M 2gb</a></h5>
                                                <p>
                                                    Premium Lenovo Yoga3 dengan Procesor terbaru Core i7 BroadWell Yg super cepat. 
                                                </p>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-2 col-sms-2 col-smb-12">
                                            <div class="f-fix">
                                                    <p class="special-price"><strong>
                                                        <span id="product-price-1" class="price">Rp 10.500.000</span></strong>
                                                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>-->
                        </ol>

                        <nav>
                          <ul class="pagination">
                              <?php echo $paging;?>
                          </ul>
                      </nav>
                  </div><!-- /.col-right -->
              </div>
          </div>
      </div><!-- /.main -->

      <!-- Modal -->
      <div id="modalprov" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:60%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header head_modal">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-th"></i> &nbsp;Pilih Lokasi Pencarian</h4>
                </div>
                <div class="modal-ku modal-body">
                    <?php
                    $jumlah = 1;

                    echo '<div class="col-md-3 kategori-bck scroll">';
                    echo '<ul class="list-group kategori-parent">';
                    echo '<li class="list-group-item ">Semua Provinsi</li>';
                    foreach ($provinsi as $q_provinsi) {
                        if(floor($jumlah/9) == 0){
                            echo '
                            <li class="list-group-item" onclick="pilihprov('.$q_provinsi->id_provinsi.')">'.$q_provinsi->nama_provinsi.'</li>
                            ';
                        }
                        $jumlah++;
                    }
                    echo '</ul>';
                    echo '</div>';

                    $jumlah = 1;
                    echo '<div class="col-md-3 kategori-bck scroll">';
                    echo '<ul class="list-group kategori-parent">';
                    foreach ($provinsi as $q_provinsi) {
                        if(floor($jumlah/9) == 1){
                            echo '
                            <li class="list-group-item" onclick="pilihprov('.$q_provinsi->id_provinsi.')">'.$q_provinsi->nama_provinsi.'</li>
                            ';
                        }
                        $jumlah++;
                    }
                    echo '</ul>';
                    echo '</div>';

                    $jumlah = 1;
                    echo '<div class="col-md-3 kategori-bck scroll">';
                    echo '<ul class="list-group kategori-parent">';
                    foreach ($provinsi as $q_provinsi) {
                        if(floor($jumlah/9) == 2){
                            echo '
                            <li class="list-group-item" onclick="pilihprov('.$q_provinsi->id_provinsi.')">'.$q_provinsi->nama_provinsi.'</li>
                            ';
                        }
                        $jumlah++;
                    }
                    echo '</ul>';
                    echo '</div>';

                    $jumlah = 1;
                    echo '<div class="col-md-3 kategori-bck scroll">';
                    echo '<ul class="list-group kategori-parent">';
                    foreach ($provinsi as $q_provinsi) {
                        if(floor($jumlah/9) == 3){
                            echo '
                            <li class="list-group-item" onclick="pilihprov('.$q_provinsi->id_provinsi.')">'.$q_provinsi->nama_provinsi.'</li>
                            ';
                        }
                        $jumlah++;
                    }
                    echo '</ul>';
                    echo '</div>';
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer modal-footer-ku">
                <article class="text-center">
                    Wikiloka , Pilih Lokasi Pencarian
                </article>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        function favorit (that) {
            var id = $(that).attr('id_iklan');
            if($(that).attr('isFavorit') === '0'){
                $.ajax({
                    'method':'POST',
                    'url':'<?php echo base_url(); ?>iklanku/favorit/add',
                    'data':{id_iklan:id},
                    'success':showAlert
                }).done(function(data){
                    console.log('data ->' + data);
                    var result = JSON.parse(data);
                    if(result['status'] === 'success'){
                        $(that).removeClass('btn-favorit');
                        $(that).addClass("btn-unfavorit");
                        $(that).attr('isFavorit', '1');
                        var jumlah_lama = $('#jumlah-favorit').text();
                        $('#jumlah-favorit').text(parseInt(jumlah_lama)+1);
                        console.log($(that));
                    }
                });
            }else{
               $.ajax({
                'method':'POST',
                'url':'<?php echo base_url(); ?>iklanku/favorit/remove',
                'data':{id_iklan:id},
                'success':showAlertRemove
            }).done(function(data){
                var result = JSON.parse(data);
                if(result['status'] === 'success'){
                    $(that).removeClass('btn-unfavorit');
                    $(that).addClass("btn-favorit");
                    $(that).attr('isFavorit', '0');
                    var jumlah_lama = $('#jumlah-favorit').text();
                    $('#jumlah-favorit').text(parseInt(jumlah_lama)-1);
                }
            });
        }
    }
</script>