        <div class="gap130"></div>

        <?php
        $nama_jenis_iklan = ''; 
        $baru_aktif = '';
        $bekas_aktif = '';
        $semua_aktif = '';
        if($jenisiklan == 1){
            $baru_aktif = 'active';
            $nama_jenis_iklan = 'Baru';
        }elseif($jenisiklan == 2){
            $bekas_aktif = 'active';
            $nama_jenis_iklan = 'Bekas';
        }else{
            $semua_aktif = 'active';
        }
        ?>

        <div class="container">
            <div class="breadcrumbs">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <?php 
                    if($datakategori != null){
                        ?>
                        <li class="active"><?php echo $datakategori->kategori ?> </li>
                        <?php
                    }
                    ?>    
                </ul>
            </div>
        </div>
        
        <div class="container container-filter">
            <div class="row">
             <div class="iklan-tengen">
                <img src="<?php echo base_url(); ?>assets/images/iklan-sih-tengen.png">
            </div>
            <div class="iklan-kiwo">
                <img src="<?php echo base_url(); ?>assets/images/iklan-sih-tengen.png">
            </div>
            <div class="col-md-12">
                <div class="kategori-box">
                    <div class="logo-kategori">
                        <?php 
                        if($datakategori != null){
                            ?>
                            <img src="<?php echo base_url(); ?>images/kategori/<?php echo $datakategori->icon ?>">
                            <?php
                        }else{
                            ?>
                            <img src="<?php echo base_url(); ?>assets/files/favicon.png">
                            <?php
                        }
                        ?>
                    </div>
                    <div class="name-kategori">
                        <?php 
                        if($datakategori != null){
                            ?>
                            <div><?php echo $datakategori->kategori ?> <?php echo $nama_jenis_iklan ?>
                                <?php
                            }elseif(isset($_GET['top-25']) == 1) {
                                echo "<div>Top 25";
                            }elseif(isset($_GET['top-shop']) == 1) {
                                echo "<div>Top Shop";
                            }elseif(isset($_GET['terlaris']) == 1) {
                                echo "<div>Terlaris";
                            }elseif(isset($_GET['recommended']) == 1) {
                                echo "<div>Recommended";
                            }
                            else{
                                ?>
                                <div>Semua 
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="pilih-kota-list">
                                <a onclick="select_kota()">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <?php 
                                    if(isset($kota)){
                                        echo $kota->nama_area;
                                    }else{
                                        echo "Semua Lokasi";
                                    } 
                                    ?> 
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-2">
                    <div class="filterbox">
                        <div class="kategoribox">

                        </div>
                        <?php 
                        if(!isset($_GET['top-25']) AND !isset($_GET['top-shop']) AND !isset($_GET['recommended']) AND !isset($_GET['terlaris']) ){
                            ?>
                            <div class="filtertool">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="">Pilih Kategori</option>
                                            <?php 
                                            foreach ($kategori as $q_kategori) {
                                                if($datakategori->id_kategori == $q_kategori->id_kategori){
                                                    $select = 'selected';
                                                }else{
                                                    $select = '';
                                                }
                                                echo '
                                                <option '.$select.' value="'.base_url().'iklan/kategori/'.$q_kategori->seo_kategori.'">'.$q_kategori->kategori.'</option>;
                                                ';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option>Pilih Sub Kategori</option>
                                            <?php 
                                            foreach ($subkategori as $q_subkategori) {
                                                if($sub1_kategori->id_sub1_kategori == $q_subkategori->id_sub1_kategori){
                                                    $select = 'selected';
                                                }else{
                                                    $select = '';
                                                }
                                                echo '<option '.$select.' value="'.base_url().'iklan/kategori/'.$this->uri->segment(3).'/'.$q_subkategori->seo_sub1_kategori.'">'.$q_subkategori->sub1_kategori.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <?php 
                                        if(sizeof($this->input->get() > 0)){
                                            ?>
                                            <select onchange="window.location.href=this.value" class="form-control input-sm">
                                                <?php
                                            }else{
                                                ?>
                                                <select onchange="window.location.href=this.value" class="form-control ">
                                                    <?php
                                                }
                                                ?>
                                                <option selected disabled>Urutan</option>
                                                <?php
                                                $termurah = '';
                                                $terbaru = ''; 
                                                if($this->input->get('sort') == 'termurah'){
                                                    $termurah = 'selected';
                                                }elseif($this->input->get('sort') == 'terbaru'){
                                                    $terbaru = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $termurah ?> value="?<?php echo http_build_query(array_merge($_GET, ['sort'=>'termurah'])) ?>">Termurah</option>
                                                <option <?php echo $terbaru ?> value="?<?php echo http_build_query(array_merge($_GET, ['sort'=>'terbaru'])) ?>">Terbaru</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="margin-bottom:20px;"></div>
                                    <?php 
                                    if($sub1_kategori != NULL AND $sub1_kategori->id_sub1_kategori == 8){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-4" style="margin-bottom:20px;">
                                                <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                    <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?filter") ?>">Pilih Merk</option>
                                                    <?php 
                                                    foreach ($sub2kategori as $q_sub2) {
                                                        if($_GET['filter'] == $q_sub2->seo_sub2_kategori){
                                                            $select = 'selected';
                                                        }else{
                                                            $select = '';
                                                        }
                                                        echo '<option '.$select.' value="?'.http_build_query(array_merge($_GET, ['filter'=>$q_sub2->seo_sub2_kategori])).'">'.$q_sub2->sub2_kategori.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <?php 
                                            if(isset($_GET['filter'])){
                                                ?>
                                                <div class="col-md-4" style="margin-bottom:20px;">
                                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "tipe") ?>">Tipe Kendaraan</option>
                                                        <?php 
                                                        foreach ($tipe as $q_tipe) {
                                                            if($_GET['tipe'] == $q_tipe->id_tipe_mobil){
                                                                $select = 'selected';
                                                            }else{
                                                                $select = '';
                                                            }
                                                            echo '<option '.$select.' value="?'.http_build_query(array_merge($_GET, ['tipe'=>$q_tipe->id_tipe_mobil])).'">'.$q_tipe->tipe.'</option>';
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                                <div class="col-md-4" style="margin-bottom:20px;">
                                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "transmisi") ?>">Transmisi</option>
                                                        <?php 
                                                        $automatic = '';
                                                        $manual = '';
                                                        $triptonic = '';
                                                        if($_GET['transmisi'] == 'automatic'){
                                                            $automatic = 'selected';
                                                        }elseif ($_GET['transmisi'] == 'manual') {
                                                            $manual = 'selected';
                                                        }elseif ($_GET['transmisi'] == 'triptonic') {
                                                            $triptonic = 'selected';
                                                        }
                                                        ?>
                                                        <option <?php echo $manual ?> value="?<?php echo http_build_query(array_merge($_GET, ['transmisi'=>'manual'])) ?>">Manual</option>
                                                        <option <?php echo $automatic ?> value="?<?php echo http_build_query(array_merge($_GET, ['transmisi'=>'automatic'])) ?>">Automatic</option>
                                                        <option <?php echo $triptonic ?> value="?<?php echo http_build_query(array_merge($_GET, ['transmisi'=>'triptonic'])) ?>">Triptonic</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-8" style="padding: 0px">
                                                    <div class="col-md-6">
                                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                            <?php 
                                                            $h50 = ''; $h60= ''; $h70= ''; $h80= ''; $h90= ''; $h100= ''; $h110= ''; $h120= ''; $h130= ''; $h140= ''; $h150 = ''; $h200=''; $h250='';
                                                            if($_GET['start'] == 50000000){
                                                                $h50 = 'selected';
                                                            }elseif ($_GET['start'] == 60000000) {
                                                                $h60 = 'selected';
                                                            }elseif ($_GET['start'] == 70000000) {
                                                                $h70 = 'selected';
                                                            }elseif ($_GET['start'] == 80000000) {
                                                                $h80 = 'selected';
                                                            }elseif ($_GET['start'] == 90000000) {
                                                                $h90 = 'selected';
                                                            }elseif ($_GET['start'] == 100000000) {
                                                                $h100 = 'selected';
                                                            }elseif ($_GET['start'] == 110000000) {
                                                                $h100 = 'selected';
                                                            }elseif ($_GET['start'] == 120000000) {
                                                                $h120 = 'selected';
                                                            }elseif ($_GET['start'] == 130000000) {
                                                                $h130 = 'selected';
                                                            }elseif ($_GET['start'] == 140000000) {
                                                                $h140 = 'selected';
                                                            }elseif ($_GET['start'] == 150000000) {
                                                                $h150 = 'selected';
                                                            }elseif ($_GET['start'] == 200000000) {
                                                                $h200 = 'selected';
                                                            }elseif ($_GET['start'] == 250000000) {
                                                                $h250 = 'selected';
                                                            }
                                                            ?>
                                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "start") ?>">Harga Awal</option>
                                                            <option <?php echo $h50 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>50000000])) ?>">50.000.000</option>
                                                            <option <?php echo $h60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>60000000])) ?>">60.000.000</option>
                                                            <option <?php echo $h70 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>70000000])) ?>">70.000.000</option>
                                                            <option <?php echo $h80 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>80000000])) ?>">80.000.000</option>
                                                            <option <?php echo $h90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>90000000])) ?>">90.000.000</option>
                                                            <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>100000000])) ?>">100.000.000</option>
                                                            <option <?php echo $h110 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>110000000])) ?>">110.000.000</option>
                                                            <option <?php echo $h120 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>120000000])) ?>">120.000.000</option>
                                                            <option <?php echo $h130 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>130000000])) ?>">130.000.000</option>
                                                            <option <?php echo $h140 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>140000000])) ?>">140.000.000</option>
                                                            <option <?php echo $h150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>150000000])) ?>">150.000.000</option>
                                                            <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>200000000])) ?>">200.000.000</option>
                                                            <option <?php echo $h250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>250000000])) ?>">250.000.000</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                            <?php 
                                                            $h50 = ''; $h60= ''; $h70= ''; $h80= ''; $h90= ''; $h100= ''; $h110= ''; $h120= ''; $h130= ''; $h140= ''; $h150 = ''; $h200=''; $h250='';
                                                            if($_GET['start'] == 50000000){
                                                                $h50 = 'selected';
                                                            }elseif ($_GET['end'] == 60000000) {
                                                                $h60 = 'selected';
                                                            }elseif ($_GET['end'] == 70000000) {
                                                                $h70 = 'selected';
                                                            }elseif ($_GET['end'] == 80000000) {
                                                                $h80 = 'selected';
                                                            }elseif ($_GET['end'] == 90000000) {
                                                                $h90 = 'selected';
                                                            }elseif ($_GET['end'] == 100000000) {
                                                                $h100 = 'selected';
                                                            }elseif ($_GET['end'] == 110000000) {
                                                                $h100 = 'selected';
                                                            }elseif ($_GET['end'] == 120000000) {
                                                                $h120 = 'selected';
                                                            }elseif ($_GET['end'] == 130000000) {
                                                                $h130 = 'selected';
                                                            }elseif ($_GET['end'] == 140000000) {
                                                                $h140 = 'selected';
                                                            }elseif ($_GET['end'] == 150000000) {
                                                                $h150 = 'selected';
                                                            }elseif ($_GET['end'] == 200000000) {
                                                                $h200 = 'selected';
                                                            }elseif ($_GET['end'] == 250000000) {
                                                                $h250 = 'selected';
                                                            }
                                                            ?>
                                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "end") ?>">Harga Akhir</option>
                                                            <option <?php echo $h50 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>50000000])) ?>">50.000.000</option>
                                                            <option <?php echo $h60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>60000000])) ?>">60.000.000</option>
                                                            <option <?php echo $h70 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>70000000])) ?>">70.000.000</option>
                                                            <option <?php echo $h80 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>80000000])) ?>">80.000.000</option>
                                                            <option <?php echo $h90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>90000000])) ?>">90.000.000</option>
                                                            <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>100000000])) ?>">100.000.000</option>
                                                            <option <?php echo $h110 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>110000000])) ?>">110.000.000</option>
                                                            <option <?php echo $h120 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>120000000])) ?>">120.000.000</option>
                                                            <option <?php echo $h130 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>130000000])) ?>">130.000.000</option>
                                                            <option <?php echo $h140 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>140000000])) ?>">140.000.000</option>
                                                            <option <?php echo $h150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>150000000])) ?>">150.000.000</option>
                                                            <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>200000000])) ?>">200.000.000</option>
                                                            <option <?php echo $h250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>250000000])) ?>">250.000.000</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "tahun") ?>">Pilih Tahun</option>
                                                        <?php 
                                                        for ($i=date('Y'); $i > 1990 ; $i--) { 
                                                         if($_GET['tahun'] == $i){
                                                            $select = 'selected';
                                                        }else{
                                                            $select = '';
                                                        }
                                                        echo '<option '.$select.' value="?'.http_build_query(array_merge($_GET, ['tahun'=>$i])).'">'.$i.'</option>';
                                                    }
                                                    ?>
                                                    <option></option>
                                                </select>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    //motor
                                }elseif ($sub1_kategori != NULL AND $sub1_kategori->id_sub1_kategori == 13) {
                                    ?>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?filter") ?>">Pilih Merk</option>
                                            <?php 
                                            foreach ($sub2kategori as $q_sub2) {
                                                if($_GET['filter'] == $q_sub2->seo_sub2_kategori){
                                                    $select = 'selected';
                                                }else{
                                                    $select = '';
                                                }
                                                echo '<option '.$select.' value="?'.http_build_query(array_merge($_GET, ['filter'=>$q_sub2->seo_sub2_kategori])).'">'.$q_sub2->sub2_kategori.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <?php
                                    if(isset($_GET['filter'])){
                                        ?>
                                        <div class="col-md-4" style="margin-bottom:20px;">
                                            <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "tipe") ?>">Tipe Kendaraan</option>
                                                <?php 
                                                foreach ($tipe_motor as $q_tipe) {
                                                    if($_GET['tipe'] == $q_tipe->id_tipe_motor){
                                                        $select = 'selected';
                                                    }else{
                                                        $select = '';
                                                    }
                                                    echo '<option '.$select.' value="?'.http_build_query(array_merge($_GET, ['tipe'=>$q_tipe->id_tipe_mobil])).'">'.$q_tipe->tipe.'</option>';
                                                }
                                                ?>

                                            </select>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="col-md-8" style="padding: 0px">
                                        <div class="col-md-6">
                                            <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                <?php 
                                                $h50 = ''; $h60= ''; $h70= ''; $h80= ''; $h90= ''; $h100= ''; $h110= ''; $h120= ''; $h130= ''; $h140= ''; $h150 = ''; $h200=''; $h250='';
                                                if($_GET['start'] == 3000000){
                                                    $h50 = 'selected';
                                                }elseif ($_GET['start'] == 4000000) {
                                                    $h60 = 'selected';
                                                }elseif ($_GET['start'] == 5000000) {
                                                    $h70 = 'selected';
                                                }elseif ($_GET['start'] == 6000000) {
                                                    $h80 = 'selected';
                                                }elseif ($_GET['start'] == 7000000) {
                                                    $h90 = 'selected';
                                                }elseif ($_GET['start'] == 8000000) {
                                                    $h100 = 'selected';
                                                }elseif ($_GET['start'] == 9000000) {
                                                    $h100 = 'selected';
                                                }elseif ($_GET['start'] == 10000000) {
                                                    $h120 = 'selected';
                                                }elseif ($_GET['start'] == 11000000) {
                                                    $h130 = 'selected';
                                                }elseif ($_GET['start'] == 12000000) {
                                                    $h140 = 'selected';
                                                }elseif ($_GET['start'] == 15000000) {
                                                    $h150 = 'selected';
                                                }elseif ($_GET['start'] == 20000000) {
                                                    $h200 = 'selected';
                                                }elseif ($_GET['start'] == 25000000) {
                                                    $h250 = 'selected';
                                                }
                                                ?>
                                                <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "start") ?>">Harga Awal</option>
                                                <option <?php echo $h50 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>3000000])) ?>">3.000.000</option>
                                                <option <?php echo $h60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>4000000])) ?>">4.000.000</option>
                                                <option <?php echo $h70 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>5000000])) ?>">5.000.000</option>
                                                <option <?php echo $h80 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>6000000])) ?>">6.000.000</option>
                                                <option <?php echo $h90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>7000000])) ?>">7.000.000</option>
                                                <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>8000000])) ?>">8.000.000</option>
                                                <option <?php echo $h110 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>9000000])) ?>">9.000.000</option>
                                                <option <?php echo $h120 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>10000000])) ?>">10.000.000</option>
                                                <option <?php echo $h130 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>11000000])) ?>">11.000.000</option>
                                                <option <?php echo $h140 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>12000000])) ?>">12.000.000</option>
                                                <option <?php echo $h150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>15000000])) ?>">15.000.000</option>
                                                <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>20000000])) ?>">20.000.000</option>
                                                <option <?php echo $h250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>25000000])) ?>">25.000.000</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control input-sm" onchange="window.location.href=this.value">
                                                <?php 
                                                $h50 = ''; $h60= ''; $h70= ''; $h80= ''; $h90= ''; $h100= ''; $h110= ''; $h120= ''; $h130= ''; $h140= ''; $h150 = ''; $h200=''; $h250='';
                                                if($_GET['start'] == 3000000){
                                                    $h50 = 'selected';
                                                }elseif ($_GET['end'] == 4000000) {
                                                    $h60 = 'selected';
                                                }elseif ($_GET['end'] == 5000000) {
                                                    $h70 = 'selected';
                                                }elseif ($_GET['end'] == 6000000) {
                                                    $h80 = 'selected';
                                                }elseif ($_GET['end'] == 7000000) {
                                                    $h90 = 'selected';
                                                }elseif ($_GET['end'] == 8000000) {
                                                    $h100 = 'selected';
                                                }elseif ($_GET['end'] == 9000000) {
                                                    $h100 = 'selected';
                                                }elseif ($_GET['end'] == 10000000) {
                                                    $h120 = 'selected';
                                                }elseif ($_GET['end'] == 11000000) {
                                                    $h130 = 'selected';
                                                }elseif ($_GET['end'] == 12000000) {
                                                    $h140 = 'selected';
                                                }elseif ($_GET['end'] == 15000000) {
                                                    $h150 = 'selected';
                                                }elseif ($_GET['end'] == 20000000) {
                                                    $h200 = 'selected';
                                                }elseif ($_GET['end'] == 25000000) {
                                                    $h250 = 'selected';
                                                }
                                                ?>
                                                <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "end") ?>">Harga Akhir</option>
                                                <option <?php echo $h50 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>3000000])) ?>">3.000.000</option>
                                                <option <?php echo $h60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>4000000])) ?>">4.000.000</option>
                                                <option <?php echo $h70 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>5000000])) ?>">5.000.000</option>
                                                <option <?php echo $h80 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>6000000])) ?>">6.000.000</option>
                                                <option <?php echo $h90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>7000000])) ?>">7.000.000</option>
                                                <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>8000000])) ?>">8.000.000</option>
                                                <option <?php echo $h110 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>9000000])) ?>">9.000.000</option>
                                                <option <?php echo $h120 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>10000000])) ?>">10.000.000</option>
                                                <option <?php echo $h130 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>11000000])) ?>">11.000.000</option>
                                                <option <?php echo $h140 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>12000000])) ?>">12.000.000</option>
                                                <option <?php echo $h150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>15000000])) ?>">15.000.000</option>
                                                <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>20000000])) ?>">20.000.000</option>
                                                <option <?php echo $h250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>25000000])) ?>">25.000.000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "tahun") ?>">Pilih Tahun</option>
                                            <?php 
                                            for ($i=2016; $i > 1990 ; $i--) { 
                                             if($_GET['tahun'] == $i){
                                                $select = 'selected';
                                            }else{
                                                $select = '';
                                            }
                                            echo '<option '.$select.' value="?'.http_build_query(array_merge($_GET, ['tahun'=>$i])).'">'.$i.'</option>';
                                        }
                                        ?>
                                        <!-- <option></option> -->
                                    </select>
                                </div>
                                <?php
                                //rumah
                            }elseif($sub1_kategori != NULL AND $sub1_kategori->id_sub1_kategori == 17){
                                ?>
                                <div class="row">
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?filter") ?>">Pilih Rumah</option>
                                            <?php
                                            foreach ($sub2kategori as $q_sub2) {
                                                if($_GET['filter'] == $q_sub2->seo_sub2_kategori){
                                                    $aktif = 'selected';
                                                }else{
                                                    $aktif = '';
                                                }
                                                echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['filter'=>$q_sub2->seo_sub2_kategori])).'">'.$q_sub2->sub2_kategori.'</option>';

                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "kamar_tidur") ?>">Kamar Tidur</option>
                                            <?php 
                                            for ($i=1; $i <= 10 ; $i++) {
                                                if($_GET['kamar_tidur'] == $i){
                                                    $aktif = 'selected';
                                                }else{
                                                    $aktif = '';
                                                } 
                                                echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_tidur'=>$i])).'">'.$i.'</option>';
                                            }
                                            if($_GET['kamar_tidur'] == 11){
                                                $aktif = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_tidur'=>11])) ?>"> > 10</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "kamar_mandi") ?>">Kamar Mandi</option>
                                            <?php 
                                            for ($i=1; $i <= 10 ; $i++) {
                                                if($_GET['kamar_mandi'] == $i){
                                                    $aktif = 'selected';
                                                }else{
                                                    $aktif = '';
                                                } 
                                                echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_mandi'=>$i])).'">'.$i.'</option>';
                                            }
                                            if($_GET['kamar_mandi'] == 11){
                                                $aktif = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_mandi'=>11])) ?>"> > 10</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "sertifikasi") ?>">Sertifikasi</option>
                                            <?php
                                            $shm = ''; $hgb = ''; $lain = '';
                                            if($_GET['sertifikasi'] == 1){
                                                $shm = 'selected';
                                            }elseif ($_GET['sertifikasi'] == 2) {
                                                $hgb = 'selected';
                                            }elseif ($_GET['sertifikasi'] == 3) {
                                                $lain = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $shm ?> value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>1])) ?>">SHM - Sertifikat Hak Milik</option>
                                            <option <?php echo $hgb ?> value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>2])) ?>">HGB - Hak Guna Bangunan</option>
                                            <option <?php echo $lain ?> value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>3])) ?>">Lainnya (PPJB,Girik, Adat, dll)</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?start") ?>">Harga Awal</option>
                                            <?php 
                                            $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                            if($_GET['start'] == 100000000){
                                                $h100 = 'selected';
                                            }elseif ($_GET['start'] == 200000000) {
                                                $h200 = 'selected';
                                            }elseif ($_GET['start'] == 300000000) {
                                                $h300 = 'selected';
                                            }elseif ($_GET['start'] == 400000000) {
                                                $h400 = 'selected';
                                            }elseif ($_GET['start'] == 500000000) {
                                                $h500 = 'selected';
                                            }elseif ($_GET['start'] == 600000000) {
                                                $h600 = 'selected';
                                            }elseif ($_GET['start'] == 700000000) {
                                                $h700 = 'selected';
                                            }elseif ($_GET['start'] == 800000000) {
                                                $h800 = 'selected';
                                            }elseif ($_GET['start'] == 900000000) {
                                                $h900 = 'selected';
                                            }elseif ($_GET['start'] == 1000000000) {
                                                $h1000 = 'selected';
                                            }elseif ($_GET['start'] == 1500000000) {
                                                $h1500 = 'selected';
                                            }elseif ($_GET['start'] == 2000000000) {
                                                $h2000 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>100000000])) ?>">100.000.000</option>
                                            <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>200000000])) ?>">200.000.000</option>
                                            <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>300000000])) ?>">300.000.000</option>
                                            <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">400.000.000</option>
                                            <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>500000000])) ?>">500.000.000</option>
                                            <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>600000000])) ?>">600.000.000</option>
                                            <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>700000000])) ?>">700.000.000</option>
                                            <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>800000000])) ?>">800.000.000</option>
                                            <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>900000000])) ?>">900.000.000</option>
                                            <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1000000000])) ?>">1.000.000.000</option>
                                            <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1500000000])) ?>">1.500.000.000</option>
                                            <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>2000000000])) ?>">2.000.000.000</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?end") ?>">Harga Akhir</option>
                                            <?php 
                                            $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                            if($_GET['end'] == 100000000){
                                                $h100 = 'selected';
                                            }elseif ($_GET['end'] == 200000000) {
                                                $h200 = 'selected';
                                            }elseif ($_GET['end'] == 300000000) {
                                                $h300 = 'selected';
                                            }elseif ($_GET['end'] == 400000000) {
                                                $h400 = 'selected';
                                            }elseif ($_GET['end'] == 500000000) {
                                                $h500 = 'selected';
                                            }elseif ($_GET['end'] == 600000000) {
                                                $h600 = 'selected';
                                            }elseif ($_GET['end'] == 700000000) {
                                                $h700 = 'selected';
                                            }elseif ($_GET['end'] == 800000000) {
                                                $h800 = 'selected';
                                            }elseif ($_GET['end'] == 900000000) {
                                                $h900 = 'selected';
                                            }elseif ($_GET['end'] == 1000000000) {
                                                $h1000 = 'selected';
                                            }elseif ($_GET['end'] == 1500000000) {
                                                $h1500 = 'selected';
                                            }elseif ($_GET['end'] == 2000000000) {
                                                $h2000 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>100000000])) ?>">100.000.000</option>
                                            <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>200000000])) ?>">200.000.000</option>
                                            <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>300000000])) ?>">300.000.000</option>
                                            <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">400.000.000</option>
                                            <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>500000000])) ?>">500.000.000</option>
                                            <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>600000000])) ?>">600.000.000</option>
                                            <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>700000000])) ?>">700.000.000</option>
                                            <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>800000000])) ?>">800.000.000</option>
                                            <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>900000000])) ?>">900.000.000</option>
                                            <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1000000000])) ?>">1.000.000.000</option>
                                            <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1500000000])) ?>">1.500.000.000</option>
                                            <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>2000000000])) ?>">2.000.000.000</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option>Lantai</option>
                                            <?php 
                                            for ($i=1; $i <= 10 ; $i++) {
                                                if($_GET['lantai'] == $i){
                                                    $aktif = 'selected';
                                                }else{
                                                    $aktif = '';
                                                } 
                                                echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['lantai'=>$i])).'">'.$i.'</option>';
                                            }
                                            if($_GET['lantai'] == 11){
                                                $aktif = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['lantai'=>11])) ?>"> > 10</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_bangunan_awal") ?>">Luas Bangunan</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_bangunan_awal'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_bangunan_akhir") ?>">Luas Bangunan Akhir</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_bangunan_akhir'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>500])) ?>">500</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_tanah_awal") ?>">Luas Tanah</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_tanah_awal'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_tanah_akhir") ?>">Luas Tanah Akhir</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_tanah_akhir'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                    <!-- <div class="col-md-4" style="margin-bottm:20px;">
                                        <select class="form-control input-sm" multiple>
                                            <option>Fasilitas</option>
                                            <option>AC</option>
                                            <option>Godyn</option>
                                        </select>
                                    </div> -->
                                </div>
                                <?php
                                //apartement
                            }elseif($sub1_kategori != NULL AND $sub1_kategori->id_sub1_kategori == 18){
                                ?>
                                <div class="row">
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option>Pilih Apartement</option>
                                            <?php 
                                            foreach ($sub2kategori as $q_sub2) {
                                                if($_GET['filter'] == $q_sub2->seo_sub2_kategori){
                                                    $aktif = 'selected';
                                                }else{
                                                    $aktif = '';
                                                }
                                                echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['filter'=>$q_sub2->seo_sub2_kategori])).'">'.$q_sub2->sub2_kategori.'</option>';

                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option>Kamar Tidur</option>
                                            <?php 
                                            for ($i=1; $i <= 10 ; $i++) { 
                                                echo '<option value="?'.http_build_query(array_merge($_GET, ['kamar_tidur'=>$i])).'">'.$i.'</option>';
                                            }
                                            ?>
                                            <option value="?<?php echo http_build_query(array_merge($_GET, ['kamar_tidur'=>11])) ?>"> > 10</option>
                                        </select>
                                    </div>
                                    
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "sertifikasi") ?>">Sertifikasi</option>
                                            <option value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>1])) ?>">SHM - Sertifikat Hak Milik</option>
                                            <option value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>2])) ?>">HGB - Hak Guna Bangunan</option>
                                            <option value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>3])) ?>">Lainnya (PPJB,Girik, Adat, dll)</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?start") ?>">Harga Awal</option>
                                            <?php 
                                            $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                            if($_GET['start'] == 100000000){
                                                $h100 = 'selected';
                                            }elseif ($_GET['start'] == 200000000) {
                                                $h200 = 'selected';
                                            }elseif ($_GET['start'] == 300000000) {
                                                $h300 = 'selected';
                                            }elseif ($_GET['start'] == 400000000) {
                                                $h400 = 'selected';
                                            }elseif ($_GET['start'] == 500000000) {
                                                $h500 = 'selected';
                                            }elseif ($_GET['start'] == 600000000) {
                                                $h600 = 'selected';
                                            }elseif ($_GET['start'] == 700000000) {
                                                $h700 = 'selected';
                                            }elseif ($_GET['start'] == 800000000) {
                                                $h800 = 'selected';
                                            }elseif ($_GET['start'] == 900000000) {
                                                $h900 = 'selected';
                                            }elseif ($_GET['start'] == 1000000000) {
                                                $h1000 = 'selected';
                                            }elseif ($_GET['start'] == 1500000000) {
                                                $h1500 = 'selected';
                                            }elseif ($_GET['start'] == 2000000000) {
                                                $h2000 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>100000000])) ?>">100.000.000</option>
                                            <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>200000000])) ?>">200.000.000</option>
                                            <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>300000000])) ?>">300.000.000</option>
                                            <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">400.000.000</option>
                                            <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>500000000])) ?>">500.000.000</option>
                                            <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>600000000])) ?>">600.000.000</option>
                                            <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>700000000])) ?>">700.000.000</option>
                                            <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>800000000])) ?>">800.000.000</option>
                                            <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>900000000])) ?>">900.000.000</option>
                                            <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1000000000])) ?>">1.000.000.000</option>
                                            <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1500000000])) ?>">1.500.000.000</option>
                                            <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>2000000000])) ?>">2.000.000.000</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?end") ?>">Harga Akhir</option>
                                            <?php 
                                            $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                            if($_GET['end'] == 100000000){
                                                $h100 = 'selected';
                                            }elseif ($_GET['end'] == 200000000) {
                                                $h200 = 'selected';
                                            }elseif ($_GET['end'] == 300000000) {
                                                $h300 = 'selected';
                                            }elseif ($_GET['end'] == 400000000) {
                                                $h400 = 'selected';
                                            }elseif ($_GET['end'] == 500000000) {
                                                $h500 = 'selected';
                                            }elseif ($_GET['end'] == 600000000) {
                                                $h600 = 'selected';
                                            }elseif ($_GET['end'] == 700000000) {
                                                $h700 = 'selected';
                                            }elseif ($_GET['end'] == 800000000) {
                                                $h800 = 'selected';
                                            }elseif ($_GET['end'] == 900000000) {
                                                $h900 = 'selected';
                                            }elseif ($_GET['end'] == 1000000000) {
                                                $h1000 = 'selected';
                                            }elseif ($_GET['end'] == 1500000000) {
                                                $h1500 = 'selected';
                                            }elseif ($_GET['end'] == 2000000000) {
                                                $h2000 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>100000000])) ?>">100.000.000</option>
                                            <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>200000000])) ?>">200.000.000</option>
                                            <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>300000000])) ?>">300.000.000</option>
                                            <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">400.000.000</option>
                                            <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>500000000])) ?>">500.000.000</option>
                                            <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>600000000])) ?>">600.000.000</option>
                                            <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>700000000])) ?>">700.000.000</option>
                                            <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>800000000])) ?>">800.000.000</option>
                                            <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>900000000])) ?>">900.000.000</option>
                                            <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1000000000])) ?>">1.000.000.000</option>
                                            <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1500000000])) ?>">1.500.000.000</option>
                                            <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>2000000000])) ?>">2.000.000.000</option>
                                        </select>
                                    </div>                                    
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_bangunan_awal") ?>">Luas Bangunan</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_bangunan_awal'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_bangunan_akhir") ?>">Luas Bangunan Akhir</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_bangunan_akhir'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option>Lantai</option>
                                            <?php 
                                            for ($i=1; $i <= 10 ; $i++) {
                                                if($_GET['lantai'] == $i){
                                                    $aktif = 'selected';
                                                }else{
                                                    $aktif = '';
                                                } 
                                                echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['lantai'=>$i])).'">'.$i.'</option>';
                                            }
                                            if($_GET['lantai'] == 11){
                                                $aktif = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['lantai'=>11])) ?>"> > 10</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_tanah_awal") ?>">Luas Tanah</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_tanah_awal'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_tanah_awal'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_awal'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_tanah_akhir") ?>">Luas Tanah Akhir</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_tanah_akhir'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_tanah_akhir'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_tanah_akhir'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                </div>
                                <?php
                            //indekos
                            }elseif($sub1_kategori != NULL AND $sub1_kategori->id_sub1_kategori == 19){
                                ?>
                                <div class="row">
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "kamar_mandi") ?>">Kamar Mandi</option>
                                            <?php 
                                            for ($i=1; $i <= 10 ; $i++) {
                                                if($_GET['kamar_mandi'] == $i){
                                                    $aktif = 'selected';
                                                }else{
                                                    $aktif = '';
                                                } 
                                                echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_mandi'=>$i])).'">'.$i.'</option>';
                                            }
                                            if($_GET['kamar_mandi'] == 11){
                                                $aktif = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_mandi'=>11])) ?>"> > 10</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?start") ?>">Harga Awal</option>
                                            <?php 
                                            $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                            if($_GET['start'] == 300000){
                                                $h100 = 'selected';
                                            }elseif ($_GET['start'] == 500000) {
                                                $h200 = 'selected';
                                            }elseif ($_GET['start'] == 1000000) {
                                                $h300 = 'selected';
                                            }elseif ($_GET['start'] == 2000000) {
                                                $h400 = 'selected';
                                            }elseif ($_GET['start'] == 5000000) {
                                                $h500 = 'selected';
                                            }elseif ($_GET['start'] == 10000000) {
                                                $h600 = 'selected';
                                            }elseif ($_GET['start'] == 15000000) {
                                                $h700 = 'selected';
                                            }elseif ($_GET['start'] == 20000000) {
                                                $h800 = 'selected';
                                            }elseif ($_GET['start'] == 25000000) {
                                                $h900 = 'selected';
                                            }elseif ($_GET['start'] == 30000000) {
                                                $h1000 = 'selected';
                                            }elseif ($_GET['start'] == 50000000) {
                                                $h1500 = 'selected';
                                            }elseif ($_GET['start'] == 100000000) {
                                                $h2000 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>100000000])) ?>">300.000</option>
                                            <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>200000000])) ?>">500.000</option>
                                            <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>300000000])) ?>">1.000.000</option>
                                            <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">2.000.000</option>
                                            <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>500000000])) ?>">5.000.000</option>
                                            <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>600000000])) ?>">10.000.000</option>
                                            <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>700000000])) ?>">15.000.000</option>
                                            <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>800000000])) ?>">20.000.000</option>
                                            <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>900000000])) ?>">25.000.000</option>
                                            <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1000000000])) ?>">30.000.000</option>
                                            <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1500000000])) ?>">50.000.000</option>
                                            <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>2000000000])) ?>">100.000.000</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?end") ?>">Harga Akhir</option>
                                            <?php 
                                            $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                            if($_GET['end'] == 300000){
                                                $h100 = 'selected';
                                            }elseif ($_GET['end'] == 500000) {
                                                $h200 = 'selected';
                                            }elseif ($_GET['end'] == 1000000) {
                                                $h300 = 'selected';
                                            }elseif ($_GET['end'] == 2000000) {
                                                $h400 = 'selected';
                                            }elseif ($_GET['end'] == 5000000) {
                                                $h500 = 'selected';
                                            }elseif ($_GET['end'] == 10000000) {
                                                $h600 = 'selected';
                                            }elseif ($_GET['end'] == 15000000) {
                                                $h700 = 'selected';
                                            }elseif ($_GET['end'] == 20000000) {
                                                $h800 = 'selected';
                                            }elseif ($_GET['end'] == 25000000) {
                                                $h900 = 'selected';
                                            }elseif ($_GET['end'] == 30000000) {
                                                $h1000 = 'selected';
                                            }elseif ($_GET['end'] == 50000000) {
                                                $h1500 = 'selected';
                                            }elseif ($_GET['end'] == 100000000) {
                                                $h2000 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>100000000])) ?>">300.000</option>
                                            <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>200000000])) ?>">500.000</option>
                                            <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>300000000])) ?>">1.000.000</option>
                                            <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>400000000])) ?>">2.000.000</option>
                                            <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>500000000])) ?>">5.000.000</option>
                                            <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>600000000])) ?>">10.000.000</option>
                                            <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>700000000])) ?>">15.000.000</option>
                                            <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>800000000])) ?>">20.000.000</option>
                                            <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>900000000])) ?>">25.000.000</option>
                                            <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1000000000])) ?>">30.000.000</option>
                                            <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1500000000])) ?>">50.000.000</option>
                                            <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>2000000000])) ?>">100.000.000</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_bangunan_awal") ?>">Luas Bangunan</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_bangunan_awal'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_bangunan_awal'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom:20px;">
                                        <select class="form-control input-sm" onchange="window.location.href=this.value">
                                            <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_bangunan_akhir") ?>">Luas Bangunan Akhir</option>
                                            <?php 
                                            $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                            if($_GET['luas_bangunan_akhir'] == 30){
                                                $l30 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 60) {
                                                $l60 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 90) {
                                                $l90 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 125) {
                                                $l125 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 150) {
                                                $l150 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 175) {
                                                $l175 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 225) {
                                                $l225 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 250) {
                                                $l250 = 'selected';
                                            }elseif ($_GET['luas_bangunan_akhir'] == 500) {
                                                $l500 = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>30])) ?>">30</option>
                                            <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>60])) ?>">60</option>
                                            <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>90])) ?>">90</option>
                                            <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>125])) ?>">125</option>
                                            <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>150])) ?>">150</option>
                                            <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>175])) ?>">175</option>
                                            <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>225])) ?>">225</option>
                                            <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>250])) ?>">250</option>
                                            <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>500])) ?>">500</option>
                                        </select>
                                    </div>
                                </div>
                                <?php
                            //Bangunan Komersil
                            }elseif($sub1_kategori != NULL AND $sub1_kategori->id_sub1_kategori == 20){
                                ?>
                                <div class="row">
                                 <div class="col-md-4" style="margin-bottom:20px;">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "kamar_mandi") ?>">Kamar Mandi</option>
                                        <?php 
                                        for ($i=1; $i <= 10 ; $i++) {
                                            if($_GET['kamar_mandi'] == $i){
                                                $aktif = 'selected';
                                            }else{
                                                $aktif = '';
                                            } 
                                            echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_mandi'=>$i])).'">'.$i.'</option>';
                                        }
                                        if($_GET['kamar_mandi'] == 11){
                                            $aktif = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_mandi'=>11])) ?>"> > 10</option>
                                    </select>
                                </div> 
                                <div class="col-md-4" style="margin-bottom:20px;">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?start") ?>">Harga Awal</option>
                                        <?php 
                                        $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                        if($_GET['start'] == 100000000){
                                            $h100 = 'selected';
                                        }elseif ($_GET['start'] == 200000000) {
                                            $h200 = 'selected';
                                        }elseif ($_GET['start'] == 300000000) {
                                            $h300 = 'selected';
                                        }elseif ($_GET['start'] == 400000000) {
                                            $h400 = 'selected';
                                        }elseif ($_GET['start'] == 500000000) {
                                            $h500 = 'selected';
                                        }elseif ($_GET['start'] == 600000000) {
                                            $h600 = 'selected';
                                        }elseif ($_GET['start'] == 700000000) {
                                            $h700 = 'selected';
                                        }elseif ($_GET['start'] == 800000000) {
                                            $h800 = 'selected';
                                        }elseif ($_GET['start'] == 900000000) {
                                            $h900 = 'selected';
                                        }elseif ($_GET['start'] == 1000000000) {
                                            $h1000 = 'selected';
                                        }elseif ($_GET['start'] == 1500000000) {
                                            $h1500 = 'selected';
                                        }elseif ($_GET['start'] == 2000000000) {
                                            $h2000 = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>100000000])) ?>">100.000.000</option>
                                        <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>200000000])) ?>">200.000.000</option>
                                        <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>300000000])) ?>">300.000.000</option>
                                        <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">400.000.000</option>
                                        <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>500000000])) ?>">500.000.000</option>
                                        <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>600000000])) ?>">600.000.000</option>
                                        <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>700000000])) ?>">700.000.000</option>
                                        <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>800000000])) ?>">800.000.000</option>
                                        <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>900000000])) ?>">900.000.000</option>
                                        <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1000000000])) ?>">1.000.000.000</option>
                                        <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1500000000])) ?>">1.500.000.000</option>
                                        <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>2000000000])) ?>">2.000.000.000</option>
                                    </select>
                                </div>
                                <div class="col-md-4" style="margin-bottom:20px;">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?end") ?>">Harga Akhir</option>
                                        <?php 
                                        $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                        if($_GET['end'] == 100000000){
                                            $h100 = 'selected';
                                        }elseif ($_GET['end'] == 200000000) {
                                            $h200 = 'selected';
                                        }elseif ($_GET['end'] == 300000000) {
                                            $h300 = 'selected';
                                        }elseif ($_GET['end'] == 400000000) {
                                            $h400 = 'selected';
                                        }elseif ($_GET['end'] == 500000000) {
                                            $h500 = 'selected';
                                        }elseif ($_GET['end'] == 600000000) {
                                            $h600 = 'selected';
                                        }elseif ($_GET['end'] == 700000000) {
                                            $h700 = 'selected';
                                        }elseif ($_GET['end'] == 800000000) {
                                            $h800 = 'selected';
                                        }elseif ($_GET['end'] == 900000000) {
                                            $h900 = 'selected';
                                        }elseif ($_GET['end'] == 1000000000) {
                                            $h1000 = 'selected';
                                        }elseif ($_GET['end'] == 1500000000) {
                                            $h1500 = 'selected';
                                        }elseif ($_GET['end'] == 2000000000) {
                                            $h2000 = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>100000000])) ?>">100.000.000</option>
                                        <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>200000000])) ?>">200.000.000</option>
                                        <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>300000000])) ?>">300.000.000</option>
                                        <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">400.000.000</option>
                                        <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>500000000])) ?>">500.000.000</option>
                                        <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>600000000])) ?>">600.000.000</option>
                                        <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>700000000])) ?>">700.000.000</option>
                                        <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>800000000])) ?>">800.000.000</option>
                                        <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>900000000])) ?>">900.000.000</option>
                                        <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1000000000])) ?>">1.000.000.000</option>
                                        <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1500000000])) ?>">1.500.000.000</option>
                                        <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>2000000000])) ?>">2.000.000.000</option>
                                    </select>
                                </div>
                                <div class="col-md-4" style="margin-bottom:20px;">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_bangunan_awal") ?>">Luas Bangunan</option>
                                        <?php 
                                        $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                        if($_GET['luas_bangunan_awal'] == 30){
                                            $l30 = 'selected';
                                        }elseif ($_GET['luas_bangunan_awal'] == 60) {
                                            $l60 = 'selected';
                                        }elseif ($_GET['luas_bangunan_awal'] == 90) {
                                            $l90 = 'selected';
                                        }elseif ($_GET['luas_bangunan_awal'] == 125) {
                                            $l125 = 'selected';
                                        }elseif ($_GET['luas_bangunan_awal'] == 150) {
                                            $l150 = 'selected';
                                        }elseif ($_GET['luas_bangunan_awal'] == 175) {
                                            $l175 = 'selected';
                                        }elseif ($_GET['luas_bangunan_awal'] == 225) {
                                            $l225 = 'selected';
                                        }elseif ($_GET['luas_bangunan_awal'] == 250) {
                                            $l250 = 'selected';
                                        }elseif ($_GET['luas_bangunan_awal'] == 500) {
                                            $l500 = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>30])) ?>">30</option>
                                        <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>60])) ?>">60</option>
                                        <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>90])) ?>">90</option>
                                        <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>125])) ?>">125</option>
                                        <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>150])) ?>">150</option>
                                        <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>175])) ?>">175</option>
                                        <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>225])) ?>">225</option>
                                        <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>250])) ?>">250</option>
                                        <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_awal'=>500])) ?>">500</option>
                                    </select>
                                </div>
                                <div class="col-md-4" style="margin-bottom:20px;">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?luas_bangunan_akhir") ?>">Luas Bangunan Akhir</option>
                                        <?php 
                                        $l30 = ''; $l60= ''; $l90= ''; $l125= ''; $l150= ''; $l175= ''; $l225= ''; $l250= ''; $l500= '';
                                        if($_GET['luas_bangunan_akhir'] == 30){
                                            $l30 = 'selected';
                                        }elseif ($_GET['luas_bangunan_akhir'] == 60) {
                                            $l60 = 'selected';
                                        }elseif ($_GET['luas_bangunan_akhir'] == 90) {
                                            $l90 = 'selected';
                                        }elseif ($_GET['luas_bangunan_akhir'] == 125) {
                                            $l125 = 'selected';
                                        }elseif ($_GET['luas_bangunan_akhir'] == 150) {
                                            $l150 = 'selected';
                                        }elseif ($_GET['luas_bangunan_akhir'] == 175) {
                                            $l175 = 'selected';
                                        }elseif ($_GET['luas_bangunan_akhir'] == 225) {
                                            $l225 = 'selected';
                                        }elseif ($_GET['luas_bangunan_akhir'] == 250) {
                                            $l250 = 'selected';
                                        }elseif ($_GET['luas_bangunan_akhir'] == 500) {
                                            $l500 = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $l30 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>30])) ?>">30</option>
                                        <option <?php echo $l60 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>60])) ?>">60</option>
                                        <option <?php echo $l90 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>90])) ?>">90</option>
                                        <option <?php echo $l125 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>125])) ?>">125</option>
                                        <option <?php echo $l150 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>150])) ?>">150</option>
                                        <option <?php echo $l175 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>175])) ?>">175</option>
                                        <option <?php echo $l225 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>225])) ?>">225</option>
                                        <option <?php echo $l250 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>250])) ?>">250</option>
                                        <option <?php echo $l500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['luas_bangunan_akhir'=>500])) ?>">500</option>
                                    </select>
                                </div>
                            </div>
                            <?php
                            //Tanah
                        }elseif($sub1_kategori != NULL AND $sub1_kategori->id_sub1_kategori == 21){
                            ?>
                            <div class="row">
                                <div class="col-md-4" style="margin-bottom:20px;">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?start") ?>">Harga Awal</option>
                                        <?php 
                                        $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                        if($_GET['start'] == 100000000){
                                            $h100 = 'selected';
                                        }elseif ($_GET['start'] == 200000000) {
                                            $h200 = 'selected';
                                        }elseif ($_GET['start'] == 300000000) {
                                            $h300 = 'selected';
                                        }elseif ($_GET['start'] == 400000000) {
                                            $h400 = 'selected';
                                        }elseif ($_GET['start'] == 500000000) {
                                            $h500 = 'selected';
                                        }elseif ($_GET['start'] == 600000000) {
                                            $h600 = 'selected';
                                        }elseif ($_GET['start'] == 700000000) {
                                            $h700 = 'selected';
                                        }elseif ($_GET['start'] == 800000000) {
                                            $h800 = 'selected';
                                        }elseif ($_GET['start'] == 900000000) {
                                            $h900 = 'selected';
                                        }elseif ($_GET['start'] == 1000000000) {
                                            $h1000 = 'selected';
                                        }elseif ($_GET['start'] == 1500000000) {
                                            $h1500 = 'selected';
                                        }elseif ($_GET['start'] == 2000000000) {
                                            $h2000 = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>100000000])) ?>">100.000.000</option>
                                        <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>200000000])) ?>">200.000.000</option>
                                        <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>300000000])) ?>">300.000.000</option>
                                        <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">400.000.000</option>
                                        <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>500000000])) ?>">500.000.000</option>
                                        <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>600000000])) ?>">600.000.000</option>
                                        <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>700000000])) ?>">700.000.000</option>
                                        <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>800000000])) ?>">800.000.000</option>
                                        <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>900000000])) ?>">900.000.000</option>
                                        <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1000000000])) ?>">1.000.000.000</option>
                                        <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>1500000000])) ?>">1.500.000.000</option>
                                        <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>2000000000])) ?>">2.000.000.000</option>
                                    </select>
                                </div>
                                <div class="col-md-4" style="margin-bottom:20px;">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "?end") ?>">Harga Akhir</option>
                                        <?php 
                                        $h100 = ''; $h200= ''; $h300= ''; $h400= ''; $h500= ''; $h600= ''; $h700= ''; $h800= ''; $h900= ''; $h1000= ''; $h1500 = ''; $h2000='';
                                        if($_GET['end'] == 100000000){
                                            $h100 = 'selected';
                                        }elseif ($_GET['end'] == 200000000) {
                                            $h200 = 'selected';
                                        }elseif ($_GET['end'] == 300000000) {
                                            $h300 = 'selected';
                                        }elseif ($_GET['end'] == 400000000) {
                                            $h400 = 'selected';
                                        }elseif ($_GET['end'] == 500000000) {
                                            $h500 = 'selected';
                                        }elseif ($_GET['end'] == 600000000) {
                                            $h600 = 'selected';
                                        }elseif ($_GET['end'] == 700000000) {
                                            $h700 = 'selected';
                                        }elseif ($_GET['end'] == 800000000) {
                                            $h800 = 'selected';
                                        }elseif ($_GET['end'] == 900000000) {
                                            $h900 = 'selected';
                                        }elseif ($_GET['end'] == 1000000000) {
                                            $h1000 = 'selected';
                                        }elseif ($_GET['end'] == 1500000000) {
                                            $h1500 = 'selected';
                                        }elseif ($_GET['end'] == 2000000000) {
                                            $h2000 = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $h100 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>100000000])) ?>">100.000.000</option>
                                        <option <?php echo $h200 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>200000000])) ?>">200.000.000</option>
                                        <option <?php echo $h300 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>300000000])) ?>">300.000.000</option>
                                        <option <?php echo $h400 ?> value="?<?php echo http_build_query(array_merge($_GET, ['start'=>400000000])) ?>">400.000.000</option>
                                        <option <?php echo $h500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>500000000])) ?>">500.000.000</option>
                                        <option <?php echo $h600 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>600000000])) ?>">600.000.000</option>
                                        <option <?php echo $h700 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>700000000])) ?>">700.000.000</option>
                                        <option <?php echo $h800 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>800000000])) ?>">800.000.000</option>
                                        <option <?php echo $h900 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>900000000])) ?>">900.000.000</option>
                                        <option <?php echo $h1000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1000000000])) ?>">1.000.000.000</option>
                                        <option <?php echo $h1500 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>1500000000])) ?>">1.500.000.000</option>
                                        <option <?php echo $h2000 ?> value="?<?php echo http_build_query(array_merge($_GET, ['end'=>2000000000])) ?>">2.000.000.000</option>
                                    </select>
                                </div>
                                <div class="col-md-4" style="margin-bottom:20px;">
                                    <select class="form-control input-sm" onchange="window.location.href=this.value">
                                        <option value="<?php echo remove_querystring_var($_SERVER["REQUEST_URI"], "sertifikasi") ?>">Sertifikasi</option>
                                        <?php
                                        $shm = ''; $hgb = ''; $lain = '';
                                        if($_GET['sertifikasi'] == 1){
                                            $shm = 'selected';
                                        }elseif ($_GET['sertifikasi'] == 2) {
                                            $hgb = 'selected';
                                        }elseif ($_GET['sertifikasi'] == 3) {
                                            $lain = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $shm ?> value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>1])) ?>">SHM - Sertifikat Hak Milik</option>
                                        <option <?php echo $hgb ?> value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>2])) ?>">HGB - Hak Guna Bangunan</option>
                                        <option <?php echo $lain ?> value="?<?php echo http_build_query(array_merge($_GET, ['sertifikasi'=>3])) ?>">Lainnya (PPJB,Girik, Adat, dll)</option>
                                    </select>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</div>

<div class="container">
    <div class="main">
        <div class="row">
            <div class="col-sm-12">
                <div class="toolbar-wikiloka">
                    <ul class="nav nav-tabs nav-tabs-ku nav-tabs-list" style="float:left">
                      <li role="presentation" class="<?php echo $baru_aktif ?>"><a href="<?php echo base_url().$uri2.'/'.$uri3.'/'.$datakategori->seo_kategori.'-baru/'.$uri5.'?'.http_build_query($this->input->get()); ?>">Barang Baru <span class="label label-success"><?php echo $barang_baru ?></span></a></li>
                      <li role="presentation" class="<?php echo $bekas_aktif ?>"><a href="<?php echo base_url().$uri2.'/'.$uri3.'/'.$datakategori->seo_kategori.'-bekas/'.$uri5.'?'.http_build_query($this->input->get()); ?>">Barang Bekas <span class="label label-success"><?php echo $barang_bekas ?></span></a></li>
                      <li role="presentation" class="<?php echo $semua_aktif ?>"><a href="<?php echo base_url().$uri2.'/'.$uri3.'/'.$datakategori->seo_kategori.'/'.$uri5.'?'.http_build_query($this->input->get()); ?>">Semua <span class="label label-success"><?php echo $barang_semua ?></span></a></li>
                  </ul>
                          <!-- <div class="pager">
                            <div class="sort-by hidden-xs">

                                <select class="form-control input-sm" onchange="urutkan(this)">
                                    <option value="terbaru" selected="selected">Terbaru</option>
                                    <option value="termurah">Termurah</option>
                                </select>
                                <a title="Set Descending Direction" href="#"><span class="fa fa-sort-amount-desc"></span></a>
                            </div>
                        </div> --><!-- /.pager -->
                        <div class="clearfix"></div>
                    </div>
                    <ol id="products-list" class="products-list">
                        <?php foreach($iklan as $i) { 

                            $gbr = $this->IklanModel->gbr_iklan($i->id_iklan);
                            $gbr = $gbr->row();
                            $isi_berita=strip_tags($i->deskripsi_iklan);
                        $isi = substr($isi_berita,0,100); // ambil sebanyak 00 karakter
                        $isi = substr($isi_berita,0,strrpos($isi," "));

                        $tgl = $i->tanggal_post; 
                        $tanggal = substr($tgl,8,2);
                        $bulan = substr($tgl,5,2);
                        $tahun = substr($tgl,0,4);
                        $jam   = substr($tgl,11,2);
                        $menit = substr($tgl,14,2);
                        $detik = substr($tgl,17,2);
                        $thn_sekarang = date('Y');
                        $bln_sekarang = date('m');

                        // $time = mktime(12, 40, 33, 6, 10, 2009); // 10 July 2009 12:40:33
                        $time = mktime($jam, $menit, $detik, $bulan, $tanggal, $tahun); // 10 July 2009 12:40:33 
                        $timediff = time() - $time; 
                        $tanggal_skr = timeInSentence($timediff, 'id', 1);
                        $tgl_aktif = explode(" ",$tanggal_skr);
                        if ($tahun < $thn_sekarang || $bulan < $bln_sekarang) {
                            $tglnya = tgl_indo($tgl);
                        }else {
                            $tglnya = $tanggal_skr;
                        }

                        if($i->jenis_iklan == 1){
                            $jenis_iklan = 'Baru';
                        }else{
                            $jenis_iklan = 'Bekas';
                        }
                        echo '
                        <li class="item">
                            <div class="item-inner">
                                ';
                                if($i->tanggal_aktif_top25 > date('Y-m-d') OR $i->tanggal_aktif_recommended > date('Y-m-d')){
                                    echo '<div class="row iqbal list-iklan-depan" style="background:rgb(254, 223, 171) none repeat scroll 0% 0%">';
                                    
                                }else{
                                    echo '<div class="row iqbal list-iklan-depan">';
                                }
                                echo '
                                <div class="col-sm-2 boby nopadding">
                                    <div class="images-container">
                                        <div class="product_icon nadif">
                                            ';
                                            if($i->status == 1){
                                                echo '<button style="width:36px; height:36px; padding:0px" title="Hapus Favorit" isFavorit="1" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-unfavorit"><i class="fa fa-star"></i></button>';
                                            }else{
                                                echo '<button style="width:36px; height:36px;padding:0px" title="Favorit" isFavorit="0" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-favorit"><i class="fa fa-star"></i></button>';
                                            }
                                            echo '
                                            <!--div class="favorit-icon"><a class="disabled-favorit" id_iklan="'.$i->id_iklan.'" onclick="favorit(this)"></a></div-->
                                        </div>
                                        <div class="product_icon fadel">
                                            ';
                                            if($i->tanggal_aktif_top25 > date('Y-m-d')){
                                                echo '<div class="label-premium">
                                                <div class="text-premium">Top 25</div>
                                            </div>';
                                                // }elseif ($i->tanggal_aktif_top25 > date('Y-m-d')) {
                                                //     echo '<div class="label-premium premium-top-shop">
                                                //         <div class="text-premium">Top Shop</div>
                                                //     </div>';
                                        }else{
                                            if($i->tanggal_aktif_terlaris > date('Y-m-d')){
                                                echo '<div class="label-premium premium-terlaris">
                                                <div class="text-premium">Terlaris</div>
                                            </div>';
                                        }
                                    }
                                    echo '
                                    <!--<div class="label-istimewa" style="background: url('.base_url().'assets/files/istimewa.png) no-repeat;"><a href="#"></a></div>-->
                                </div>'.'
                                <a class="product-image" style="object-fit:cover/contain;height:150px;" title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'" rel="'.$i->owner.'"><img class="img-cover-list" alt="'.$i->judul_iklan.'" src="'.base_url().'assets/produk/a/1.jpg"></a>
                            </div>
                        </div>
                        <div class="product-shop col-sm-7 col-sms-4 col-smb-12">
                            <div class="f-fix">
                                <h5 class="product-name"><a title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'">'.$i->judul_iklan.'</a><span class="jenis_iklan"><i style="color: rgb(240, 133, 25);" class="fa fa-bookmark"></i> '.$jenis_iklan.'</span></h5>
                                <div class="list-kategori-iklan">
                                    <label class="label label-warning bck-org"><i class="fa fa-tags"></i> '.$i->kategori->kategori.'</label>
                                    <i class="fa fa-angle-double-right"></i> '.$i->sub1->sub1_kategori.'';
                                    if($i->sub2 != null){
                                        echo '
                                        <i class="fa fa-angle-double-right"></i> '.$i->sub2->sub2_kategori.'
                                        ';
                                    }
                                    echo '
                                </div>
                                <div style="margin-top:15px">
                                    <i class="fa fa-clock-o icon-interval-time"></i> <div class="text-interval-time">'.$tglnya.'</div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="product-shop col-sm-3 bar-right nopadding">
                            <h4 class="nomargin pad-8 bck-org text-center">
                                <i class="fa fa-tag"></i> 
                                <label style="color:#000">Rp '.number_format($i->harga_iklan,0,',','.').'</label>
                            </h4>
                            <div class="pad-8" style="font-size:14px;">
                                <ul>
                                    <li>
                                        <span class="label label-success">
                                            <i class="fa fa-calendar"></i> &nbsp;'.tgl_indo($tgl).'
                                        </span>
                                    </li>
                                    <li>
                                        <a href="'.$i->web_iklan.'">
                                            <span class="label label-primary">
                                                <i class="fa fa-chain"></i> Visit Website
                                            </span>
                                        </a>
                                    </li>
                                    <li style="border-top:1px dashed#d3d3d3;padding-top:5px;margin-top:10px;">
                                        <i class="text-small" style="font-size:10px;color:#000">
                                            <i class="fa fa-map-marker"></i> &nbsp;
                                            '.$i->toko->nama_toko.', '.$i->mall->nama_mall.', '.$i->kota->nama_area.', '.$i->provinsi->nama_provinsi.' 
                                        </i>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            ';
        } ?>
                            <!-- biasa 
                            <li class="item" style="text-align: center;">
                                <a class="" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/contoh-iklan.jpg"></a>
                            </li>
                            <li class="item faisal">
                                <div class="item-inner">
                                    <div class="row iqbal">
                                        <div class="col-sm-2 col-sms-3 col-smb-12 boby">
                                            <div class="images-container">
                                                <div class="product_icon nadif">
                                                    <div class="favorit-icon"><a href="#"></a></div>
                                                </div>
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/products/53448763_1_644x461_alphard-g-atpm-hitam-2012-km-20-ribuan-jakarta-selatan.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-8 col-sms-6 col-smb-12">
                                            <div class="f-fix">
                                                <h5 class="product-name"><a title="Lenovo Yoga" href="detail.html">Lenovo Yoga 3 Core i7 Nvidia 940M 2gb</a></h5>
                                                <p>
                                                    Premium Lenovo Yoga3 dengan Procesor terbaru Core i7 BroadWell Yg super cepat. 
                                                </p>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-2 col-sms-2 col-smb-12">
                                            <div class="f-fix">
                                                    <p class="special-price"><strong>
                                                        <span id="product-price-1" class="price">Rp 10.500.000</span></strong>
                                                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>-->
                        </ol>

                        <nav>
                          <ul class="pagination">
                              <?php echo $paging;?>
                          </ul>
                      </nav>
                  </div><!-- /.col-right -->
              </div>
          </div>
      </div><!-- /.main -->

      <!-- Modal -->
      <div id="modalprov" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:60%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header head_modal">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-th"></i> &nbsp;Pilih Lokasi Pencarian</h4>
                </div>
                <div class="modal-ku modal-body">
                    <?php
                    $jumlah = 1;

                    echo '<div class="col-md-3 kategori-bck scroll">';
                    echo '<ul class="list-group kategori-parent">';
                    echo '<li class="list-group-item ">Semua Provinsi</li>';
                    foreach ($provinsi as $q_provinsi) {
                        if(floor($jumlah/9) == 0){
                            echo '
                            <li class="list-group-item" onclick="pilihprov('.$q_provinsi->id_provinsi.')">'.$q_provinsi->nama_provinsi.'</li>
                            ';
                        }
                        $jumlah++;
                    }
                    echo '</ul>';
                    echo '</div>';

                    $jumlah = 1;
                    echo '<div class="col-md-3 kategori-bck scroll">';
                    echo '<ul class="list-group kategori-parent">';
                    foreach ($provinsi as $q_provinsi) {
                        if(floor($jumlah/9) == 1){
                            echo '
                            <li class="list-group-item" onclick="pilihprov('.$q_provinsi->id_provinsi.')">'.$q_provinsi->nama_provinsi.'</li>
                            ';
                        }
                        $jumlah++;
                    }
                    echo '</ul>';
                    echo '</div>';

                    $jumlah = 1;
                    echo '<div class="col-md-3 kategori-bck scroll">';
                    echo '<ul class="list-group kategori-parent">';
                    foreach ($provinsi as $q_provinsi) {
                        if(floor($jumlah/9) == 2){
                            echo '
                            <li class="list-group-item" onclick="pilihprov('.$q_provinsi->id_provinsi.')">'.$q_provinsi->nama_provinsi.'</li>
                            ';
                        }
                        $jumlah++;
                    }
                    echo '</ul>';
                    echo '</div>';

                    $jumlah = 1;
                    echo '<div class="col-md-3 kategori-bck scroll">';
                    echo '<ul class="list-group kategori-parent">';
                    foreach ($provinsi as $q_provinsi) {
                        if(floor($jumlah/9) == 3){
                            echo '
                            <li class="list-group-item" onclick="pilihprov('.$q_provinsi->id_provinsi.')">'.$q_provinsi->nama_provinsi.'</li>
                            ';
                        }
                        $jumlah++;
                    }
                    echo '</ul>';
                    echo '</div>';
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer modal-footer-ku">
                <article class="text-center">
                    Wikiloka , Pilih Lokasi Pencarian
                </article>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        function favorit (that) {
            var id = $(that).attr('id_iklan');
            if($(that).attr('isFavorit') === '0'){
                $.ajax({
                    'method':'POST',
                    'url':'<?php echo base_url(); ?>iklanku/favorit/add',
                    'data':{id_iklan:id},
                    'success':showAlert
                }).done(function(data){
                    console.log('data ->' + data);
                    var result = JSON.parse(data);
                    if(result['status'] === 'success'){
                        $(that).removeClass('btn-favorit');
                        $(that).addClass("btn-unfavorit");
                        $(that).attr('isFavorit', '1');
                        var jumlah_lama = $('#jumlah-favorit').text();
                        $('#jumlah-favorit').text(parseInt(jumlah_lama)+1);
                        console.log($(that));
                    }
                });
            }else{
               $.ajax({
                'method':'POST',
                'url':'<?php echo base_url(); ?>iklanku/favorit/remove',
                'data':{id_iklan:id},
                'success':showAlertRemove
            }).done(function(data){
                var result = JSON.parse(data);
                if(result['status'] === 'success'){
                    $(that).removeClass('btn-unfavorit');
                    $(that).addClass("btn-favorit");
                    $(that).attr('isFavorit', '0');
                    var jumlah_lama = $('#jumlah-favorit').text();
                    $('#jumlah-favorit').text(parseInt(jumlah_lama)-1);
                }
            });
        }
    }
</script>