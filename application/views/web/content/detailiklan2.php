
<?php 
if($this->session->flashdata('laporan')){
    echo '
    <div class="alert alert-warning alert-dismissible" role="alert" style="margin-left:15%; margin-right:15%">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>Info!</strong> '.$this->session->flashdata('laporan').'
  </div>
  ';
}
?>
<div class="container">
    <div class="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="<?php echo base_url();?>iklan">Iklan</a></li>
            <li class="active"><?php echo $detail->judul_iklan; ?></li>
        </ul>
    </div>
        
        <div class="main">
        	<div class="container">
                <div class="row">
                    <div class="col-sm-3 col-left">
                        <div class="block block-layered-nav">
                            <div class="block-title">
                                <strong><span>Sort By</span></strong>
                            </div>
                            <div class="block-content">
                                <p class="block-subtitle">Shopping Options</p>
                                <div id="narrow-by-list">
                                    <div class="layered layered-Category">
                                        <h2>Category</h2>
                                        <div class="content-shopby">
                                            <ol>
                                                <li><a href="#">Clothing</a></li>
                                                <li><a href="#">Accessories</a></li>
                                                <li><a href="#">Bags</a></li>
                                                <li><a href="#">Shoes</a></li>
                                                <li><a href="#">HandBag</a></li>
                                                <li><a href="#">Fashion</a></li>
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="layered layered-price">
                                        <h2>Price</h2>
                                        <div class="content-shopby">
                                            <div id="slider-range"></div>
                                            <div id="search_pr">
                                                <input type="text" name="first_price" class="form-control">
                                                <input type="text" name="last_price" class="form-control">
                                                <button id="search_price" name="search_price" type="button" class="btn btn-default">SHOW</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layered layered-Color">
                                        <h2>Color</h2>
                                        <div class="content-shopby">
                                            <ol>
                                                <li><a href="#">Red</a></li>
                                                <li><a href="#">Orange</a></li>
                                                <li><a href="#">Blue</a></li>
                                                <li><a href="#">Black</a></li>
                                                <li><a href="#">Green</a></li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /sort -->
                        <div class="block block-list">
                            <div class="block-title">
                                <strong><span>Compare</span></strong>
                            </div>
                            <div class="block-content">
                                <p class="empty">You have no items to compare.</p>
                            </div>
                        </div><!-- /.compare -->
                        <div class="banner-left"><a href="#"><img src="images/ads/ads-01.jpg" alt=""></a>
                            <div class="banner-content">
                                <h1>sale up to</h1>
                                <h2>20% off</h2>
                                <p>on selected products</p>
                                <a href="#">buy now</a>
                            </div>
                        </div>
                    </div><!-- /.col-left -->
                    <div class="col-sm-9 col-right">
                        <div class="product-view">
                        	<div class="row">
                            	<div class="col-sm-5">
                                	<div class="product-img-box">
                                        <p class="product-image">
                                            <a href="<?php echo base_url();?>assets/images/products/1.jpg" class="cloud-zoom" id="ma-zoom1">
                                                <img src="<?php echo base_url();?>assets/images/products/1.jpg" alt="Fusce aliquam" title="Fusce aliquam" />
                                            </a>
                                        </p>
                                        <div class="more-views thumbnail-container">
                                            <ul class="bxslider">
                                                <li class="thumbnail-item">
                                                    <a href="<?php echo base_url();?>assets/images/products/1.jpg" class="cloud-zoom-gallery" title="" name="images/products/1.jpg" rel="useZoom: 'ma-zoom1', smallImage: 'images/products/1.jpg'">
                                                        <img src="<?php echo base_url();?>assets/images/products/1.jpg" alt="" />
                                                    </a>
                                                </li>
                                                <li class="thumbnail-item">
                                                    <a href="<?php echo base_url();?>assets/images/products/2.jpg" class="cloud-zoom-gallery" title="" name="images/products/2.jpg" rel="useZoom: 'ma-zoom1', smallImage: 'images/products/2.jpg'">
                                                        <img src="<?php echo base_url();?>assets/images/products/2.jpg" alt="" />
                                                    </a>
                                                </li>
                                                <li class="thumbnail-item">
                                                    <a href="<?php echo base_url();?>assets/images/products/3.jpg" class="cloud-zoom-gallery" title="" name="images/products/3.jpg" rel="useZoom: 'ma-zoom1', smallImage: 'images/products/3.jpg'">
                                                        <img src="<?php echo base_url();?>assets/images/products/3.jpg" alt="" />
                                                    </a>
                                                </li>
                                                <li class="thumbnail-item">
                                                    <a href="<?php echo base_url();?>assets/images/products/4.jpg" class="cloud-zoom-gallery" title="" name="images/products/4.jpg" rel="useZoom: 'ma-zoom1', smallImage: 'images/products/4.jpg'">
                                                        <img src="<?php echo base_url();?>assets/images/products/4.jpg" alt="" />
                                                    </a>
                                                </li>
                                                <li class="thumbnail-item">
                                                    <a href="<?php echo base_url();?>assets/images/products/5.jpg" class="cloud-zoom-gallery" title="" name="images/products/5.jpg" rel="useZoom: 'ma-zoom1', smallImage: 'images/products/5.jpg'">
                                                        <img src="<?php echo base_url();?>assets/images/products/5.jpg" alt="" />
                                                    </a>
                                                </li>
                                                <li class="thumbnail-item">
                                                    <a href="<?php echo base_url();?>assets/images/products/6.jpg" class="cloud-zoom-gallery" title="" name="images/products/6.jpg" rel="useZoom: 'ma-zoom1', smallImage: 'images/products/6.jpg'">
                                                        <img src="<?php echo base_url();?>assets/images/products/6.jpg" alt="" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-shop col-sm-7">
                                	<div class="product-name">
                                        <h3><?php echo $detail->judul_iklan; ?></h2>
                                    </div>
                                    <div class="ratings">
                                        <span class="price">
                                            <i class="fa fa-eye" style="color:rgb(235, 47, 10); font-size:18px;"></i> Penjual <?php echo $detail->nama_member; ?>
                                        </span><br>
                                        <span class="price"><img src="<?php echo base_url();?>assets/images/jasa-pembuatan-website-direction.png" width="20"> <?php echo $kota->nama_area ?>, <?php echo $kota->provinsi->nama_provinsi ?></span><br>
                                        <span class="price">
                                            <i class="fa fa-eye" style="color:rgb(235, 47, 10); font-size:18px;"></i> Dilihat <?php echo $detail->dilihat ?> Kali
                                        </span>

                        			</div>
                                    <div class="box-container2"> 
                                        <div class="price-box">
                                            <p class="special-price">
                                                <span class="price-label">Special Price</span>
                                            <span id="product-price-1" class="price">RP. <?php echo $this->cart->format_number($detail->harga_iklan); ?></span>
                                            </p>
                                            <p class="old-price">
                                                <span class="price-label">Regular Price:</span>
                                                <span id="old-price-1" class="price">RP. <?php echo $this->cart->format_number(2*$detail->harga_iklan); ?></span>
                                            </p>
                                   		</div>
                                    </div>
                                    <div class="short-description thu">
                                    <h3>Deskripsi Barang:</h3>
                                        <div class="thumbnail" style="min-height: 120px;">
                                            <div class="std bg-info" style="min-height: 120px;"><?php echo $detail->deskripsi_iklan; ?></div>
                                        </div>
                                    </div>
                                    <p class="availability in-stock">Availability: <span>In stock</span></p>
                                </div><!-- /.product-shop -->
                            </div>
                            <!-- <div class="product-tab tab-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#product-desc" data-toggle="tab">PRODUCT DESCRIPTION</a></li>
                                    <li><a href="#product-review" data-toggle="tab">CUSTOMER REVIEW</a></li>
                                    <li><a href="#product-tags" data-toggle="tab">PRODUCT TAGS</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="product-desc">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus
                                    </div>
                                    <div class="tab-pane" id="product-review">...</div>
                                    <div class="tab-pane" id="product-tags">...</div>
                                </div>
                            </div> --><!-- /.product-tab -->
                        </div><!-- /.product-view -->
                    	<!-- <div class="featuredproductslider-container"> 
							<div class="title-group1"><h2>Upsell products</h2></div>
                            <div id="featured-products" class="owl-container">
                                <div class="owl">
                                    <div class='productslider-item item'>
                                        <div class="item-inner">
                                            <div class="images-container">
                                                <div class="product_icon">
                                                    <div class='new-icon'><span>new</span></div>
                                                </div>
                                                <a href="#" title="Nunc facilisis" class="product-image">
                                                	<img src="<?php echo base_url();?>assets/images/products/8.jpg" alt="Nunc facilisis" />
                                                </a>
                                                <div class="box-hover">
                                                    <ul class="add-to-links">
                                                        <li><a href="#" class="link-quickview">Quick View</a></li>
                                                        <li><a href="#" class="link-wishlist">Add to Wishlist</a></li>
                                                        <li><a href="#" class="link-compare">Add to Compare</a></li>
                                                        <li><a href="#" class="link-cart">Add to Cart</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="des-container">
                                                <h2 class="product-name"><a href="#" title="Nunc facilisis">Nunc facilisis</a></h2>
                                                <div class="price-box">
                                                    <p class="special-price">
                                                        <span class="price-label">Special Price</span>
                                                        <span class="price">$169.99</span>
                                                    </p>
                                                    <p class="old-price">
                                                    	<span class="price-label">Regular Price: </span>
                                                        <span class="price">$189.00</span>
                                                    </p>
                                                </div>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <div class="rating" style="width:67%"></div>
                                                    </div>
                                                    <span class="amount"><a href="#">3 Review(s)</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='productslider-item item'>
                                        <div class="item-inner">
                                            <div class="images-container">
                                                <div class="product_icon">
                                                    <div class='new-icon'><span>new</span></div>
                                                </div>
                                                <a href="#" title="Nunc facilisis" class="product-image">
                                                	<img src="<?php echo base_url();?>assets/images/products/3.jpg" alt="Nunc facilisis" />
                                                </a>
                                                <div class="box-hover">
                                                    <ul class="add-to-links">
                                                        <li><a href="#" class="link-quickview">Quick View</a></li>
                                                        <li><a href="#" class="link-wishlist">Add to Wishlist</a></li>
                                                        <li><a href="#" class="link-compare">Add to Compare</a></li>
                                                        <li><a href="#" class="link-cart">Add to Cart</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="des-container">
                                                <h2 class="product-name"><a href="#" title="Nunc facilisis">Nunc facilisis</a></h2>
                                                <div class="price-box">
                                                    <p class="special-price">
                                                        <span class="price-label">Special Price</span>
                                                        <span class="price">$169.99</span>
                                                    </p>
                                                    <p class="old-price">
                                                    	<span class="price-label">Regular Price: </span>
                                                        <span class="price">$189.00</span>
                                                    </p>
                                                </div>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <div class="rating" style="width:67%"></div>
                                                    </div>
                                                    <span class="amount"><a href="#">3 Review(s)</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='productslider-item item'>
                                        <div class="item-inner">
                                            <div class="images-container">
                                                <div class="product_icon">
                                                    <div class='new-icon'><span>new</span></div>
                                                </div>
                                                <a href="#" title="Nunc facilisis" class="product-image">
                                                	<img src="<?php echo base_url();?>assets/images/products/21.jpg" alt="Nunc facilisis" />
                                                </a>
                                                <div class="box-hover">
                                                    <ul class="add-to-links">
                                                        <li><a href="#" class="link-quickview">Quick View</a></li>
                                                        <li><a href="#" class="link-wishlist">Add to Wishlist</a></li>
                                                        <li><a href="#" class="link-compare">Add to Compare</a></li>
                                                        <li><a href="#" class="link-cart">Add to Cart</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="des-container">
                                                <h2 class="product-name"><a href="#" title="Nunc facilisis">Nunc facilisis</a></h2>
                                                <div class="price-box">
                                                    <p class="special-price">
                                                        <span class="price-label">Special Price</span>
                                                        <span class="price">$169.99</span>
                                                    </p>
                                                    <p class="old-price">
                                                    	<span class="price-label">Regular Price: </span>
                                                        <span class="price">$189.00</span>
                                                    </p>
                                                </div>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <div class="rating" style="width:67%"></div>
                                                    </div>
                                                    <span class="amount"><a href="#">3 Review(s)</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='productslider-item item'>
                                        <div class="item-inner">
                                            <div class="images-container">
                                                <div class="product_icon">
                                                    <div class='new-icon'><span>new</span></div>
                                                </div>
                                                <a href="#" title="Nunc facilisis" class="product-image">
                                                	<img src="<?php echo base_url();?>assets/images/products/6.jpg" alt="Nunc facilisis" />
                                                </a>
                                                <div class="box-hover">
                                                    <ul class="add-to-links">
                                                        <li><a href="#" class="link-quickview">Quick View</a></li>
                                                        <li><a href="#" class="link-wishlist">Add to Wishlist</a></li>
                                                        <li><a href="#" class="link-compare">Add to Compare</a></li>
                                                        <li><a href="#" class="link-cart">Add to Cart</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="des-container">
                                                <h2 class="product-name"><a href="#" title="Nunc facilisis">Nunc facilisis</a></h2>
                                                <div class="price-box">
                                                    <p class="special-price">
                                                        <span class="price-label">Special Price</span>
                                                        <span class="price">$169.99</span>
                                                    </p>
                                                    <p class="old-price">
                                                    	<span class="price-label">Regular Price: </span>
                                                        <span class="price">$189.00</span>
                                                    </p>
                                                </div>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <div class="rating" style="width:67%"></div>
                                                    </div>
                                                    <span class="amount"><a href="#">3 Review(s)</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='productslider-item item'>
                                        <div class="item-inner">
                                            <div class="images-container">
                                                <div class="product_icon">
                                                    <div class='new-icon'><span>new</span></div>
                                                </div>
                                                <a href="#" title="Nunc facilisis" class="product-image">
                                                	<img src="<?php echo base_url();?>assets/images/products/10.jpg" alt="Nunc facilisis" />
                                                </a>
                                                <div class="box-hover">
                                                    <ul class="add-to-links">
                                                        <li><a href="#" class="link-quickview">Quick View</a></li>
                                                        <li><a href="#" class="link-wishlist">Add to Wishlist</a></li>
                                                        <li><a href="#" class="link-compare">Add to Compare</a></li>
                                                        <li><a href="#" class="link-cart">Add to Cart</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="des-container">
                                                <h2 class="product-name"><a href="#" title="Nunc facilisis">Nunc facilisis</a></h2>
                                                <div class="price-box">
                                                    <p class="special-price">
                                                        <span class="price-label">Special Price</span>
                                                        <span class="price">$169.99</span>
                                                    </p>
                                                    <p class="old-price">
                                                    	<span class="price-label">Regular Price: </span>
                                                        <span class="price">$189.00</span>
                                                    </p>
                                                </div>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <div class="rating" style="width:67%"></div>
                                                    </div>
                                                    <span class="amount"><a href="#">3 Review(s)</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='productslider-item item'>
                                        <div class="item-inner">
                                            <div class="images-container">
                                                <div class="product_icon">
                                                    <div class='new-icon'><span>new</span></div>
                                                </div>
                                                <a href="#" title="Nunc facilisis" class="product-image">
                                                	<img src="<?php echo base_url();?>assets/images/products/14.jpg" alt="Nunc facilisis" />
                                                </a>
                                                <div class="box-hover">
                                                    <ul class="add-to-links">
                                                        <li><a href="#" class="link-quickview">Quick View</a></li>
                                                        <li><a href="#" class="link-wishlist">Add to Wishlist</a></li>
                                                        <li><a href="#" class="link-compare">Add to Compare</a></li>
                                                        <li><a href="#" class="link-cart">Add to Cart</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="des-container">
                                                <h2 class="product-name"><a href="#" title="Nunc facilisis">Nunc facilisis</a></h2>
                                                <div class="price-box">
                                                    <p class="special-price">
                                                        <span class="price-label">Special Price</span>
                                                        <span class="price">$169.99</span>
                                                    </p>
                                                    <p class="old-price">
                                                    	<span class="price-label">Regular Price: </span>
                                                        <span class="price">$189.00</span>
                                                    </p>
                                                </div>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <div class="rating" style="width:67%"></div>
                                                    </div>
                                                    <span class="amount"><a href="#">3 Review(s)</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='productslider-item item'>
                                        <div class="item-inner">
                                            <div class="images-container">
                                                <div class="product_icon">
                                                    <div class='new-icon'><span>new</span></div>
                                                </div>
                                                <a href="#" title="Nunc facilisis" class="product-image">
                                                	<img src="<?php echo base_url();?>assets/images/products/20.jpg" alt="Nunc facilisis" />
                                                </a>
                                                <div class="box-hover">
                                                    <ul class="add-to-links">
                                                        <li><a href="#" class="link-quickview">Quick View</a></li>
                                                        <li><a href="#" class="link-wishlist">Add to Wishlist</a></li>
                                                        <li><a href="#" class="link-compare">Add to Compare</a></li>
                                                        <li><a href="#" class="link-cart">Add to Cart</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="des-container">
                                                <h2 class="product-name"><a href="#" title="Nunc facilisis">Nunc facilisis</a></h2>
                                                <div class="price-box">
                                                    <p class="special-price">
                                                        <span class="price-label">Special Price</span>
                                                        <span class="price">$169.99</span>
                                                    </p>
                                                    <p class="old-price">
                                                    	<span class="price-label">Regular Price: </span>
                                                        <span class="price">$189.00</span>
                                                    </p>
                                                </div>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <div class="rating" style="width:67%"></div>
                                                    </div>
                                                    <span class="amount"><a href="#">3 Review(s)</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            	</div>
                        	</div>
                        </div> --><!-- /.featuredproductslider-container -->
                    </div><!-- /.col-right -->
                </div>
            </div>
        </div><!-- /.main -->
</div>
<script src="<?php echo base_url();?>assets/js/jquery.bxslider.min.js"></script>