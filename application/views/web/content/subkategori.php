<div class="gap130">
	
</div>
<div class="container"> 
	<div class="main">
		<div class="row">
			<div class="col-md-4">
				<div class="kategori-utama">
					<ul class="nav nav-pills nav-stacked">
						<li role="presentation"><a href="#">Semua Iklan</a></li>
						<hr>
						<?php
						$sub = $this->uri->segment(3);
						$aktif = '';
						foreach ($kategori as $q_kategori) {
							if($q_kategori->seo_kategori == $sub){
								$aktif = 'class="active"';
							}else{
								$aktif='';
							}
							?>
							<li <?php echo $aktif ?> role="presentation"><a role="tab" data-toggle="tab" href="#<?php echo $q_kategori->seo_kategori ?>"><?php echo $q_kategori->kategori ?></a></li>
							<?php
						}
						?>
					</ul>
				</div>
			</div>
			<div class="col-md-8">
				<div class="tab-panel">
					<!-- Tab panes -->
					<div class="tab-content">
						<?php
						foreach ($kategori as $q_kategori) {
							if($q_kategori->seo_kategori == $sub){
								$aktif = 'active';
							}else{
								$aktif='';
							}
							?>
								<div role="tabpanel" class="tab-pane <?php echo $aktif ?>" id="<?php echo $q_kategori->seo_kategori; ?>">
									<div style="text-align:center">
										<img style="width:100px;" src="<?php echo base_url(); ?>images/kategori/<?php echo $q_kategori->icon ?>">
										<div style="font-size:20px; font-weight:600"><?php echo $q_kategori->kategori ?></div>
										<div style="color:gray"><?php echo $q_kategori->jumlahkategoriiklan ?> iklan</div>
										<div><a href="<?php echo base_url(); ?>iklan/kategori/<?php echo $q_kategori->seo_kategori ?>">Iklan kategori ini <i class="fa fa-angle-double-right"></i></a></div>
									</div>
									<hr>
									<div class="col-md-12">
										<div class="sub-kategori">
											<?php 
												foreach ($q_kategori->sub1_kategori as $q_sub1_kategori) {
													echo '<li><a href="'.base_url().'iklan/kategori/'.$q_kategori->seo_kategori.'/'.$q_sub1_kategori->seo_sub1_kategori.'">'.$q_sub1_kategori->sub1_kategori.'</a></li>';
												}
											?>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							<?php
						}
						?>
						
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		
	</div>
</div>