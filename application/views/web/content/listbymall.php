
        <div class="container gap130">
            <div class="breadcrumbs">
                <ul class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Handphone </li>
                </ul>
            </div>
        </div>
        
        <div class="container">
            <div class="main">
                <!-- <div class="iklan-tengen">
                    <img src="<?php echo base_url(); ?>assets/images/iklan-sih-tengen.png">
                </div> -->
                <div class="row">  
                    <div class="col-sm-3 col-left">
                    </div><!-- /.col-left -->
                    <div class="col-sm-9">
                        <div class="toolbar">
                            <div class="sorter">
                                <p class="view-mode">
                                    <label>View as:</label>
                                    <a class="grid" title="Grid" href="#">Grid</a>&nbsp;
                                    <strong class="list" title="Lit">List</strong>&nbsp;
                                </p>
                            </div><!-- /.sorter -->
                            <div class="pager">
                                <div class="sort-by hidden-xs">
                                    <label>Sort By:</label>
                                    <select class="form-control input-sm">
                                        <option selected="selected">Position</option>
                                        <option>Name</option>
                                        <option>Price</option>
                                    </select>
                                    <a title="Set Descending Direction" href="#"><span class="fa fa-sort-amount-desc"></span></a>
                                </div>
                                <div class="limiter hidden-xs">
                                    <label>Show:</label>
                                    <div class="limiter-inner">
                                        <select class="form-control input-sm">
                                            <option>9</option>
                                            <option selected="selected">12</option>
                                            <option>24</option>
                                            <option>36</option>
                                        </select> 
                                    </div>
                                </div>
                            </div><!-- /.pager -->
                        </div><!-- /.toolbar -->
                       
                        <ol id="products-list" class="products-list">
                        <?php foreach($iklan as $i) { 
                            
                        $gbr = $this->IklanModel->gbr_iklan($i->id_iklan);
                        $gbr = $gbr->row();
                        $isi_berita=strip_tags($i->deskripsi_iklan);
                        $isi = substr($isi_berita,0,100); // ambil sebanyak 00 karakter
                        $isi = substr($isi_berita,0,strrpos($isi," "));

                            echo '
                            <li class="item">
                                <div class="item-inner">
                                    <div class="row iqbal list-iklan-depan">
                                        <div class="col-sm-2 boby nopadding">
                                            <div class="images-container">
                                                <div class="product_icon nadif">
                                                ';
                                                if($i->status == 1){
                                                    echo '<button title="Hapus Favorit" isFavorit="1" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-unfavorit"><i class="fa fa-star"></i></button>';
                                                }else{
                                                    echo '<button title="Favorit" isFavorit="0" onclick="favorit(this)" id_iklan="'.$i->id_iklan.'" class="btn btn-favorit"><i class="fa fa-star"></i></button>';
                                                }
                                                echo '
                                                    <!--div class="favorit-icon"><a class="disabled-favorit" id_iklan="'.$i->id_iklan.'" onclick="favorit(this)"></a></div-->
                                                </div>
                                                <div class="product_icon fadel">
                                                    <div class="label-istimewa"><a href="#"></a></div>
                                                </div>';
                                                // <a class="product-image" style="object-fit:cover/contain;height:150px;" title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'" rel="'.$i->owner.'"><img alt="'.$i->judul_iklan.'" src="'.base_url().'images/iklan/'.$gbr->photo.'"></a>
                                            echo '</div>
                                        </div>
                                        <div class="product-shop col-sm-8 col-sms-4 col-smb-12">
                                            <div class="f-fix">
                                                <h5 class="product-name"><a title="'.$i->judul_iklan.'" href="'.base_url().'iklan/detail/'.$i->seo_iklan.'">'.$i->judul_iklan.'</a></h5>
                                                <div class="list-kategori-iklan">
                                                    <label class="label label-warning bck-org"><i class="fa fa-tags"></i> '.$i->kategori.'</label>
                                                    <i class="fa fa-angle-double-right"></i> '.$i->nama_mall.'</div>
                                                <article class="text-small text-mute" title="'.$i->deskripsi_iklan.'">
                                                    '.read_more($i->deskripsi_iklan,250).'
                                                </article>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-2  bar-right nopadding">
                                            <h4 class="nomargin pad-8 bck-org text-center">
                                                <i class="fa fa-tag"></i> 
                                                Rp '.number_format($i->harga_iklan,0,',','.').'
                                            </h4>
                                            <div class="pad-8" style="font-size:14px;">
                                                <ul>
                                                    <li>
                                                        <span class="label label-success">
                                                            <i class="fa fa-calendar"></i> &nbsp;'.tgl_indo($i->tanggal_post).'
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <a href="'.$i->web_iklan.'">
                                                            <span class="label label-primary">
                                                            <i class="fa fa-chain"></i> Visit Website
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li style="border-top:1px dashed#d3d3d3;padding-top:5px;margin-top:10px;">
                                                        <i class="text-small" style="font-size:10px;color:#d3d3d3">
                                                            <i class="fa fa-map-marker"></i> &nbsp;
                                                            '.$i->nama_area.' ,  Jawa Timur 
                                                        </i>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            ';

                            } ?>
                            <!-- biasa 
                            <li class="item" style="text-align: center;">
                                <a class="" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/contoh-iklan.jpg"></a>
                            </li>
                            <li class="item faisal">
                                <div class="item-inner">
                                    <div class="row iqbal">
                                        <div class="col-sm-2 col-sms-3 col-smb-12 boby">
                                            <div class="images-container">
                                                <div class="product_icon nadif">
                                                    <div class="favorit-icon"><a href="#"></a></div>
                                                </div>
                                                <a class="product-image" title="Fusce aliquam" href="#" rel="author"><img alt="Fusce aliquam" src="images/products/53448763_1_644x461_alphard-g-atpm-hitam-2012-km-20-ribuan-jakarta-selatan.jpg"></a>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-8 col-sms-6 col-smb-12">
                                            <div class="f-fix">
                                                <h5 class="product-name"><a title="Lenovo Yoga" href="detail.html">Lenovo Yoga 3 Core i7 Nvidia 940M 2gb</a></h5>
                                                <p>
                                                    Premium Lenovo Yoga3 dengan Procesor terbaru Core i7 BroadWell Yg super cepat. 
                                                </p>
                                            </div>
                                        </div>
                                        <div class="product-shop col-sm-2 col-sms-2 col-smb-12">
                                            <div class="f-fix">
                                                    <p class="special-price"><strong>
                                                        <span id="product-price-1" class="price">Rp 10.500.000</span></strong>
                                                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>-->
                        </ol>

                        <nav>
                          <ul class="pagination">
                          <?php echo $paging;?>
                          </ul>
                        </nav>
                    </div><!-- /.col-right -->
                </div>
            </div>
        </div><!-- /.main -->
        <script type="text/javascript">
        function favorit (that) {
            var id = $(that).attr('id_iklan');
            if($(that).attr('isFavorit') === '0'){
                $.ajax({
                'method':'POST',
                'url':'<?php echo base_url(); ?>iklanku/favorit/add',
                'data':{id_iklan:id},
                'success':showAlert
                }).done(function(data){
                    console.log('data ->' + data);
                    var result = JSON.parse(data);
                    if(result['status'] === 'success'){
                        $(that).removeClass('btn-favorit');
                        $(that).addClass("btn-unfavorit");
                        $(that).attr('isFavorit', '1');
                        var jumlah_lama = $('#jumlah-favorit').text();
                        $('#jumlah-favorit').text(parseInt(jumlah_lama)+1);
                        console.log($(that));
                    }
                });
            }else{
                 $.ajax({
                'method':'POST',
                'url':'<?php echo base_url(); ?>iklanku/favorit/remove',
                'data':{id_iklan:id},
                'success':showAlertRemove
                }).done(function(data){
                    var result = JSON.parse(data);
                    if(result['status'] === 'success'){
                        $(that).removeClass('btn-unfavorit');
                        $(that).addClass("btn-favorit");
                        $(that).attr('isFavorit', '0');
                         var jumlah_lama = $('#jumlah-favorit').text();
                        $('#jumlah-favorit').text(parseInt(jumlah_lama)-1);
                    }
                });
            }
        }
    </script>