<div style="margin-top:100px;">
    <div class="home-background">
        <div class="container container-home">
            <div class="row">
                <form class="home-search" method="get" action="<?php echo base_url(); ?>iklan/search">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-white search-form-container">
                                <div class="col-sm-1 col-sm-offset-1 icon nopadding" style="border-top-left-radius:6px;border-bottom-left-radius:6px;"><i class="fa fa-search fa-2x"></i>
                                </div>
                                <div class="col-sm-4 search nopadding text-center ">
                                    <div style="position: relative;">
                                        <input autocomplete="off" id="" placeholder="Apa yang anda cari " name="keyword" class="form-control mar-center" style="width:95%; border-radius:5px; height:40px; font-size:14px;" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-2 search nopadding text-center ">
                                    <div style="position: relative;">
                                        <input autocomplete="off" onclick="select_kota()" id="kota" placeholder="Semua Kota " class="form-control mar-center" style="width:95%; border-radius:5px; height:40px; font-size:14px;" type="text">
                                        <input type="hidden" id="value_kota" name="kota" />
                                    </div>
                                </div>
                                <div class="separator-line"></div>
                                <div class="col-sm-3 kategori nopadding text-center" style="border-top-right-radius:6px;border-bottom-right-radius:6px;">
                                    <div>
                                        <select name="kategori" class="form-control" style="width:95%; border-radius:5%; height:40px; font-size:14px;">
                                            <option value="">Pilihan Kategori</option>

                                            <?php 
                                            foreach ($kategori as $q_kategori) {
                                                echo '
                                                <option value="'.$q_kategori->seo_kategori.'">'.$q_kategori->kategori.'</option>
                                                ';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-1 search-button text-center">
                                    <button class="btn btn-success btn-home-search">
                                        <i class="fa fa-search fa-2x"></i>
                                    </button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                   <!--  <div class="row" id="advanced-search" style="display:none">
                        <div class="col-md-12" style="padding-left:25px;padding-right:25px;">
                                <div class="col-md-12 advanced-search-container">
                                     <div class="col-md-3 bor-gray-right-1 scroll-red">
                                        <h4 class="col-red"><strong><i class="fa fa-cutlery col-gray"></i> &nbsp;Cuisine</strong></h4>
                                        <div class="form-group scroll-red hover-scroll" style="max-height:210px;">
                                                                                    <div class="clearfix"></div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-3 bor-gray-right-1" style="height:211px">
                                        <h4 class="col-red"><strong><i class="fa fa-star col-gray"></i> &nbsp;Restaurant Type</strong></h4>
                                        <div class="form-group scroll-red hover-scroll" style="max-height:210px;">
                                                                                    <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 bor-gray-right-1" style="height:211px">
                                        <h4 class="col-red"><strong><i class="fa fa-plus col-gray"></i> &nbsp;Restaurant Features</strong></h4>
                                        <div class="form-group scroll-red hover-scroll" style="max-height:210px;">
                                                                                    <div class="col-md-6 nopadding sm-text">
                                                <div class="checkbox">
                                                    <label>
                                                        <div class="i-check"><input style="position: absolute; opacity: 0;" class="i-check" name="feature[]" value="Wi-Fi" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> Wi-Fi
                                                    </label>
                                                </div>
                                            </div>
                                                                                    <div class="col-md-6 nopadding sm-text">
                                                <div class="checkbox">
                                                    <label>
                                                        <div class="i-check"><input style="position: absolute; opacity: 0;" class="i-check" name="feature[]" value="TV" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> TV
                                                    </label>
                                                </div>
                                            </div>
                                                                                    <div class="col-md-6 nopadding sm-text">
                                                <div class="checkbox">
                                                    <label>
                                                        <div class="i-check"><input style="position: absolute; opacity: 0;" class="i-check" name="feature[]" value="Outdoor Seat" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> Outdoor Seat
                                                    </label>
                                                </div>
                                            </div>
                                                                                    <div class="col-md-6 nopadding sm-text">
                                                <div class="checkbox">
                                                    <label>
                                                        <div class="i-check"><input style="position: absolute; opacity: 0;" class="i-check" name="feature[]" value="Service Charge" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> Service Charge
                                                    </label>
                                                </div>
                                            </div>
                                                                                    <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div> -->
                </form>
            </div>
        </div>
        <div class="home-kategori">
            <div>
                <?php 
                foreach ($kategori as $q_kategori) {
                    echo '
                    <div style="width:10%; float:left">
                        <div style="padding:15px;" class="text-center">
                            <a href="'.base_url().'iklan/kategori/'.$q_kategori->seo_kategori.'/pilih">
                                <img style="background: white none repeat scroll 0% 0%;border-radius: 50%;" class="img-responsive" src="'.base_url().'images/kategori/'.$q_kategori->icon.'" />
                                <div style="color: white;font-size: 17px;font-weight: 600;">'.$q_kategori->kategori.'</div>
                            </a>
                        </div>
                    </div>
                    ';
                }
                ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modalprov" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:60%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header head_modal">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-th"></i> &nbsp;Pilih Lokasi Pencarian</h4>
            </div>
            <div class="modal-ku modal-body">
                <?php
                $jumlah = 1;

                echo '<div class="col-md-3 kategori-bck scroll">';
                echo '<ul class="list-group kategori-parent">';
                echo '<li class="list-group-item ">Semua Provinsi</li>';
                foreach ($mall as $mall_item) {
                    if(floor($jumlah/9) == 0){
                        echo '
                        <li class="list-group-item" onclick="pilihprov('.$mall_item->id_mall.')">'.$mall_item->nama_mall.'</li>
                        ';
                    }
                $jumlah++;
                }
                echo '</ul>';
                echo '</div>';

                $jumlah = 1;
                echo '<div class="col-md-3 kategori-bck scroll">';
                echo '<ul class="list-group kategori-parent">';
                foreach ($mall as $mall_item) {
                    if(floor($jumlah/9) == 1){
                        echo '
                        <li class="list-group-item" onclick="pilihprov('.$mall_item->id_mall.')">'.$mall_item->nama_mall.'</li>
                        ';
                    }
                $jumlah++;
                }
                echo '</ul>';
                echo '</div>';

                $jumlah = 1;
                echo '<div class="col-md-3 kategori-bck scroll">';
                echo '<ul class="list-group kategori-parent">';
                foreach ($mall as $mall_item) {
                    if(floor($jumlah/9) == 2){
                        echo '
                        <li class="list-group-item" onclick="pilihprov('.$mall_item->id_mall.')">'.$mall_item->nama_mall.'</li>
                        ';
                    }
                $jumlah++;
                }
                echo '</ul>';
                echo '</div>';
                
                $jumlah = 1;
                echo '<div class="col-md-3 kategori-bck scroll">';
                echo '<ul class="list-group kategori-parent">';
                foreach ($mall as $mall_item) {
                    if(floor($jumlah/9) == 3){
                        echo '
                        <li class="list-group-item" onclick="pilihprov('.$mall_item->id_mall.')">'.$mall_item->nama_mall.'</li>
                        ';
                    }
                $jumlah++;
                }
                echo '</ul>';
                echo '</div>';
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="modal-footer modal-footer-ku">
            <article class="text-center">
                Wikiloka , Pilih Lokasi Pencarian
            </article>
        </div>
    </div>

</div>
</div>

<!--         <div class="container">
        	<div class="main">
            	<div class="row">
                    <div class="col-md-9 col-md-offset-3">
                    	<div class="flexslider ma-nivoslider">
                            <div class="ma-loading"></div>
                            <div id="ma-inivoslider-banner7" class="slides">
	                            <img src="<?php echo base_url(); ?>assets/images/slider/slide-01.jpg" class="dn" alt="" title="#banner7-caption1"  />                           
                                <img src="<?php echo base_url(); ?>assets/images/slider/slide-02.jpg" class="dn" alt="" title="#banner7-caption2"  />
                            </div>
                            <div id="banner7-caption1" class="banner7-caption nivo-html-caption nivo-caption">
                            	<div class="timethai"></div>
                                <div class="banner7-content slider-1">
                                	<div class="title-container">
                                     	<h1 class="title1">headphones az12</h1>
                                        <h2 class="title2" >Typi non habent claritatem insitam; est usus legentis</h2>											
                                    </div>
                                    <div class="banner7-des">
                                    	<div class="des">
            								<h1>sale up to!</h1>
            								<h2>30% off</h2>
            								<div class="check-box">
                                                <ul class="list-unstyled">
                                                    <li>With all products in shop</li>
                                                    <li>All combos $69.96</li>
                                                </ul>
                                    		</div>
                                    	</div>
                              		</div>																								
                                	<img class="img1" src="<?php echo base_url(); ?>assets/images/slider/img-04.png" alt="" />																				
                      			</div>
                          	</div>						
                            <div id="banner7-caption2" class="banner7-caption nivo-html-caption nivo-caption">
                            	<div class="timethai"></div>
                                <div class="banner7-content slider-2">
                                    <div class="title-container">
                                        <h1 class="title1">Samsung s5</h1>
                                        <h2 class="title2" >Typi non habent claritatem insitam; est usus legentis</h2>											
                                    </div>
                                    <div class="banner7-des">
                                   		<div class="des">
											<h1>sale up to!</h1>
											<h2>50% off</h2>
										</div>
                                    </div>																								
                                    <img class="img1" src="<?php echo base_url(); ?>assets/images/slider/img-05.png" alt="" />																					
                                </div>
                            </div>
                        </div> --><!-- /.flexslider -->
                        <!-- <form class="form-search">
                            <img src="images/icon1.png">
                            <input type="text" class="input-text" name="q" id="search" placeholder="Lokasi">
                            <input type="text" class="input-text" name="q" id="search" placeholder="Search">
                            <button type="submit" class="btn btn-danger"><span class="fa fa-search"></span></button>
                        </form> -->