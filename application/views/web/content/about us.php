
        <div class="breadcrumbs">
            <div class="container">
                <ul class="breadcrumb">
                	<li><a href="#">Home</a></li>
                    <li class="active">About Us</li>
               	</ul>
            </div>
        </div>
        
        <div class="main">
        	<div class="container">
            	<div class="row">
                    <div class="col-main col-xs-12 col-sm-9">
                        <div class="std">
                            <div class="page-title">
                                <h1>About Magento Store</h1>
                            </div>
                            <div class="col3-set">
                                <div class="col-1">
                                    <p style="line-height:1.2em;"><small>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.</small>
                                    </p>
                                    <p style="color:#888; font:1.2em/1.4em georgia, serif;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta.</p>
                                </div>
                                <div class="col-2">
                                    <p><strong style="color:#de036f;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.</strong>
                                    </p>
                                    <p>Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo. </p>
                                    <p>Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus. Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi. Vestibulum sapien dolor, aliquet nec, porta ac, malesuada a, libero. Praesent feugiat purus eget est. Nulla facilisi. Vestibulum tincidunt sapien eu velit. Mauris purus. Maecenas eget mauris eu orci accumsan feugiat. Pellentesque eget velit. Nunc tincidunt.</p>
                                </div>
                                <div class="col-3">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper </p>
                                    <p><strong style="color:#de036f;">Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus.</strong>
                                    </p>
                                    <p>Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi.</p>
                                    <div class="divider"></div>
                                    <p>To all of you, from all of us at Magento Store - Thank you and Happy eCommerce!</p>
                                    <p style="line-height:1.2em;"><strong style="font:italic 2em Georgia, serif;">John Doe</strong>
                                        <br />
                                        <small>Some important guy</small>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.col-main -->
                    <div class="col-right sidebar col-xs-12 col-sm-3">
                        <div class="block block-cart">
                            <div class="block-title">
                                <strong><span>My Cart</span></strong>
                            </div>
                            <div class="block-content">
                                <p class="empty">You have no items in your shopping cart.</p>
                            </div>
                        </div>
                        <div class="block block-poll">
                            <div class="block-title">
                                <strong><span>Community Poll</span></strong>
                            </div>
                            <form id="pollForm" method="post">
                                <div class="block-content">
                                    <p class="block-subtitle">What is your favorite color</p>
                                    <ul id="poll-answers">
                                        <li>
                                            <input type="radio" name="vote" class="radio poll_vote" id="vote_1" value="1" />
                                            <span class="label"><label for="vote_1">Green</label></span>
                                        </li>
                                        <li>
                                            <input type="radio" name="vote" class="radio poll_vote" id="vote_2" value="2" />
                                            <span class="label"><label for="vote_2">Red</label></span>
                                        </li>
                                        <li>
                                            <input type="radio" name="vote" class="radio poll_vote" id="vote_3" value="3" />
                                            <span class="label"><label for="vote_3">Black</label></span>
                                        </li>
                                        <li>
                                            <input type="radio" name="vote" class="radio poll_vote" id="vote_4" value="4" />
                                            <span class="label"><label for="vote_4">Magenta</label></span>
                                        </li>
                                    </ul>
                                    <div class="actions">
                                        <button type="submit" title="Vote" class="button"><span><span>Vote</span></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.sidebar -->
                </div>
            </div>
        </div><!-- /.main -->