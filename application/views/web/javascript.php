    <!-- Javascript Umum -->
    <!-- Jquery Js -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!-- Load Javascript Setiap Page -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/wikiloka.js"></script>
    <?php
    $url1 = $this->uri->segment(1);
    $url2 = $this->uri->segment(2);
    switch ($url1) {
        case 'iklanku':
        ?>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <script type="text/JavaScript" src="https://cdn.datatables.net/responsive/2.0.2/js/dataTables.responsive.min.js"></script>
        <script type="text/JavaScript" src="https://cdn.datatables.net/responsive/2.0.2/js/responsive.bootstrap.min.js"></script>

        <?php
        switch ($url2) {
            case 'berbayar':
            ?>
            <script type="text/javascript">
                $(':checkbox').change(function(){
                    if(this.checked && this.name === 'top25'){
                        console.log(this.siblings);
                    }
                })
            </script>
            <?php
            break;
            
            default:
                # code...
            break;
        }
        break;
        case 'iklan':
        ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script type="text/javascript">
            $("select").select2({
                minimumResultsForSearch: Infinity,
                theme: "classic"
            });
        </script>
        <?php
        switch ($url2) {
            case 'category':
            ?>
            <script type="text/javascript">
                function urutkan(me){
                    $.ajax({
                        'method':'POST',
                        'url':'<?php echo base_url(); ?>ajax/getKategoriIklanByUrutan',
                        'data':{jenis_urutkan:me.value}
                    }).done(function(data){

                    })
                }
            </script>
            <?php
            break;
            case 'detail':
            ?>
            <script type="text/javascript">
             function kirim_reply(){
                $("#reply_form").submit(function(e) {
                    var postData = $(this).serializeArray();        
                    $.ajax({
                        'url':'<?php echo base_url() ?>ajax/reply/',
                        'method':'post',
                        'data': postData
                    })
                    .done(function(data){
                        var json = JSON.parse(data);
                        var html = '<div class="media" id="media-reply">'+
                        '<div class="media-left">'+ 
                        '<a href="#"> <img class="img-circle" data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTRiOTAwM2E5NCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NGI5MDAzYTk0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" style="width: 32px; height: 32px;" class="media-object" data-src="holder.js/64x64" alt="64x64"> </a>'+ 
                        '</div>'+
                        '<div class="media-body">'+
                        '<h5 class="media-heading">'+json.profil.nama_member+'</h5>'+
                        '<div>'+json.reply+'</div>'+
                        '</div>'+
                        '</div>'
                        $('#media-reply').append(html);
                        $('#reply_komentar').val('');
                    });
    e.preventDefault();
});
}


$("#komentar_form").submit(function(e) {
    var postData = $(this).serializeArray();        
    $.ajax({
        'url':'<?php echo base_url() ?>ajax/komentar',
        'method':'post',
        'data': postData
    })
    .done(function(data){
        var json = JSON.parse(data);
        var html = '<div class="media" id="media-reply">'+
        '<div class="media-left">'+ 
        '<a href="#"> <img class="img-circle" data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTRiOTAwM2E5NCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NGI5MDAzYTk0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" style="width: 32px; height: 32px;" class="media-object" data-src="holder.js/64x64" alt="64x64"> </a>'+ 
        '</div>'+
        '<div class="media-body">'+
        '<h5 class="media-heading">'+json.profil.nama_member+'</h5>'+
        '<div>'+json.komentar+'</div>'+
        '</div>'+
        '</div>'
        $('this').next().append(html);
        $('#reply_komentar').val('');
    });
    e.preventDefault();
});

    function tampil_reply(that, id_komentar){
        $.ajax({
            'url':'<?php echo base_url() ?>ajax/tampil_reply/'+id_komentar,
            'method':'post'
        })
        .done(function(data){
            var json = JSON.parse(data);
            var html = '';
            var form_reply = '';
            for (var i = 0; i < json.length; i++) {
                html += '<li class="media">'+ 
                '<div class="media-left"> '+
                '<a href="#"> <img class="img-circle" data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTRiOTAwM2E5NCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NGI5MDAzYTk0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" style="width: 32px; height: 32px;" class="media-object" data-src="holder.js/64x64" alt="64x64"> </a> '+
                '</div> '+
                '<div class="media-body"> '+
                '<h5 class="media-heading">'+json[i].nama_member+'</h5>'+ 
                '<div>'+json[i].reply+'</div>'+  
                '</div>'+ 
                '</li>'
            };
            form_reply =    '<div class="media-left"> '+
            '<a href="#"> <img class="img-circle" data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTRiOTAwM2E5NCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NGI5MDAzYTk0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" style="width: 32px; height: 32px;" class="media-object" data-src="holder.js/64x64" alt="64x64"> </a> '+
            '</div> '+
            '<div class="media-body"> '+
            '<form class="form" method="post" id="reply_form">'+
            '<div class="form-group">'+
            '<div>'+
            '<input id="reply_komentar" name="reply_komentar" class="form-control input-sm" />'+
            '<input type="hidden" name="id_komentar" value="'+id_komentar+'"/>'+
            '</div>'+      
            '</div>'+
            '</form>'+
            '</div>';

            $(that).next().append(html);
            $(that).next().next().append(form_reply);
            kirim_reply();
            $(that).remove();

        })
        //console.log(id_komentar);
    }

</script>
<?php
break;

default:

break;
}
break;
default:
?>
<?php
break;
}
?>
<!-- Owl Carousel Js -->
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<!-- Custom Js -->
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

<script src="<?php echo base_url(); ?>assets/js/notify.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.5.1/jquery.nicescroll.min.js"></script>
<script type="text/javascript">
    $('html').niceScroll({
        cursorcolor: "#000",
        cursorborder: "0px solid #fff",
        railpadding: {
            top: 0,
            right: 0,
            left: 0,
            bottom: 0
        },
        cursorwidth: "5px",
        cursorborderradius: "0px",
        cursoropacitymin: 0,
        cursoropacitymax: 0.7,
        boxzoom: true,
        horizrailenabled: false,
        zindex: 9999
    });
</script>
<!-- FlexSlider -->
<script src="<?php echo base_url(); ?>assets/flexslider/js/jquery.flexslider.js"></script>
<!-- Summerote -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
<script type="text/javascript">
    $(window).load(function(){
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: true,
        animationLoop: true,
        slideshow: false,
        itemWidth: 108,
        itemMargin: 5,
        asNavFor: '#slider'
    });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
          $('body').removeClass('loading');
      }
  });

      $("#carousel").find('.flex-viewport').css({
        overflow:'visible',
        position:'relative'
    })


  });
</script>

<?php
if ($this->uri->segment(1) == 'iklan') {
    ?>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

    <!-- Cloud Zoom Js -->
    <script src="<?php echo base_url(); ?>assets/js/cloud-zoom.js"></script>

    <?php    
} else {
    ?>        
    <!-- BX Slider Js -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js"></script>
    <!-- Nivo Slider Js -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.nivo.slider.pack.js"></script>
    <!-- Countdown Js -->
    <script src="<?php echo base_url(); ?>assets/js/countdown.js"></script>
    <!-- Kek ono Rule Cak Mok Lak Bee View Sing Butuh Databae Ben Networth e sitik -->



    <script type="text/javascript">
        /* Main Slideshow */

        jQuery(window).load(function() {
            jQuery(document).off('mouseenter').on('mouseenter', '.pos-slideshow', function(e){
                $('.ma-banner7-container .timethai').addClass('pos_hover');
            });
            jQuery(document).off('mouseleave').on('mouseleave', '.pos-slideshow', function(e){
                $('.ma-banner7-container .timethai').removeClass('pos_hover');
            });
        });
        jQuery(window).load(function() {
            $('#ma-inivoslider-banner7').nivoSlider({
                effect: 'random',
                slices: 15,
                boxCols: 8,
                boxRows: 4,
                animSpeed: 1000,
                pauseTime: 6000,
                startSlide: 0,
                controlNav: false,
                controlNavThumbs: false,
                pauseOnHover: true,
                manualAdvance: false,
                prevText: 'Prev',
                nextText: 'Next',
                afterLoad: function(){
                    $('.ma-loading').css("display","none");
                    $('.banner7-title, .banner7-des, .banner7-readmore').css("left","100px") ;
                },     
                beforeChange: function(){ 
                    $('.banner7-title, .banner7-des').css("left","-2000px" );
                    $('.banner7-readmore').css("left","-2000px"); 
                }, 
                afterChange: function(){ 
                    $('.banner7-title, .banner7-des, .banner7-readmore').css("left","0") 
                }
            });
        });            
    </script>        
<!-- Hot Deals Timer 1-->
<script type="text/javascript">
    var dthen1 = new Date("12/25/33 11:59:00 PM");
    start = "08/04/15 03:02:11 AM";
    start_date = Date.parse(start);
    var dnow1 = new Date(start_date);
    if(CountStepper>0)
        ddiff= new Date((dnow1)-(dthen1));
    else
        ddiff = new Date((dthen1)-(dnow1));
    gsecs1 = Math.floor(ddiff.valueOf()/1000);

    var iid1 = "countbox_1";
    CountBack_slider(gsecs1,"countbox_1", 1);
</script>
<!-- Hot Deals Timer 2-->
<script type="text/javascript">
    var dthen2 = new Date("05/21/26 11:59:00 PM");
    start = "08/04/15 03:02:11 AM";
    start_date = Date.parse(start);
    var dnow2 = new Date(start_date);
    if(CountStepper>0)
        ddiff= new Date((dnow2)-(dthen2));
    else
        ddiff = new Date((dthen2)-(dnow2));
    gsecs2 = Math.floor(ddiff.valueOf()/1000);

    var iid2 = "countbox_2";
    CountBack_slider(gsecs2,"countbox_2", 2);
</script>
<!-- Hot Deals Timer 3-->
<script type="text/javascript">
    var dthen3 = new Date("05/21/33 11:59:00 PM");
    start = "08/04/15 03:02:11 AM";
    start_date = Date.parse(start);
    var dnow3 = new Date(start_date);
    if(CountStepper>0)
        ddiff= new Date((dnow3)-(dthen3));
    else
        ddiff = new Date((dthen3)-(dnow3));
    gsecs3 = Math.floor(ddiff.valueOf()/1000);

    var iid3 = "countbox_3";
    CountBack_slider(gsecs3,"countbox_3", 3);
</script>
<?php  
}
?>
<?php 
if($this->session->userdata('tipe_user') == 'member'){
    ?>
    <script type="text/javascript">

        function getKota(that){
            $.ajax({
                'method':'POST',
                'url':'<?php echo base_url(); ?>api/getKota',
                'data':{id_provinsi:that.value},
                'beforeSend': function() {
                    $('#kota').parent().parent().remove();
                }
            }).done(function(data){
                var result = JSON.parse(data);

                $(that).parent().parent().after(
                    '<div class="form-group">'+
                    '<label class="col-sm-2 control-label">Kota <em>*</em></label>'+
                    '<div class="col-sm-3">'+
                    '<select id="kota" class="form-control" name="kota">'+
                    '<option disabled selected>Pilih Kota</option>'+
                    '</select>'+
                    '</div>'+
                    '</div>'
                    );
                for (var i = 0; i <= result.length; i++) {
                    $('#kota').append("<option value="+result[i].id_kota+">"+result[i].nama_area+"</option>")
                };
            })
        }
    </script>

    <?php
}
?>
<script type="text/javascript">
    function showAlert(){
       $.notify("Ditambahkan ke Favorit", "success");
   }
   function showAlertRemove(){
       $.notify("Dihapus dari Favorit", "success");
   }
</script>
<script type="text/javascript">
    $('.datatable-ku').DataTable({
        responsive: false,
        "oLanguage": {
           "oPaginate": {
             "sNext": "<i class='fa fa-arrow-right'></i>",
             "sPrevious": "<i class='fa fa-arrow-left'></i>"
         }
     }
 });
    $('.datatable-ku').removeClass( 'display' ).addClass('table table-striped table-bordered');
</script>
<script type="text/javascript">
    $(".summernote").summernote({
        height:200,
        toolbar: [
        
        ],
    });
</script>   
<?php 
if ($this->uri->segment(1) == '') {
    ?>
    <script type="text/javascript">
        function pilihkota(id, nama_area){
            $('#modalprov').modal('hide');
            $('#kota').val(nama_area);
            var id_kota = id;
            $('#value_kota').val(id);

        }
    </script>
    <?php

}else{
    ?>
    <script type="text/javascript">
        function pilihkota(id, nama_area){
            //window.location.href='<?php echo base_url().'iklan/kategori/'.$this->uri->segment(3).'/semua/'; ?>?kota='+id
            window.location.href='<?php echo base_url("iklan/kategori/")."/".$this->uri->segment(3)."/".$this->uri->segment(4)."?".http_build_query(array_merge($_GET, ["kota"=>""])) ?>'+id;
        }
    </script>
    <?php
}
?>

<script type="text/javascript">
    var modalku = '';
    function select_kota(){
        $('#modalprov').modal('show');
    }
    function backtoprov(){
        $('.modal-ku').html(modalku);
    }

    function pilihprov(id){
        modalku = $('.modal-ku').html();
        //console.log(modalku);
        $.ajax({
            'method':'GET',
            'url':'<?php echo base_url(); ?>ajax/getKota'+'/'+id
            //'data':{id_provinsi:id}
        }).done(function(data){
            var kotaArray = JSON.parse(data);
            var listkota = '<li class="list-group-item" onclick="backtoprov()">...Pilih Provinsi</li>';
            var daftar = '';
            var posi = 0;
            var index = 1;
            console.log('jumlah kota = '+kotaArray.length);
            for (var i = 0; i < kotaArray.length; i++) {
                if(kotaArray.length > 9){
                    if(posi+1 === 9){
                        daftar += 
                        '<div class="col-md-3 kategori-bck scroll">'+
                        '<ul class="list-group kategori-parent">'+
                        listkota+
                        '</ul>'+
                        '</div>'
                        listkota = '';
                        posi=0; 
                    }else{
                        listkota +='<li onclick="pilihkota('+kotaArray[i].id_kota+',\''+kotaArray[i].nama_area+'\')" class="list-group-item">'+kotaArray[i].nama_area+'</li>'; 
                        posi++;
                    }
                }else{
                    if(i+1 < kotaArray.length){
                        console.log('array index i ='+i);
                        listkota +='<li onclick="pilihkota('+kotaArray[i].id_kota+',\''+kotaArray[i].nama_area+'\')" class="list-group-item">'+kotaArray[i].nama_area+'</li>'; 
                    }else{
                        listkota +='<li onclick="pilihkota('+kotaArray[i].id_kota+',\''+kotaArray[kotaArray.length-1].nama_area+'\')" class="list-group-item">'+kotaArray[kotaArray.length-1].nama_area+'</li>'; 
                        daftar += 
                        '<div class="col-md-3 kategori-bck scroll">'+
                        '<ul class="list-group kategori-parent">'+
                        listkota+
                        '</ul>'+
                        '</div>'
                        listkota = ''; 
                    }
                }
            };
            $('.modal-ku').html(daftar);
        })
}
</script>

<script type="text/javascript">
    function like(id_iklan){
        $.ajax({
            'url':'<?php echo base_url(); ?>ajax/like/'+id_iklan,
            'method':'post'
        })
        .done(function(data){
            var json = JSON.parse(data);
            $('#like_total').text(' '+json.total_like+' Suka');
        })
    }
    function unlike(id_iklan){
        $.ajax({
            'url':'<?php echo base_url(); ?>ajax/unlike/'+id_iklan,
            'method':'post'
        })
        .done(function(data){
            var json = JSON.parse(data);
            $('#unlike_total').text(' '+json.total_like+' Tidak Suka');
        })
    }
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
<script type="text/javascript">

    $.blockUI.defaults = { 
                // message displayed when blocking (use null for no message) 

                title: null,        // title string; only used when theme == true 
                draggable: true,    // only used when theme == true (requires jquery-ui.js to be loaded) 

                theme: false, // set to true to use with jQuery UI themes 

                // styles for the message when blocking; if you wish to disable 
                // these and use an external stylesheet then do this in your code: 
                // $.blockUI.defaults.css = {}; 
                css: { 
                    padding:        0, 
                    margin:         0, 
                    width:          '30%', 
                    top:            '2%', 
                    left:           '35%', 
                    textAlign:      'center', 
                    color:          '#FFF', 
                    padding:        '15px',
                    background:     '#f08519',
                    'border-radius':'3px',
                    cursor:         'wait' 
                }, 

                // minimal style set used when themes are used 
                themedCSS: { 
                    width:  '30%', 
                    top:    '40%', 
                    left:   '35%' 
                }, 

                // styles for the overlay 
                overlayCSS:  { 
                    backgroundColor: '#fff', 
                    opacity:         0.6, 
                    cursor:          'wait' 
                }, 

                // style to replace wait cursor before unblocking to correct issue 
                // of lingering wait cursor 
                cursorReset: 'default', 

                // styles applied when using $.growlUI 
                growlCSS: { 
                    width:    '350px', 
                    top:      '10px', 
                    left:     '', 
                    right:    '10px', 
                    border:   'none', 
                    padding:  '5px', 
                    opacity:   0.6, 
                    cursor:    null, 
                    color:    '#fff', 
                    backgroundColor: '#fff', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius':    '10px' 
                }, 

                // IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w 
                // (hat tip to Jorge H. N. de Vasconcelos) 
                iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank', 

                // force usage of iframe in non-IE browsers (handy for blocking applets) 
                forceIframe: false, 

                // z-index for the blocking overlay 
                baseZ: 1000, 

                // set these to true to have the message automatically centered 
                centerX: true, // <-- only effects element blocking (page block controlled via css above) 
                centerY: true, 

                // allow body element to be stetched in ie6; this makes blocking look better 
                // on "short" pages.  disable if you wish to prevent changes to the body height 
                allowBodyStretch: true, 

                // enable if you want key and mouse events to be disabled for content that is blocked 
                bindEvents: true, 

                // be default blockUI will supress tab navigation from leaving blocking content 
                // (if bindEvents is true) 
                constrainTabKey: true, 

                // fadeIn time in millis; set to 0 to disable fadeIn on block 
                fadeIn:  200, 

                // fadeOut time in millis; set to 0 to disable fadeOut on unblock 
                fadeOut:  400, 

                // time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock 
                timeout: 0, 

                // disable if you don't want to show the overlay 
                showOverlay: true, 

                // if true, focus will be placed in the first available input field when 
                // page blocking 
                focusInput: true, 

                // suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity) 
                // no longer needed in 2012 
                // applyPlatformOpacityRules: true, 

                // callback method invoked when fadeIn has completed and blocking message is visible 
                onBlock: null, 

                // callback method invoked when unblocking has completed; the callback is 
                // passed the element that has been unblocked (which is the window object for page 
                // blocks) and the options that were passed to the unblock call: 
                //   onUnblock(element, options) 
                onUnblock: null, 

                // don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493 
                quirksmodeOffsetHack: 4, 

                // class name of the message block 
                blockMsgClass: 'blockMsg', 

                // if it is already blocked, then ignore it (don't unblock and reblock) 
                ignoreIfBlocked: false 
            }; 



        </script>

        <script src='https://www.google.com/recaptcha/api.js'></script>