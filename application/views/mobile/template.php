<!DOCTYPE HTML>
<html>
<head>
	<title>Wikiloka</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/mobile/vendor/waves/waves.min.css" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/mobile/vendor/wow/animate.css" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/mobile/css/nativedroid2.css" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/mobile/css/nativedroid2.color.deep-orange.css" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/mobile/css/custom.css" />
	<?php 
	if($this->uri->segment('3') == 'detail'){
		?>
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/mobile/swiper/dist/css/swiper.css" />
		<?php
	}
	?>
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<style type="text/css">
		html{
			font-family: "Helvetica Neue",Helvetica,Arial,Sans-serif !important;
			font-weight: 400 !important;
		}
	</style>

</head>
<body>
	<div data-role="page" class="nd2-no-menu-swipe">
		<!-- panel left -->
		<div data-role="panel" id="leftpanel" data-display="overlay" data-position-fixed="true" >

			<div class='nd2-sidepanel-profile wow fadeInDown'>
				
				<div class="row">
					<div class='col-xs-4 center-xs'>
						<div class='box'>
							<img class="profile-thumbnail" src="<?php echo base_url() ?>assets/mobile/img/profile.png" />
						</div>
					</div>
					<div class='col-xs-8'>
						<div class='box profile-text'>
							<strong>wiratmoko11</strong>
							<span class='subline'>Log in sebagai</span>
						</div>
					</div>
				</div>
			</div>

			<?php 
			if($this->session->userdata('tipe_user') == 'member'){
				?>
				<ul id="menu-member" data-role="listview" data-icon="true">
					<li>
						<a href="<?php echo base_url() ?>mobile">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/home.png" class="ui-li-icon" />
							Home
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>mobile/iklanku/pengaturan">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/gantiprofile.png" class="ui-li-icon" />
							Ganti Profil
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>mobile/iklanku">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/listiklan.png" class="ui-li-icon" />
							List Iklan
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>mobile/iklanku/favorit">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/fav.png" class="ui-li-icon" />
							Favorit
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>mobile/iklanku/buatwebsite">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/webgratis.png" class="ui-li-icon" />
							Buat Website Gratis
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>mobile/iklanku/berbayar">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/iklanpremium.png" class="ui-li-icon" />
							Beli Iklan Premium
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>mobile/iklanku/belipoin">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/belipoin.png" class="ui-li-icon" />
							Beli Poin
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>mobile/iklanku/upgrade">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/upgrade.png" class="ui-li-icon" />
							Upgrade
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>mobile/iklanku/logout">
							<img src="<?php echo base_url(); ?>assets/mobile/icon/logout.png" class="ui-li-icon" />
							Logout
						</a>
					</li>

				</ul>

				<?php
			}else{
				?>
				<ul class="login-register" data-role="listview" data-icon="false">
					<li><a href="<?php echo base_url(); ?>mobile/register" class="ui-btn ui-btn-raised clr-register">Register</a></li>
					<li><a href="<?php echo base_url(); ?>mobile/login" class="ui-btn ui-btn-raised clr-primary">Login</a></li>
				</ul>
				<?php
			}
			?>
			
			<div data-role="collapsible" data-inset="false" data-collapsed="false"  data-collapsed-icon="carat-d" data-expanded-icon="carat-d" data-iconpos="right">
				<h3>Kategori</h3>
				<ul data-role="listview" data-inset="false" data-icon="false">
					<?php 
					foreach ($kategori as $q_kategori) {
						echo '<li><a href="'.base_url().'mobile/iklan/kategori/'.$q_kategori->seo_kategori.'" data-ajax="false" data-icon="false">'.$q_kategori->kategori.'</a></li>';
					}
					?>
				</ul>
			</div>

		</div>
		<!-- /panel left -->

		<?php 
		if($filter_kategori == true){
			?>
			<!-- panel filter -->
			<div data-role="panel" id="filterpanel" data-display="overlay" data-position="right" data-position-fixed="true" >
				<div class="panel-header">
					Filter
				</div>
				<div style="padding:1em;">					
					<form id="form-filter">
						<label>Pencarian</label>
						<input type="text" value="<?php echo $this->input->get('keyword') ?>" name="keyword"/>

						<label for="select-choice-1b" class="select">Lokasi</label>
						<select name="kota" data-native-menu="true" data-mini="true">
							<option selected disabled>Pilih Lokasi</option>
							<?php
							foreach ($provinsi as $q_provinsi) {
								echo '<option style="font-weight:600; color:black;" disabled>'.$q_provinsi->nama_provinsi.'</option>';
								foreach ($q_provinsi->kota as $q_kota) {
									?>
									<option <?php echo $q_kota->id_kota == $this->input->get('kota')? 'selected':'' ?> value="<?php echo $q_kota->id_kota ?>"><?php echo $q_kota->nama_area ?></option>
									<?php
								}
							}
							?>
						</select>

						<label class="select">Kategori</label>
						<select name="kategori" data-native-menu="true" data-mini="true">
							<option selected disabled>Pilih Kategori</option>
							<?php 
							foreach ($kategori as $q_kategori) {
								?>
								<option <?php echo $datakategori->seo_kategori == $q_kategori->seo_kategori? 'selected':'' ?> value="<?php echo $q_kategori->seo_kategori ?>"><?php echo $q_kategori->kategori ?></option>
								<?php
							}
							?>
						</select>
						
						<div id="filter-sub">
							<label for="select-choice-1b" class="select">Sub Kategori</label>
							<select name="subkategori" data-native-menu="true" data-mini="true">
								<option selected disabled>Pilih Sub Kategori</option>
								<?php 
								foreach ($subkategori as $q_subkategori) {
									if(isset($sub1_kategori)){
										?>
										<option <?php echo $sub1_kategori->seo_sub1_kategori == $q_subkategori->seo_sub1_kategori? 'selected':'' ?> value="<?php echo $q_subkategori->seo_sub1_kategori ?>"><?php echo $q_subkategori->sub1_kategori ?></option>
										<?php
									}else{
										?>
										<option value="<?php echo $q_subkategori->seo_sub1_kategori ?>"><?php echo $q_subkategori->sub1_kategori ?></option>
										<?php
									}
								}
								?>
							</select>							
						</div>
						<?php
						if(isset($sub1_kategori) && $this->input->get('filter')){
							?>
							<div id="filter-sub2">
								<?php
							}else{
								?>
								<div id="filter-sub2" style="display:none;">
									<?php
								}	
								?>
								<label class="select">Sub 2 Kategori</label>
								<select name="sub2kategori" data-native-menu="true" data-mini="true">
									<option selected disabled>Pilih Sub Kategori</option>
									<?php 
									foreach ($sub2kategori as $q_sub2kategori) {
										?>
										<option <?php echo $sub2_kategori->seo_sub2_kategori == $q_sub2kategori->seo_sub2_kategori? 'selected':'' ?> value="<?php echo $q_sub2kategori->seo_sub2_kategori ?>"><?php echo $q_sub2kategori->sub2_kategori ?></option>
										<?php
									}
									?>
								</select>							
							</div>


							<label for="select-choice-1b" class="select">Harga Minimum</label>
							<select name="start" id="select-choice-1b" data-native-menu="true" data-mini="true">
								<option selected disabled>Pilih Harga</option>
								<?php 
								foreach ($harga_mobil as $harga) {
									?>
									<option <?php echo $this->input->get('start') == $harga? 'selected':'' ?>  value="<?php echo $harga ?>">Rp. <?php echo number_format($harga) ?></option>'
									<?php
								}
								?>
							</select>

							<label for="select-choice-1b" class="select">Harga Maksimum</label>
							<select name="end" id="select-choice-1b" data-native-menu="true" data-mini="true">
								<option selected disabled>Pilih Harga</option>
								<?php 
								foreach ($harga_mobil as $harga) {
									?>
									<option <?php echo $this->input->get('end') == $harga? 'selected':'' ?>  value="<?php echo $harga ?>">Rp. <?php echo number_format($harga) ?></option>'
									<?php
								}
								?>
							</select>
							<?php 
							?>
							<div id="filter-khusus">
								
							</div>

							<div class="filter-button">
								<button type="button" data-rel="close" class="ui-btn ui-btn-raised clr-success ui-mini">Batal</button>
								<button type="submit" class="ui-btn ui-btn-raised clr-primary ui-mini">Tampilkan Hasil</button>
								<div style="clear:both"></div>
							</div>
						</form>
					</div>
				</div>
				<!-- /panel left -->
				<?php
			}
			?>

			<div data-role="panel" id="bottomsheet" data-animate="false" data-position='bottom' data-display="overlay">
				<nd2-include data-src="/examples/fragments/bottom.sheet.html"></nd2-include>
			</div>

			<div data-role="header" data-position="fixed" class="wow fadeInDown" data-wow-delay="0.2s">
				<!-- <a href="#filterpanel" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-more-vert"></i></a> -->
				<a href="#leftpanel" class="ui-btn ui-btn-left wow fadeIn" data-wow-delay='0.8s'><i class="zmdi zmdi-menu"></i></a>
				<h1 class="wow fadeIn" data-wow-delay='0.4s'>Wikiloka</h1>
				<?php 
				if($this->uri->segment(2)=='iklanku'){
					?>
					<ul data-role="nd2tabs" data-swipe="true">
						<li data-tab="iklan-aktif" data-tab-active="true">Iklan Aktif (0)</li>
						<li data-tab="iklan-tidak-aktif">Iklan Tidak Aktif (0)</li>
						<li data-tab="iklan-laku">Iklan Laku (0)</li>
						<li data-tab="iklan-premium">Iklan Premium (0)</li>
						<li data-tab="iklan-ditolak">Iklan Ditolak (0)</li>
					</ul>
					<?php
				}else{
					?>
					<div data-role="navbar">
						<ul>
							<li><a href="#">Travel</a></li>
							<li><a href="#">Kuliner</a></li>
							<li><a href="#">Top Website</a></li>
							<li><a href="#">Berita</a></li>
						</ul>
					</div><!-- /navbar -->
					<?php
				}
				?>
				<?php 
				if($this->uri->segment(2)=='iklan'){
					?>
					<div data-role="navbar">
						<ul>
							<li><a href="#">Semua</a></li>
							<li><a href="#">Top 25</a></li>
							<li><a href="#">Top Shop</a></li>
							<li><a href="#">Rekomendasi</a></li>
						</ul>
					</div><!-- /navbar -->
					<?php
				}
				?>
			</div>

			<?php echo $_content; ?>

			<?php 
			if($filter_kategori == true){
				?>
				<div data-role="footer" id="footer" data-position="fixed" style="overflow:hidden;">
					<div data-role="popup" id="popupDialog" style="width:275px; min-height:450px;">
						<div data-role="content" style="width:100%; max-height:450px; overflow:scroll">
							<div data-role="collapsible" data-inset="false" data-icon="none" style="width:100%;">
								<h4>Kategori Utama</h4>
								<ul data-role="listview" id="filterlist-kategori">
									<?php 
									foreach ($kategori as $q_kategori) {
										echo '<li data-seo="'.$q_kategori->seo_kategori.'" data-id="'.$q_kategori->id_kategori.'"><a href="#" data-ajax="false" data-icon="false">'.$q_kategori->kategori.'</a></li>';
									}
									?>
								</ul>
							</div>	
							<div data-role="collapsible" data-inset="false" data-icon="none" style="width:100%;">
								<h4>Sub Kategori 1</h4>
								<ul data-role="listview" id="filterlist-subkategori">

								</ul>
							</div>
							<div data-role="collapsible" data-inset="false" data-icon="none" style="width:100%;">
								<h4>Sub Kategori 2</h4>
								<ul data-role="listview" id="filterlist-subkategori2">

								</ul>
							</div>		
						</div>
					</div>
					<div style="padding:0;" data-role="popup" id="positionOrigin" data-transition="slideup" class="ui-content ui-bck-gray" data-theme="a">
						<div style="padding: 10px;background: white;font-weight: 500;border-bottom: 1px solid #e0e0e0;color: #545454;">
							Urutkan
						</div>
						<div style="padding:1em;">
							<form>
								<div class="radio-sort">
									<div style="float:left;">
										<input <?php echo $this->input->get('sorting') == 'terbaru'? 'checked':'' ?> value="terbaru" type="radio" name="sorting">
									</div>
									<div style="float:left; margin-left:35px; margin-top:-3px;">
										Terbaru
									</div>
									<div style="clear:both"></div>
								</div>
								<div class="radio-sort">
									<div style="float:left;">
										<input  <?php echo $this->input->get('sorting') == 'termurah'? 'checked':'' ?> value="termurah" type="radio" name="sorting">
									</div>
									<div style="float:left; margin-left:35px; margin-top:-3px;">
										Termurah
									</div>
									<div style="clear:both"></div>
								</div>
							</form>
						</div>
					</div>
					<div id="filter">
						<div id="filter-kategori">
							<div class="icon">
								<img src="<?php echo base_url('images/kategori').'/'.$datakategori->icon; ?>">
							</div>
							<a href="#popupDialog" data-rel="popup" data-position-to="window" data-transition="pop">
								<div class="sub-kategori">
									<?php
									if($datakategori && $sub1_kategori && isset($sub2_kategori)){
										?>
										<div class="nama-kategori"><?php echo $sub1_kategori->sub1_kategori ?></div>
										<div class="nama-sub"><?php echo $sub2_kategori->sub2_kategori ?></div>
										<?php
									}elseif($datakategori && $sub1_kategori){
										?>
										<div class="nama-kategori"><?php echo $datakategori->kategori ?></div>
										<div class="nama-sub"><?php echo $sub1_kategori->sub1_kategori ?></div>
										<?php
									}else{
										?>
										<div class="single-kategori">Semua di <?php echo $datakategori->kategori ?></div>
										<?php
									}
									?>

								</div>
							</a>
						</div>
						<div id="filter-btn">
							<div><a style="text-decoration:none" href="#filterpanel">FILTER</a></div>
						</div>
						<div id="sorting">
							<a href="#positionOrigin" data-rel="popup" data-position-to="origin"><img src="https://cdn2.iconfinder.com/data/icons/toolbar-icons/512/Funnel-512.png"></a>
						</div>
					</div>
				</div><!-- /footer -->
				<?php
			}
			?>
			<?php 
			if($this->session->userdata('username')){
				?>
				<div>
					<a href="<?php echo base_url(); ?>mobile/iklanku/pasang"><i class="zmdi zmdi-plus-circle btn-pasang"></i></a>
				</div>
				<?php
			}
			?>
		</div>
		<script type="text/javascript">
			var base_url = '<?php echo base_url() ?>';
		</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript">
			$(document).on("mobileinit", function(){
				$.mobile.ajaxEnabled=false;
				$.mobile.loadingMessage=false;
			});
		</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
		<script src="<?php echo base_url() ?>assets/mobile/vendor/waves/waves.min.js"></script>
		<script src="<?php echo base_url() ?>assets/mobile/vendor/wow/wow.min.js"></script>
		<script src="<?php echo base_url() ?>assets/mobile/js/nativedroid2.js"></script>
		<script src="<?php echo base_url() ?>assets/mobile/nd2settings.js"></script>

		<?php 
		if($this->uri->segment('3') == 'detail'){
			?>
			<script src="<?php echo base_url() ?>assets/mobile/swiper/dist/js/swiper.min.js"></script>		
			<script>
				setTimeout(function() {
					var swiper_single = new Swiper('.single-item', {autoplay:3000});
				}, 0.01);
			</script>
			<?php
		}elseif ($this->uri->segment(3) == 'pasang') {
			?>
			<script src="<?php echo base_url() ?>assets/mobile/js/pasang.js"></script>
			<?php
		}
		?>
		<script src="<?php echo base_url() ?>assets/mobile/js/filter.js"></script>
		<?php 
		if($this->session->flashdata('email_available')){
			?>
			<script type="text/javascript">
				new $.nd2Toast({
					message : "Email sudah terdaftar",
					action : {
						title : "close",
						fn : function() {
						//console.log("I am the function called by 'Pick phone...'");
					},
					color : "lime"
				},
				ttl : 5000
			});


			</script>
			<?php
		}elseif ($this->session->flashdata('username_available')) {
			?>
			<script type="text/javascript">
				new $.nd2Toast({
					message : "Username sudah terdaftar",
					action : {
						title : "close",
						fn : function() {
						//console.log("I am the function called by 'Pick phone...'");
					},
					color : "lime"
				},
				ttl : 5000
			});
			</script>
			<?php
		}
		?>

		<?php 
		if($this->session->flashdata('login_gagal')){
			?>
			<script type="text/javascript">
				new $.nd2Toast({
					message : "<?php echo $this->session->flashdata('login_gagal') ?>",
					action : {
						title : "close",
						fn : function() {
						//console.log("I am the function called by 'Pick phone...'");
					},
					color : "lime"
				},
				ttl : 5000
			});


			</script>
			<?php
		}
		?>

	</body>
	</html>
