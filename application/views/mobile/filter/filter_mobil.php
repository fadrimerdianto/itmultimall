<label for="select-choice-1b" class="select">Tranmsisi</label>
<select name="transmisi" data-native-menu="true" data-mini="true">
	<option selected disabled>Pilih Transmisi</option>
	<option>Automatic</option>
	<option>Manual</option>
	<option>Triptonic</option>
</select>

<label for="select-choice-1b" class="select">Jenis Kendaraan</label>
<select name="jenis" data-native-menu="true" data-mini="true">
	<option selected disabled>Pilih Jenis Kendaraan</option>
	<option>Automatic</option>
	<option>Manual</option>
	<option>Triptonic</option>
</select>

<label for="select-choice-1b" class="select">Tahun</label>
<select name="tahun" data-native-menu="true" data-mini="true">
	<option selected disabled>Pilih Tahun</option>
	<?php 
		for ($i=date('Y'); $i >= 1990; $i--) { 
			echo '<option name="tahun" value="'.$i.'">'.$i.'</option>';
		}
	?>
</select>
