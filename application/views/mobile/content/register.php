<div role="main" class="ui-content" data-inset="true">
	<div>
		<div class="page-title">Sudah punya akun ?</div>
		<a href="<?php echo base_url(); ?>mobile/login"><button class="ui-btn ui-btn-raised clr-primary ui-mini btn-login-on-reg">Login</button></a>

		<div class="line-through"><p><span class="muted">Atau</span></p></div>

		<div class="page-title">Akun Baru</div>
		<form method="post" action="<?php echo base_url('mobile/main/register/do') ?>">
			<div style="padding:10px 20px;">
				<label>Email:</label>
				<input name="email" id="un" value="" data-theme="a" type="email" required>
				<label>Username:</label>
				<input name="username" id="un" value="" data-theme="a" type="text" required>
				<label>Password:</label>
				<input name="password" id="pw" value="" data-theme="a" type="password" required>
				<label>Konfirmasi Password:</label>
				<input name="confirmation" id="pw" value="" data-theme="a" type="password" required>
				
				<button type="submit" class="ui-btn ui-corner-all ui-shadow ui-btn-b clr-green ui-mini">Register</button>
			</div>
		</form>
	</div>
</div>


