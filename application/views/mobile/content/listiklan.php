<div role="main" style="background-color:#e1e1e1;" class="ui-content wow fadeIn" data-inset="false" data-wow-delay="0.2s">
	<ul data-role="listview" data-icon="false">
		<?php 
		foreach ($iklan as $i) {
			$gbr = $this->IklanModel->gbr_iklan($i->id_iklan);
			$gbr = $gbr->row();
			$isi_berita=strip_tags($i->deskripsi_iklan);
            $isi = substr($isi_berita,0,100); // ambil sebanyak 00 karakter
            $isi = substr($isi_berita,0,strrpos($isi," "));

            $tgl = $i->tanggal_post; 
            $tanggal = substr($tgl,8,2);
            $bulan = substr($tgl,5,2);
            $tahun = substr($tgl,0,4);
            $jam   = substr($tgl,11,2);
            $menit = substr($tgl,14,2);
            $detik = substr($tgl,17,2);
            $thn_sekarang = date('Y');
            $bln_sekarang = date('m');

            // $time = mktime(12, 40, 33, 6, 10, 2009); // 10 July 2009 12:40:33
            $time = mktime($jam, $menit, $detik, $bulan, $tanggal, $tahun); // 10 July 2009 12:40:33 
            $timediff = time() - $time; 
            $tanggal_skr = timeInSentence($timediff, 'id', 1);
            $tgl_aktif = explode(" ",$tanggal_skr);
            if ($tahun < $thn_sekarang || $bulan < $bln_sekarang) {
            	$tglnya = tgl_indo($tgl);
            }else {
            	$tglnya = $tanggal_skr;
            }

            if($i->jenis_iklan == 1){
            	$jenis_iklan = 'Baru';
            }else{
            	$jenis_iklan = 'Bekas';
            }
            echo '
            <li class="single-iklan">
            	<a href="'.base_url().'mobile/iklan/detail/'.$i->seo_iklan.'">
                        <div class="list-gambar">
                              <img src="'.base_url().'images/iklan/'.$gbr->photo.'" class="ui-thumbnail img-list" />
                              <button></button>
                        </div>
                        <div class="list-detail">
                              <div class="judul-iklan">'.$i->judul_iklan.'</div>
                              <div class="list-harga-iklan">Rp '.number_format($i->harga_iklan,0,',','.').'</div>
                              <div class="lokasi">
                                    <i class="icon-location"></i><div class="nama-lokasi">'.$i->kota->nama_area.', '.$i->provinsi->nama_provinsi.' </div></div>
                        </div>
                        <div class="clearfix"></div>
                  </a>
            </li>
            ';
      }
      ?>
</ul>
</div>
