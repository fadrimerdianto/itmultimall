<?php
$gbr = $this->IklanModel->gbr_iklan($detail->id_iklan);
?>
<div class="single-item">
	<div class="swiper-wrapper">
		<?php
		foreach ($gbr->result() as $q_gambar) {
			?>
			<div class="swiper-slide">
				<img style="height: 250px; object-fit: cover; margin-left:auto; margin-right:auto;" src="<?php echo base_url('images/iklan/'.$q_gambar->photo) ?>">
			</div>
			<?php
		}
		?>
	</div>
	<div class="swiper-pagination"></div>
	<div class="harga-iklan">
		Rp. <?php echo number_format($detail->harga_iklan) ?>
	</div>
</div>

<div class="detail-iklan">
	<div class="judul-iklan">
		<?php echo strtoupper($detail->judul_iklan); ?>
	</div>
	<hr>
	<div class="iklan-owner">
		<i class="icon-user"> </i><div class="float-left text-profil-detail"><?php echo $detail->nama_member; ?></div>
		<div class="clearfix"></div>
	</div>
	<div class="lokasi">
		<i class="icon-location"></i><div class="float-left text-profil-detail"> <?php echo $kota->nama_area ?>, <?php echo $kota->provinsi->nama_provinsi ?></div>
		<div class="clearfix"></div>
	</div>
	<!-- <div class="kategori">
		Mobil > Merk > Honda
	</div> -->
	<div class="tanggal-post">
		<i class="icon-time"></i><div class="float-left text-profil-detail"> <?php echo tgl_indo($detail->tanggal_post) ?> </div>
		<div class="clearfix"></div>
	</div>
	<!-- <div class="laporkan-penjual">
		<button class="ui-btn ui-btn-raised clr-warning"><i class="zmdi zmdi-alert-triangle"></i></button>
	</div> -->
	<div class="dilihat">
		Dilihat : <strong><?php echo $detail->dilihat ?> kali</strong>
	</div>
	<div class="clearfix"></div>
</div>

<?php 
	//mobil
if($detail->sub1_kategori == 8){
	?>
	<div class="spesifikasi">
		<div class="detail-spesifikasi"><span>Tahun</span> : <strong><?php echo $detail->tahun ?></strong></div>
		<div class="detail-spesifikasi"><span>Tranmsisi</span> : <strong><?php echo $detail->transmisi ?></strong></div>
		<div class="detail-spesifikasi"><span>Tipe</span> : <strong><?php echo $detail->tipe ?></strong></div>
		<div class="clearfix"></div>
	</div>	
	<?php
		//rumah
}elseif ($detail->sub1_kategori == 17) {
	?>
	<div class="spesifikasi">
		<div class="detail-spesifikasi"><span>Luas Tanah</span> : <strong><?php echo $detail->luas_tanah ?> m<sup>2</sup></strong></div>
		<div class="detail-spesifikasi"><span>Luas Bangunan</span> : <strong><?php echo $detail->luas_bangunan ?> m<sup>2</sup></strong></div>
		<div class="detail-spesifikasi"><span>Lantai</span> : <strong><?php echo $detail->lantai ?></strong></div>
		<div class="detail-spesifikasi"><span>Kamar Tidur</span> : <strong><?php echo $detail->kamar_tidur ?></strong></div>
		<div class="detail-spesifikasi"><span>Kamar Mandi</span> : <strong><?php echo $detail->kamar_mandi ?></strong></div>
		<div class="detail-spesifikasi"><span>Sertifikasi</span> : 
			<strong>
				<?php 
				if($detail->sertifikasi == 1){
					echo 'SHM';
				}elseif ($detail->sertifikasi == 2) {
					echo 'HGB';
				}else{
					echo "Lainnya";
				}
				?>	
			</strong>
		</div>
		<div class="clearfix"></div>	
	</div>
	<?php
}
?>
<?php 

if($detail->sub1_kategori == 17 || $detail->sub1_kategori == 18){
	?>
	<div class="spesifikasi">
		<strong>Lokasi</strong>
		<div><?php echo $detail->alamat_lokasi ?></div>		
		<div class="clearfix"></div>	
	</div>
	<?php
}

if($detail->sub1_kategori == 17 || $detail->sub1_kategori == 18){
	?>
	<div class="spesifikasi">
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> AC</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Kolam Renang</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Carport</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Taman</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Garasi</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Refrigerator</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Fire Extenguiser</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Telephone</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> PAM</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Microwave</strong></div>
		<div class="detail-spesifikasi"><span><i class="zmdi zmdi-check-circle"></i></span><strong> Stove</strong></div>
		<div class="clearfix"></div>	
	</div>
	<?php
}
?>



<div class="deskripsi">
	<?php echo $detail->deskripsi_iklan ?>
</div>
<div class="share">
	<div class="text">Bagikan</div>
	<div class="share-icon">
		<ul>
			<li><img src="<?php echo base_url() ?>assets/images/socialmedia/facebook.png"></li>
			<li><img src="<?php echo base_url() ?>assets/images/socialmedia/g.png"></li>
			<li><img src="<?php echo base_url() ?>assets/images/socialmedia/twitter.png"></li>
			<li><img src="<?php echo base_url() ?>assets/images/socialmedia/line.png"></li>
			<li><img src="<?php echo base_url() ?>assets/images/socialmedia/whatsapp.png"></li>
		</ul>
	</div>
	<div class="clearfix"></div>
</div>

<div class="cta clr-green">
	<ul>
		<li>
			<img src="phone.png">
		</li>
		<li>
			<img src="bbm.png">
		</li>
		<li>
			<img src="phone.png">
		</li>
		<li>
			<img src="email.png">
		</li>
	</ul>
</div>