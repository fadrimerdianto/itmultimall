<div role="main" class="ui-content" data-inset="false">
	<div>
		<form method="post" action="<?php echo base_url('mobile/main/login/do') ?>">
			<div style="padding:5px 5px;">
				<div class="login-title">Masuk ke Wikiloka</div>
				<div class="login-sub-title">Belum punya akun wikiloka? Register <a href="<?php echo base_url() ?>mobile/main/register">disini</a></div>
				<label>Username:</label>
				<input name="username" id="un" value="" data-theme="a" type="text">
				<label>Password:</label>
				<input name="password" id="pw" value="" data-theme="a" type="password">

				<button type="submit" class="ui-btn ui-btn-raised ui-corner-all ui-shadow ui-mini clr-primary">Masuk</button>
			</div>
		</form>
	</div>
</div>