<div role="main" class="ui-content pasang-content" data-inset="false">
	<form action="<?php echo base_url(); ?>mobile/iklanku/pasang/do" method="post" enctype="multipart/form-data">

		<div>
			<label>Judul iklan</label>
			<input type="text" name="judul_iklan" value="" data-clear-btn="true">

			<label>Jenis Barang</label>
			<select data-mini="true" name="jenis_iklan" data-native-menu="true">
				<option>Pilih Jenis Barang</option>
				<option value="1">Barang Baru</option>
				<option value="2">Barang Bekas</option>
			</select>

			<div>
				<div id="wrap-harga">
					<label>Harga</label>
					<input type="number" name="harga_iklan" value="" data-clear-btn="true">
				</div>
				<div id="wrap-pilih-kategori">
					<label>Kategori</label>
					<select data-mini="true" name="id_kategori" id="pilih-kategori" data-native-menu="true">
						<option>Pilih Kategori</option>
						<?php 
						foreach ($kategori as $q_kategori) {
							echo '<option value="'.$q_kategori->id_kategori.'">'.$q_kategori->kategori.'</option>';
						}
						?>
					</select>
				</div>
				<div class="clearfix"></div>
			</div>

			

			<div id="wrap-pilih-subkategori">
				<label>Sub Kategori 1</label>
				<select data-mini="true" name="id_sub1_kategori" id="pilih-subkategori" data-native-menu="true">
					<option>Pilih Sub Kategori</option>

				</select>
			</div>

			<div id="wrap-pilih-sub2kategori">
				<label>Sub Kategori 2</label>
				<select data-mini="true" name="id_sub2_kategori" id="pilih-sub2kategori" data-native-menu="true">
					<option>Pilih Sub 2 Kategori</option>

				</select>
			</div>

		</div>
		

		<div id="form-filter">

		</div>
		<hr>
		<label>Provinsi</label>
		<select data-mini="true" name="provinsi" id="pilih-provinsi" data-native-menu="true">
			<option>Pilih Provinsi</option>
			<?php 
			foreach ($provinsi as $q_provinsi) {
				echo '<option value="'.$q_provinsi->id_provinsi.'">'.$q_provinsi->nama_provinsi.'</option>';
			}
			?>
		</select>

		<div id="wrap-pilih-kota">
			<label>Kota</label>
			<select data-mini="true" name="kota" id="pilih-kota" data-native-menu="true">
				<option>Pilih Kota</option>

			</select>
		</div>

		<label>Upload Gambar</label>
		<input type="file" name="userfile[]" id="name2b" data-clear-btn="true">
		<input type="file" name="userfile[]" id="name2b" data-clear-btn="true">
		<input type="file" name="userfile[]" id="name2b" data-clear-btn="true">
		<input type="file" name="userfile[]" id="name2b" data-clear-btn="true">


		<label>Deskripsi</label>
		<textarea name="deskripsi_iklan" rows="10"></textarea>

		<hr>

		<label>Nama</label>
		<input name="nama" value="Wirat Moko Hadi S" disabled data-clear-btn="false"/>
		<label>Email</label>
		<input name="email" type="email" value="<?php echo $member->email; ?>" disabled data-clear-btn="false"/>
		<label>No Telepon</label>
		<input name="no_telepon" value="<?php echo $member->telepon_member ?>" disabled data-clear-btn="false"/>
		<div>
			<label>Whatsapp</label>
			<input name="wa_available" type="checkbox">
		</div>
		
		<label>No Telepon 2</label>
		<input name="no_telepon2" value="<?php echo $member->telepon_member2; ?>" disabled data-clear-btn="false"/>
		<label>Line ID</label>
		<input name="line_id" value="<?php echo $member->id_line; ?>" disabled data-clear-btn="false"/>
		<label>BBM</label>
		<input name="pinbb" value="<?php echo $member->bbm_member; ?>" disabled data-clear-btn="false"/>
		<label>Website</label>
		<input name="web" value="" readonly data-clear-btn="false"/>

		<button class="ui-btn ui-btn-raised ui-mini clr-primary" type="submit">Pasang</button>
	</form>
</div>
