<div role="main" class="ui-content">
	<ul data-role="listview" data-icon="false" id="upgrade-list">
		
		<li>
			<div style="float:left; width:60%">
				<h3>Pembaruan Otomatis</h3>
				<p>Harga 25 Poin / tahun</p>
			</div>
			<div style="float:left; padding-left:5px;padding-top:10px;width:35%">
				<button class="ui-btn ui-btn-raised clr-green ui-mini">Upgrade</button>
			</div>
			<div style="clear:both"></div>
		</li>
		<li>
			<div style="float:left; width:60%">
				<h3>+ 10 iklan upload</h3>
				<p>Harga 25 Poin / tahun</p>
			</div>
			
			<div style="float:left; padding-left:5px;padding-top:10px;width:35%">
				<button class="ui-btn ui-btn-raised clr-green ui-mini">Upgrade</button>
			</div>
			<div style="clear:both"></div>
		</li>
		<li>
			<div style="float:left; width:60%">
				<h3>+ 25 iklan upload</h3>
				<p>Harga 25 Poin / tahun</p>
			</div>
			<div style="float:left; padding-left:5px;padding-top:10px;width:35%">
				<button class="ui-btn ui-btn-raised clr-green ui-mini">Upgrade</button>
			</div>
			<div style="clear:both"></div>
		</li>
		<li>
			<div style="float:left; width:60%">
				<h3>+ 50 iklan upload</h3>
				<p>Harga 25 Poin / tahun</p>
			</div>
			<div style="float:left; padding-left:5px;padding-top:10px;width:35%">
				<button class="ui-btn ui-btn-raised clr-green ui-mini">Upgrade</button>
			</div>
			<div style="clear:both"></div>
		</li>
		<li>
			<div style="float:left; width:60%">
				<h3>+ Unlimited iklan upload</h3>
				<p>Harga 25 Poin / tahun</p>
			</div>
			<div style="float:left; padding-left:5px;padding-top:10px;width:35%">
				<button class="ui-btn ui-btn-raised clr-green ui-mini">Upgrade</button>
			</div>
			<div style="clear:both"></div>
		</li>
		<li>
			<div style="float:left; width:60%">
				<h3>Pembaruan Otomatis</h3>
				<p>Harga 25 Poin / tahun</p>
			</div>
			<div style="float:left; padding-left:5px;padding-top:10px;width:35%">
				<button class="ui-btn ui-btn-raised clr-green ui-mini">Upgrade</button>
			</div>
			<div style="clear:both"></div>
		</li>
		<li>
			<div style="float:left; width:60%">
				<h3>Domain Sendiri</h3>
				<p>Harga 25 Poin / tahun</p>
			</div>
			<div style="float:left; padding-left:5px;padding-top:10px;width:35%">
				<button class="ui-btn ui-btn-raised clr-green ui-mini">Upgrade</button>
			</div>
			<div style="clear:both"></div>
		</li>
		<li>
			<div style="float:left; width:60%">
				<h3>Multi Kategori</h3>
				<p>Harga 25 Poin / tahun</p>
			</div>
			<div style="float:left; padding-left:5px;padding-top:10px;width:35%">
				<button class="ui-btn ui-btn-raised clr-green ui-mini">Upgrade</button>
			</div>
			<div style="clear:both"></div>
		</li>
	</ul>

</div>