  <div role="main" class="ui-content" data-inset="false">
    <ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false">
      <?php 
      foreach ($iklan_off as $q_iklan) {
        ?>
        <li>
          <a href="#">
            <img src="<?php echo base_url().'images/iklan/'.$q_iklan->gambar[0]->photo ?>" class="ui-thumbnail" />
            <h2><?php echo $q_iklan->judul_iklan ?></h2>
            <p><?php echo tgl_indo($q_iklan->tanggal_post) ?></p>
          </a>
          <a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Purchase album</a>
        </li>
        <?php
      }
      ?>
    </ul>
  </div>