  <div role="main" class="ui-content wow fadeIn" data-inset="false" data-wow-delay="0.2s">

    <div data-role="nd2tab" data-tab="iklan-aktif">
      <nd2-ad data-banner="sample.banner"></nd2-ad>
      <ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false">
        <?php 
        if(!$iklan){
          ?>
          <center style="font-size: 14px;color: gray; margin-top:25px;">
            <div>Kamu belum memiliki iklan aktif saat ini</div>
            <div>Ayo pasang iklan</div>
            <img style="height:200px; margin-top:20px; margin-bottom:20px;" src="<?php echo base_url(); ?>assets/mobile/img/person.png">
            <a href="<?php echo base_url() ?>mobile/iklanku/pasang"><button style="width:40%" class="ui-btn ui-btn-raised clr-primary"><i class="zmdi zmdi-plus"></i> Pasang</button></a>
          </center>            
          <?php
        }else{
          foreach ($iklan as $q_iklan) {
            ?>
            <li>
              <a href="#">
                <img src="<?php echo base_url().'images/iklan/'.$q_iklan->gambar[0]->photo ?>" class="ui-thumbnail" />
                <h2><?php echo $q_iklan->judul_iklan ?></h2>
                <p><?php echo tgl_indo($q_iklan->tanggal_post) ?></p>
              </a>
              <a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Purchase album</a>
            </li>
            <?php
          }
        }
        ?>
      </ul>
    </div>

    <div data-role="nd2tab" data-tab="iklan-tidak-aktif">
      <ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false">
        <?php 
        if(!$iklan_off){
          ?>
          <center style="font-size: 14px;color: gray; margin-top:25px;">
            <div>Kamu belum memiliki iklan tidak aktif saat ini</div>
            <div>Ayo pasang iklan</div>
            <img style="height:200px; margin-top:20px; margin-bottom:20px;" src="<?php echo base_url(); ?>assets/mobile/img/person.png">
            <a href="<?php echo base_url() ?>mobile/iklanku/pasang"><button style="width:40%" class="ui-btn ui-btn-raised clr-primary"><i class="zmdi zmdi-plus"></i> Pasang</button></a>
          </center>            
          <?php
        }else{
          foreach ($iklan_off as $q_iklan) {
            ?>
            <li>
              <a href="#">
                <?php 
                if(isset($q_iklan->gambar[0])){
                  ?>
                  <img src="<?php echo base_url().'images/iklan/'.$q_iklan->gambar[0]->photo ?>" class="ui-thumbnail" />
                  <?php
                }else{
                  ?>
                  <img src="">
                  <?php
                }
                ?>
                <h2><?php echo $q_iklan->judul_iklan ?></h2>
                <p><?php echo tgl_indo($q_iklan->tanggal_post) ?></p>
              </a>
              <a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Purchase album</a>
            </li>
            <?php
          }
        }
        ?>
      </ul>

    </div>

    <div data-role="nd2tab" data-tab="iklan-laku">

      <ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false">
        <?php 
        if(!$iklan_laku){
          ?>
          <center style="font-size: 14px;color: gray; margin-top:25px;">
            <div>Kamu belum memiliki iklan aktif saat ini</div>
            <div>Ayo pasang iklan</div>
            <img style="height:200px; margin-top:20px; margin-bottom:20px;" src="<?php echo base_url(); ?>assets/mobile/img/person.png">
            <a href="<?php echo base_url() ?>mobile/iklanku/pasang"><button style="width:40%" class="ui-btn ui-btn-raised clr-primary"><i class="zmdi zmdi-plus"></i> Pasang</button></a>
          </center>            
          <?php
        }else{
          foreach ($iklan_laku as $q_iklan) {
            ?>
            <li>
              <a href="#">
                <?php 
                if(isset($q_iklan->gambar[0])){
                  ?>
                  <img src="<?php echo base_url().'images/iklan/'.$q_iklan->gambar[0]->photo ?>" class="ui-thumbnail" />
                  <?php
                }else{
                  ?>
                  <img src="">
                  <?php
                }
                ?>
                <h2><?php echo $q_iklan->judul_iklan ?></h2>
                <p><?php echo tgl_indo($q_iklan->tanggal_post) ?></p>
              </a>
              <a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Purchase album</a>
            </li>
            <?php
          }
        }
        ?>
      </ul>      

    </div>

    <div data-role="nd2tab" data-tab="iklan-premium">

     <ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false">
      <?php 
      if(!$iklan_premium){
        ?>
        <center style="font-size: 14px;color: gray; margin-top:25px;">
          <div>Kamu belum memiliki iklan premium saat ini</div>
          <div>Ayo pasang iklan</div>
          <img style="height:200px; margin-top:20px; margin-bottom:20px;" src="<?php echo base_url(); ?>assets/mobile/img/person.png">
          <a href="<?php echo base_url() ?>mobile/iklanku/pasang"><button style="width:40%" class="ui-btn ui-btn-raised clr-primary"><i class="zmdi zmdi-plus"></i> Pasang</button></a>
        </center>            
        <?php
      }else{
        foreach ($iklan_premium as $q_iklan) {
          ?>
          <li>
            <a href="#">
              <?php 
              if(isset($q_iklan->gambar[0])){
                ?>
                <img src="<?php echo base_url().'images/iklan/'.$q_iklan->gambar[0]->photo ?>" class="ui-thumbnail" />
                <?php
              }else{
                ?>
                <img src="">
                <?php
              }
              ?>
              <h2><?php echo $q_iklan->judul_iklan ?></h2>
              <p><?php echo tgl_indo($q_iklan->tanggal_post) ?></p>
            </a>
            <a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Purchase album</a>
          </li>
          <?php
        }
      }
      ?>
    </ul>      

  </div>

  <div data-role="nd2tab" data-tab="iklan-ditolak">

   <ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false">
    <?php 
    if(!$iklan_ditolak){
      ?>
      <center style="font-size: 14px;color: gray; margin-top:25px;">
        <div>Kamu belum memiliki iklan ditolak saat ini</div>
        <div>Ayo pasang iklan</div>
        <img style="height:200px; margin-top:20px; margin-bottom:20px;" src="<?php echo base_url(); ?>assets/mobile/img/person.png">
        <a href="<?php echo base_url() ?>mobile/iklanku/pasang"><button style="width:40%" class="ui-btn ui-btn-raised clr-primary"><i class="zmdi zmdi-plus"></i> Pasang</button></a>
      </center>            
      <?php
    }else{
      foreach ($iklan_ditolak as $q_iklan) {
        ?>
        <li>
          <a href="#">
            <?php 
            if(isset($q_iklan->gambar[0])){
              ?>
              <img src="<?php echo base_url().'images/iklan/'.$q_iklan->gambar[0]->photo ?>" class="ui-thumbnail" />
              <?php
            }else{
              ?>
              <img src="">
              <?php
            }
            ?>
            <h2><?php echo $q_iklan->judul_iklan ?></h2>
            <p><?php echo tgl_indo($q_iklan->tanggal_post) ?></p>
          </a>
          <a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Purchase album</a>
        </li>
        <?php
      }
    }
    ?>
  </ul>      

</div>

</div>