<div role="main">
	<ul data-role="listview" data-icon="false">
		<li data-role="list-divider">
			Pilih Paket Poin
		</li>
		<?php 
		foreach ($poin as $q_poin) {
			?>
			<li>
				<a href="#">
					<img src="<?php echo base_url().'images/poin/'.$q_poin->gambar_poin ?>" class="ui-thumbnail ui-thumbnail-circular" />
					<h2><?php echo $q_poin->nama_poin ?></h2>
					<p>Harga Rp. <?php echo number_format($q_poin->harga_poin) ?></p>
				</a>
			</li>
			<?php
		}
		?>
	</ul>