<div role="main" class="ui-content wow fadeIn" data-inset="false" data-wow-delay="0.2s">
	<ul data-role="listview" data-split-theme="a" data-inset="false">
		<li style="margin-top:20px;" data-role="list-divider">
			Pilih Iklan
		</li>
		<?php 
		if(!$iklan){
			?>
			<center style="font-size: 14px;color: gray; margin-top:25px;">
				<div>Kamu belum memiliki iklan aktif saat ini.</div>
			</center>            
			<?php
		}else{
			?>
			<form method="post">
				<input name="pilihpremium" type="hidden" value="<?php echo $this->input->get('premium') ?>">
				<?php
				foreach ($iklan as $q_iklan) {
					?>
					<li class="list-iklan-berbayar">
						<div class="iklan">
							<img src="<?php echo base_url().'images/iklan/'.$q_iklan->gambar[0]->photo ?>" class="ui-thumbnail" />
							<div style="float:left; width:60%;">
								<h3><?php echo $q_iklan->judul_iklan ?></h3>
								<p>Rp. <?php echo number_format($q_iklan->harga_iklan) ?></p>
							</div>
							
						</div>
						<div class="checkbox">
							<input name="iklan[]" type="checkbox" >
						</div>
						<div style="clear:both"></div>
					</li>
					<?php
				}
				?>
				<button style="width:80%; margin-right:auto; margin-left:auto;" class="ui-mini ui-btn ui-btn-raised clr-primary" type="submit">Daftarkan</button>
			</form>
			<?php
		}
		?>

	</ul>
</div>