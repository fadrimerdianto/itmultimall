<!-- Blog Post Area -->
		<div class="main-blog-page single-blog blog-post-area shop-product-area">
			<div class="container">
				<div class="row">
					
					<div class="col-md-8 col-md-offset-2">
						<!-- single-blog start -->
						<article class="blog-post-wrapper">
							<div class="post-thumbnail">
								<img src="<?php echo base_url(); ?>images/about_replika/<?php echo $website->about_foto ?>" alt="" />
							</div>
							<div class="post-information">
								<h2>Tentang <?php echo $website->nama_website ?></h2>
								<div class="entry-content">
									<?php echo $website->about ?>
								</div>
							</div>
						</article>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div><!-- End Blog Post Area -->