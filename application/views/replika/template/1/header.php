<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $website->nama_website ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/replika/template/1/img/logo/favicon.ico">
		
		<!-- Google Fonts
		============================================ -->		
       <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	   <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	   <link href="https://fonts.googleapis.com/css?family=Fruktur" rel="stylesheet"> 
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/bootstrap.min.css">
		<!-- Font Awesome CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/font-awesome.min.css">
		<!-- Mean Menu CSS
		============================================ -->      
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/meanmenu.min.css">
		<!-- owl.carousel CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/owl.theme.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/owl.transitions.css">
		<!-- nivo-slider css
		============================================ --> 
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/nivo-slider.css">
		<!-- Price slider css
		============================================ --> 
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/jquery-ui-slider.css">
		<!-- Simple Lence css
		============================================ --> 
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/replika/template/1/css/jquery.simpleLens.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/replika/template/1/css/jquery.simpleGallery.css">
		<!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/animate.css">
		<!-- normalize CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/normalize.css">
		<!-- main CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/main.css">
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/style.css">
		<!-- responsive CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/css/responsive.css">
		<!-- modernizr JS
		============================================ -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/replika/template/1/custom.css">
        <script src="<?php echo base_url(); ?>assets/replika/template/1/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		
		<!-- Header Area -->
        <div class="header-area home-4-header-area">
			<!-- Header top bar -->
			<div class="header-top-bar">
				<div class="container">
					<div class="header-top-inner">
						<div class="row">
							<div class="col-md-8 col-sm-12">
								<div class="header-top-left">
									<div class="phone">
										<label>Telepon</label> <?php echo $website->telepon_member ?>
									</div>
									<div class="e-mail">
										<label>Email:</label> <?php echo $website->email ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- End Header Top bar -->
			<!-- Header bottom -->
			<div class="header-bottom another-home-header-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-12">
							<div class="heder-bottom-left">
								<!-- Header logo -->
								<div class="header-logo">
									<!-- <a href="<?php echo base_url().$website->alamat_website ?>"><img style="height:72px;" src="<?php echo base_url(); ?>images/logo_replika/<?php echo $website->logo ?>" alt="logo"></a> -->
									<a href="<?php echo base_url().$website->alamat_website ?>"><div style="font-family: 'Fruktur', cursive; font-size: 45px; color:white;"><?php echo $website->alamat_website ?></div></a>
								</div>
							</div>
						</div>
						<div class="col-md-9 col-sm-12">
							<div class="header-bottom-right">
								<div class="row">
									<div class="col-md-12">
										
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<!-- Main Menu Area -->
										<div class="main-menu-area another-home-mainmenu-area home-4-main-menu-area">
											<!-- Main Menu -->
											<div class="main-menu hidden-sm hidden-xs">
												<nav>
													<ul class="main-ul">
														<li><a href="<?php echo base_url(); ?><?php echo $website->alamat_website  ?>" class="active">Home</a></li>
														<li><a href="<?php echo base_url(); ?><?php echo $website->alamat_website  ?>/about-us">Tentang Kami</a>
														</li>
														<li><a href="<?php echo base_url(); ?><?php echo $website->alamat_website  ?>/produk-kami">Produk Kami</a>
															
														</li>
														<li><a href="<?php echo base_url(); ?><?php echo $website->alamat_website  ?>/hubungi-kami">Hubungi Kami</a>
															
														</li>
													</ul>
												</nav>
											</div><!-- End Main Menu -->
											<!-- Start Mobile Menu -->
											<div class="mobile-menu hidden-md hidden-lg">
												<nav>
													<ul>
														<li class=""><a href="index.html">Home</a>
															<ul class="sub-menu">
																<li><a href="index-2.html">Home Page 2</a></li>
																<li><a href="index-3.html">Home Page 3</a></li>
																<li><a href="index-4.html">Home Page 4</a></li>
																<li><a href="index-5.html">Home Page 5</a></li>
																<li><a href="index-6.html">Home Page 6</a></li>
															</ul>
														</li>
														<li><a href="shop.html">Women</a>
															<ul class="">
																<li><a href="">Clother</a>
																	<ul>
																		<li><a href="#">Cocktail</a></li>														
																		<li><a href="#">Day</a></li>
																		<li><a href="#">Evening</a></li>
																		<li><a href="#">Sports</a></li>
																		<li><a href="#">Sexy Dress</a></li>
																		<li><a href="#">Fsshion Skirt</a></li>
																		<li><a href="#">Evening Dress</a></li>
																		<li><a href="#">Children's Clothing</a></li>
																	</ul>
																</li>
																<li><a href="#">Dress and skirt</a>
																	<ul>
																		<li><a href="#">Sports</a></li>														
																		<li><a href="#">Run</a></li>
																		<li><a href="#">Sandals</a></li>
																		<li><a href="#">Books</a></li>
																		<li><a href="#">A-line Dress</a></li>
																		<li><a href="#">Lacy Looks</a></li>
																		<li><a href="#">Shirts-T-Shirts</a></li>
																	</ul>
																</li>
																<li><a href="#">shoes</a>
																	<ul>
																		<li><a href="#">blazers</a></li>														
																		<li><a href="#">table</a></li>
																		<li><a href="#">coats</a></li>
																		<li><a href="#">Sports</a></li>
																		<li><a href="#">kids</a></li>
																		<li><a href="#">Sweater</a></li>
																		<li><a href="#">Coat</a></li>
																	</ul>
																</li>
															</ul>
														</li>
														<li class=""><a href="shop.html">Men</a>
															<ul class="">
																<li><a href="#">Bages</a>
																	<ul>
																		<li><a href="#">Bootes Bages</a></li>														
																		<li><a href="#">Blazers</a></li>
																		<li><a href="#">Mermaid</a></li>
																	</ul>
																</li>
																<li><a href="#">Clothing</a>
																	<ul>
																		<li><a href="#">coats</a></li>														
																		<li><a href="#">T-shirt</a></li>
																	</ul>
																</li>
																<li><a href="#">lingerie</a>
																	<ul>
																		<li><a href="#">brands</a></li>														
																		<li><a href="#">furniture</a></li>
																	</ul>
																</li>
															</ul>
														</li>
														<li><a href="shop.html">Handbags</a>
															<ul class="">
																<li><a href="#">Footwear Man</a>
																	<ul>
																		<li><a href="#">Gold Rigng</a></li>														
																		<li><a href="#">paltinum Rings</a></li>
																		<li><a href="#">Silver Ring</a></li>
																		<li><a href="#">Tungsten Ring</a></li>
																	</ul>	
																</li>
																<li><a href="#">Footwear Womens</a>
																	<ul>
																		<li><a href="#">Brand Gold</a></li>														
																		<li><a href="#">paltinum Rings</a></li>
																		<li><a href="#">Silver Ring</a></li>
																		<li><a href="#">Tungsten Ring</a></li>
																	</ul>	
																</li>
																<li><a href="#">Band</a>
																	<ul>
																		<li><a href="#">Platinum Necklaces</a></li>														
																		<li><a href="#">Gold Ring</a></li>
																		<li><a href="#">silver ring</a></li>
																		<li><a href="#">Diamond Bracelets</a></li>
																	</ul>	
																</li>	
															</ul>
														</li>
														<li><a href="shop.html">Shoes</a>
															<ul class="">
																<li><a href="#">Rings</a>
																	<ul>
																		<li><a href="#">Coats & jackets</a></li>														
																		<li><a href="#">blazers</a></li>
																		<li><a href="#">raincoats</a></li>
																	</ul>	
																</li>
																<li><a href="#">Dresses</a>
																	<ul>
																		<li><a href="#">footwear</a></li>														
																		<li><a href="#">blazers</a></li>
																		<li><a href="#">clog sandals</a></li>
																		<li><a href="#">combat boots</a></li>
																	</ul>	
																</li>
																<li><a href="#">Accessories</a>
																	<ul>
																		<li><a href="#">bootees Bags</a></li>														
																		<li><a href="#">blazers</a></li>
																		<li><a href="#">jackets</a></li>
																		<li><a href="#">kids</a></li>
																		<li><a href="#">shoes</a></li>
																	</ul>	
																</li>
																<li><a href="#">Top</a>
																	<ul>
																		<li><a href="#">briefs</a></li>														
																		<li><a href="#">camis</a></li>
																		<li><a href="#">nigthwear</a></li>
																		<li><a href="#">kids</a></li>
																		<li><a href="#">shapewer</a></li>
																	</ul>	
																</li>
															</ul>
														</li>
														<li class=""><a href="">Pages</a>
															<ul class="">
																<li><a href="blog.html">Blog</a></li>
																<li><a href="blog-details.html">Blog Details</a></li>
																<li><a href="cart.html">Cart</a></li>
																<li><a href="checkout.html">Checkout</a></li>
																<li><a href="contact.html">Contact</a></li>
																<li><a href="shop.html">Shop</a></li>
																<li><a href="shop-list.html">Shop List</a></li>
																<li><a href="product-details.html">Product Details</a></li>
																<li><a href="my-account.html">My Account</a></li>
																<li><a href="wishlist.html">Wishlist</a></li>
															</ul>
														</li>
													</ul>
												</nav>
											</div><!-- End Mobile Menu -->
										</div><!-- End Main Menu Area -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- End Header bottom -->
		</div><!-- End Header Area -->