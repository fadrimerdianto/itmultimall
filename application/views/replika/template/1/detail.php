<?php 
$d = $iklan;
$gbr = $this->IklanModel->gbr_iklan($d->id_iklan)->result();
	//$gbr1 = $gbr->row();
?> 
<!-- Breadcurb Area -->
<div class="breadcurb-area">
	<div class="container">
		<ul class="breadcrumb" style="width:100%">
			<li><a href="#">Home</a></li>
			<li>Produk</li>
			<li><?php echo $iklan->judul_iklan ?></li>
		</ul>
	</div>
</div><!-- End Breadcurb Area -->
<!-- Single Product details Area -->
<div class="single-product-detaisl-area">
	<!-- Single Product View Area -->
	<div class="product-view-area">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12 col-xs-12">
					<!-- Single Product View -->
					<h2><?php echo $iklan->judul_iklan ?></h2>
					<div class="single-procuct-view">
						<!-- Simple Lence Gallery Container -->
						<div class="simpleLens-gallery-container" id="p-view">
							<div class="simpleLens-container tab-content">
								<?php 
								$i=1;
								foreach ($gbr as $q_gbr) {
									if($i == 1){
										$active = 'active';
									}else{
										$active = '';
									}
									?>
									<div class="tab-pane <?php echo $active ?>" id="p-view-<?php echo $i ?>">
										<div class="simpleLens-big-image-container">
											<a class="simpleLens-lens-image" data-lens-image="<?php echo base_url() ?>images/iklan/<?php echo $q_gbr->photo ?>">
												<img src="<?php echo base_url() ?>images/iklan/<?php echo $q_gbr->photo ?>" class="simpleLens-big-image" alt="productd">
											</a>
										</div>
									</div>
									<?php
									$i++;
								} 
								?>
							</div>
							<!-- Simple Lence Thumbnail -->
							<div class="simpleLens-thumbnails-container">
								<h2>Foto Lainnya</h2>
								<div id="single-product" class="owl-carousel">
									<ul class="nav nav-tabs" role="tablist">
										<?php 
										$i=1;
										foreach ($gbr as $q_gbr) {
											if($i == 1){
												$active = 'active';
											}else{
												$active = '';
											}
											echo '<li class="'.$active.'"><a href="#p-view-'.$i.'" role="tab" data-toggle="tab"><img style="width:137; height:107px" src="'.base_url().'images/iklan/'.$q_gbr->photo.'" alt="productd"></a></li>';
											$i++;
										}

										?>

									</ul>
								</div>
							</div><!-- End Simple Lence Thumbnail -->
						</div><!-- End Simple Lence Gallery Container -->
					</div><!-- End Single Product View -->
					<div class="single-product-content-view">
						<div class="short-description">
							<div class="std">
								<?php echo $iklan->deskripsi_iklan ?>
							</div>
						</div>
						<div class="price-box">
							<span class="regular-price">
								<span class="price"><?php echo number_format($iklan->harga_iklan) ?></span> 
							</span>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-left">
					<div>
						<div class="harga-iklan">
							<div class="harga">
								<div class="harga-icon">
									<i class=" fa fa-tags"></i>
								</div>
								<div class="harga-detail">RP. <?php echo $this->cart->format_number($d->harga_iklan); ?></div>
							</div>
						</div>
						<div class="owner-iklan">
							<div class="owner-container">
								<div class="user-icon">
									<i class="fa fa-user"></i>
								</div>
								<div class="user-detail">
									<h2  style="margin-bottom:8px; font-size:16px; margin-top:10px;" class="owner-name"><?php echo $d->nama_member; ?></h2>
								</div>

								<div class="sejak-box">
									<span class="sejak">Sejak jan 2016</span>

								</div>
								<div class="ratings">
									<span class="price" style="color:black"><img src="<?php echo base_url();?>assets/images/jasa-pembuatan-website-direction.png" width="20"> <?php echo $kota_member->nama_area ?>, <?php echo $kota_member->provinsi->nama_provinsi ?></span>
								</div>
								<div class="laporkan-penjual">
									<a href="#" data-toggle="modal" data-target="#modal_lapor"><i class="fa fa-ban"></i> Laporkan Penjual</a>
								</div>

							</div>
							<div class="clearfix"></div>
							<div class="">
								<span class="label label-primary" style="display:block; padding:5px; margin-bottom:5px;"><?php echo '<i class="fa fa-calendar"></i> &nbsp;'.tgl_indo($d->tanggal_post) ?></span>
								<span class="label label-success"  style="display:block; padding:5px;"><?php echo '<i class="fa fa-chain"></i> &nbsp;' ?></span>
							</div>
						</div>
						<div class="clearfix"></div>
						<hr>
						<div class="tips">
							<div class="tips-icon" style="width:10%">
								<i class="fa fa-exclamation-circle fa-2x"></i>
							</div>
							<div class="tips-judul">
								<div>Tips untuk membeli</div>
							</div>
							<ul class="tips-list">
								<li><i class="fa fa-check"></i>Ketemuan di tempat aman</li>
								<li><i class="fa fa-check"></i>Teliti sebelum membeli </li>
								<li><i class="fa fa-check"></i>Bayar setelah barang diterima</li>
							</ul>
						</div>
						<div class="clearfix"></div>
						<hr>
						<div class="kontak-iklan">
							<ul class="kontak-list">
								<li>
									<div class="list-icon">
										<img src="<?php echo base_url() ?>assets/images/phone.png">
									</div>
									<div class="kontak-list-detail">
										<div class="kontak-hp"><?php echo $d->telepon_member ?></div>
										<?php 
										if($d->wa_available == 1){
											?>
											<div class="whatsapp-status"><i style="color:green" class="fa fa-whatsapp"></i> Ada Whatsapp</div>
											<?php
										}else{
											?>
											<div class="whatsapp-status"><i style="color:red" class="fa fa-whatsapp"></i> Tidak Ada Whatsapp</div>
											<?php
										}
										?>
									</div>
									<div class="clearfix"></div>
								</li>
								<li>
									<div class="list-icon">
										<img src="<?php echo base_url() ?>assets/images/bbm.png">
									</div>
									<div class="kontak-bbm kontak-list-detail">
										<?php echo $d->bbm_member ?>
									</div>
									<div class="clearfix"></div>
								</li>
								<li>
									<div class="list-icon">
										<img src="<?php echo base_url() ?>assets/images/profil.png">
									</div>
									<div class="kontak kontak-list-detail">
										<?php echo $d->id_line ?>
									</div>
									<div class="clearfix"></div>
								</li>

                <!-- <li>
                    <div class="list-icon">
                        <img src="<?php echo base_url() ?>assets/images/email.png">
                    </div>
                    <div class="kontak-email">
                        <?php echo $d->email_member ?>
                    </div>
                    <div class="clearfix"></div>
                </li> -->
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

</div><!-- /.compare -->
<div class="col-md-8 col-sm-8 col-xs-12">
	<!-- Single Product Content View -->
	<!-- End Single Product Content View -->
</div>
</div>
</div>
</div><!-- End Single Product View Area -->
</div><!-- End Single Product details Area -->
