<div class="shop-product-area">
			<div class="container">
				<div class="row">
					
					<div class="col-md-8 col-sm-12 col-md-offset-2">
						<!-- Shop Product Right -->
						<div class="shop-product-right">
							<div class="product-tab-area">
								<!-- Tab Content -->
								<div class="tab-content">
									<div class="tab-pane active" id="shop-product">
										<div class="row tab-content-row">
											<?php 
												foreach ($iklan as $q_iklan) {
													?>
														<!-- Start Single Product Column -->
														<div class="col-md-4 col-sm-4">
															<div class="single-product">
																<div class="single-product-img">
																	<a href="#">
																		<img class="primary-img" src="<?php echo base_url().'images/iklan/'.$q_iklan->gambar[0]->photo ?>" alt="product">
																	</a>
																</div>
																<div class="single-product-content">
																	<div class="product-content-head">
																		<h2 class="product-title"><a href="#">Cras neque metus</a></h2>
																		<p class="product-price">$155.00</p>
																	</div>
																	<div class="product-bottom-action">
																		<div class="product-action">
																			<div class="action-button">
																				<button class="btn" type="button"><i class="fa fa-shopping-cart"></i> <span>Add to bag</span></button>
																			</div>
																			<div class="action-view">
																				<button type="button" class="btn" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i>Quick view</button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div><!-- End Single Product Column -->
													<?php
												}
											?>
										</div>
									</div>
								</div><!-- End Tab Content -->
								<!-- Tab Bar -->
								<div class="tab-bar">
									<div class="toolbar">
										<div class="sorter">
											<div class="sort-by">
												<label>Sort By</label>
												<select>
													<option value="position">Position</option>
													<option value="name">Name</option>
													<option value="price">Price</option>
												</select>
												<a href="#"><i class="fa fa-long-arrow-up"></i></a>
											</div>
										</div>
										<div class="pages">
											<strong>Page:</strong>
											<ol>
												<li class="current">1</li>
												<li><a href="#">2</a></li>
												<li><a title="Next" href="#"><i class="fa fa-arrow-right"></i></a></li>
											</ol>
										</div>
									</div>
								</div><!-- End Tab Bar -->
							</div>
						</div><!-- End Shop Product Left -->
					</div>
				</div>
			</div>
		</div>