		<!-- Main Slider Area -->
		<div class="main-slider-area home-4-main-slaider-area entire-home-main-slider">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!-- Main Slider -->
						<div class="main-slider">
							<div class="slider">
								<div id="mainSlider" class="nivoSlider slider-image">
									<img src="<?php echo base_url(); ?>images/slider_replika/<?php echo $slider->slider1 ?>" alt="main slider" title="#htmlcaption1"/>
									<img src="<?php echo base_url(); ?>images/slider_replika/<?php echo $slider->slider2 ?>" alt="main slider" title="#htmlcaption2"/>
									<img src="<?php echo base_url(); ?>images/slider_replika/<?php echo $slider->slider3 ?>" alt="main slider" title="#htmlcaption2"/>
									<?php 
											if($slider->slider4 != ''){
												?>
													<img src="<?php echo base_url(); ?>images/slider_replika/<?php echo $slider->slider4 ?>" alt="main slider" title="#htmlcaption2"/>
												<?php
											}
									?>
								</div>
								<!-- Slider Caption One -->
								<!-- <div id="htmlcaption1" class="nivo-html-caption slider-caption-1">
									<div class="slider-progress"></div>									
									<div class="slide-text">
										<div class="middle-text">
											<div class="cap-dec">
												<h1 class="cap-dec wow zoomInRight" data-wow-duration="1.1s" data-wow-delay="0s">Huge sale</h1>
												<p class="cap-dec wow zoomInRight" data-wow-duration="1.3s" data-wow-delay="0s"> up to 70% off Fahion collection Shop now</p>
											</div>	
											<div class="cap-readmore wow zoomInRight" data-wow-duration=".9s" data-wow-delay=".5s">
												<a href="#">Shop Now</a>
											</div>	
										</div>	
									</div>
								</div> -->
								<!-- Slider Caption Two -->
								<!-- <div id="htmlcaption2" class="nivo-html-caption slider-caption-2">
									<div class="slider-progress"></div>					
									<div class="slide-text slide-text-2">
										<div class="middle-text">
											<div class="cap-dec">
												<h1 class="wow zoomInUp" data-wow-duration="1.1s" data-wow-delay="0s">Huge sale</h1>
												<p class="wow zoomInUp" data-wow-duration="1.3s" data-wow-delay="0s"> up to 70% off Fahion collection Shop now</p>
											</div>	
											<div class="cap-readmore wow zoomInUp" data-wow-duration="1.3s" data-wow-delay=".3s">
												<a href="#">Shop Now</a>
											</div>	
										</div>	
									</div>
								</div> -->
							</div>
						</div><!-- End Main Slider -->
					</div>
					
				</div>
			</div>
		</div><!-- End Main Slider Area -->		
		<!-- Product area -->
		<div class="product-area">
			<div class="container">
				<div class="row">
					<div style="margin-bottom:40px;"></div>
					<div class="col-md-12">
						<!-- Single Product area -->
						<div class="c-carousel-button">	
							<!-- Tab Content -->
							<div class="tab-content">
								<!-- Tab Pane One -->
								<div class="tab-pane active" >
									<div class="row">
										<!-- Single Product Carousel-->
										<div id="single-product-bestseller">
											<!-- Start Single Product Column-->
											<div class="col-md-12">
												<?php
												$i = 0; 
												foreach ($iklan as $q_iklan) {
													if($i < 12){
														echo '
															<div class="col-md-3">
																<div class="single-product">
																	<div class="single-product-img">
																		<a href="'.base_url().$website->alamat_website.'/detail/'.$q_iklan->seo_iklan.'">
																			<img style="width:300px; object-fit:cover; height:250px" class="primary-img img-responsive img-thumbnail" src="'.base_url().'images/iklan/'.$q_iklan->gambar[0]->photo.'" alt="product">
																		</a>
																		
																	</div>
																	<div class="single-product-content">
																		<div class="product-content-head">
																			<h2 class="product-title"><a href="#">'.$q_iklan->judul_iklan.'</a></h2>
																			<p class="product-price">Rp. '.number_format($q_iklan->harga_iklan).'</p>
																		</div>
																		<div class="product-bottom-action">
																			<div class="product-action" style="text-align:center">
																				<div class="action-button" style="float:none">
																					<button class="btn" type="button"><i class="fa fa-search"></i> <span>Detail</span></button>
																				</div>
																				
																			</div>
																		</div>
																	</div>
																</div>
															</div><!-- End Single Product Column -->
														';
													}
													
												$i++;
												}
											?>
											</div>
										</div><!-- End Single Product Carousel-->
									</div>
								</div><!-- End Tab Pane One -->
								<!-- Tab Pane Two -->
								
								<!-- Tab Pane Three -->
								
							</div><!-- End Tab Content -->
						</div><!-- End Single Product area -->
					</div>
				</div>
			</div>
		</div><!-- End Product area -->