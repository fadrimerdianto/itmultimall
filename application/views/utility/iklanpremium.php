<div class="clearfix"></div>
<div class="pilih-premium">
	<div id="pilih-iklan-premium">
		<div class="table-responsive">
			<table class="table custom-table datatable-ku">
				<thead>
					<tr class="first last">
						<th></th>
						<th>Tanggal</th>
						<th style="width:35%">Judul Iklan</th>
						<th></th>
						<th>Harga</th>

					</tr>
				</thead>
				<tbody>
					<?php 
					$i = 0;
					foreach ($iklan as $q_iklan) {
							if($i == 0){
								$req = 'required';
							}else{
								$req = '';
							}
							$i++;
							echo '
							<tr>
								<td>
									<input class="checkbox" type="checkbox" name="iklan[]" value="'.$q_iklan->seo_iklan.'" />

								</td>
								<td>
									<center>'.convertDateTime($q_iklan->tanggal_post).'</center>

								</td>
								<td>
									<div class="link-judul-iklan"><a href="'.base_url().'iklan/detail/'.$q_iklan->seo_iklan.'">'.$q_iklan->judul_iklan.'</a></div>
									<div><i class="fa fa-eye"></i> Dilihat '.$q_iklan->dilihat.' Kali</div>
								</td>
								<td><a class="product-image" title="'.$q_iklan->judul_iklan.'" href="#">
									<center><img style="width:50px;" alt="'.$q_iklan->judul_iklan.'" src="'.base_url().'images/iklan/'.$q_iklan->gambar[0]->photo.'"></center>
								</a></td>


								<td class="subtotal">Rp. '.number_format($q_iklan->harga_iklan).'</td>

							</tr>
							';
					}
					?>

				</tbody>
			</table>
		</div>
		<div class="pilih-premium">
			<div class="submit-button">
				<button class="btn btn-success" onClick="return confirm('Apakah Anda yakin mendaftar iklan premium ?')">Daftar</button>
			</div>
		</div>
	</div>

</div>
<div class="clearfix"></div>