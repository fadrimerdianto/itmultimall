<?php
switch ($sub) {
	case 8:
	?>
	<div style="width:50%; float:left">
		<label>Transmisi</label>
		<select data-mini="true" name="transmisi" id="pilih-transmisi" data-native-menu="true">
			<option>Pilih Transmisi</option>
			<option value="Automatic">Automatic</option>
			<option value="Manual">Manual</option>
			<option value="Triptonic">Triptonic</option>
		</select>
	</div>
	<div style="width:50%; float:left">
		<label>Tahun</label>
		<select data-mini="true" name="tahun" id="pilih-tahun" data-native-menu="true">
			<option value="javascript">Pilih Tahun</option>
			<?php 
			for ($i=date('Y'); $i > 1990 ; $i--) { 
				echo '<option value="'.$i.'">'.$i.'</option>';
			}
			?>
		</select>
	</div>
	<div style="clear:both"></div>
	<label>Tipe Mobil</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Tipe</option>
		<?php 
		foreach ($tipe_mobil as $q_tipe) {
			echo '<option value="'.$q_tipe->id_tipe_mobil.'">'.$q_tipe->tipe.'</option>';
		}
		?>
	</select>
	<?php
	break;
	case 13:
	?>
	<label>Tahun:</label>
	<select data-mini="true" name="tahun" id="pilih-tahun" data-native-menu="true">
		<option>Pilih Tahun</option>
		<?php 
		for ($i=date('Y'); $i > 1990 ; $i--) { 
			echo '<option value="'.$i.'">'.$i.'</option>';
		}
		?>
	</select>
	<label>Tipe Motor:</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Tipe</option>
		<?php 
		foreach ($tipe_motor as $q_tipe) {
			echo '<option value="'.$q_tipe->id_tipe_motor.'">'.$q_tipe->tipe.'</option>';
		}
		?>
	</select>
	<?php
	break;
	//rumah
	case 17:
	?>
	<label>Luas Tanah</label>
	<input required type="number" name="luas_tanah" data-clear-btn="true"/>

	<label>Luas Bangunan</label>
	<input required type="number" name="luas_bangunan" data-clear-btn="true"/>

	<label>Jumlah Lantai</label>
	<input required type="number" name="lantai" data-clear-btn="true"/>

	<label>Kamar Tidur</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Kamar Tidur</option>
		<?php 
		for ($i= 1 ; $i <= 10; $i++) { 
			echo '<option value="'.$i.'">'.$i.'</option>';
		}
		?>
		<option value="11"> > 10</option>
	</select>

	<label>Kamar Mandi</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Kamar Mandi</option>
		<?php 
		for ($i= 1 ; $i <= 10; $i++) { 
			echo '<option value="'.$i.'">'.$i.'</option>';
		}
		?>
		<option value="11"> > 10</option>
	</select>

	<label>Sertifikasi</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Serttifikasi</option>
		<option value="1"> SHM - Sertifikat Hak Milik</option>
		<option value="2"> HGB - Hak Guna Bangunan</option>
		<option value="3"> Lainnya (PPJB, Girik, Adat, Dll)</option>
	</select>

	<label>Fasilitas</label>
	<div>
		<fieldset data-role="controlgroup" data-mini="true" class="fasilitas-checkbox">
			<input type="checkbox" name="checkbox-1a" id="checkbox-1ab">
			<label for="checkbox-1ab">AC</label>
			<input type="checkbox" name="checkbox-2a" id="checkbox-2ab">
			<label for="checkbox-2ab">Swimming Pool</label>
			<input type="checkbox" name="checkbox-3a" id="checkbox-3ab">
			<label for="checkbox-3ab">Carport</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Garden</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Garasi</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Telephone</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">PAM</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Water Heater</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Refrigerator</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Gordyn</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
		</fieldset>
	</div>

	<?php
	break;
	//apartemen
	case 18:
	?>
	<label>Luas Bangunan</label>
	<input required type="number" name="luas_bangunan" data-clear-btn="true"/>
	
	<label>Jumlah Lantai</label>
	<input required type="number" name="lantai" data-clear-btn="true"/>

	<label>Kamar Tidur</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Kamar Tidur</option>
		<?php 
		for ($i= 1 ; $i <= 10; $i++) { 
			echo '<option value="'.$i.'">'.$i.'</option>';
		}
		?>
		<option value="11"> > 10</option>
	</select>

	<label>Sertifikasi</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Serttifikasi</option>
		<option value="1"> SHM - Sertifikat Hak Milik</option>
		<option value="2"> HGB - Hak Guna Bangunan</option>
		<option value="3"> Lainnya (PPJB, Girik, Adat, Dll)</option>
	</select>

	<div>
		<label>Fasilitas</label>
		<fieldset data-role="controlgroup" data-mini="true" class="fasilitas-checkbox">
			<input type="checkbox" name="checkbox-1a" id="checkbox-1ab">
			<label for="checkbox-1ab">AC</label>
			<input type="checkbox" name="checkbox-2a" id="checkbox-2ab">
			<label for="checkbox-2ab">Swimming Pool</label>
			<input type="checkbox" name="checkbox-3a" id="checkbox-3ab">
			<label for="checkbox-3ab">Carport</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Garden</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Garasi</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Telephone</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">PAM</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Water Heater</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Refrigerator</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Gordyn</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
		</fieldset>
	</div>

	<?php
	break;
	//indekos
	case 19:
	?>
	<label>Luas Bangunan</label>
	<input required type="number" name="luas_bangunan" data-clear-btn="true"/>

	<div>
		<label>Fasilitas</label>
		<fieldset data-role="controlgroup" data-mini="true" class="fasilitas-checkbox">
			<input type="checkbox" name="checkbox-1a" id="checkbox-1ab">
			<label for="checkbox-1ab">AC</label>
			<input type="checkbox" name="checkbox-2a" id="checkbox-2ab">
			<label for="checkbox-2ab">Swimming Pool</label>
			<input type="checkbox" name="checkbox-3a" id="checkbox-3ab">
			<label for="checkbox-3ab">Carport</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Garden</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Garasi</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Telephone</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">PAM</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Water Heater</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Refrigerator</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Gordyn</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
		</fieldset>
	</div>

	<?php
	break;
	//bangunan komersil
	case 20:
	?>
	<label>Luas Tanah</label>
	<input required type="number" name="luas_tanah" data-clear-btn="true"/>

	<label>Kamar Bangunan</label>
	<input required type="number" name="luas_bangunan" data-clear-btn="true"/>

	<label>Sertifikasi</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Serttifikasi</option>
		<option value="1"> SHM - Sertifikat Hak Milik</option>
		<option value="2"> HGB - Hak Guna Bangunan</option>
		<option value="3"> Lainnya (PPJB, Girik, Adat, Dll)</option>
	</select>

	<div>
		<label>Fasilitas</label>
		<fieldset data-role="controlgroup" data-mini="true" class="fasilitas-checkbox">
			<input type="checkbox" name="checkbox-1a" id="checkbox-1ab">
			<label for="checkbox-1ab">AC</label>
			<input type="checkbox" name="checkbox-2a" id="checkbox-2ab">
			<label for="checkbox-2ab">Swimming Pool</label>
			<input type="checkbox" name="checkbox-3a" id="checkbox-3ab">
			<label for="checkbox-3ab">Carport</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Garden</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Garasi</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Telephone</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">PAM</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Water Heater</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Refrigerator</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
			<label for="checkbox-4ab">Gordyn</label>
			<input type="checkbox" name="checkbox-4a" id="checkbox-4ab">
		</fieldset>
	</div>

	<?php
	break;
	//tanah
	case 21:
	?>
	<label>Luas Tanah</label>
	<input required type="number" name="luas_tanah" data-clear-btn="true"/>

	<label>Sertifikasi</label>
	<select data-mini="true" name="tipe" id="pilih-tipe" data-native-menu="true">
		<option>Pilih Serttifikasi</option>
		<option value="1"> SHM - Sertifikat Hak Milik</option>
		<option value="2"> HGB - Hak Guna Bangunan</option>
		<option value="3"> Lainnya (PPJB, Girik, Adat, Dll)</option>
	</select>

	<?php
	break;
	default:
		# code...
	break;
}
?>