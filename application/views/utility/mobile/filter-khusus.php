<?php
switch ($subkategori) {
	case 8:
	?>
	<label>Transmisi</label>
	<select name="transmisi" data-native-menu="true" data-mini="true">
		<option>Pilih Tranmsisi</option>
		<option value="automatic">Automatic</option>
		<option value="manual">Manual</option>
		<option value="triprionic">Triptonic</option>
	</select>

	<label>Tipe</label>
	<select name="tipe" data-native-menu="true" data-mini="true">
		<option>Pilih Tipe</option>
		<?php 
		foreach ($tipe as $q_tipe) {
			echo '<option value="'.$q_tipe->id_tipe_mobil.'">'.$q_tipe->tipe.'</option>';
		}
		?>
	</select>

	<label>Tahun</label>
	<select name="tahun" data-native-menu="true" data-mini="true">
		<option>Pilih Tahun</option>
		<?php 
		for ($i=2016; $i > 1990 ; $i--) { 
			echo '<option>'.$i.'</option>';
		}
		?>
	</select>
	<?php
	break;
	case 13:
	?>
	<label>Tipe</label>
	<select name="tipe" data-native-menu="true" data-mini="true">
		<option>Pilih Tipe</option>
	</select>

	<label>Tahun</label>
	<select name="tahun" data-native-menu="true" data-mini="true">
		<?php 
		for ($i=2016; $i > 1990 ; $i--) { 
			echo '<option>'.$i.'</option>';
		}
		?>
	</select>
	<?php
	break;
	//rumah
	case 17:
	//var_dump($this->input->get());
	?>
	<label>Luas Bangunan Min</label>
	<select name="luas_bangunan_awal" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Bangunan</option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 30? 'selected':'' ?> value="30">30 m<sup>2</sup></option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 60? 'selected':'' ?> value="60">60 m<sup>2</sup></option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 90? 'selected':'' ?> value="90">90 m<sup>2</sup></option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 125? 'selected':'' ?> value="125">125 m<sup>2</sup></option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 150? 'selected':'' ?> value="150">150 m<sup>2</sup></option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 175? 'selected':'' ?> value="175">175 m<sup>2</sup></option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 225? 'selected':'' ?> value="225">225 m<sup>2</sup></option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 250? 'selected':'' ?> value="250">250 m<sup>2</sup></option>
		<option <?php echo $this->input->get('luas_bangunan_awal') == 500? 'selected':'' ?> value="500">500 m<sup>2</sup></option>
	</select>

	<label>Luas Bangunan Max</label>
	<select name="luas_bangunan_akhir" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Bangunan</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Luas Tanah Min</label>
	<select name="luas_tanah_awal" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Tanah</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Luas Tanah Max</label>
	<select name="luas_tanah_akhir" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Tanah</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Lantai</label>
	<select name="lantai" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Lantai</option>
		<?php 
		for ($i=1; $i <= 10 ; $i++) {
			if($_GET['lantai'] == $i){
				$aktif = 'selected';
			}else{
				$aktif = '';
			} 
			echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['lantai'=>$i])).'">'.$i.'</option>';
		}
		if($_GET['lantai'] == 11){
			$aktif = 'selected';
		}
		?>
		<option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['lantai'=>11])) ?>"> > 10</option>

	</select>

	<label>Kamar Tidur</label>
	<select name="kamar_tidur" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Kamar Tidur</option>
		<?php 
		for ($i=1; $i <= 10 ; $i++) {
			if($_GET['kamar_tidur'] == $i){
				$aktif = 'selected';
			}else{
				$aktif = '';
			} 
			echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_tidur'=>$i])).'">'.$i.'</option>';
		}
		if($_GET['kamar_tidur'] == 11){
			$aktif = 'selected';
		}
		?>
		<option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_tidur'=>11])) ?>"> > 10</option>
	</select>

	<label>Kamar Mandi</label>
	<select name="kamar_mandi" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Kamar Mandi</option>
		<?php 
		for ($i=1; $i <= 10 ; $i++) {
			if($_GET['kamar_mandi'] == $i){
				$aktif = 'selected';
			}else{
				$aktif = '';
			} 
			echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_mandi'=>$i])).'">'.$i.'</option>';
		}
		if($_GET['kamar_mandi'] == 11){
			$aktif = 'selected';
		}
		?>
		<option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_mandi'=>11])) ?>"> > 10</option>

	</select>

	<label>Sertifikasi</label>
	<select name="sertifikasi" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Sertifikasi</option>
		<?php
		$shm = ''; $hgb = ''; $lain = '';
		if($_GET['sertifikasi'] == 1){
			$shm = 'selected';
		}elseif ($_GET['sertifikasi'] == 2) {
			$hgb = 'selected';
		}elseif ($_GET['sertifikasi'] == 3) {
			$lain = 'selected';
		}
		?>
		<option <?php echo $shm ?> value="1">SHM - Sertifikat Hak Milik</option>
		<option <?php echo $hgb ?> value="2">HGB - Hak Guna Bangunan</option>
		<option <?php echo $lain ?> value="3">Lainnya (PPJB,Girik, Adat, dll)</option>

	</select>

	<label>Fasilitas</label>
	
	
	<?php
	break;
	//apartemen
	case 18:
	?>
	<label>Luas Bangunan Min</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Bangunan</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Luas Bangunan Max</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Bangunan</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Lantai</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Lantai</option>
		<?php 
		for ($i=1; $i <= 10 ; $i++) {
			if($_GET['lantai'] == $i){
				$aktif = 'selected';
			}else{
				$aktif = '';
			} 
			echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['lantai'=>$i])).'">'.$i.'</option>';
		}
		if($_GET['lantai'] == 11){
			$aktif = 'selected';
		}
		?>
		<option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['lantai'=>11])) ?>"> > 10</option>

	</select>

	<label>Kamar Tidur</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Kamar Tidur</option>
		<?php 
		for ($i=1; $i <= 10 ; $i++) {
			if($_GET['kamar_tidur'] == $i){
				$aktif = 'selected';
			}else{
				$aktif = '';
			} 
			echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_tidur'=>$i])).'">'.$i.'</option>';
		}
		if($_GET['kamar_tidur'] == 11){
			$aktif = 'selected';
		}
		?>
		<option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_tidur'=>11])) ?>"> > 10</option>
	</select>

	<label>Sertifikasi</label>
	<select name="sertifikasi" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Sertifikasi</option>
		<?php
		$shm = ''; $hgb = ''; $lain = '';
		if($_GET['sertifikasi'] == 1){
			$shm = 'selected';
		}elseif ($_GET['sertifikasi'] == 2) {
			$hgb = 'selected';
		}elseif ($_GET['sertifikasi'] == 3) {
			$lain = 'selected';
		}
		?>
		<option <?php echo $shm ?> value="1">SHM - Sertifikat Hak Milik</option>
		<option <?php echo $hgb ?> value="2">HGB - Hak Guna Bangunan</option>
		<option <?php echo $lain ?> value="3">Lainnya (PPJB,Girik, Adat, dll)</option>

	</select>

	<?php
	break;
	//indekos
	case 19:
	?>
	<label>Kamar Mandi</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Kamar Mandi</option>
		<?php 
		for ($i=1; $i <= 10 ; $i++) {
			if($_GET['kamar_mandi'] == $i){
				$aktif = 'selected';
			}else{
				$aktif = '';
			} 
			echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_mandi'=>$i])).'">'.$i.'</option>';
		}
		if($_GET['kamar_mandi'] == 11){
			$aktif = 'selected';
		}
		?>
		<option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_mandi'=>11])) ?>"> > 10</option>

	</select>

	<label>Luas Bangunan Min</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Bangunan</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Luas Bangunan Max</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Bangunan</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>
	<?php
	break;
	//
	case 20:
	?>
	<label>Kamar Mandi</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Kamar Mandi</option>
		<?php 
		for ($i=1; $i <= 10 ; $i++) {
			if($_GET['kamar_mandi'] == $i){
				$aktif = 'selected';
			}else{
				$aktif = '';
			} 
			echo '<option '.$aktif.' value="?'.http_build_query(array_merge($_GET, ['kamar_mandi'=>$i])).'">'.$i.'</option>';
		}
		if($_GET['kamar_mandi'] == 11){
			$aktif = 'selected';
		}
		?>
		<option <?php echo $aktif; ?> value="?<?php echo http_build_query(array_merge($_GET, ['kamar_mandi'=>11])) ?>"> > 10</option>

	</select>

	<label>Luas Bangunan Min</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Bangunan</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Luas Bangunan Max</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Bangunan</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>
	<?php
	break;

	//tanah
	case 21:
	?>

	<label>Luas Tanah Min</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Tanah</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Luas Tanah Max</label>
	<select data-native-menu="true" data-mini="true"> 
		<option selected disabled>Luas Tanah</option>
		<option value="30">30 m<sup>2</sup></option>
		<option value="60">60 m<sup>2</sup></option>
		<option value="90">90 m<sup>2</sup></option>
		<option value="125">125 m<sup>2</sup></option>
		<option value="150">150 m<sup>2</sup></option>
		<option value="175">175 m<sup>2</sup></option>
		<option value="225">225 m<sup>2</sup></option>
		<option value="250">250 m<sup>2</sup></option>
		<option value="500">500 m<sup>2</sup></option>
	</select>

	<label>Sertifikasi</label>
	<select name="sertifikasi" data-native-menu="true" data-mini="true"> 
		<option selected disabled>Sertifikasi</option>
		<?php
		$shm = ''; $hgb = ''; $lain = '';
		if($_GET['sertifikasi'] == 1){
			$shm = 'selected';
		}elseif ($_GET['sertifikasi'] == 2) {
			$hgb = 'selected';
		}elseif ($_GET['sertifikasi'] == 3) {
			$lain = 'selected';
		}
		?>
		<option <?php echo $shm ?> value="1">SHM - Sertifikat Hak Milik</option>
		<option <?php echo $hgb ?> value="2">HGB - Hak Guna Bangunan</option>
		<option <?php echo $lain ?> value="3">Lainnya (PPJB,Girik, Adat, dll)</option>

	</select>
	<?php
	break;
	default:
			# code...
	break;
}