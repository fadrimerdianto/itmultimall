<?php
switch ($sub) {
	//merk mobil
	case 8:
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label">Transmisi <em>*</em></label>
		<div class="col-sm-2">
			<select class="form-control" name="transmisi">
				<option>Pilih</option>
				<option value="Manual">Manual</option>
				<option value="Automatic">Automatic</option>
				<option value="Triptonic">Triptonic</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Tipe Kendaraan <em>*</em></label>
		<div class="col-sm-3">
			<select class="form-control" name="tipe">
				<option>Pilih</option>
				<?php 
				foreach ($tipe_mobil as $q_tipe) {
					echo '<option value="'.$q_tipe->id_tipe_mobil.'">'.$q_tipe->tipe.'</option>';
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Tahun <em>*</em></label>
		<div class="col-sm-3">
			<select class="form-control" name="tahun">
				<option>Pilih</option>
				<?php 
				for ($i=date('Y'); $i >= 1990;  $i--) { 
					echo '<option value="'.$i.'">'.$i.'</option>';
				}
				?>
				<option>< 1990</option>
			</select>
		</div>
	</div>
	<?php
	break;

	//merk motor
	case 13:
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label">Tipe Kendaraan <em>*</em></label>
		<div class="col-sm-3">
			<select class="form-control">
				<option>Pilih</option>
				<?php 
				foreach ($tipe_motor as $q_tipe) {
					echo '<option value="'.$q_tipe->id_tipe_motor.'">'.$q_tipe->tipe.'</option>';
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Tahun <em>*</em></label>
		<div class="col-sm-3">
			<select class="form-control">
				<option>Pilih</option>
				<?php 
				for ($i=date('Y'); $i >= 1990;  $i--) { 
					echo '<option value="'.$i.'">'.$i.'</option>';
				}
				?>
				<option>< 1995</option>
			</select>
		</div>
	</div>
	<?php
	break;
	//properti rumah
	case 17:
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label">Luas Tanah <em>*</em></label>
		<div class="col-sm-2">
			<input required type="number" name="luas_tanah" class="form-control" />
		</div>
		<div>m&sup2;</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Luas Bangunan <em>*</em></label>
		<div class="col-sm-2">
			<input required type="number" name="luas_bangunan" class="form-control" />
		</div>
		<div>m&sup2;</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Lantai <em>*</em></label>
		<div class="col-sm-2">
			<input required type="number" name="lantai" class="form-control" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Kamar Tidur <em>*</em></label>
		<div class="col-sm-2">
			<select required class="form-control" name="kamar_tidur">
				<option>Pilih</option>
				<?php 
				for ($i=0; $i < 10; $i++) { 
					echo '<option>'.($i+1).'</option>';
				}
				?>
				<option> > 10</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Kamar Mandi <em>*</em></label>
		<div class="col-sm-2">
			<select required class="form-control" name="kamar_mandi">
				<option>Pilih</option>
				<?php 
				for ($i=0; $i < 10; $i++) { 
					echo '<option>'.($i+1).'</option>';
				}
				?>
				<option value="11"> > 10</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Sertifikasi <em>*</em></label>
		<div class="col-sm-2">
			<select required class="form-control" name="sertifikasi">
				<option>Pilih</option>
				<option value="1"> SHM - Sertifikat Hak Milik</option>
				<option value="2"> HGB - Hak Guna Bangunan</option>
				<option value="3"> Lainnya (PPJB, Girik, Adat, Dll)</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Fasilitas <em>*</em></label>
		<div style="margin-left:10px;"></div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="ac" value="1"> AC
			</label>
			<label class="checkbox">
				<input type="checkbox" name="swimming_pool" value="1"> Swimming Pool
			</label>
			<label class="checkbox">
				<input type="checkbox" name="carpot" value="1"> Carport
			</label>
			<label class="checkbox">
				<input type="checkbox" name="garden" value="1"> Garden
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="garasi" value="1"> Garasi
			</label>
			<label class="checkbox">
				<input type="checkbox" name="telephone" value="1"> Telephone
			</label>
			<label class="checkbox">
				<input type="checkbox" name="pam" value="1"> PAM
			</label>
			<label class="checkbox">
				<input type="checkbox" name="water_heater" value="1"> Water Heater
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="refrigerator" value="1"> Refrigerator
			</label>
			<label class="checkbox">
				<input type="checkbox" name="stove" value="1"> Stove
			</label>
			<label class="checkbox">
				<input type="checkbox" name="microwave" value="1"> Microwave
			</label>
			<label class="checkbox">
				<input type="checkbox" name="oven" value="1"> Oven
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="fire_extenguisher" value="1"> Fire Extenguisher
			</label>
			<label class="checkbox">
				<input type="checkbox" name="gordyn" value="1"> Gordyn
			</label>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Alamat Lokasi <em>*</em></label>
		<div class="col-sm-6">
			<textarea class="form-control" name="alamat_lokasi"></textarea>
		</div>
	</div>
	<?php
	break;
	//properti apartement
	case 18:
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label">Luas Bangunan <em>*</em></label>
		<div class="col-sm-2">
			<input required type="number" name="luas_bangunan" class="form-control" />
		</div>
		<div>m&sup2;</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Lantai <em>*</em></label>
		<div class="col-sm-2">
			<input required type="number" name="lantai" class="form-control" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Kamar Tidur <em>*</em></label>
		<div class="col-sm-2">
			<select required class="form-control" name="kamar_tidur">
				<option>Pilih</option>
				<?php 
				for ($i=0; $i < 10; $i++) { 
					echo '<option>'.($i+1).'</option>';
				}
				?>
				<option> > 10</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Sertifikasi <em>*</em></label>
		<div class="col-sm-2">
			<select required class="form-control" name="sertifikasi">
				<option>Pilih</option>
				<option value="1"> SHM - Sertifikat Hak Milik</option>
				<option value="2"> HGB - Hak Guna Bangunan</option>
				<option value="3"> Lainnya (PPJB, Girik, Adat, Dll)</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Fasilitas <em>*</em></label>
		<div style="margin-left:10px;"></div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="ac" value="1"> AC
			</label>
			<label class="checkbox">
				<input type="checkbox" name="swimming_pool" value="1"> Swimming Pool
			</label>
			<label class="checkbox">
				<input type="checkbox" name="carpot" value="1"> Carport
			</label>
			<label class="checkbox">
				<input type="checkbox" name="garden" value="1"> Garden
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="garasi" value="1"> Garasi
			</label>
			<label class="checkbox">
				<input type="checkbox" name="telephone" value="1"> Telephone
			</label>
			<label class="checkbox">
				<input type="checkbox" name="pam" value="1"> PAM
			</label>
			<label class="checkbox">
				<input type="checkbox" name="water_heater" value="1"> Water Heater
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="refrigerator" value="1"> Refrigerator
			</label>
			<label class="checkbox">
				<input type="checkbox" name="stove" value="1"> Stove
			</label>
			<label class="checkbox">
				<input type="checkbox" name="microwave" value="1"> Microwave
			</label>
			<label class="checkbox">
				<input type="checkbox" name="oven" value="1"> Oven
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="fire_extenguisher" value="1"> Fire Extenguisher
			</label>
			<label class="checkbox">
				<input type="checkbox" name="gordyn" value="1"> Gordyn
			</label>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Alamat Lokasi <em>*</em></label>
		<div class="col-sm-6">
			<textarea class="form-control" name="alamat_lokasi"></textarea>
		</div>
	</div>
	<?php
	break;
	//indekos
	case 19:
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label">Luas Bangunan <em>*</em></label>
		<div class="col-sm-2">
			<input required type="number" name="luas_bangunan" class="form-control" />
		</div>
		<div>m&sup2;</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Fasilitas <em>*</em></label>
		<div style="margin-left:10px;"></div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="ac" value="1"> AC
			</label>
			<label class="checkbox">
				<input type="checkbox" name="swimming_pool" value="1"> Swimming Pool
			</label>
			<label class="checkbox">
				<input type="checkbox" name="carpot" value="1"> Carport
			</label>
			<label class="checkbox">
				<input type="checkbox" name="garden" value="1"> Garden
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="garasi" value="1"> Garasi
			</label>
			<label class="checkbox">
				<input type="checkbox" name="telephone" value="1"> Telephone
			</label>
			<label class="checkbox">
				<input type="checkbox" name="pam" value="1"> PAM
			</label>
			<label class="checkbox">
				<input type="checkbox" name="water_heater" value="1"> Water Heater
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="refrigerator" value="1"> Refrigerator
			</label>
			<label class="checkbox">
				<input type="checkbox" name="stove" value="1"> Stove
			</label>
			<label class="checkbox">
				<input type="checkbox" name="microwave" value="1"> Microwave
			</label>
			<label class="checkbox">
				<input type="checkbox" name="oven" value="1"> Oven
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="fire_extenguisher" value="1"> Fire Extenguisher
			</label>
			<label class="checkbox">
				<input type="checkbox" name="gordyn" value="1"> Gordyn
			</label>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Alamat Lokasi <em>*</em></label>
		<div class="col-sm-6">
			<textarea class="form-control" name="alamat_lokasi"></textarea>
		</div>
	</div>		
	<?php
	break;
	//properti bangunan komersil
	case 20:
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label">Luas Tanah <em>*</em></label>
		<div class="col-sm-2">
			<input required type="number" name="luas_tanah" class="form-control" />
		</div>
		<div>m&sup2;</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Luas Bangunan <em>*</em></label>
		<div class="col-sm-2">
			<input required type="number" name="luas_bangunan" class="form-control" />
		</div>
		<div>m&sup2;</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Sertifikasi <em>*</em></label>
		<div class="col-sm-4">
			<select class="form-control">
				<option value="1"> SHM - Sertifikat Hak Milik</option>
				<option value="2"> HGB - Hak Guna Bangunan</option>
				<option value="3"> Lainnya (PPJB, Girik, Adat, Dll)</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Fasilitas <em>*</em></label>
		<div style="margin-left:10px;"></div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="ac" value="1"> AC
			</label>
			<label class="checkbox">
				<input type="checkbox" name="swimming_pool" value="1"> Swimming Pool
			</label>
			<label class="checkbox">
				<input type="checkbox" name="carpot" value="1"> Carport
			</label>
			<label class="checkbox">
				<input type="checkbox" name="garden" value="1"> Garden
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="garasi" value="1"> Garasi
			</label>
			<label class="checkbox">
				<input type="checkbox" name="telephone" value="1"> Telephone
			</label>
			<label class="checkbox">
				<input type="checkbox" name="pam" value="1"> PAM
			</label>
			<label class="checkbox">
				<input type="checkbox" name="water_heater" value="1"> Water Heater
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="refrigerator" value="1"> Refrigerator
			</label>
			<label class="checkbox">
				<input type="checkbox" name="stove" value="1"> Stove
			</label>
			<label class="checkbox">
				<input type="checkbox" name="microwave" value="1"> Microwave
			</label>
			<label class="checkbox">
				<input type="checkbox" name="oven" value="1"> Oven
			</label>
		</div>
		<div class="col-md-2">
			<label class="checkbox">
				<input type="checkbox" name="fire_extenguisher" value="1"> Fire Extenguisher
			</label>
			<label class="checkbox">
				<input type="checkbox" name="gordyn" value="1"> Gordyn
			</label>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Alamat Lokasi <em>*</em></label>
		<div class="col-sm-6">
			<textarea class="form-control" name="alamat_lokasi"></textarea>
		</div>
	</div>
	<?php
	break;

	default:
			# code...
	break;
}