<?php

class Migration_Create_reply_komentar_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_reply'			=> [
				'type'			=>	'int',
				'auto_increment'=> true
			],
			'id_komentar'	=> [
				'type'		=> 'int',
			],
			'username'	=> [
				'type'		=> 'varchar',
				'constraint'=> 40
			],
			'reply' => [
				'type' => 'text'
			],
			'waktu'	=> [
				'type'		=> 'datetime',
			]
		]);

		$this->dbforge->add_key('id_reply', TRUE);
		$this->dbforge->create_table('reply_komentar', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('reply_komentar');
	}
}