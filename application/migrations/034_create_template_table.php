<?php

class Migration_Create_template_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_template'			=> [
				'type'				=>	'int',
				'auto_increment'	=> true
			],
			'nama_template'		=> [
				'type'		=> 'varchar',
				'constraint'	=> 30
			],
			'harga_template'	=> [
				'type'	=> 'int'
			],
			'thumbnail_template'	=>[
				'type'	=> 'varchar',
				'constraint'=>80
			]
		]);

		$this->dbforge->add_key('id_template', TRUE);
		$this->dbforge->create_table('template', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('template');
	}
}