<?php

class Migration_Create_transaksi_poin_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_transaksi_poin'			=> [
				'type'			=>	'int',
				'auto_increment'=> true
			],
			'id_poin'	=> [
				'type'		=> 'int',
			],
			'username'	=> [
				'type'		=> 'varchar',
				'constraint'=> 40
			],
			'tanggal_transaksi'	=> [
				'type'			=> 'datetime',
			],
			'isKonfirmasi'	=> [
				'type'		=> 'int',
				'default'	=> 0
			],
			'biaya_transaksi'=> [
				'type'		=> 'int',
				'default'	=> 0
			]
		]);

		$this->dbforge->add_key('id_transaksi_poin', TRUE);
		$this->dbforge->create_table('transaksi_poin', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('transaksi_poin');
	}
}