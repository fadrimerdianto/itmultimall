<?php

class Migration_Create_tipe_mobil_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_tipe_mobil'=>[
				'type'=> 'int',
				'auto_increment'=>true
			],
			'id_merk'=>[
				'type'=> 'int'
			],
			'tipe' => [
				'type'=>'varchar',
				'constraint'=>30
			]
		]);

		$this->dbforge->add_key('id_tipe_mobil', TRUE);
		$this->dbforge->create_table('tipe_mobil', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('tipe_mobil');
	}
}