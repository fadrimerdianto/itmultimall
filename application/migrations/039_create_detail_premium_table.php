<?php

class Migration_Create_detail_premium_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_detail_premium'=>[
				'type'=> 'int',
				'auto_increment'=>true
			],
			'id_iklan'=>[
				'type'=>'int'
			],
			'top25'=>[
				'type'=>'smallint',
			],
			'tanggal_aktif_top25'=>[
				'type'=>'date'
			],
			'topwebsite'=>[
				'type'=>'smallint',
			],
			'tanggal_aktif_topwebsite'=>[
				'type'=>'date'
			],
			'terlaris'=>[
				'type'=>'smallint',
			],
			'tanggal_aktif_terlaris'=>[
				'type'=>'date'
			],
			'recommended'=>[
				'type'=>'smallint',
			],
			'tanggal_aktif_recommended'=>[
				'type'=>'date'
			]
		]);

		$this->dbforge->add_key('id_detail_premium', TRUE);
		$this->dbforge->create_table('detail_premium', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('detail_premium');
	}
}