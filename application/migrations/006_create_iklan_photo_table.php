<?php

class Migration_Create_iklan_photo_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_photo'			=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'id_iklan'		=>[
				'type' 		=> 'INT'
			],
			'photo'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'40'
			]
		]);

		$this->dbforge->add_key('id_photo', true);
		$this->dbforge->create_table('iklan_photo', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('iklan_photo');
	}
}