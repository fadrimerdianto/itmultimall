<?php

class Migration_Add_column_kategori_to_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('iklan', [
			'kategori'	=>	[
				'type'		=>	'INT',
			]
		]);

		$this->dbforge->add_column('iklan', [
			'sub1_kategori'	=>	[
				'type'		=>	'INT',
			]
		]);

		$this->dbforge->add_column('iklan', [
			'sub2_kategori'	=>	[
				'type'		=>	'INT',
			]
		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('iklan','kategori');
		$this->dbforge->drop_column('iklan','sub1_kategori');
		$this->dbforge->drop_column('iklan','sub2_kategori');
	}
}