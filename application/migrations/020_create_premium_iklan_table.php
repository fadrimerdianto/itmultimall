<?php

class Migration_Create_premium_iklan_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_premium'			=> [
				'type'			=>	'int',
				'auto_increment'=> true
			],
			'nama_premium'	=> [
				'type'		=> 'varchar',
				'constraint'	=> '20'
			],
			'harga_premium'	=> [
				'type'		=> 'int',
				'default'	=> 0
			],
			'gambar_premium'	=> [
				'type'			=> 'varchar',
				'constraint'	=> '50'
			]
		]);

		$this->dbforge->add_key('id_premium', TRUE);
		$this->dbforge->create_table('premium_iklan', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('premium_iklan');
	}
}