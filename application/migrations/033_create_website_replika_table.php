<?php

class Migration_Create_website_replika_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_website'			=> [
				'type'				=>	'int',
				'auto_increment'	=> true
			],
			'username'				=> [
				'type'				=> 'varchar',
				'constraint'		=>40
			],
			'nama_website'			=> [
				'type'				=>'varchar',
				'constraint'		=>40
			],
			'alamat_website'		=> [
				'type'				=>'varchar',
				'constraint'		=>40
			],
			'template'				=> [
				'type'	=> 'int'
			],
			'langkah_pembuatan'		=> [
				'type' => 'int'
			]
		]);

		$this->dbforge->add_key('id_website', TRUE);
		$this->dbforge->create_table('website_replika', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('website_replika');
	}
}