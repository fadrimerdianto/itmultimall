<?php

class Migration_Create_fasilitas_rumah_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_fasilitas_rumah'=>[
				'type'=> 'int',
				'auto_increment'=>true
			],
			'id_iklan_rumah'=>[
				'type'=>'int'
			],
			'ac'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'swimming_pool'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'carport'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'garden'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'garasi'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'telephone'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'pam'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'water_heater'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'refrigerator'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'stove'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'microwave'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'oven'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'fire_extenguisher'=>[
				'type'=>'smallint',
				'default'=>0
			],
			'gordyn'=>[
				'type'=>'smallint',
				'default'=>0
			]
		]);

		$this->dbforge->add_key('id_fasilitas_rumah', TRUE);
		$this->dbforge->create_table('fasilitas_rumah', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('fasilitas_rumah');
	}
}