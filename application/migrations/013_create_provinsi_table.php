<?php

class Migration_Create_provinsi_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_provinsi'			=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'nama_provinsi'		=> [
				'type'			=>	'varchar',
				'constraint'	=> 50
			]
		]);

		$this->dbforge->add_key('id_provinsi', TRUE);
		$this->dbforge->create_table('provinsi', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('provinsi');
	}
}