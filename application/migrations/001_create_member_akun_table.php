<?php

class Migration_Create_member_akun_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'username'	=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'20'
			],
			'email'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'40'
			],
			'password'	=>	[
				'type'			=>	'VARCHAR',
				'constraint'	=>	'100'
			]
		]);

		$this->dbforge->add_key('username', true);
		$this->dbforge->create_table('member_akun', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('member_akun');
	}
}