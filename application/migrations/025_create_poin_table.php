<?php

class Migration_Create_poin_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_poin'			=> [
				'type'			=>	'int',
				'auto_increment'=> true
			],
			'nama_poin'		=> [
				'type'		=> 'varchar',
				'constraint'=> 20
			],
			'jumlah_poin'	=> [
				'type'		=> 'int',
				'default'	=> 0
			],
			'harga_poin'	=> [
				'type'		=> 'int',
				'default'	=> 0
			],
			'gambar_poin'	=> [
				'type'			=> 'varchar',
				'constraint'	=> '50'
			]
		]);

		$this->dbforge->add_key('id_poin', TRUE);
		$this->dbforge->create_table('poin', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('poin');
	}
}