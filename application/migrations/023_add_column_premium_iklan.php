<?php

class Migration_Add_column_premium_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('iklan', [
			'id_premium' => [
				'type'		=> 'int',
				'default'	=> 0
			],
			'tanggal_aktif_premium'	=> [
				'type' => 'date'
			]
		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('iklan','id_premium');
		$this->dbforge->drop_column('iklan','tanggal_aktif_premium');
	}
}