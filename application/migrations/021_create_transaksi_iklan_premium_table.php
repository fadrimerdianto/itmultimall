<?php

class Migration_Create_transaksi_iklan_premium_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_transaksi'			=> [
				'type'			=>	'int',
				'auto_increment'=> true
			],
			'id_premium_iklan'	=> [
				'type'		=> 'int',
			],
			'id_iklan'	=> [
				'type'		=> 'int',
			],
			'tanggal_transaksi'=>[
				'type'		=> 'datetime'
			]
		]);

		$this->dbforge->add_key('id_transaksi', TRUE);
		$this->dbforge->create_table('transaksi_iklan_premium', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('transaksi_iklan_premium');
	}
}