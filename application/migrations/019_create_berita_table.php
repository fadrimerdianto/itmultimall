<?php

class Migration_Create_berita_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_berita'			=> [
				'type'			=>	'int',
				'auto_increment'=> true
			],
			'judul_berita'	=> [
				'type'		=> 'varchar',
				'constraint'	=> '200'
			],
			'isi_berita'	=> [
				'type'		=> 'text'
			],
			'seo_berita'	=> [
				'type'		=> 'varchar',
				'constraint'=>	'200'
			],
			'sumber_berita'	=> [
				'type'		=> 'text'
			]
		]);

		$this->dbforge->add_key('id_berita', TRUE);
		$this->dbforge->create_table('berita', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('berita');
	}
}