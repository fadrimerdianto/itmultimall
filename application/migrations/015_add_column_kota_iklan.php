<?php

class Migration_Add_column_kota_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('iklan', [
			'kota'	=>	[
				'type'		=>	'INT',
			]
		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('iklan','kota');
	}
}