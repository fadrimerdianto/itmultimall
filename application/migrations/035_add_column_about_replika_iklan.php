<?php

class Migration_Add_column_about_replika_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('website_replika', [
			'about'=>[
				'type'	=> 'text',
			],
			'about_foto'=>[
				'type'=> 'varchar',
				'constraint'=>100
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('website_replika','about');
		$this->dbforge->drop_column('website_replika','about_foto');
	}
}