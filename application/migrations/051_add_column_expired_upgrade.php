<?php

class Migration_Add_column_expired_upgrade extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('upgrade', [
			'expired_upload10'=>[
				'type'=> 'date',
			],
			'expired_upload25'=>[
				'type'=> 'date',
			],'expired_pembaruan'=>[
				'type'=> 'date',
			],
			'expired_upload50'=>[
				'type'=> 'date',
			],
			'expired_uploadunl'=>[
				'type'=> 'date',
			],
			'expired_domainsendiri'=>[
				'type'=> 'date',
			],
			'expired_multikategori'=>[
				'type'=> 'date',
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('upgrade','expired_pembaruan');
		$this->dbforge->drop_column('upgrade','expired_domainsendiri');
		$this->dbforge->drop_column('upgrade','expired_multikategori');
		$this->dbforge->drop_column('upgrade','expired_upload10');
		$this->dbforge->drop_column('upgrade','expired_upload25');
		$this->dbforge->drop_column('upgrade','expired_upload50');
		$this->dbforge->drop_column('upgrade','expired_uploadunl');
	}
}