<?php

class Migration_Create_kategori_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_kategori'			=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'kategori'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'60'
			]
		]);

		$this->dbforge->add_key('id_kategori', true);
		$this->dbforge->create_table('kategori', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('kategori');
	}
}