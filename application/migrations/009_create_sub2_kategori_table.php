<?php

class Migration_Create_sub2_kategori_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_sub2_kategori'			=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'id_sub1_kategori'		=> [
				'type'			=> 'INT',
			],
			'sub2_kategori'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'60'
			]
		]);

		$this->dbforge->add_key('id_sub2_kategori', true);
		$this->dbforge->create_table('sub2_kategori', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('sub2_kategori');
	}
}