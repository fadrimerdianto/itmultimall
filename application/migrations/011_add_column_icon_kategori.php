<?php

class Migration_Add_column_icon_kategori extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('kategori', [
			'icon'	=>	[
				'type'		=>	'varchar',
				'constraint'	=> '40'
			]
		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('kategori','icon');
	}
}