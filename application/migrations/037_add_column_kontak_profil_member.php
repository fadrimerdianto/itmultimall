<?php

class Migration_Add_column_kontak_profil_member extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('profil_akun', [
			'telepon_member2'=>[
				'type'=> 'varchar',
				'constraint'=>15
			],
			'id_line'=>[
				'type'=>'varchar',
				'constraint'=>25
			],
			'alamat'=>[
				'type'=>'varchar',
				'constraint'=>150
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('profil_akun','telepon_member2');
		$this->dbforge->drop_column('profil_akun','id_line');
		$this->dbforge->drop_column('profil_akun','alamat');
	}
}