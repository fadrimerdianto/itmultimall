<?php

class Migration_Create_komentar_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_komentar'			=> [
				'type'			=>	'int',
				'auto_increment'=> true
			],
			'id_iklan'	=> [
				'type'		=> 'int',
			],
			'username'	=> [
				'type'		=> 'varchar',
				'constraint'=> 40
			],
			
			'komentar' => [
				'type' => 'text'
			],
			'waktu'	=> [
				'type'		=> 'datetime',
			]
		]);

		$this->dbforge->add_key('id_komentar', TRUE);
		$this->dbforge->create_table('komentar', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('komentar');
	}
}