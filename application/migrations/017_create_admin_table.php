<?php

class Migration_Create_admin_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'username'			=> [
				'type'			=>	'varchar',
				'constraint'	=> 20
			],
			'password'			=> [
				'type'			=> 'varchar',
				'constraint'	=> 100
			]
		]);

		$this->dbforge->add_key('username', TRUE);
		$this->dbforge->create_table('admin', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('admin');
	}
}