<?php

class Migration_Add_column_logo_replika_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('website_replika', [
			'logo'=>[
				'type'=> 'varchar',
				'constraint'=>100
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('website_replika','logo');
	}
}