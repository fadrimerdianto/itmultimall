<?php

class Migration_Create_iklan_apartement_properti_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_iklan_apartement_properti'=>[
				'type'=> 'int',
				'auto_increment'=>true
			],
			'id_iklan'=>[
				'type'=> 'int'
			],
			'luas_bangunan'=>[
				'type'=>'int'
			],
			'lantai'=>[
				'type'=>'int'
			],
			'kamar_tidur'=>[
				'type'=>'int'
			],
			'kamar_mandi'=>[
				'type'=>'int'
			],
			'sertifikasi'=>[
				'type'=>'int'
			],
			'alamat_lokasi'=>[
				'type'=>'text'
			]
		]);

		$this->dbforge->add_key('id_iklan_apartement_properti', TRUE);
		$this->dbforge->create_table('iklan_apartement_properti', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('iklan_apertement_properti');
	}
}