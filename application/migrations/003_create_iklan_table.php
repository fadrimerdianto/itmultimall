<?php

class Migration_Create_iklan_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_iklan'			=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'judul_iklan'	=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	100
			],
			'seo_iklan' => [
				'type'			=> 'VARCHAR',
				'constraint'	=> 120
			],
			'deskripsi_iklan'	=> [
				'type'			=>	'TEXT'
			],
			'harga_iklan'		=> [
				'type'			=>	'BIGINT'
			],
			'web_iklan'			=> [
				'type'			=>	'VARCHAR',
				'constraint'	=> 100,
			]
			
		]);

		$this->dbforge->add_key('id_iklan', true);
		$this->dbforge->create_table('iklan', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('iklan');
	}
}