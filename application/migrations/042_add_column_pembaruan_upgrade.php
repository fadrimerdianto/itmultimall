<?php

class Migration_Add_column_pembaruan_upgrade extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('upgrade', [
			'pembaruan'=>[
				'type'=> 'int',
				'default'=>0
			],
			'domainsendiri'=>[
				'type'=> 'int',
				'default'=>0
			],
			'multikategori'=>[
				'type'=> 'int',
				'default'=>0
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('upgrade','pembaruan');
		$this->dbforge->drop_column('upgrade','domainsendiri');
		$this->dbforge->drop_column('upgrade','multikategori');
	}
}