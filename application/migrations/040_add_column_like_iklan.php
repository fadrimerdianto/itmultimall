<?php

class Migration_Add_column_like_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('iklan', [
			'like'=>[
				'type'=> 'int',
				'default'=>0
			],
			'unlike'=>[
				'type'=>'int',
				'default'=>0
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('iklan','like');
		$this->dbforge->drop_column('iklan','unlike');
	}
}