<?php

class Migration_Create_upgrade_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_upgrade'=>[
				'type'=> 'int',
				'auto_increment'=>true
			],
			'username'=>[
				'type'=> 'varchar',
				'constraint'=>20
			],
			'upload10'=>[
				'type'=>'smallint',
			],
			'upload25'=>[
				'type'=>'smallint',
			],
			'upload50'=>[
				'type'=>'smallint',
			],
			'uploadunl'=>[
				'type'=>'smallint',
			]
		]);

		$this->dbforge->add_key('id_upgrade', TRUE);
		$this->dbforge->create_table('upgrade', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('upgrade');
	}
}