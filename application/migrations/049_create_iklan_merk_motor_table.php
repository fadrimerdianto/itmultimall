<?php

class Migration_Create_iklan_merk_motor_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_iklan_merk_motor'=>[
				'type'=> 'int',
				'auto_increment'=>true
			],
			'id_iklan'=>[
				'type'=> 'int'
			],
			'id_sub2_kategori' => [
				'type'=>'int'
			],
			'tipe' => [
				'type'=>'int'
			],
			'tahun'=>[
				'type'=>'year'
			]
		]);

		$this->dbforge->add_key('id_iklan_merk_motor', TRUE);
		$this->dbforge->create_table('iklan_merk_motor', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('iklan_merk_motor');
	}
}