<?php

class Migration_Add_column_seo_kategori_to_kategori extends CI_Migration {

	public function up() {
		$this->dbforge->add_column('kategori', [
			'seo_kategori'	=>	[
				'type'		=>	'VARCHAR',
				'default'   => 	'',
				'constraint'=> 255
			]
		]);
		$this->dbforge->add_column('sub1_kategori', [
			'seo_sub1_kategori'	=>	[
				'type'		=>	'VARCHAR',
				'default'   => 	'',
				'constraint'=> 255
			]
		]);
		$this->dbforge->add_column('sub2_kategori', [
			'seo_sub2_kategori'	=>	[
				'type'		=>	'VARCHAR',
				'default'   => 	'',
				'constraint'=> 255
			]
		]);
	}

	public function down() {
		$this->dbforge->drop_column('kategori', 'seo_kategori');
		$this->dbforge->drop_column('sub1_kategori', 'seo_sub1_kategori');
		$this->dbforge->drop_column('sub2_kategori', 'seo_sub2_kategori');
	}
}