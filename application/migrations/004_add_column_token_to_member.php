<?php

class Migration_Add_column_token_to_member extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('member_akun', [
			'status'	=>	[
				'type'		=>	'INT',
				'constraint'=>	'1',
				'default' 	=> 0
			]
		]);

		$this->dbforge->add_column('member_akun', [
			'token'	=>	[
				'type'		=>	'VARCHAR',
				'default'   => 	'',
				'constraint'=> 100
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('member_akun', 'status');
		$this->dbforge->drop_column('member_akun', 'token');
	}
}