<?php

class Migration_Create_laporan_penjual_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_laporan'			=> [
				'type'			=>	'int',
				'auto_increment'=> true
			],
			'nama_pelapor'	=> [
				'type'		=> 'varchar',
				'constraint'=>40
			],
			'email_pelapor'	=> [
				'type'		=> 'varchar',
				'constraint'=> 40
			],
			'pesan' => [
				'type' => 'text'
			],
			'waktu'	=> [
				'type'		=> 'timestamp',
			]
		]);

		$this->dbforge->add_key('id_laporan', TRUE);
		$this->dbforge->create_table('laporan_penjual', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('laporan_penjual');
	}
}