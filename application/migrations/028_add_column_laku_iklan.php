<?php

class Migration_Add_column_laku_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('iklan', [
			'hide_email'=>[
				'type'	=> 'int',
				'default'=>0,
			],
			'hide_bbm'=>[
				'type'=> 'int',
				'default'=>0
			],
			'wa_available'=>[
				'type'=>'int',
				'default'=>0
			]

		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('iklan','hide_email');
		$this->dbforge->drop_column('iklan','hide_bbm');
		$this->dbforge->drop_column('iklan','wa_available');
	}
}