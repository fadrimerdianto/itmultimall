<?php

class Migration_Add_column_aktif_premium extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('premium_iklan', [
			'masa_aktif' => [
				'type'		=> 'int',
				'default'	=> 0
			]
		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('premium_iklan','masa_aktif');
	}
}