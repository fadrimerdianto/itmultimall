<?php

class Migration_Create_kota_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_kota'			=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'nama_area'		=> [
				'type'			=>	'varchar',
				'constraint'	=> '40'
			],
			'id_provinsi'		=> [
				'type'			=>	'INT',
			]
		]);

		$this->dbforge->add_key('id_kota', TRUE);
		$this->dbforge->create_table('kota', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('kota');
	}
}