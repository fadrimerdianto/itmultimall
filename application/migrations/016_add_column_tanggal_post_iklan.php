<?php

class Migration_Add_column_tanggal_post_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('iklan', [
			'tanggal_post'	=>	[
				'type'		=>	'DATETIME',
			]
		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('iklan','tanggal_post');
	}
}