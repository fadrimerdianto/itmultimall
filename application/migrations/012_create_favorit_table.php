<?php

class Migration_Create_favorit_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_favorit'			=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'id_iklan'		=> [
				'type'			=>	'INT'
			],
			'username'		=> [
				'type'			=>	'varchar',
				'constraint'		=>	'40'
			]
		]);

		$this->dbforge->add_key('id_favorit', TRUE);
		$this->dbforge->create_table('favorit', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('favorit');
	}
}