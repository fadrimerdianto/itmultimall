<?php

class Migration_Create_tipe_motor_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_tipe_motor'=>[
				'type'=> 'int',
				'auto_increment'=>true
			],
			'id_merk'=>[
				'type'=> 'int'
			],
			'tipe' => [
				'type'=>'varchar',
				'constraint'=>30
			]
		]);

		$this->dbforge->add_key('id_tipe_motor', TRUE);
		$this->dbforge->create_table('tipe_motor', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('tipe_motor');
	}
}