<?php

class Migration_Add_column_poin_member extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('profil_akun', [
			'poin_member'	=>	[
				'type'		=>	'int',
				'default'	=> 0
			]
		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('profil_akun','poin_member');
	}
}