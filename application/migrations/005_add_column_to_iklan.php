<?php

class Migration_Add_column_to_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('iklan', [
			'owner'	=>	[
				'type'		=>	'varchar',
				'constraint'=>	'40'
			]
		]);

		$this->dbforge->add_column('iklan', [
			'status'	=>	[
				'type'		=>	'INT',
				'constraint'=>	'1'
			]
		]);

		
	}

	public function down()
	{
		$this->dbforge->drop_column('owner');
		$this->dbforge->drop_column('status');
	}
}