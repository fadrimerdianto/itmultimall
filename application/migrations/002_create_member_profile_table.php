<?php

class Migration_Create_member_profile_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_profil'	=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'username'	=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'20'
			],
			'provinsi_member'	=> [
				'type'			=>	'INT'
			],
			'kota_member'		=> [
				'type'			=>	'INT'
			],
			'nama_member'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=> 80,
				'default'		=> ''
			],
			'telepon_member'	=> [
				'type'			=>	'VARCHAR',
				'constraint'	=> 20,
				'default'		=> ''
			],
			'bbm_member'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=> 10,
				'default'		=> ''
			],
			'wa_member'			=> [
				'type'			=>	'VARCHAR',
				'constraint'	=> 20,
				'default'		=> ''
			]
			
		]);

		$this->dbforge->add_key('id_profil', true);
		$this->dbforge->create_table('profil_akun', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('profil_akun');
	}
}