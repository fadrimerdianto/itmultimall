<?php

class Migration_Create_slider_web_replika_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_slider'=>[
				'type'=> 'int',
				'auto_increment'=>true
			],
			'id_website'=>[
				'type'=>'int'
			],
			'slider1'=>[
				'type'=>'varchar',
				'constraint'=>40
			],
			'slider2'=>[
				'type'=>'varchar',
				'constraint'=>40
			],
			'slider3'=>[
				'type'=>'varchar',
				'constraint'=>40
			],
			'slider4'=>[
				'type'=>'varchar',
				'constraint'=>40
			]
		]);

		$this->dbforge->add_key('id_slider', TRUE);
		$this->dbforge->create_table('slider_replika', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('slider_replika');
	}
}