<?php

class Migration_Create_sub1_kategori_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_sub1_kategori'			=> [
				'type'			=>	'INT',
				'auto_increment'=>	true
			],
			'id_kategori'		=> [
				'type'			=> 'INT',
			],
			'sub1_kategori'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'60'
			]
		]);

		$this->dbforge->add_key('id_sub1_kategori', true);
		$this->dbforge->create_table('sub1_kategori', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('sub1_kategori');
	}
}