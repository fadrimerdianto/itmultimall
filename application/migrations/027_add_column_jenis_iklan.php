<?php

class Migration_Add_column_jenis_iklan extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('iklan', [
			'jenis_iklan' => [
				'type'		=> 'int',
				'default'	=> 0
			]
		]);
		
	}

	public function down()
	{
		$this->dbforge->drop_column('iklan','jenis_iklan');
	}
}