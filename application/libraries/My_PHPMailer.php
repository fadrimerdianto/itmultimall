<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_PHPMailer {
    public function My_PHPMailer() {
        require_once('PHPMailer/PHPMailerAutoload.php');		
    }

	function send_email($toEmail,$toName,$subject,$msg){
		require_once('PHPMailer/PHPMailerAutoload.php');

		$fromEmail = 'support.wikiloka@aksamedia.co.id';
		$fromName = 'Wikiloka.com';

		$mail   = new PHPMailer();
		$mail->IsSMTP(); // telling the class to use SMTP
		
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
		                                           // 1 = errors and messages,  2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mail->Host       = "smtp.emailarray.com";      // sets GMAIL as the SMTP server
		$mail->Port       = "587";                   // set the SMTP port for the GMAIL server
		$mail->Username   = "noreply@aksamedia.co.id";  // GMAIL username
		$mail->Password   = "Beon123#"; 
		
		$mail->SetFrom($fromEmail, $fromName);
		$mail->AddReplyTo($fromEmail, $fromName);
		$mail->Subject    = $subject;	
		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test	
		$mail->MsgHTML($msg);
		$mail->AddAddress($toEmail, $toName);

		if (!$mail->Send()){
			return false;
		} else{
			return true;
		}
	}

	function email_view(){
		echo '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
				<title>Wikiloka - Konfirmasi Pendaftaran</title>
				<style type="text/css">
					/* RESET STYLES */
					html { background-color:#E1E1E1; margin:0; padding:0; }
					body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
					table{border-collapse:collapse;}
					table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
					img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
					a {text-decoration:none !important;border-bottom: 1px solid;}
					h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}

					/* CLIENT-SPECIFIC STYLES */
					.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
					.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
					table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
					#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
					img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
					body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
					.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */

					/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

					/* ========== Page Styles ========== */
					h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
					h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
					h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
					h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
					.flexibleImage{height:auto;}
					.linkRemoveBorder{border-bottom:0 !important;}
					table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}

					body, #bodyTable{background-color:#E1E1E1;}
					#emailHeader{background-color:#E1E1E1;}
					#emailBody{background-color:#FFFFFF;}
					#emailFooter{background-color:#E1E1E1;}
					.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
					.emailButton{background-color:#205478; border-collapse:separate;}
					.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
					.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
					.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
					.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
					.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
					.imageContentText {margin-top: 10px;line-height:0;}
					.imageContentText a {line-height:0;}
					#invisibleIntroduction {display:none !important;} 
					span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} 
					span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
					span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}

					.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
					.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
					@media only screen and (max-width: 480px){
						body{width:100% !important; min-width:100% !important;} 
						table[id="emailHeader"],
						table[id="emailBody"],
						table[id="emailFooter"],
						table[class="flexibleContainer"],
						td[class="flexibleContainerCell"] {width:100% !important;}
						td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
						td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
						img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
						img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
						table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}
						table[class="emailButton"]{width:100% !important;}
						td[class="buttonContent"]{padding:0 !important;}
						td[class="buttonContent"] a{padding:15px !important;}

					}

					@media only screen and (-webkit-device-pixel-ratio:.75){
					}

					@media only screen and (-webkit-device-pixel-ratio:1){
						
					}

					@media only screen and (-webkit-device-pixel-ratio:1.5){
						/* Put CSS for high density (hdpi) Android layouts in here */
					}
					=====================================================*/
					@media only screen and (min-device-width : 320px) and (max-device-width:568px) {

					}
					/* end IOS targeting */
				</style>
			</head>
			<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

				<center style="background-color:#E1E1E1;">
					<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
						<tr>
							<td align="center" valign="top" id="bodyCell">
								<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

									<!-- HEADER ROW // -->
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent">
																							<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																								Online Reservation 
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // END -->

								</table>
							
								<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<!--
												The centering table keeps the content
												tables centered in the emailBody table,
												in case its width is set to 100%.
											-->
											<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#FFF">
												<tr>
													<td align="center" valign="top">
														<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td align="center" valign="top" width="500" class="flexibleContainerCell">
																	<table border="0" cellpadding="30" cellspacing="0" width="100%">
																		<tr>
																			<td align="center" valign="top" class="textContent">
																				<h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">
																					<img src="http://aksamedia.co.id/wikiloka/files/wiki2.png" style="width:200px;margin-left:auto;margin-right:auto">
																				</h1>
																				<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#777;line-height:135%;">Dapatkan kemudahan dalam mencari barang  - barang yang anda inginkan.</div>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>						
										
									
									
									
									<tr mc:hideable>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%" >
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="top" class="flexibleContainerBox">
																				<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;overflow:hidden;">
																					<tr>
																						<td align="left" class="textContent">
																							<h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:8px;text-align:left;">
																								<span style="color:rgb(95, 95, 95)">Invoice</span> 
																								<span style="color:#FDA051">Pembelian Poin</span>
																							</h3>
																							
																							<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
																								Poin yang dibeli &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: <b>Paket Poin 2</b>
																								<br>Harga Poin(+Kode Pembelian) &nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp: <b>Rp. '.number_format(20212).'</b>

																								<p>
																								Mohon melakukan pembayaran melalui 
																								<br>
																								No rekening &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : 675757571253
																								<br>
																								a.n. Wirat Moko Hadi S
																							</div>
																						</td>
																					</tr>
																					
																				</table>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // MODULE ROW -->


									<!-- MODULE ROW // -->
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="top" class="flexibleContainerBox" style="background-color:#5F5F5F;">
																				<table border="0" cellpadding="30" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent" style="vertical-align:middle">
																							<img src="email.png" style="width:80px;margin-top:8px;">
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td align="right" valign="top" class="flexibleContainerBox" style="background-color:#ff8b24;">
																				<table class="flexibleContainerBoxNext" border="0" cellpadding="30" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent">
																							<h3 style="color:#FFFFFF;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Wikiloka</h3>
																							<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#FFFFFF;line-height:135%;">
																								Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // MODULE ROW -->



								</table>
							
								<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														&nbsp;
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>

								</table>
								<!-- // END -->

							</td>
						</tr>
					</table>
				</center>
			</body>
			</html>

		';
	}

	function template_konfirmasi($nama_website, $toName, $alamatweb, $link){
		$msg = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
				<title>Wikiloka - Konfirmasi Pendaftaran</title>
				<style type="text/css">
					/* RESET STYLES */
					html { background-color:#E1E1E1; margin:0; padding:0; }
					body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
					table{border-collapse:collapse;}
					table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
					img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
					a {text-decoration:none !important;border-bottom: 1px solid;}
					h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}

					/* CLIENT-SPECIFIC STYLES */
					.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
					.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
					table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
					#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
					img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
					body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
					.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */

					/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

					/* ========== Page Styles ========== */
					h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
					h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
					h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
					h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
					.flexibleImage{height:auto;}
					.linkRemoveBorder{border-bottom:0 !important;}
					table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}

					body, #bodyTable{background-color:#E1E1E1;}
					#emailHeader{background-color:#E1E1E1;}
					#emailBody{background-color:#FFFFFF;}
					#emailFooter{background-color:#E1E1E1;}
					.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
					.emailButton{background-color:#205478; border-collapse:separate;}
					.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
					.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
					.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
					.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
					.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
					.imageContentText {margin-top: 10px;line-height:0;}
					.imageContentText a {line-height:0;}
					#invisibleIntroduction {display:none !important;} 
					span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} 
					span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
					span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}

					.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
					.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
					@media only screen and (max-width: 480px){
						body{width:100% !important; min-width:100% !important;} 
						table[id="emailHeader"],
						table[id="emailBody"],
						table[id="emailFooter"],
						table[class="flexibleContainer"],
						td[class="flexibleContainerCell"] {width:100% !important;}
						td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
						td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
						img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
						img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
						table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}
						table[class="emailButton"]{width:100% !important;}
						td[class="buttonContent"]{padding:0 !important;}
						td[class="buttonContent"] a{padding:15px !important;}

					}

					@media only screen and (-webkit-device-pixel-ratio:.75){
					}

					@media only screen and (-webkit-device-pixel-ratio:1){
						
					}

					@media only screen and (-webkit-device-pixel-ratio:1.5){
						/* Put CSS for high density (hdpi) Android layouts in here */
					}
					=====================================================*/
					@media only screen and (min-device-width : 320px) and (max-device-width:568px) {

					}
					/* end IOS targeting */
				</style>
			</head>
			<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

				<center style="background-color:#E1E1E1;">
					<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
						<tr>
							<td align="center" valign="top" id="bodyCell">
								<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

									<!-- HEADER ROW // -->
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent">
																							<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																								Online Reservation 
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // END -->

								</table>
							
								<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<!--
												The centering table keeps the content
												tables centered in the emailBody table,
												in case its width is set to 100%.
											-->
											<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#FFF">
												<tr>
													<td align="center" valign="top">
														<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td align="center" valign="top" width="500" class="flexibleContainerCell">
																	<table border="0" cellpadding="30" cellspacing="0" width="100%">
																		<tr>
																			<td align="center" valign="top" class="textContent">
																				<h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">
																					<img src="http://aksamedia.co.id/wikiloka/files/wiki2.png" style="width:200px;margin-left:auto;margin-right:auto">
																				</h1>
																				<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#777;line-height:135%;">Dapatkan kemudahan dalam mencari barang  - barang yang anda inginkan.</div>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>						
										
									
									
									
									<tr mc:hideable>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%" >
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="top" class="flexibleContainerBox">
																				<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;overflow:hidden;">
																					<tr>
																						<td align="left" class="textContent">
																							<h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:8px;text-align:left;">
																								<span style="color:rgb(95, 95, 95)">Konfirmasi</span> 
																								<span style="color:#FDA051">Pendaftaran</span>
																							</h3>
																							
																							<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">Akun anda telah di konfirmasi oleh pihak Wikiloka. Silahkan klik link dibawah untuk melanjutkan.</div>
																						</td>
																					</tr>
																					
																				</table>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // MODULE ROW -->


									<!-- MODULE ROW // -->
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr style="padding-top:0;">
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td style="padding-top:0;" align="center" valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table border="0" cellpadding="0" cellspacing="0" width="50%" class="emailButton" style="background-color: #FDA051;">
																		<tr>
																			<td align="center" valign="middle" class="buttonContent" style="padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;">
																				<a href="'.$link.'" style="color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;" href="#" target="_blank">Konfirmasi</a>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // MODULE ROW -->


									<!-- MODULE ROW // -->
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="top" class="flexibleContainerBox" style="background-color:#5F5F5F;">
																				<table border="0" cellpadding="30" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent" style="vertical-align:middle">
																							<img src="email.png" style="width:80px;margin-top:8px;">
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td align="right" valign="top" class="flexibleContainerBox" style="background-color:#ff8b24;">
																				<table class="flexibleContainerBoxNext" border="0" cellpadding="30" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent">
																							<h3 style="color:#FFFFFF;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Wikiloka</h3>
																							<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#FFFFFF;line-height:135%;">
																								Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // MODULE ROW -->



								</table>
							
								<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														&nbsp;
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>

								</table>
								<!-- // END -->

							</td>
						</tr>
					</table>
				</center>
			</body>
			</html>
		';		

		// $mail   = new My_PHPMailer();
		// $kirim = $mail->send_email($toEmail,$toName,$subject,$msg);
		return $msg;
	}

	function template_invoice($nama_website, $toName, $alamatweb, $data){
		$msg = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
				<title>Wikiloka - Konfirmasi Pendaftaran</title>
				<style type="text/css">
					/* RESET STYLES */
					html { background-color:#E1E1E1; margin:0; padding:0; }
					body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
					table{border-collapse:collapse;}
					table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
					img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
					a {text-decoration:none !important;border-bottom: 1px solid;}
					h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}

					/* CLIENT-SPECIFIC STYLES */
					.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
					.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
					table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
					#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
					img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
					body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
					.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */

					/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

					/* ========== Page Styles ========== */
					h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
					h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
					h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
					h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
					.flexibleImage{height:auto;}
					.linkRemoveBorder{border-bottom:0 !important;}
					table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}

					body, #bodyTable{background-color:#E1E1E1;}
					#emailHeader{background-color:#E1E1E1;}
					#emailBody{background-color:#FFFFFF;}
					#emailFooter{background-color:#E1E1E1;}
					.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
					.emailButton{background-color:#205478; border-collapse:separate;}
					.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
					.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
					.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
					.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
					.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
					.imageContentText {margin-top: 10px;line-height:0;}
					.imageContentText a {line-height:0;}
					#invisibleIntroduction {display:none !important;} 
					span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} 
					span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
					span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}

					.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
					.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
					@media only screen and (max-width: 480px){
						body{width:100% !important; min-width:100% !important;} 
						table[id="emailHeader"],
						table[id="emailBody"],
						table[id="emailFooter"],
						table[class="flexibleContainer"],
						td[class="flexibleContainerCell"] {width:100% !important;}
						td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
						td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
						img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
						img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
						table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}
						table[class="emailButton"]{width:100% !important;}
						td[class="buttonContent"]{padding:0 !important;}
						td[class="buttonContent"] a{padding:15px !important;}

					}

					@media only screen and (-webkit-device-pixel-ratio:.75){
					}

					@media only screen and (-webkit-device-pixel-ratio:1){
						
					}

					@media only screen and (-webkit-device-pixel-ratio:1.5){
						/* Put CSS for high density (hdpi) Android layouts in here */
					}
					=====================================================*/
					@media only screen and (min-device-width : 320px) and (max-device-width:568px) {

					}
					/* end IOS targeting */
				</style>
			</head>
			<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

				<center style="background-color:#E1E1E1;">
					<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
						<tr>
							<td align="center" valign="top" id="bodyCell">
								<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

									<!-- HEADER ROW // -->
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent">
																							<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																								Online Reservation 
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // END -->

								</table>
							
								<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<!--
												The centering table keeps the content
												tables centered in the emailBody table,
												in case its width is set to 100%.
											-->
											<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#FFF">
												<tr>
													<td align="center" valign="top">
														<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td align="center" valign="top" width="500" class="flexibleContainerCell">
																	<table border="0" cellpadding="30" cellspacing="0" width="100%">
																		<tr>
																			<td align="center" valign="top" class="textContent">
																				<h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">
																					<img src="http://aksamedia.co.id/wikiloka/files/wiki2.png" style="width:200px;margin-left:auto;margin-right:auto">
																				</h1>
																				<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#777;line-height:135%;">Dapatkan kemudahan dalam mencari barang  - barang yang anda inginkan.</div>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>						
										
									
									
									
									<tr mc:hideable>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%" >
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="top" class="flexibleContainerBox">
																				<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;overflow:hidden;">
																					<tr>
																						<td align="left" class="textContent">
																							<h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:8px;text-align:left;">
																								<span style="color:rgb(95, 95, 95)">Invoice</span> 
																								<span style="color:#FDA051">Pembelian Poin</span>
																							</h3>
																							
																							<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
																								Poin yang dibeli : <b>'.$data['paket_poin'].'</b>
																								<br>Harga Poin(+Kode Pembelian) : <b>Rp. '.number_format($data['biaya_transaksi']).'</b>

																								<p>
																								Mohon melakukan pembayaran melalui 
																								<br>
																								No rekening : 675757571253
																								<br>
																								a.n. Wirat Moko Hadi S
																							</div>
																						</td>
																					</tr>
																					
																				</table>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // MODULE ROW -->


									<!-- MODULE ROW // -->
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														<!-- FLEXIBLE CONTAINER // -->
														<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
															<tr>
																<td valign="top" width="500" class="flexibleContainerCell">

																	<!-- CONTENT TABLE // -->
																	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td align="left" valign="top" class="flexibleContainerBox" style="background-color:#5F5F5F;">
																				<table border="0" cellpadding="30" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent" style="vertical-align:middle">
																							<img src="email.png" style="width:80px;margin-top:8px;">
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td align="right" valign="top" class="flexibleContainerBox" style="background-color:#ff8b24;">
																				<table class="flexibleContainerBoxNext" border="0" cellpadding="30" cellspacing="0" width="100%" style="max-width:100%;">
																					<tr>
																						<td align="left" class="textContent">
																							<h3 style="color:#FFFFFF;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Wikiloka</h3>
																							<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#FFFFFF;line-height:135%;">
																								Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi
																							</div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																	<!-- // CONTENT TABLE -->

																</td>
															</tr>
														</table>
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>
									<!-- // MODULE ROW -->



								</table>
							
								<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
									<tr>
										<td align="center" valign="top">
											<!-- CENTERING TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="center" valign="top">
														&nbsp;
														<!-- // FLEXIBLE CONTAINER -->
													</td>
												</tr>
											</table>
											<!-- // CENTERING TABLE -->
										</td>
									</tr>

								</table>
								<!-- // END -->

							</td>
						</tr>
					</table>
				</center>
			</body>
			</html>
		';		

		// $mail   = new My_PHPMailer();
		// $kirim = $mail->send_email($toEmail,$toName,$subject,$msg);
		return $msg;
	}
}