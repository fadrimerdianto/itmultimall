<?php
	class Template_web{
		protected $_ci;
		function __construct(){
			$this->_ci =&get_instance();
		}

		function display($template, $data=null){
			$data['_header'] = $this->_ci->load->view('/web/header', $data, true);
			$data['_content'] = $this->_ci->load->view($template, $data, true);
			$data['_footer'] = $this->_ci->load->view('/web/footer', $data, true);
			if($this->_ci->session->userdata('tipe_user') == 'member'){
				$data['_sidebar'] = $this->_ci->load->view('/web/sidebar', $data, true);
			}
			$data['_javascript'] = $this->_ci->load->view('/web/javascript', $data, true);
			$this->_ci->load->view('/web/template.php', $data);

		}
	}
?>