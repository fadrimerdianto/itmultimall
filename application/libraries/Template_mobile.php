<?php
	class Template_mobile{
		protected $_ci;
		function __construct(){
			$this->_ci =&get_instance();
		}

		function display($template, $data=null){
			$data['_header'] = $this->_ci->load->view('/mobile/header', $data, true);
			$data['_content'] = $this->_ci->load->view($template, $data, true);
			$data['_footer'] = $this->_ci->load->view('/mobile/footer', $data, true);
			$data['_javascript'] = $this->_ci->load->view('/mobile/javascript', $data, true);
			$this->_ci->load->view('/mobile/template.php', $data);

		}
	}