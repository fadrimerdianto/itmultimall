<?php 
 	class Sidebar{
 		protected $_ci;
 		function __construct(){
 			$this->_ci =&get_instance();

 		}
 		public function sidebar(){
 			$this->_ci->load->model('WebsiteReplikaModel');
 			$url = $this->_ci->uri->segment(2);
 			$website = $this->_ci->WebsiteReplikaModel->getWebsiteByUsername($this->_ci->session->userdata('username'));
 			$list['pengaturan'] = '';
 			$list['home'] = '';
 			$list['favorit'] ='';
 			$list['berbayar']='';
 			$list['belipoin']='';
 			$list['toko']='';
 			$list['buatwebsite']='';
 			$list['kelola']='';
 			if($url == '' || $url == 'tidakaktif'){
 				$list['home'] = 'active';
 			}elseif($url == 'pengaturan'){
 				$list['pengaturan'] = 'active';
 			}elseif($url == 'toko'){
 				$list['toko'] = 'active';
 			}elseif($url == 'favorit'){
 				$list['favorit'] = 'active';
 			}elseif($url == 'berbayar'){
 				$list['berbayar'] = 'active';
 			}elseif($url == 'belipoin'){
 				$list['belipoin'] = 'active';
 			}elseif ($url == 'buatwebsite') {
 				$list['buatwebsite'] = 'active';
 			}elseif ($url == 'kelola') {
 				$list['kelola'] = 'active';
 			}
 			if($website == null OR $website->langkah_pembuatan != 4){
			  	$web =  '<li role="presentation" class="'.$list['buatwebsite'].'"><a href="'.base_url().'iklanku/buatwebsite">Buat Website Gratis</a></li>';
			  }else{
			  	$web =  '<li role="presentation" class="'.$list['kelola'].'"><a href="'.base_url().'iklanku/kelola">Kelola Website</a></li>';
			  }
 			$sidebar =  '
					<ul class="nav nav-pills nav-stacked">
					  <li role="presentation" class="'.$list['pengaturan'].'"><a href="'.base_url().'iklanku/pengaturan">Profil</a></li>
					  <li role="presentation" class="'.$list['home'].'"><a href="'.base_url().'iklanku/">Iklan</a></li>
					  <li role="presentation" class="'.$list['toko'].'"><a href="'.base_url().'iklanku/toko">Toko</a></li>
					  <li role="presentation" class="'.$list['favorit'].'"><a href="'.base_url().'iklanku/favorit">Favorite</a></li>
					  '.$web.'
					  <li role="presentation" class="'.$list['berbayar'].'"><a href="'.base_url().'iklanku/berbayar">Iklan Premium</a></li>
					  <li role="presentation" class="'.$list['belipoin'].'"><a href="'.base_url().'iklanku/belipoin">Beli Poin</a></li>
					  <li role="presentation"><a href="'.base_url().'iklanku/upgrade">Upgrade</a></li>
					  <li role="presentation"><a href="'.base_url().'iklanku/logout">Logout</a></li>

					</ul>
 			';
 			return $sidebar;
 		}
 	}